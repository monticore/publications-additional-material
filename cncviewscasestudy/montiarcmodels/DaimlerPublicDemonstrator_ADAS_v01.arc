/* (c) https://github.com/MontiCore/monticore */
package daimlerPublicDemonstrator_ADAS_v01;
import java.lang.*;
import java.util.*;
<<Type="SubSystem">> component DaimlerPublicDemonstrator_ADAS_v01 {
  <<Type="SubSystem">> component DEMO_FAS {
    ports
      in Boolean parkingBrake_b,
      in Double brakeForce_pedal_pc,
      in Double acceleration_pedal_pc,
      in Boolean cruiseControl_b,
      in Boolean limiter_b,
      in Boolean leverUp_b,
      in Boolean leverDown_b,
      in Double v_Vehicle_kmh,
      out Boolean cC_active_b,
      out Double cCSetValue_kmh,
      out Double limiterSetValue_kmh,
      out Boolean limiter_active_b,
      out Double acceleration_pc;
    <<Type="SubSystem">> component DEMO_FAS {
      ports
        in Boolean _ParkingBrake_bIn1,
        in Double _BrakeForce_pedal_pcIn2,
        in Double _Acceleration_pedal_pcIn3,
        in Boolean _CruiseControl_bIn4,
        in Boolean _Limiter_bIn5,
        in Boolean _LeverUp_bIn6,
        in Boolean _LeverDown_bIn7,
        in Double _V_Vehicle_kmhIn8,
        out Boolean _CC_active_bOut1,
        out Double _Acceleration_pcOut2,
        out Double _CCSetValue_kmhOut3,
        out Double _LimiterSetValue_kmhOut4,
        out Boolean _Limiter_active_bOut5;
      <<Type="SubSystem">> component Subsystem {
        ports
          in Boolean _ParkingBrake_bIn1,
          in Double _BrakeForce_pedal_pcIn2,
          in Double _Acceleration_pedal_pcIn3,
          in Boolean _CruiseControl_bIn4,
          in Boolean _Limiter_bIn5,
          in Boolean _LeverUp_bIn6,
          in Boolean _LeverDown_bIn7,
          in Double _V_Vehicle_kmhIn8,
          out Boolean _CC_active_bOut1,
          out Double _Acceleration_pcOut2,
          out Double _CCSetValue_kmhOut3,
          out Double _LimiterSetValue_kmhOut4,
          out Boolean _Limiter_active_bOut5;
        <<Type="SubSystem">> component DEMO_FAS {
          ports
            in Boolean _ParkingBrake_bIn1,
            in Double _BrakeForce_pedal_pcIn2,
            in Double _Acceleration_pedal_pcIn3,
            in Boolean _CruiseControl_bIn4,
            in Boolean _Limiter_bIn5,
            in Boolean _LeverUp_bIn6,
            in Boolean _LeverDown_bIn7,
            in Double _V_Vehicle_kmhIn8,
            out Boolean _CC_active_bOut1,
            out Double _Acceleration_pcOut2,
            out Double _CCSetValue_kmhOut3,
            out Double _LimiterSetValue_kmhOut4,
            out Boolean _Limiter_active_bOut5;
          <<Type="SubSystem">> component DEMO_FAS_Funktion {
            ports
              out Boolean cC_active_b,
              out Double acceleration_pc,
              out Double cCSetValue_kmh,
              out Double limiterSetValue_kmh,
              out Boolean limiter_active_b,
              in Boolean parkingBrake_b,
              in Boolean parkingBrake_b1,
              in Double brakeForce_pedal_pc,
              in Double brakeForce_pedal_pc1,
              in Double acceleration_pedal_pc,
              in Double acceleration_pedal_pc1,
              in Boolean cruiseControl_b,
              in Boolean cruiseControl_b1,
              in Boolean limiter_b,
              in Boolean limiter_b1,
              in Boolean leverUp_b,
              in Boolean leverUp_b1,
              in Boolean leverDown_b,
              in Boolean leverDown_b1,
              in Double v_Vehicle_kmh,
              in Double v_Vehicle_kmh1,
              in Double v_Vehicle_kmh2;
            <<Type="SubSystem">> component FAS_Input {
              ports
                out Boolean parkingBrake_b,
                out Double brakeForce_pedal_pc,
                out Boolean cruiseControl_b,
                out Double v_Vehicle_kmh,
                out Boolean limiter_b,
                out Boolean leverUp_b,
                out Boolean leverDown_b,
                out Double acceleration_pedal_pc,
                out Double v_Vehicle_kmh1,
                in Boolean parkingBrake_b1,
                in Boolean parkingBrake_b2,
                in Double brakeForce_pedal_pc1,
                in Double brakeForce_pedal_pc2,
                in Double acceleration_pedal_pc1,
                in Double acceleration_pedal_pc2,
                in Boolean cruiseControl_b1,
                in Boolean cruiseControl_b2,
                in Boolean limiter_b1,
                in Boolean limiter_b2,
                in Boolean leverUp_b1,
                in Boolean leverUp_b2,
                in Boolean leverDown_b1,
                in Boolean leverDown_b2,
                in Double v_Vehicle_kmh2,
                in Double v_Vehicle_kmh3,
                in Double v_Vehicle_kmh4,
                out Boolean limiter_b3,
                out Double acceleration_pedal_pc3,
                out Double v_Vehicle_kmh5,
                out Boolean leverUp_b3,
                out Boolean leverDown_b3;
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component VERSION_INFO vERSION_INFO;
              connect parkingBrake_b1 -> parkingBrake_b;
              connect brakeForce_pedal_pc1 -> brakeForce_pedal_pc;
              connect cruiseControl_b1 -> cruiseControl_b;
              connect v_Vehicle_kmh4 -> v_Vehicle_kmh;
              connect limiter_b1 -> limiter_b;
              connect leverUp_b2 -> leverUp_b;
              connect leverDown_b2 -> leverDown_b;
              connect acceleration_pedal_pc1 -> acceleration_pedal_pc;
              connect v_Vehicle_kmh2 -> v_Vehicle_kmh1;
              connect limiter_b2 -> limiter_b3;
              connect acceleration_pedal_pc2 -> acceleration_pedal_pc3;
              connect v_Vehicle_kmh3 -> v_Vehicle_kmh5;
              connect leverUp_b1 -> leverUp_b3;
              connect leverDown_b1 -> leverDown_b3;
            }
            <<Type="SubSystem">> component Limiter {
              ports
                in Boolean limiter_bIn1,
                in Double acceleration_pedal_pcIn2,
                in Double v_Vehicle_kmhIn3,
                in Boolean leverUp_bIn4,
                in Boolean leverDown_bIn5,
                out Double vMax_kmhOut1,
                out Boolean limiter_active_bOut2,
                in Double in1,
                in Double in2,
                in Double in3,
                in Double in4,
                in Double in5,
                in Double in6,
                in Double in7,
                out Double out1;
              <<Type="Constant",Value="1">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="SubSystem">> component Limiter_enabled {
                ports
                  in Boolean limiter_bIn1,
                  in Double acceleration_pedal_pcIn2,
                  in Double v_Vehicle_kmhIn3,
                  in Boolean leverUp_bIn4,
                  in Boolean leverDown_bIn5,
                  out Double vMax_kmhOut1,
                  out Boolean limiter_active_bOut2,
                  in Double in1,
                  in Double in2,
                  in Double in3,
                  in Double in4,
                  in Double in5,
                  in Double in6,
                  out Double out1;
                <<Type="SubSystem">> component Limiter_Function {
                  ports
                    in Boolean limiter_bIn1,
                    in Double acceleration_pedal_pcIn2,
                    out Double vMax_kmhOut1,
                    out Boolean limiter_active_bOut2,
                    in Double in1;
                  <<Type="SubSystem">> component Limiter_Active {
                    ports
                      out Double vMax_kmhOut1,
                      out Boolean limiter_active_bOut2,
                      in Double in1;
                    <<Type="Gain",Gain="1">> component Gain {
                      ports
                        in Double vMax_kmhIn1,
                        out Double vMax_kmhOut1;
                      effect vMax_kmhIn1 -> vMax_kmhOut1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean limiter_active_bOut1;
                      effect in1 -> limiter_active_bOut1;
                      effect in2 -> limiter_active_bOut1;
                    }
                    <<Type="SubSystem">> component TrueBlock {
                      ports
                        out Boolean yOut1;
                      <<Type="Constant",Value="1">> component One {
                        ports
                          out Boolean out1;
                      }
                      component One one;
                      connect one.out1 -> yOut1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Gain gain;
                    component LogOp logOp;
                    component TrueBlock trueBlock;
                    component VERSION_INFO vERSION_INFO;
                    connect in1 -> gain.vMax_kmhIn1;
                    connect logOp.limiter_active_bOut1 -> limiter_active_bOut2;
                    connect gain.vMax_kmhOut1 -> vMax_kmhOut1;
                    connect trueBlock.yOut1 -> logOp.in1;
                    connect trueBlock.yOut1 -> logOp.in2;
                  }
                  <<Type="SubSystem">> component Limiter_Deactive {
                    ports
                      out Double vMax_kmhOut1,
                      out Boolean limiter_active_bOut2;
                    <<Type="Constant",Value="300">> component Constant {
                      ports
                        out Double vMax_kmhOut1;
                    }
                    <<Type="SubSystem">> component FalseBlock {
                      ports
                        out Boolean yOut1;
                      <<Type="Constant",Value="0">> component Zero {
                        ports
                          out Boolean out1;
                      }
                      component Zero zero;
                      connect zero.out1 -> yOut1;
                    }
                    <<Type="Gain",Gain="1">> component Gain {
                      ports
                        in Double vMax_kmhIn1,
                        out Double vMax_kmhOut1;
                      effect vMax_kmhIn1 -> vMax_kmhOut1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean limiter_active_bOut1;
                      effect in1 -> limiter_active_bOut1;
                      effect in2 -> limiter_active_bOut1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Constant constant;
                    component FalseBlock falseBlock;
                    component Gain gain;
                    component LogOp logOp;
                    component VERSION_INFO vERSION_INFO;
                    connect logOp.limiter_active_bOut1 -> limiter_active_bOut2;
                    connect falseBlock.yOut1 -> logOp.in1;
                    connect falseBlock.yOut1 -> logOp.in2;
                    connect gain.vMax_kmhOut1 -> vMax_kmhOut1;
                    connect constant.vMax_kmhOut1 -> gain.vMax_kmhIn1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Type="Switch">> component SwitchBlock {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u2 & u1 <= 90",Type="Condition">> component Condition {
                    ports
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock1 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="!(u2 & u1 <= 90)",Type="Condition">> component Condition1 {
                    ports
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                    ports
                      in Double valueIn,
                      out Double valueOut;
                  }
                  <<Type="Switch">> component SwitchBlock2 {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                  }
                  <<Condition="u2 & u1 <= 90",Type="Condition">> component Condition2 {
                    ports
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock3 {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                  }
                  <<Condition="!(u2 & u1 <= 90)",Type="Condition">> component Condition3 {
                    ports
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                    ports
                      in Boolean valueIn,
                      out Boolean valueOut;
                  }
                  component Limiter_Active limiter_Active;
                  component Limiter_Deactive limiter_Deactive;
                  component VERSION_INFO vERSION_INFO;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  component SwitchBlock1 switchBlock1;
                  component Condition1 condition1;
                  component UnitDelay unitDelay;
                  component SwitchBlock2 switchBlock2;
                  component Condition2 condition2;
                  component SwitchBlock3 switchBlock3;
                  component Condition3 condition3;
                  component UnitDelay1 unitDelay1;
                  connect switchBlock.out1 -> vMax_kmhOut1;
                  connect switchBlock2.out1 -> limiter_active_bOut2;
                  connect in1 -> limiter_Active.in1;
                  connect acceleration_pedal_pcIn2 -> condition.in1;
                  connect limiter_bIn1 -> condition.in2;
                  connect condition.out1 -> switchBlock.condition;
                  connect limiter_Active.vMax_kmhOut1 -> switchBlock.ifIn;
                  connect acceleration_pedal_pcIn2 -> condition1.in1;
                  connect limiter_bIn1 -> condition1.in2;
                  connect condition1.out1 -> switchBlock1.condition;
                  connect limiter_Deactive.vMax_kmhOut1 -> switchBlock1.ifIn;
                  connect switchBlock1.out1 -> switchBlock.elseIn;
                  connect switchBlock.out1 -> unitDelay.valueIn;
                  connect unitDelay.valueOut -> switchBlock1.elseIn;
                  connect acceleration_pedal_pcIn2 -> condition2.in1;
                  connect limiter_bIn1 -> condition2.in2;
                  connect condition2.out1 -> switchBlock2.condition;
                  connect limiter_Active.limiter_active_bOut2 -> switchBlock2.ifIn;
                  connect acceleration_pedal_pcIn2 -> condition3.in1;
                  connect limiter_bIn1 -> condition3.in2;
                  connect condition3.out1 -> switchBlock3.condition;
                  connect limiter_Deactive.limiter_active_bOut2 -> switchBlock3.ifIn;
                  connect switchBlock3.out1 -> switchBlock2.elseIn;
                  connect switchBlock2.out1 -> unitDelay1.valueIn;
                  connect unitDelay1.valueOut -> switchBlock3.elseIn;
                }
                <<Type="SubSystem">> component Limiter_InitialSetValue {
                  ports
                    in Double v_Vehicle_kmhIn1,
                    out Double out1,
                    out Double out2,
                    out Double out3,
                    out Double out4,
                    out Double out5,
                    out Double out6;
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component VERSION_INFO vERSION_INFO;
                  connect v_Vehicle_kmhIn1 -> out1;
                  connect v_Vehicle_kmhIn1 -> out2;
                  connect v_Vehicle_kmhIn1 -> out3;
                  connect v_Vehicle_kmhIn1 -> out4;
                  connect v_Vehicle_kmhIn1 -> out5;
                  connect v_Vehicle_kmhIn1 -> out6;
                }
                <<Type="SubSystem">> component Limiter_SetValue {
                  ports
                    in Boolean leverUp_bIn1,
                    in Boolean leverDown_bIn2,
                    in Double in1,
                    in Double in2,
                    in Double in3,
                    out Double out1,
                    in Double in4,
                    out Double out2;
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Type="SubSystem">> component V_LimSetValueMinus {
                    ports
                      in Double v_LimiterSetValueIn1,
                      out Double out1,
                      out Double out2;
                    <<Type="Constant",Value="5">> component Parameter {
                      ports
                        out Double out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Parameter parameter;
                    component Sum sum;
                    component VERSION_INFO vERSION_INFO;
                    connect sum.out1 -> out1;
                    connect sum.out1 -> out2;
                    connect parameter.out1 -> sum.in2;
                    connect v_LimiterSetValueIn1 -> sum.in1;
                  }
                  <<Type="SubSystem">> component V_LimSetValuePlus {
                    ports
                      in Double v_LimiterSetValueIn1,
                      out Double out1,
                      out Double out2;
                    <<Type="Constant",Value="5">> component Parameter {
                      ports
                        out Double out1;
                    }
                    <<Type="Sum",ListOfSigns="++">> component Sum {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Parameter parameter;
                    component Sum sum;
                    component VERSION_INFO vERSION_INFO;
                    connect sum.out1 -> out1;
                    connect sum.out1 -> out2;
                    connect parameter.out1 -> sum.in2;
                    connect v_LimiterSetValueIn1 -> sum.in1;
                  }
                  <<Type="SubSystem">> component RisingEdgeDetector {
                    ports
                      in Boolean in1In1,
                      out Boolean out1Out1;
                    <<Type="SubSystem">> component CompareToZero {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero1 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero2 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="<",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero3 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">=",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero4 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="Constant",Value="1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="0">> component Constant1 {
                      ports
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory1 {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                    }
                    <<Condition="u2 > 0",Type="Condition">> component Condition {
                      ports
                        in Double in1,
                        out Boolean out1;
                    }
                    component CompareToZero compareToZero;
                    component CompareToZero1 compareToZero1;
                    component CompareToZero2 compareToZero2;
                    component CompareToZero3 compareToZero3;
                    component CompareToZero4 compareToZero4;
                    component Constant constant;
                    component Constant1 constant1;
                    component LogicalOperator logicalOperator;
                    component LogicalOperator1 logicalOperator1;
                    component LogicalOperator2 logicalOperator2;
                    component Memory memory;
                    component Memory1 memory1;
                    component Memory2 memory2;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    connect switchBlock.out1 -> out1Out1;
                    connect compareToZero4.yOut1 -> logicalOperator.in3;
                    connect memory2.out1 -> compareToZero4.uIn1;
                    connect logicalOperator.out1 -> logicalOperator2.in1;
                    connect constant1.out1 -> switchBlock.elseIn;
                    connect constant.out1 -> memory1.in1;
                    connect condition.out1 -> switchBlock.condition;
                    connect logicalOperator2.out1 -> switchBlock.ifIn;
                    connect logicalOperator2.out1 -> memory2.in1;
                    connect logicalOperator1.out1 -> logicalOperator2.in2;
                    connect compareToZero3.yOut1 -> logicalOperator1.in2;
                    connect compareToZero2.yOut1 -> logicalOperator1.in1;
                    connect compareToZero1.yOut1 -> logicalOperator.in2;
                    connect compareToZero.yOut1 -> logicalOperator.in1;
                    connect memory.out1 -> compareToZero.uIn1;
                    connect memory.out1 -> compareToZero2.uIn1;
                    connect in1In1 -> memory.in1;
                    connect in1In1 -> compareToZero1.uIn1;
                    connect in1In1 -> compareToZero3.uIn1;
                    connect memory1.out1 -> condition.in1;
                  }
                  <<Type="SubSystem">> component RisingEdgeDetector1 {
                    ports
                      in Boolean in1In1,
                      out Boolean out1Out1;
                    <<Type="SubSystem">> component CompareToZero {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero1 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero2 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="<",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero3 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">=",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero4 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="Constant",Value="1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="0">> component Constant1 {
                      ports
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory1 {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                    }
                    <<Condition="u2 > 0",Type="Condition">> component Condition {
                      ports
                        in Double in1,
                        out Boolean out1;
                    }
                    component CompareToZero compareToZero;
                    component CompareToZero1 compareToZero1;
                    component CompareToZero2 compareToZero2;
                    component CompareToZero3 compareToZero3;
                    component CompareToZero4 compareToZero4;
                    component Constant constant;
                    component Constant1 constant1;
                    component LogicalOperator logicalOperator;
                    component LogicalOperator1 logicalOperator1;
                    component LogicalOperator2 logicalOperator2;
                    component Memory memory;
                    component Memory1 memory1;
                    component Memory2 memory2;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    connect switchBlock.out1 -> out1Out1;
                    connect compareToZero4.yOut1 -> logicalOperator.in3;
                    connect memory2.out1 -> compareToZero4.uIn1;
                    connect logicalOperator.out1 -> logicalOperator2.in1;
                    connect constant1.out1 -> switchBlock.elseIn;
                    connect constant.out1 -> memory1.in1;
                    connect condition.out1 -> switchBlock.condition;
                    connect logicalOperator2.out1 -> switchBlock.ifIn;
                    connect logicalOperator2.out1 -> memory2.in1;
                    connect logicalOperator1.out1 -> logicalOperator2.in2;
                    connect compareToZero3.yOut1 -> logicalOperator1.in2;
                    connect compareToZero2.yOut1 -> logicalOperator1.in1;
                    connect compareToZero1.yOut1 -> logicalOperator.in2;
                    connect compareToZero.yOut1 -> logicalOperator.in1;
                    connect memory.out1 -> compareToZero.uIn1;
                    connect memory.out1 -> compareToZero2.uIn1;
                    connect in1In1 -> memory.in1;
                    connect in1In1 -> compareToZero1.uIn1;
                    connect in1In1 -> compareToZero3.uIn1;
                    connect memory1.out1 -> condition.in1;
                  }
                  <<Type="Switch">> component SwitchBlock {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock1 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock2 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock3 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition3 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component VERSION_INFO vERSION_INFO;
                  component V_LimSetValueMinus v_LimSetValueMinus;
                  component V_LimSetValuePlus v_LimSetValuePlus;
                  component RisingEdgeDetector risingEdgeDetector;
                  component RisingEdgeDetector1 risingEdgeDetector1;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  component SwitchBlock1 switchBlock1;
                  component Condition1 condition1;
                  component SwitchBlock2 switchBlock2;
                  component Condition2 condition2;
                  component SwitchBlock3 switchBlock3;
                  component Condition3 condition3;
                  connect in2 -> v_LimSetValueMinus.v_LimiterSetValueIn1;
                  connect in1 -> v_LimSetValuePlus.v_LimiterSetValueIn1;
                  connect leverUp_bIn1 -> risingEdgeDetector.in1In1;
                  connect leverDown_bIn2 -> risingEdgeDetector1.in1In1;
                  connect risingEdgeDetector.out1Out1 -> condition.in1;
                  connect v_LimSetValuePlus.out1 -> switchBlock.ifIn;
                  connect condition.out1 -> switchBlock.condition;
                  connect risingEdgeDetector1.out1Out1 -> condition1.in1;
                  connect v_LimSetValueMinus.out1 -> switchBlock1.ifIn;
                  connect condition1.out1 -> switchBlock1.condition;
                  connect in3 -> switchBlock1.elseIn;
                  connect switchBlock1.out1 -> switchBlock.elseIn;
                  connect switchBlock.out1 -> out1;
                  connect risingEdgeDetector.out1Out1 -> condition2.in1;
                  connect v_LimSetValuePlus.out2 -> switchBlock2.ifIn;
                  connect condition2.out1 -> switchBlock2.condition;
                  connect risingEdgeDetector1.out1Out1 -> condition3.in1;
                  connect v_LimSetValueMinus.out2 -> switchBlock3.ifIn;
                  connect condition3.out1 -> switchBlock3.condition;
                  connect in4 -> switchBlock3.elseIn;
                  connect switchBlock3.out1 -> switchBlock2.elseIn;
                  connect switchBlock2.out1 -> out2;
                }
                <<Type="SubSystem">> component Limiter_StartUpSetValue {
                  ports
                    out Double out1;
                  <<Type="Constant",Value="300">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component VERSION_INFO vERSION_INFO;
                  connect constant.out1 -> out1;
                }
                <<Type="SubSystem">> component SysInit {
                  ports
                    out Double yOut1;
                  <<Type="UnitDelay",InitialCondition="1">> component Memory_Init {
                    ports
                      in Double in1,
                      out Double out1;
                    effect in1 -> out1;
                  }
                  <<Type="Constant",Value="0">> component Zero_Init {
                    ports
                      out Double out1;
                  }
                  component Memory_Init memory_Init;
                  component Zero_Init zero_Init;
                  connect memory_Init.out1 -> yOut1;
                  connect zero_Init.out1 -> memory_Init.in1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Type="SubSystem">> component RisingEdgeDetector {
                  ports
                    in Double in1In1,
                    out Boolean out1Out1;
                  <<Type="SubSystem">> component CompareToZero {
                    ports
                      in Double uIn1,
                      out Boolean yOut1;
                    <<Operator="==",Type="RelationalOperator">> component Compare {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero1 {
                    ports
                      in Double uIn1,
                      out Boolean yOut1;
                    <<Operator=">",Type="RelationalOperator">> component Compare {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero2 {
                    ports
                      in Double uIn1,
                      out Boolean yOut1;
                    <<Operator="<",Type="RelationalOperator">> component Compare {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero3 {
                    ports
                      in Double uIn1,
                      out Boolean yOut1;
                    <<Operator=">=",Type="RelationalOperator">> component Compare {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero4 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator="==",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="Constant",Value="1">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant1 {
                    ports
                      out Boolean out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="Memory">> component Memory {
                    ports
                      in Double in1,
                      out Double out1;
                  }
                  <<Type="Memory">> component Memory1 {
                    ports
                      in Double in1,
                      out Double out1;
                  }
                  <<Type="Memory">> component Memory2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                  }
                  <<Condition="u2 > 0",Type="Condition">> component Condition {
                    ports
                      in Double in1,
                      out Boolean out1;
                  }
                  component CompareToZero compareToZero;
                  component CompareToZero1 compareToZero1;
                  component CompareToZero2 compareToZero2;
                  component CompareToZero3 compareToZero3;
                  component CompareToZero4 compareToZero4;
                  component Constant constant;
                  component Constant1 constant1;
                  component LogicalOperator logicalOperator;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator2 logicalOperator2;
                  component Memory memory;
                  component Memory1 memory1;
                  component Memory2 memory2;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  connect switchBlock.out1 -> out1Out1;
                  connect compareToZero4.yOut1 -> logicalOperator.in3;
                  connect memory2.out1 -> compareToZero4.uIn1;
                  connect logicalOperator.out1 -> logicalOperator2.in1;
                  connect constant1.out1 -> switchBlock.elseIn;
                  connect constant.out1 -> memory1.in1;
                  connect condition.out1 -> switchBlock.condition;
                  connect logicalOperator2.out1 -> switchBlock.ifIn;
                  connect logicalOperator2.out1 -> memory2.in1;
                  connect logicalOperator1.out1 -> logicalOperator2.in2;
                  connect compareToZero3.yOut1 -> logicalOperator1.in2;
                  connect compareToZero2.yOut1 -> logicalOperator1.in1;
                  connect compareToZero1.yOut1 -> logicalOperator.in2;
                  connect compareToZero.yOut1 -> logicalOperator.in1;
                  connect memory.out1 -> compareToZero.uIn1;
                  connect memory.out1 -> compareToZero2.uIn1;
                  connect in1In1 -> memory.in1;
                  connect in1In1 -> compareToZero1.uIn1;
                  connect in1In1 -> compareToZero3.uIn1;
                  connect memory1.out1 -> condition.in1;
                }
                <<Type="SubSystem">> component RisingEdgeDetector1 {
                  ports
                    in Boolean in1In1,
                    out Boolean out1Out1;
                  <<Type="SubSystem">> component CompareToZero {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator="==",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero1 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator=">",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero2 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator="<",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero3 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator=">=",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero4 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator="==",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="Constant",Value="1">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant1 {
                    ports
                      out Boolean out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                  }
                  <<Type="Memory">> component Memory {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Memory">> component Memory1 {
                    ports
                      in Double in1,
                      out Double out1;
                  }
                  <<Type="Memory">> component Memory2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                  }
                  <<Condition="u2 > 0",Type="Condition">> component Condition {
                    ports
                      in Double in1,
                      out Boolean out1;
                  }
                  component CompareToZero compareToZero;
                  component CompareToZero1 compareToZero1;
                  component CompareToZero2 compareToZero2;
                  component CompareToZero3 compareToZero3;
                  component CompareToZero4 compareToZero4;
                  component Constant constant;
                  component Constant1 constant1;
                  component LogicalOperator logicalOperator;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator2 logicalOperator2;
                  component Memory memory;
                  component Memory1 memory1;
                  component Memory2 memory2;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  connect switchBlock.out1 -> out1Out1;
                  connect compareToZero4.yOut1 -> logicalOperator.in3;
                  connect memory2.out1 -> compareToZero4.uIn1;
                  connect logicalOperator.out1 -> logicalOperator2.in1;
                  connect constant1.out1 -> switchBlock.elseIn;
                  connect constant.out1 -> memory1.in1;
                  connect condition.out1 -> switchBlock.condition;
                  connect logicalOperator2.out1 -> switchBlock.ifIn;
                  connect logicalOperator2.out1 -> memory2.in1;
                  connect logicalOperator1.out1 -> logicalOperator2.in2;
                  connect compareToZero3.yOut1 -> logicalOperator1.in2;
                  connect compareToZero2.yOut1 -> logicalOperator1.in1;
                  connect compareToZero1.yOut1 -> logicalOperator.in2;
                  connect compareToZero.yOut1 -> logicalOperator.in1;
                  connect memory.out1 -> compareToZero.uIn1;
                  connect memory.out1 -> compareToZero2.uIn1;
                  connect in1In1 -> memory.in1;
                  connect in1In1 -> compareToZero1.uIn1;
                  connect in1In1 -> compareToZero3.uIn1;
                  connect memory1.out1 -> condition.in1;
                }
                <<Type="Switch">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock2 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition2 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock3 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition3 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock4 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition4 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock5 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition5 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock6 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition6 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock7 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition7 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock8 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1>0",Type="Condition">> component Condition8 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Limiter_Function limiter_Function;
                component Limiter_InitialSetValue limiter_InitialSetValue;
                component Limiter_SetValue limiter_SetValue;
                component Limiter_StartUpSetValue limiter_StartUpSetValue;
                component SysInit sysInit;
                component VERSION_INFO vERSION_INFO;
                component RisingEdgeDetector risingEdgeDetector;
                component RisingEdgeDetector1 risingEdgeDetector1;
                component SwitchBlock switchBlock;
                component Condition condition;
                component SwitchBlock1 switchBlock1;
                component Condition1 condition1;
                component SwitchBlock2 switchBlock2;
                component Condition2 condition2;
                component SwitchBlock3 switchBlock3;
                component Condition3 condition3;
                component SwitchBlock4 switchBlock4;
                component Condition4 condition4;
                component SwitchBlock5 switchBlock5;
                component Condition5 condition5;
                component SwitchBlock6 switchBlock6;
                component Condition6 condition6;
                component SwitchBlock7 switchBlock7;
                component Condition7 condition7;
                component SwitchBlock8 switchBlock8;
                component Condition8 condition8;
                connect sysInit.yOut1 -> risingEdgeDetector.in1In1;
                connect limiter_bIn1 -> risingEdgeDetector1.in1In1;
                connect risingEdgeDetector1.out1Out1 -> condition.in1;
                connect limiter_InitialSetValue.out1 -> switchBlock.ifIn;
                connect condition.out1 -> switchBlock.condition;
                connect in1 -> switchBlock.elseIn;
                connect switchBlock.out1 -> limiter_SetValue.in1;
                connect risingEdgeDetector1.out1Out1 -> condition1.in1;
                connect limiter_InitialSetValue.out2 -> switchBlock1.ifIn;
                connect condition1.out1 -> switchBlock1.condition;
                connect in2 -> switchBlock1.elseIn;
                connect switchBlock1.out1 -> limiter_SetValue.in2;
                connect risingEdgeDetector1.out1Out1 -> condition2.in1;
                connect limiter_InitialSetValue.out3 -> switchBlock2.ifIn;
                connect condition2.out1 -> switchBlock2.condition;
                connect in3 -> switchBlock2.elseIn;
                connect switchBlock2.out1 -> limiter_SetValue.in3;
                connect limiter_bIn1 -> condition3.in1;
                connect limiter_SetValue.out1 -> switchBlock3.ifIn;
                connect condition3.out1 -> switchBlock3.condition;
                connect risingEdgeDetector1.out1Out1 -> condition4.in1;
                connect limiter_InitialSetValue.out4 -> switchBlock4.ifIn;
                connect condition4.out1 -> switchBlock4.condition;
                connect in4 -> switchBlock4.elseIn;
                connect switchBlock4.out1 -> switchBlock3.elseIn;
                connect switchBlock3.out1 -> limiter_Function.in1;
                connect risingEdgeDetector.out1Out1 -> condition5.in1;
                connect limiter_StartUpSetValue.out1 -> switchBlock5.ifIn;
                connect condition5.out1 -> switchBlock5.condition;
                connect risingEdgeDetector1.out1Out1 -> condition6.in1;
                connect limiter_InitialSetValue.out5 -> switchBlock6.ifIn;
                connect condition6.out1 -> switchBlock6.condition;
                connect in5 -> switchBlock6.elseIn;
                connect switchBlock6.out1 -> limiter_SetValue.in4;
                connect limiter_bIn1 -> condition7.in1;
                connect limiter_SetValue.out2 -> switchBlock7.ifIn;
                connect condition7.out1 -> switchBlock7.condition;
                connect risingEdgeDetector1.out1Out1 -> condition8.in1;
                connect limiter_InitialSetValue.out6 -> switchBlock8.ifIn;
                connect condition8.out1 -> switchBlock8.condition;
                connect in6 -> switchBlock8.elseIn;
                connect switchBlock8.out1 -> switchBlock7.elseIn;
                connect switchBlock7.out1 -> switchBlock5.elseIn;
                connect switchBlock5.out1 -> out1;
                connect limiter_Function.limiter_active_bOut2 -> limiter_active_bOut2;
                connect acceleration_pedal_pcIn2 -> limiter_Function.acceleration_pedal_pcIn2;
                connect limiter_bIn1 -> limiter_Function.limiter_bIn1;
                connect limiter_Function.vMax_kmhOut1 -> vMax_kmhOut1;
                connect v_Vehicle_kmhIn3 -> limiter_InitialSetValue.v_Vehicle_kmhIn1;
                connect leverUp_bIn4 -> limiter_SetValue.leverUp_bIn1;
                connect leverDown_bIn5 -> limiter_SetValue.leverDown_bIn2;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Type="Switch">> component SwitchBlock {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition1 {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Type="Constant",Value="300">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition2 {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock2 {
                ports
                  in Boolean ifIn,
                  in Boolean condition,
                  in Boolean elseIn,
                  out Boolean out1;
              }
              <<Type="Constant",Value="0">> component Constant2 {
                ports
                  out Boolean out1;
              }
              component Constant constant;
              component Limiter_enabled limiter_enabled;
              component VERSION_INFO vERSION_INFO;
              component SwitchBlock switchBlock;
              component Condition condition;
              component Condition1 condition1;
              component SwitchBlock1 switchBlock1;
              component Constant1 constant1;
              component Condition2 condition2;
              component SwitchBlock2 switchBlock2;
              component Constant2 constant2;
              connect in1 -> limiter_enabled.in1;
              connect in2 -> limiter_enabled.in2;
              connect in3 -> limiter_enabled.in3;
              connect in4 -> limiter_enabled.in4;
              connect in5 -> limiter_enabled.in5;
              connect in6 -> limiter_enabled.in6;
              connect constant.out1 -> condition.in1;
              connect limiter_enabled.out1 -> switchBlock.ifIn;
              connect condition.out1 -> switchBlock.condition;
              connect in7 -> switchBlock.elseIn;
              connect switchBlock.out1 -> out1;
              connect constant.out1 -> condition1.in1;
              connect condition1.out1 -> switchBlock1.condition;
              connect limiter_enabled.vMax_kmhOut1 -> switchBlock1.ifIn;
              connect constant1.out1 -> switchBlock1.elseIn;
              connect constant.out1 -> condition2.in1;
              connect condition2.out1 -> switchBlock2.condition;
              connect limiter_enabled.limiter_active_bOut2 -> switchBlock2.ifIn;
              connect constant2.out1 -> switchBlock2.elseIn;
              connect switchBlock2.out1 -> limiter_active_bOut2;
              connect switchBlock1.out1 -> vMax_kmhOut1;
              connect leverDown_bIn5 -> limiter_enabled.leverDown_bIn5;
              connect leverUp_bIn4 -> limiter_enabled.leverUp_bIn4;
              connect v_Vehicle_kmhIn3 -> limiter_enabled.v_Vehicle_kmhIn3;
              connect acceleration_pedal_pcIn2 -> limiter_enabled.acceleration_pedal_pcIn2;
              connect limiter_bIn1 -> limiter_enabled.limiter_bIn1;
            }
            <<Type="SubSystem">> component Tempomat {
              ports
                in Boolean parkingBrake_bIn1,
                in Double brakeForce_pedal_pcIn2,
                in Boolean cruiseControl_bIn3,
                in Double v_Vehicle_kmhIn4,
                in Boolean limiter_bIn5,
                in Boolean leverUp_bIn6,
                in Boolean leverDown_bIn7,
                out Double v_CC_delta_kmhOut1,
                out Boolean cC_active_bOut2,
                in Double in1,
                in Double in2,
                in Double in3,
                in Double in4,
                in Double in5,
                in Double in6,
                in Double in7,
                in Double in8,
                in Double in9,
                in Double in10,
                in Double in11,
                in Double in12,
                in Double in13,
                out Double out1;
              <<Type="Constant",Value="1">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="SubSystem">> component Tempomat_Function {
                ports
                  in Boolean parkingBrake_bIn1,
                  in Double brakeForce_pedal_pcIn2,
                  in Boolean cruiseControl_bIn3,
                  in Double v_Vehicle_kmhIn4,
                  in Boolean limiter_bIn5,
                  in Boolean leverUp_bIn6,
                  in Boolean leverDown_bIn7,
                  out Double v_CC_delta_kmhOut1,
                  out Boolean cC_active_bOut2,
                  in Double in1,
                  in Double in2,
                  in Double in3,
                  in Double in4,
                  in Double in5,
                  in Double in6,
                  in Double in7,
                  in Double in8,
                  in Double in9,
                  in Double in10,
                  in Double in11,
                  in Double in12,
                  out Double out1;
                <<Type="SubSystem">> component CC_On_Off {
                  ports
                    in Boolean parkingBrake_bIn1,
                    in Double brakeForce_pedal_pcIn2,
                    in Boolean cruiseControl_bIn3,
                    in Double v_Vehicle_kmhIn4,
                    in Boolean limiter_bIn5,
                    in Boolean leverUp_bIn6,
                    in Boolean leverDown_bIn7,
                    out Boolean cC_active_bOut1;
                  <<Type="Constant",Value="0">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="20">> component Constant1 {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component EdgeFalling {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect switch_R.out1 -> logOp_A.in2;
                    connect uIn1 -> logOp_N.in1;
                    connect uIn1 -> memory_U.in1;
                    connect logOp_N.out1 -> logOp_A.in1;
                    connect logOp_A.out1 -> yOut1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component EdgeRising {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect logOp_N.out1 -> logOp_A.in2;
                    connect logOp_A.out1 -> yOut1;
                    connect uIn1 -> logOp_A.in1;
                    connect uIn1 -> memory_U.in1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component FalseBlock {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Type="SubSystem">> component FalseBlock1 {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      in Boolean in4,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                    effect in3 -> out1;
                    effect in4 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator3 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                    effect in3 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator4 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator5 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="SubSystem">> component RSFlipFlop {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_S.condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect sIn1 -> condition1.in1;
                    connect memory_Q.out1 -> switch_S.elseIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect logOp_N.out1 -> nOT_QOut2;
                  }
                  <<Operator="<",Type="RelationalOperator">> component RelOp1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator=">",Type="RelationalOperator">> component RelOp2 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Terminator">> component Terminator {
                    ports
                      in Boolean in1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant1 constant1;
                  component EdgeFalling edgeFalling;
                  component EdgeRising edgeRising;
                  component FalseBlock falseBlock;
                  component FalseBlock1 falseBlock1;
                  component LogicalOperator logicalOperator;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator3 logicalOperator3;
                  component LogicalOperator4 logicalOperator4;
                  component LogicalOperator5 logicalOperator5;
                  component RSFlipFlop rSFlipFlop;
                  component RelOp1 relOp1;
                  component RelOp2 relOp2;
                  component Terminator terminator;
                  component VERSION_INFO vERSION_INFO;
                  connect falseBlock1.yOut1 -> edgeFalling.iVIn3;
                  connect logicalOperator5.out1 -> logicalOperator4.in1;
                  connect logicalOperator3.out1 -> logicalOperator4.in2;
                  connect logicalOperator4.out1 -> logicalOperator1.in1;
                  connect logicalOperator4.out1 -> edgeRising.uIn1;
                  connect limiter_bIn5 -> logicalOperator5.in1;
                  connect edgeFalling.yOut1 -> logicalOperator.in1;
                  connect v_Vehicle_kmhIn4 -> relOp1.in1;
                  connect cruiseControl_bIn3 -> logicalOperator3.in3;
                  connect cruiseControl_bIn3 -> edgeFalling.uIn1;
                  connect cruiseControl_bIn3 -> edgeFalling.rIn2;
                  connect leverUp_bIn6 -> logicalOperator3.in1;
                  connect leverDown_bIn7 -> logicalOperator3.in2;
                  connect relOp1.out1 -> logicalOperator.in4;
                  connect rSFlipFlop.qOut1 -> cC_active_bOut1;
                  connect constant1.out1 -> relOp1.in2;
                  connect falseBlock.yOut1 -> edgeRising.iVIn3;
                  connect logicalOperator1.out1 -> edgeRising.rIn2;
                  connect brakeForce_pedal_pcIn2 -> relOp2.in1;
                  connect rSFlipFlop.nOT_QOut2 -> terminator.in1;
                  connect parkingBrake_bIn1 -> logicalOperator.in2;
                  connect constant.out1 -> relOp2.in2;
                  connect relOp2.out1 -> logicalOperator.in3;
                  connect logicalOperator.out1 -> rSFlipFlop.rIn2;
                  connect edgeRising.yOut1 -> rSFlipFlop.sIn1;
                }
                <<Type="SubSystem">> component CC_SetValue {
                  ports
                    in Boolean limiter_bIn1,
                    in Boolean cC_enabled_bIn2,
                    in Boolean cruiseControl_bIn3,
                    in Double v_Vehicle_kmhIn4,
                    in Boolean leverUp_bIn5,
                    in Boolean leverDown_bIn6,
                    in Double in1,
                    in Double in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    in Double in6,
                    in Double in7,
                    in Double in8,
                    out Double out1,
                    in Double in9,
                    in Double in10,
                    in Double in11,
                    in Double in12,
                    out Double out2;
                  <<Type="SubSystem">> component CC_ChangeSetValue {
                    ports
                      in Boolean cCUp_bIn1,
                      in Boolean cCDown_bIn2,
                      in Double in1,
                      in Double in2,
                      in Double in3,
                      out Double out1,
                      in Double in4,
                      out Double out2,
                      in Double in5,
                      out Double out3,
                      in Double in6,
                      out Double out4,
                      in Double in7,
                      out Double out5;
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    <<Type="SubSystem">> component V_SetValueMinus {
                      ports
                        in Double v_CCSetValueIn1,
                        out Double out1,
                        out Double out2,
                        out Double out3,
                        out Double out4,
                        out Double out5;
                      <<Type="Constant",Value="5">> component Parameter {
                        ports
                          out Double out1;
                      }
                      <<Type="Sum",ListOfSigns="+-">> component Sum {
                        ports
                          in Double in1,
                          in Double in2,
                          out Double out1;
                        effect in1 -> out1;
                        effect in2 -> out1;
                      }
                      <<Type="SubSystem">> component VERSION_INFO {
                        <<Type="SubSystem">> component Copyright {
                        }
                        component Copyright copyright;
                      }
                      component Parameter parameter;
                      component Sum sum;
                      component VERSION_INFO vERSION_INFO;
                      connect sum.out1 -> out1;
                      connect sum.out1 -> out2;
                      connect sum.out1 -> out3;
                      connect sum.out1 -> out4;
                      connect sum.out1 -> out5;
                      connect parameter.out1 -> sum.in2;
                      connect v_CCSetValueIn1 -> sum.in1;
                    }
                    <<Type="SubSystem">> component V_SetValuePlus {
                      ports
                        in Double v_CCSetValueIn1,
                        out Double out1,
                        out Double out2,
                        out Double out3,
                        out Double out4,
                        out Double out5;
                      <<Type="Constant",Value="5">> component Parameter {
                        ports
                          out Double out1;
                      }
                      <<Type="Sum",ListOfSigns="++">> component Sum {
                        ports
                          in Double in1,
                          in Double in2,
                          out Double out1;
                        effect in1 -> out1;
                        effect in2 -> out1;
                      }
                      <<Type="SubSystem">> component VERSION_INFO {
                        <<Type="SubSystem">> component Copyright {
                        }
                        component Copyright copyright;
                      }
                      component Parameter parameter;
                      component Sum sum;
                      component VERSION_INFO vERSION_INFO;
                      connect sum.out1 -> out1;
                      connect sum.out1 -> out2;
                      connect sum.out1 -> out3;
                      connect sum.out1 -> out4;
                      connect sum.out1 -> out5;
                      connect parameter.out1 -> sum.in2;
                      connect v_CCSetValueIn1 -> sum.in1;
                    }
                    <<Type="SubSystem">> component RisingEdgeDetector {
                      ports
                        in Boolean in1In1,
                        out Boolean out1Out1;
                      <<Type="SubSystem">> component CompareToZero {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="==",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero1 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator=">",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero2 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="<",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero3 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator=">=",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero4 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="==",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="Constant",Value="1">> component Constant {
                        ports
                          out Double out1;
                      }
                      <<Type="Constant",Value="0">> component Constant1 {
                        ports
                          out Boolean out1;
                      }
                      <<Operator="AND",Type="Logic">> component LogicalOperator {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          in Boolean in3,
                          out Boolean out1;
                      }
                      <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                      }
                      <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                      }
                      <<Type="Memory">> component Memory {
                        ports
                          in Boolean in1,
                          out Boolean out1;
                      }
                      <<Type="Memory">> component Memory1 {
                        ports
                          in Double in1,
                          out Double out1;
                      }
                      <<Type="Memory">> component Memory2 {
                        ports
                          in Boolean in1,
                          out Boolean out1;
                      }
                      <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                        ports
                          in Boolean ifIn,
                          in Boolean condition,
                          in Boolean elseIn,
                          out Boolean out1;
                      }
                      <<Condition="u2 > 0",Type="Condition">> component Condition {
                        ports
                          in Double in1,
                          out Boolean out1;
                      }
                      component CompareToZero compareToZero;
                      component CompareToZero1 compareToZero1;
                      component CompareToZero2 compareToZero2;
                      component CompareToZero3 compareToZero3;
                      component CompareToZero4 compareToZero4;
                      component Constant constant;
                      component Constant1 constant1;
                      component LogicalOperator logicalOperator;
                      component LogicalOperator1 logicalOperator1;
                      component LogicalOperator2 logicalOperator2;
                      component Memory memory;
                      component Memory1 memory1;
                      component Memory2 memory2;
                      component SwitchBlock switchBlock;
                      component Condition condition;
                      connect switchBlock.out1 -> out1Out1;
                      connect compareToZero4.yOut1 -> logicalOperator.in3;
                      connect memory2.out1 -> compareToZero4.uIn1;
                      connect logicalOperator.out1 -> logicalOperator2.in1;
                      connect constant1.out1 -> switchBlock.elseIn;
                      connect constant.out1 -> memory1.in1;
                      connect condition.out1 -> switchBlock.condition;
                      connect logicalOperator2.out1 -> switchBlock.ifIn;
                      connect logicalOperator2.out1 -> memory2.in1;
                      connect logicalOperator1.out1 -> logicalOperator2.in2;
                      connect compareToZero3.yOut1 -> logicalOperator1.in2;
                      connect compareToZero2.yOut1 -> logicalOperator1.in1;
                      connect compareToZero1.yOut1 -> logicalOperator.in2;
                      connect compareToZero.yOut1 -> logicalOperator.in1;
                      connect memory.out1 -> compareToZero.uIn1;
                      connect memory.out1 -> compareToZero2.uIn1;
                      connect in1In1 -> memory.in1;
                      connect in1In1 -> compareToZero1.uIn1;
                      connect in1In1 -> compareToZero3.uIn1;
                      connect memory1.out1 -> condition.in1;
                    }
                    <<Type="SubSystem">> component RisingEdgeDetector1 {
                      ports
                        in Boolean in1In1,
                        out Boolean out1Out1;
                      <<Type="SubSystem">> component CompareToZero {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="==",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero1 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator=">",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero2 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="<",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero3 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator=">=",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="SubSystem">> component CompareToZero4 {
                        ports
                          in Boolean uIn1,
                          out Boolean yOut1;
                        <<Operator="==",Type="RelationalOperator">> component Compare {
                          ports
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                        }
                        <<Type="Constant",Value="0">> component Constant {
                          ports
                            out Double out1;
                        }
                        component Compare compare;
                        component Constant constant;
                        connect compare.out1 -> yOut1;
                        connect uIn1 -> compare.in1;
                        connect constant.out1 -> compare.in2;
                      }
                      <<Type="Constant",Value="1">> component Constant {
                        ports
                          out Double out1;
                      }
                      <<Type="Constant",Value="0">> component Constant1 {
                        ports
                          out Boolean out1;
                      }
                      <<Operator="AND",Type="Logic">> component LogicalOperator {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          in Boolean in3,
                          out Boolean out1;
                      }
                      <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                      }
                      <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                        ports
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                      }
                      <<Type="Memory">> component Memory {
                        ports
                          in Boolean in1,
                          out Boolean out1;
                      }
                      <<Type="Memory">> component Memory1 {
                        ports
                          in Double in1,
                          out Double out1;
                      }
                      <<Type="Memory">> component Memory2 {
                        ports
                          in Boolean in1,
                          out Boolean out1;
                      }
                      <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                        ports
                          in Boolean ifIn,
                          in Boolean condition,
                          in Boolean elseIn,
                          out Boolean out1;
                      }
                      <<Condition="u2 > 0",Type="Condition">> component Condition {
                        ports
                          in Double in1,
                          out Boolean out1;
                      }
                      component CompareToZero compareToZero;
                      component CompareToZero1 compareToZero1;
                      component CompareToZero2 compareToZero2;
                      component CompareToZero3 compareToZero3;
                      component CompareToZero4 compareToZero4;
                      component Constant constant;
                      component Constant1 constant1;
                      component LogicalOperator logicalOperator;
                      component LogicalOperator1 logicalOperator1;
                      component LogicalOperator2 logicalOperator2;
                      component Memory memory;
                      component Memory1 memory1;
                      component Memory2 memory2;
                      component SwitchBlock switchBlock;
                      component Condition condition;
                      connect switchBlock.out1 -> out1Out1;
                      connect compareToZero4.yOut1 -> logicalOperator.in3;
                      connect memory2.out1 -> compareToZero4.uIn1;
                      connect logicalOperator.out1 -> logicalOperator2.in1;
                      connect constant1.out1 -> switchBlock.elseIn;
                      connect constant.out1 -> memory1.in1;
                      connect condition.out1 -> switchBlock.condition;
                      connect logicalOperator2.out1 -> switchBlock.ifIn;
                      connect logicalOperator2.out1 -> memory2.in1;
                      connect logicalOperator1.out1 -> logicalOperator2.in2;
                      connect compareToZero3.yOut1 -> logicalOperator1.in2;
                      connect compareToZero2.yOut1 -> logicalOperator1.in1;
                      connect compareToZero1.yOut1 -> logicalOperator.in2;
                      connect compareToZero.yOut1 -> logicalOperator.in1;
                      connect memory.out1 -> compareToZero.uIn1;
                      connect memory.out1 -> compareToZero2.uIn1;
                      connect in1In1 -> memory.in1;
                      connect in1In1 -> compareToZero1.uIn1;
                      connect in1In1 -> compareToZero3.uIn1;
                      connect memory1.out1 -> condition.in1;
                    }
                    <<Type="Switch">> component SwitchBlock {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock1 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock2 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock3 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition3 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock4 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition4 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock5 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition5 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock6 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition6 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock7 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition7 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock8 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition8 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock9 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1>0",Type="Condition">> component Condition9 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component VERSION_INFO vERSION_INFO;
                    component V_SetValueMinus v_SetValueMinus;
                    component V_SetValuePlus v_SetValuePlus;
                    component RisingEdgeDetector risingEdgeDetector;
                    component RisingEdgeDetector1 risingEdgeDetector1;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    component SwitchBlock1 switchBlock1;
                    component Condition1 condition1;
                    component SwitchBlock2 switchBlock2;
                    component Condition2 condition2;
                    component SwitchBlock3 switchBlock3;
                    component Condition3 condition3;
                    component SwitchBlock4 switchBlock4;
                    component Condition4 condition4;
                    component SwitchBlock5 switchBlock5;
                    component Condition5 condition5;
                    component SwitchBlock6 switchBlock6;
                    component Condition6 condition6;
                    component SwitchBlock7 switchBlock7;
                    component Condition7 condition7;
                    component SwitchBlock8 switchBlock8;
                    component Condition8 condition8;
                    component SwitchBlock9 switchBlock9;
                    component Condition9 condition9;
                    connect in1 -> v_SetValuePlus.v_CCSetValueIn1;
                    connect in2 -> v_SetValueMinus.v_CCSetValueIn1;
                    connect cCUp_bIn1 -> risingEdgeDetector.in1In1;
                    connect cCDown_bIn2 -> risingEdgeDetector1.in1In1;
                    connect risingEdgeDetector.out1Out1 -> condition.in1;
                    connect v_SetValuePlus.out1 -> switchBlock.ifIn;
                    connect condition.out1 -> switchBlock.condition;
                    connect risingEdgeDetector1.out1Out1 -> condition1.in1;
                    connect v_SetValueMinus.out1 -> switchBlock1.ifIn;
                    connect condition1.out1 -> switchBlock1.condition;
                    connect in3 -> switchBlock1.elseIn;
                    connect switchBlock1.out1 -> switchBlock.elseIn;
                    connect switchBlock.out1 -> out1;
                    connect risingEdgeDetector.out1Out1 -> condition2.in1;
                    connect v_SetValuePlus.out2 -> switchBlock2.ifIn;
                    connect condition2.out1 -> switchBlock2.condition;
                    connect risingEdgeDetector1.out1Out1 -> condition3.in1;
                    connect v_SetValueMinus.out2 -> switchBlock3.ifIn;
                    connect condition3.out1 -> switchBlock3.condition;
                    connect in4 -> switchBlock3.elseIn;
                    connect switchBlock3.out1 -> switchBlock2.elseIn;
                    connect switchBlock2.out1 -> out2;
                    connect risingEdgeDetector.out1Out1 -> condition4.in1;
                    connect v_SetValuePlus.out3 -> switchBlock4.ifIn;
                    connect condition4.out1 -> switchBlock4.condition;
                    connect risingEdgeDetector1.out1Out1 -> condition5.in1;
                    connect v_SetValueMinus.out3 -> switchBlock5.ifIn;
                    connect condition5.out1 -> switchBlock5.condition;
                    connect in5 -> switchBlock5.elseIn;
                    connect switchBlock5.out1 -> switchBlock4.elseIn;
                    connect switchBlock4.out1 -> out3;
                    connect risingEdgeDetector.out1Out1 -> condition6.in1;
                    connect v_SetValuePlus.out4 -> switchBlock6.ifIn;
                    connect condition6.out1 -> switchBlock6.condition;
                    connect risingEdgeDetector1.out1Out1 -> condition7.in1;
                    connect v_SetValueMinus.out4 -> switchBlock7.ifIn;
                    connect condition7.out1 -> switchBlock7.condition;
                    connect in6 -> switchBlock7.elseIn;
                    connect switchBlock7.out1 -> switchBlock6.elseIn;
                    connect switchBlock6.out1 -> out4;
                    connect risingEdgeDetector.out1Out1 -> condition8.in1;
                    connect v_SetValuePlus.out5 -> switchBlock8.ifIn;
                    connect condition8.out1 -> switchBlock8.condition;
                    connect risingEdgeDetector1.out1Out1 -> condition9.in1;
                    connect v_SetValueMinus.out5 -> switchBlock9.ifIn;
                    connect condition9.out1 -> switchBlock9.condition;
                    connect in7 -> switchBlock9.elseIn;
                    connect switchBlock9.out1 -> switchBlock8.elseIn;
                    connect switchBlock8.out1 -> out5;
                  }
                  <<Type="SubSystem">> component CC_InitialSetValue {
                    ports
                      in Double v_Vehicle_kmhIn1,
                      in Double in1,
                      in Double in2,
                      out Double out1,
                      in Double in3,
                      out Double out2;
                    <<Type="SubSystem">> component CC_NoInitialSetValue {
                      ports
                        in Double v_Vehicle_kmhIn1,
                        out Double out1,
                        out Double out2;
                      <<Type="SubSystem">> component VERSION_INFO {
                        <<Type="SubSystem">> component Copyright {
                        }
                        component Copyright copyright;
                      }
                      component VERSION_INFO vERSION_INFO;
                      connect v_Vehicle_kmhIn1 -> out1;
                      connect v_Vehicle_kmhIn1 -> out2;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Operator="<",Type="RelationalOperator">> component RelOp {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    <<Type="Switch">> component SwitchBlock {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch">> component SwitchBlock1 {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                    }
                    <<Condition="u1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component CC_NoInitialSetValue cC_NoInitialSetValue;
                    component Constant constant;
                    component RelOp relOp;
                    component VERSION_INFO vERSION_INFO;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    component SwitchBlock1 switchBlock1;
                    component Condition1 condition1;
                    connect in1 -> relOp.in1;
                    connect relOp.out1 -> condition.in1;
                    connect cC_NoInitialSetValue.out1 -> switchBlock.ifIn;
                    connect condition.out1 -> switchBlock.condition;
                    connect in2 -> switchBlock.elseIn;
                    connect switchBlock.out1 -> out1;
                    connect relOp.out1 -> condition1.in1;
                    connect cC_NoInitialSetValue.out2 -> switchBlock1.ifIn;
                    connect condition1.out1 -> switchBlock1.condition;
                    connect in3 -> switchBlock1.elseIn;
                    connect switchBlock1.out1 -> out2;
                    connect constant.out1 -> relOp.in2;
                    connect v_Vehicle_kmhIn1 -> cC_NoInitialSetValue.v_Vehicle_kmhIn1;
                  }
                  <<Type="SubSystem">> component CC_OnSet_SetValue {
                    ports
                      in Double v_Vehicle_kmhIn1,
                      out Double out1,
                      out Double out2;
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component VERSION_INFO vERSION_INFO;
                    connect v_Vehicle_kmhIn1 -> out1;
                    connect v_Vehicle_kmhIn1 -> out2;
                  }
                  <<Type="SubSystem">> component CC_StartUpSetValue {
                    ports
                      out Double out1,
                      out Double out2;
                    <<Type="Constant",Value="-1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Constant constant;
                    component VERSION_INFO vERSION_INFO;
                    connect constant.out1 -> out1;
                    connect constant.out1 -> out2;
                  }
                  <<Type="SubSystem">> component CompareToZero {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator=">",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="SubSystem">> component CompareToZero1 {
                    ports
                      in Boolean uIn1,
                      out Boolean yOut1;
                    <<Operator=">",Type="RelationalOperator">> component Compare {
                      ports
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double out1;
                    }
                    component Compare compare;
                    component Constant constant;
                    connect compare.out1 -> yOut1;
                    connect uIn1 -> compare.in1;
                    connect constant.out1 -> compare.in2;
                  }
                  <<Type="Constant",Value="20">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component EdgeRising {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect logOp_N.out1 -> logOp_A.in2;
                    connect logOp_A.out1 -> yOut1;
                    connect uIn1 -> logOp_A.in1;
                    connect uIn1 -> memory_U.in1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component FalseBlock {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator2 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                    effect in3 -> out1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator3 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator4 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator5 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator6 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="SubSystem">> component RSFlipFlop {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition.out1 -> switch_S.condition;
                    connect condition1.out1 -> switch_R.condition;
                    connect sIn1 -> condition.in1;
                    connect rIn2 -> condition1.in1;
                    connect memory_Q.out1 -> switch_S.elseIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect logOp_N.out1 -> nOT_QOut2;
                  }
                  <<Operator=">=",Type="RelationalOperator">> component RelOp {
                    ports
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="SubSystem">> component SysInit {
                    ports
                      out Double yOut1;
                    <<Type="UnitDelay",InitialCondition="1">> component Memory_Init {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_Init {
                      ports
                        out Double out1;
                    }
                    component Memory_Init memory_Init;
                    component Zero_Init zero_Init;
                    connect memory_Init.out1 -> yOut1;
                    connect zero_Init.out1 -> memory_Init.in1;
                  }
                  <<Type="Terminator">> component Terminator {
                    ports
                      in Boolean in1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Type="SubSystem">> component RisingEdgeDetector {
                    ports
                      in Double in1In1,
                      out Boolean out1Out1;
                    <<Type="SubSystem">> component CompareToZero {
                      ports
                        in Double uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero1 {
                      ports
                        in Double uIn1,
                        out Boolean yOut1;
                      <<Operator=">",Type="RelationalOperator">> component Compare {
                        ports
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero2 {
                      ports
                        in Double uIn1,
                        out Boolean yOut1;
                      <<Operator="<",Type="RelationalOperator">> component Compare {
                        ports
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero3 {
                      ports
                        in Double uIn1,
                        out Boolean yOut1;
                      <<Operator=">=",Type="RelationalOperator">> component Compare {
                        ports
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero4 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="Constant",Value="1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="0">> component Constant1 {
                      ports
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory1 {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                    }
                    <<Condition="u2 > 0",Type="Condition">> component Condition {
                      ports
                        in Double in1,
                        out Boolean out1;
                    }
                    component CompareToZero compareToZero;
                    component CompareToZero1 compareToZero1;
                    component CompareToZero2 compareToZero2;
                    component CompareToZero3 compareToZero3;
                    component CompareToZero4 compareToZero4;
                    component Constant constant;
                    component Constant1 constant1;
                    component LogicalOperator logicalOperator;
                    component LogicalOperator1 logicalOperator1;
                    component LogicalOperator2 logicalOperator2;
                    component Memory memory;
                    component Memory1 memory1;
                    component Memory2 memory2;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    connect switchBlock.out1 -> out1Out1;
                    connect compareToZero4.yOut1 -> logicalOperator.in3;
                    connect memory2.out1 -> compareToZero4.uIn1;
                    connect logicalOperator.out1 -> logicalOperator2.in1;
                    connect constant1.out1 -> switchBlock.elseIn;
                    connect constant.out1 -> memory1.in1;
                    connect condition.out1 -> switchBlock.condition;
                    connect logicalOperator2.out1 -> switchBlock.ifIn;
                    connect logicalOperator2.out1 -> memory2.in1;
                    connect logicalOperator1.out1 -> logicalOperator2.in2;
                    connect compareToZero3.yOut1 -> logicalOperator1.in2;
                    connect compareToZero2.yOut1 -> logicalOperator1.in1;
                    connect compareToZero1.yOut1 -> logicalOperator.in2;
                    connect compareToZero.yOut1 -> logicalOperator.in1;
                    connect memory.out1 -> compareToZero.uIn1;
                    connect memory.out1 -> compareToZero2.uIn1;
                    connect in1In1 -> memory.in1;
                    connect in1In1 -> compareToZero1.uIn1;
                    connect in1In1 -> compareToZero3.uIn1;
                    connect memory1.out1 -> condition.in1;
                  }
                  <<Type="SubSystem">> component RisingEdgeDetector1 {
                    ports
                      in Boolean in1In1,
                      out Boolean out1Out1;
                    <<Type="SubSystem">> component CompareToZero {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero1 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero2 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="<",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero3 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">=",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero4 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="Constant",Value="1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="0">> component Constant1 {
                      ports
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory1 {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                    }
                    <<Condition="u2 > 0",Type="Condition">> component Condition {
                      ports
                        in Double in1,
                        out Boolean out1;
                    }
                    component CompareToZero compareToZero;
                    component CompareToZero1 compareToZero1;
                    component CompareToZero2 compareToZero2;
                    component CompareToZero3 compareToZero3;
                    component CompareToZero4 compareToZero4;
                    component Constant constant;
                    component Constant1 constant1;
                    component LogicalOperator logicalOperator;
                    component LogicalOperator1 logicalOperator1;
                    component LogicalOperator2 logicalOperator2;
                    component Memory memory;
                    component Memory1 memory1;
                    component Memory2 memory2;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    connect switchBlock.out1 -> out1Out1;
                    connect compareToZero4.yOut1 -> logicalOperator.in3;
                    connect memory2.out1 -> compareToZero4.uIn1;
                    connect logicalOperator.out1 -> logicalOperator2.in1;
                    connect constant1.out1 -> switchBlock.elseIn;
                    connect constant.out1 -> memory1.in1;
                    connect condition.out1 -> switchBlock.condition;
                    connect logicalOperator2.out1 -> switchBlock.ifIn;
                    connect logicalOperator2.out1 -> memory2.in1;
                    connect logicalOperator1.out1 -> logicalOperator2.in2;
                    connect compareToZero3.yOut1 -> logicalOperator1.in2;
                    connect compareToZero2.yOut1 -> logicalOperator1.in1;
                    connect compareToZero1.yOut1 -> logicalOperator.in2;
                    connect compareToZero.yOut1 -> logicalOperator.in1;
                    connect memory.out1 -> compareToZero.uIn1;
                    connect memory.out1 -> compareToZero2.uIn1;
                    connect in1In1 -> memory.in1;
                    connect in1In1 -> compareToZero1.uIn1;
                    connect in1In1 -> compareToZero3.uIn1;
                    connect memory1.out1 -> condition.in1;
                  }
                  <<Type="SubSystem">> component RisingEdgeDetector2 {
                    ports
                      in Boolean in1In1,
                      out Boolean out1Out1;
                    <<Type="SubSystem">> component CompareToZero {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero1 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero2 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="<",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero3 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator=">=",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="SubSystem">> component CompareToZero4 {
                      ports
                        in Boolean uIn1,
                        out Boolean yOut1;
                      <<Operator="==",Type="RelationalOperator">> component Compare {
                        ports
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                      }
                      <<Type="Constant",Value="0">> component Constant {
                        ports
                          out Double out1;
                      }
                      component Compare compare;
                      component Constant constant;
                      connect compare.out1 -> yOut1;
                      connect uIn1 -> compare.in1;
                      connect constant.out1 -> compare.in2;
                    }
                    <<Type="Constant",Value="1">> component Constant {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="0">> component Constant1 {
                      ports
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogicalOperator1 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Operator="OR",Type="Logic">> component LogicalOperator2 {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Memory">> component Memory1 {
                      ports
                        in Double in1,
                        out Double out1;
                    }
                    <<Type="Memory">> component Memory2 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                    }
                    <<Condition="u2 > 0",Type="Condition">> component Condition {
                      ports
                        in Double in1,
                        out Boolean out1;
                    }
                    component CompareToZero compareToZero;
                    component CompareToZero1 compareToZero1;
                    component CompareToZero2 compareToZero2;
                    component CompareToZero3 compareToZero3;
                    component CompareToZero4 compareToZero4;
                    component Constant constant;
                    component Constant1 constant1;
                    component LogicalOperator logicalOperator;
                    component LogicalOperator1 logicalOperator1;
                    component LogicalOperator2 logicalOperator2;
                    component Memory memory;
                    component Memory1 memory1;
                    component Memory2 memory2;
                    component SwitchBlock switchBlock;
                    component Condition condition;
                    connect switchBlock.out1 -> out1Out1;
                    connect compareToZero4.yOut1 -> logicalOperator.in3;
                    connect memory2.out1 -> compareToZero4.uIn1;
                    connect logicalOperator.out1 -> logicalOperator2.in1;
                    connect constant1.out1 -> switchBlock.elseIn;
                    connect constant.out1 -> memory1.in1;
                    connect condition.out1 -> switchBlock.condition;
                    connect logicalOperator2.out1 -> switchBlock.ifIn;
                    connect logicalOperator2.out1 -> memory2.in1;
                    connect logicalOperator1.out1 -> logicalOperator2.in2;
                    connect compareToZero3.yOut1 -> logicalOperator1.in2;
                    connect compareToZero2.yOut1 -> logicalOperator1.in1;
                    connect compareToZero1.yOut1 -> logicalOperator.in2;
                    connect compareToZero.yOut1 -> logicalOperator.in1;
                    connect memory.out1 -> compareToZero.uIn1;
                    connect memory.out1 -> compareToZero2.uIn1;
                    connect in1In1 -> memory.in1;
                    connect in1In1 -> compareToZero1.uIn1;
                    connect in1In1 -> compareToZero3.uIn1;
                    connect memory1.out1 -> condition.in1;
                  }
                  <<Type="Switch">> component SwitchBlock {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock1 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock2 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock3 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition3 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock4 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition4 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock5 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition5 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock6 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition6 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock7 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition7 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock8 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition8 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock9 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition9 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock10 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1>0",Type="Condition">> component Condition10 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component CC_ChangeSetValue cC_ChangeSetValue;
                  component CC_InitialSetValue cC_InitialSetValue;
                  component CC_OnSet_SetValue cC_OnSet_SetValue;
                  component CC_StartUpSetValue cC_StartUpSetValue;
                  component CompareToZero compareToZero;
                  component CompareToZero1 compareToZero1;
                  component Constant constant;
                  component EdgeRising edgeRising;
                  component FalseBlock falseBlock;
                  component LogicalOperator logicalOperator;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator2 logicalOperator2;
                  component LogicalOperator3 logicalOperator3;
                  component LogicalOperator4 logicalOperator4;
                  component LogicalOperator5 logicalOperator5;
                  component LogicalOperator6 logicalOperator6;
                  component RSFlipFlop rSFlipFlop;
                  component RelOp relOp;
                  component SysInit sysInit;
                  component Terminator terminator;
                  component VERSION_INFO vERSION_INFO;
                  component RisingEdgeDetector risingEdgeDetector;
                  component RisingEdgeDetector1 risingEdgeDetector1;
                  component RisingEdgeDetector2 risingEdgeDetector2;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  component SwitchBlock1 switchBlock1;
                  component Condition1 condition1;
                  component SwitchBlock2 switchBlock2;
                  component Condition2 condition2;
                  component SwitchBlock3 switchBlock3;
                  component Condition3 condition3;
                  component SwitchBlock4 switchBlock4;
                  component Condition4 condition4;
                  component SwitchBlock5 switchBlock5;
                  component Condition5 condition5;
                  component SwitchBlock6 switchBlock6;
                  component Condition6 condition6;
                  component SwitchBlock7 switchBlock7;
                  component Condition7 condition7;
                  component SwitchBlock8 switchBlock8;
                  component Condition8 condition8;
                  component SwitchBlock9 switchBlock9;
                  component Condition9 condition9;
                  component SwitchBlock10 switchBlock10;
                  component Condition10 condition10;
                  connect sysInit.yOut1 -> risingEdgeDetector.in1In1;
                  connect logicalOperator2.out1 -> risingEdgeDetector1.in1In1;
                  connect logicalOperator.out1 -> risingEdgeDetector2.in1In1;
                  connect in1 -> cC_ChangeSetValue.in1;
                  connect in2 -> cC_ChangeSetValue.in2;
                  connect in3 -> cC_ChangeSetValue.in3;
                  connect cC_enabled_bIn2 -> condition.in1;
                  connect cC_ChangeSetValue.out1 -> switchBlock.ifIn;
                  connect condition.out1 -> switchBlock.condition;
                  connect in4 -> switchBlock.elseIn;
                  connect switchBlock.out1 -> cC_InitialSetValue.in1;
                  connect risingEdgeDetector.out1Out1 -> condition1.in1;
                  connect cC_StartUpSetValue.out1 -> switchBlock1.ifIn;
                  connect condition1.out1 -> switchBlock1.condition;
                  connect risingEdgeDetector1.out1Out1 -> condition2.in1;
                  connect cC_OnSet_SetValue.out1 -> switchBlock2.ifIn;
                  connect condition2.out1 -> switchBlock2.condition;
                  connect in5 -> cC_ChangeSetValue.in4;
                  connect cC_enabled_bIn2 -> condition3.in1;
                  connect cC_ChangeSetValue.out2 -> switchBlock3.ifIn;
                  connect condition3.out1 -> switchBlock3.condition;
                  connect in6 -> switchBlock3.elseIn;
                  connect switchBlock3.out1 -> cC_InitialSetValue.in2;
                  connect risingEdgeDetector2.out1Out1 -> condition4.in1;
                  connect cC_InitialSetValue.out1 -> switchBlock4.ifIn;
                  connect condition4.out1 -> switchBlock4.condition;
                  connect in7 -> cC_ChangeSetValue.in5;
                  connect cC_enabled_bIn2 -> condition5.in1;
                  connect cC_ChangeSetValue.out3 -> switchBlock5.ifIn;
                  connect condition5.out1 -> switchBlock5.condition;
                  connect in8 -> switchBlock5.elseIn;
                  connect switchBlock5.out1 -> switchBlock4.elseIn;
                  connect switchBlock4.out1 -> switchBlock2.elseIn;
                  connect switchBlock2.out1 -> switchBlock1.elseIn;
                  connect switchBlock1.out1 -> out1;
                  connect risingEdgeDetector.out1Out1 -> condition6.in1;
                  connect cC_StartUpSetValue.out2 -> switchBlock6.ifIn;
                  connect condition6.out1 -> switchBlock6.condition;
                  connect risingEdgeDetector1.out1Out1 -> condition7.in1;
                  connect cC_OnSet_SetValue.out2 -> switchBlock7.ifIn;
                  connect condition7.out1 -> switchBlock7.condition;
                  connect in9 -> cC_ChangeSetValue.in6;
                  connect cC_enabled_bIn2 -> condition8.in1;
                  connect cC_ChangeSetValue.out4 -> switchBlock8.ifIn;
                  connect condition8.out1 -> switchBlock8.condition;
                  connect in10 -> switchBlock8.elseIn;
                  connect switchBlock8.out1 -> cC_InitialSetValue.in3;
                  connect risingEdgeDetector2.out1Out1 -> condition9.in1;
                  connect cC_InitialSetValue.out2 -> switchBlock9.ifIn;
                  connect condition9.out1 -> switchBlock9.condition;
                  connect in11 -> cC_ChangeSetValue.in7;
                  connect cC_enabled_bIn2 -> condition10.in1;
                  connect cC_ChangeSetValue.out5 -> switchBlock10.ifIn;
                  connect condition10.out1 -> switchBlock10.condition;
                  connect in12 -> switchBlock10.elseIn;
                  connect switchBlock10.out1 -> switchBlock9.elseIn;
                  connect switchBlock9.out1 -> switchBlock7.elseIn;
                  connect switchBlock7.out1 -> switchBlock6.elseIn;
                  connect switchBlock6.out1 -> out2;
                  connect compareToZero.yOut1 -> logicalOperator3.in1;
                  connect compareToZero1.yOut1 -> logicalOperator3.in2;
                  connect leverDown_bIn6 -> cC_ChangeSetValue.cCDown_bIn2;
                  connect leverDown_bIn6 -> compareToZero1.uIn1;
                  connect leverUp_bIn5 -> cC_ChangeSetValue.cCUp_bIn1;
                  connect leverUp_bIn5 -> compareToZero.uIn1;
                  connect rSFlipFlop.qOut1 -> logicalOperator2.in3;
                  connect rSFlipFlop.nOT_QOut2 -> terminator.in1;
                  connect logicalOperator6.out1 -> rSFlipFlop.rIn2;
                  connect logicalOperator5.out1 -> logicalOperator2.in1;
                  connect limiter_bIn1 -> logicalOperator5.in1;
                  connect edgeRising.yOut1 -> rSFlipFlop.sIn1;
                  connect logicalOperator4.out1 -> logicalOperator2.in2;
                  connect logicalOperator1.out1 -> edgeRising.rIn2;
                  connect falseBlock.yOut1 -> edgeRising.iVIn3;
                  connect logicalOperator3.out1 -> edgeRising.uIn1;
                  connect cC_enabled_bIn2 -> logicalOperator6.in1;
                  connect cC_enabled_bIn2 -> logicalOperator1.in1;
                  connect cruiseControl_bIn3 -> logicalOperator.in1;
                  connect cruiseControl_bIn3 -> logicalOperator4.in1;
                  connect relOp.out1 -> logicalOperator.in2;
                  connect constant.out1 -> relOp.in2;
                  connect v_Vehicle_kmhIn4 -> cC_InitialSetValue.v_Vehicle_kmhIn1;
                  connect v_Vehicle_kmhIn4 -> cC_OnSet_SetValue.v_Vehicle_kmhIn1;
                  connect v_Vehicle_kmhIn4 -> relOp.in1;
                }
                <<Type="SubSystem">> component CC_enabled {
                  ports
                    in Boolean cC_active_bIn1,
                    in Double v_Vehicle_kmhIn2,
                    out Double v_CC_delta_kmhOut1,
                    in Double in1;
                  <<Type="SubSystem">> component Tempomat_Active {
                    ports
                      in Double v_Vehicle_kmhIn1,
                      out Double v_CC_delta_kmhOut1,
                      in Double in1;
                    <<Type="Sum",ListOfSigns="-+">> component Sum {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double v_CC_delta_kmhOut1;
                      effect in1 -> v_CC_delta_kmhOut1;
                      effect in2 -> v_CC_delta_kmhOut1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Sum sum;
                    component VERSION_INFO vERSION_INFO;
                    connect in1 -> sum.in2;
                    connect sum.v_CC_delta_kmhOut1 -> v_CC_delta_kmhOut1;
                    connect v_Vehicle_kmhIn1 -> sum.in1;
                  }
                  <<Type="SubSystem">> component Tempomat_Deactive {
                    ports
                      out Double v_CC_delta_kmhOut1;
                    <<Type="Constant",Value="0">> component Constant {
                      ports
                        out Double v_CC_delta_kmhOut1;
                    }
                    <<Type="SubSystem">> component VERSION_INFO {
                      <<Type="SubSystem">> component Copyright {
                      }
                      component Copyright copyright;
                    }
                    component Constant constant;
                    component VERSION_INFO vERSION_INFO;
                    connect constant.v_CC_delta_kmhOut1 -> v_CC_delta_kmhOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Type="Switch">> component SwitchBlock {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="u1",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="Switch">> component SwitchBlock1 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                  }
                  <<Condition="!(u1)",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                    ports
                      in Double valueIn,
                      out Double valueOut;
                  }
                  component Tempomat_Active tempomat_Active;
                  component Tempomat_Deactive tempomat_Deactive;
                  component VERSION_INFO vERSION_INFO;
                  component SwitchBlock switchBlock;
                  component Condition condition;
                  component SwitchBlock1 switchBlock1;
                  component Condition1 condition1;
                  component UnitDelay unitDelay;
                  connect in1 -> tempomat_Active.in1;
                  connect cC_active_bIn1 -> condition.in1;
                  connect condition.out1 -> switchBlock.condition;
                  connect tempomat_Active.v_CC_delta_kmhOut1 -> switchBlock.ifIn;
                  connect cC_active_bIn1 -> condition1.in1;
                  connect condition1.out1 -> switchBlock1.condition;
                  connect tempomat_Deactive.v_CC_delta_kmhOut1 -> switchBlock1.ifIn;
                  connect switchBlock1.out1 -> switchBlock.elseIn;
                  connect switchBlock.out1 -> unitDelay.valueIn;
                  connect unitDelay.valueOut -> switchBlock1.elseIn;
                  connect switchBlock.out1 -> v_CC_delta_kmhOut1;
                  connect v_Vehicle_kmhIn2 -> tempomat_Active.v_Vehicle_kmhIn1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                component CC_On_Off cC_On_Off;
                component CC_SetValue cC_SetValue;
                component CC_enabled cC_enabled;
                component VERSION_INFO vERSION_INFO;
                connect in1 -> cC_SetValue.in1;
                connect in2 -> cC_SetValue.in2;
                connect in3 -> cC_SetValue.in3;
                connect in4 -> cC_SetValue.in4;
                connect in5 -> cC_SetValue.in5;
                connect in6 -> cC_SetValue.in6;
                connect in7 -> cC_SetValue.in7;
                connect in8 -> cC_SetValue.in8;
                connect cC_SetValue.out1 -> cC_enabled.in1;
                connect in9 -> cC_SetValue.in9;
                connect in10 -> cC_SetValue.in10;
                connect in11 -> cC_SetValue.in11;
                connect in12 -> cC_SetValue.in12;
                connect cC_SetValue.out2 -> out1;
                connect limiter_bIn5 -> cC_On_Off.limiter_bIn5;
                connect limiter_bIn5 -> cC_SetValue.limiter_bIn1;
                connect cC_On_Off.cC_active_bOut1 -> cC_enabled.cC_active_bIn1;
                connect cC_On_Off.cC_active_bOut1 -> cC_SetValue.cC_enabled_bIn2;
                connect cC_On_Off.cC_active_bOut1 -> cC_active_bOut2;
                connect brakeForce_pedal_pcIn2 -> cC_On_Off.brakeForce_pedal_pcIn2;
                connect parkingBrake_bIn1 -> cC_On_Off.parkingBrake_bIn1;
                connect cruiseControl_bIn3 -> cC_SetValue.cruiseControl_bIn3;
                connect cruiseControl_bIn3 -> cC_On_Off.cruiseControl_bIn3;
                connect v_Vehicle_kmhIn4 -> cC_On_Off.v_Vehicle_kmhIn4;
                connect v_Vehicle_kmhIn4 -> cC_enabled.v_Vehicle_kmhIn2;
                connect v_Vehicle_kmhIn4 -> cC_SetValue.v_Vehicle_kmhIn4;
                connect cC_enabled.v_CC_delta_kmhOut1 -> v_CC_delta_kmhOut1;
                connect leverUp_bIn6 -> cC_SetValue.leverUp_bIn5;
                connect leverUp_bIn6 -> cC_On_Off.leverUp_bIn6;
                connect leverDown_bIn7 -> cC_SetValue.leverDown_bIn6;
                connect leverDown_bIn7 -> cC_On_Off.leverDown_bIn7;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Type="Switch">> component SwitchBlock {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition1 {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Condition="u1>0",Type="Condition">> component Condition2 {
                ports
                  in Double in1,
                  out Boolean out1;
              }
              <<Type="Switch">> component SwitchBlock2 {
                ports
                  in Boolean ifIn,
                  in Boolean condition,
                  in Boolean elseIn,
                  out Boolean out1;
              }
              <<Type="Constant",Value="0">> component Constant2 {
                ports
                  out Boolean out1;
              }
              component Constant constant;
              component Tempomat_Function tempomat_Function;
              component VERSION_INFO vERSION_INFO;
              component SwitchBlock switchBlock;
              component Condition condition;
              component Condition1 condition1;
              component SwitchBlock1 switchBlock1;
              component Constant1 constant1;
              component Condition2 condition2;
              component SwitchBlock2 switchBlock2;
              component Constant2 constant2;
              connect in1 -> tempomat_Function.in1;
              connect in2 -> tempomat_Function.in2;
              connect in3 -> tempomat_Function.in3;
              connect in4 -> tempomat_Function.in4;
              connect in5 -> tempomat_Function.in5;
              connect in6 -> tempomat_Function.in6;
              connect in7 -> tempomat_Function.in7;
              connect in8 -> tempomat_Function.in8;
              connect in9 -> tempomat_Function.in9;
              connect in10 -> tempomat_Function.in10;
              connect in11 -> tempomat_Function.in11;
              connect in12 -> tempomat_Function.in12;
              connect constant.out1 -> condition.in1;
              connect tempomat_Function.out1 -> switchBlock.ifIn;
              connect condition.out1 -> switchBlock.condition;
              connect in13 -> switchBlock.elseIn;
              connect switchBlock.out1 -> out1;
              connect constant.out1 -> condition1.in1;
              connect condition1.out1 -> switchBlock1.condition;
              connect tempomat_Function.v_CC_delta_kmhOut1 -> switchBlock1.ifIn;
              connect constant1.out1 -> switchBlock1.elseIn;
              connect constant.out1 -> condition2.in1;
              connect condition2.out1 -> switchBlock2.condition;
              connect tempomat_Function.cC_active_bOut2 -> switchBlock2.ifIn;
              connect constant2.out1 -> switchBlock2.elseIn;
              connect switchBlock2.out1 -> cC_active_bOut2;
              connect switchBlock1.out1 -> v_CC_delta_kmhOut1;
              connect leverDown_bIn7 -> tempomat_Function.leverDown_bIn7;
              connect leverUp_bIn6 -> tempomat_Function.leverUp_bIn6;
              connect limiter_bIn5 -> tempomat_Function.limiter_bIn5;
              connect v_Vehicle_kmhIn4 -> tempomat_Function.v_Vehicle_kmhIn4;
              connect cruiseControl_bIn3 -> tempomat_Function.cruiseControl_bIn3;
              connect brakeForce_pedal_pcIn2 -> tempomat_Function.brakeForce_pedal_pcIn2;
              connect parkingBrake_bIn1 -> tempomat_Function.parkingBrake_bIn1;
            }
            <<Type="SubSystem">> component VERSION_INFO {
              <<Type="SubSystem">> component Copyright {
              }
              component Copyright copyright;
            }
            <<Type="SubSystem">> component VelocityControl {
              ports
                in Double v_CC_delta_kmhIn1,
                in Double vMax_kmhIn2,
                in Double acceleration_pedal_pcIn3,
                in Double v_Vehicle_kmhIn4,
                out Double acceleration_pcOut1;
              <<Type="Constant",Value="0">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant2 {
                ports
                  out Double out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<BreakpointsForDimension1="[0 1 5 10]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[0 20 60 100]",BreakpointsSpecification="Explicit values">> component LookUpTable {
                ports
                  in Double in1,
                  out Double out1;
                effect in1 -> out1;
              }
              <<Function="max",Type="MinMax">> component MinMax1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Double out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">",Type="RelationalOperator">> component RelOp {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">",Type="RelationalOperator">> component RelOp1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Type="Saturate",LowerLimit="0",UpperLimit="10">> component Saturation {
                ports
                  in Double in1,
                  out Double out1;
                effect in1 -> out1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double acceleration_pcOut1;
                effect ifIn -> acceleration_pcOut1;
                effect condition -> acceleration_pcOut1;
                effect elseIn -> acceleration_pcOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              component Constant1 constant1;
              component Constant2 constant2;
              component LogOp logOp;
              component LookUpTable lookUpTable;
              component MinMax1 minMax1;
              component RelOp relOp;
              component RelOp1 relOp1;
              component Saturation saturation;
              component SwitchBlock switchBlock;
              component VERSION_INFO vERSION_INFO;
              component Condition condition;
              connect condition.out1 -> switchBlock.condition;
              connect logOp.out1 -> condition.in1;
              connect saturation.out1 -> lookUpTable.in1;
              connect constant2.out1 -> relOp1.in2;
              connect minMax1.out1 -> switchBlock.elseIn;
              connect switchBlock.acceleration_pcOut1 -> acceleration_pcOut1;
              connect acceleration_pedal_pcIn3 -> minMax1.in1;
              connect vMax_kmhIn2 -> relOp.in2;
              connect vMax_kmhIn2 -> relOp1.in1;
              connect v_Vehicle_kmhIn4 -> relOp.in1;
              connect lookUpTable.out1 -> minMax1.in2;
              connect v_CC_delta_kmhIn1 -> saturation.in1;
              connect relOp.out1 -> logOp.in1;
              connect relOp1.out1 -> logOp.in2;
              connect constant1.out1 -> switchBlock.ifIn;
            }
            <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
              ports
                in Double valueIn,
                out Double valueOut;
            }
            <<Type="UnitDelay",InitialCondition="-1">> component UnitDelay1 {
              ports
                in Double valueIn,
                out Double valueOut;
            }
            component FAS_Input fAS_Input;
            component Limiter limiter;
            component Tempomat tempomat;
            component VERSION_INFO vERSION_INFO;
            component VelocityControl velocityControl;
            component UnitDelay unitDelay;
            component UnitDelay1 unitDelay1;
            connect fAS_Input.limiter_b -> tempomat.limiter_bIn5;
            connect limiter.vMax_kmhOut1 -> velocityControl.vMax_kmhIn2;
            connect tempomat.v_CC_delta_kmhOut1 -> velocityControl.v_CC_delta_kmhIn1;
            connect fAS_Input.acceleration_pedal_pc3 -> limiter.acceleration_pedal_pcIn2;
            connect fAS_Input.acceleration_pedal_pc -> velocityControl.acceleration_pedal_pcIn3;
            connect fAS_Input.v_Vehicle_kmh1 -> velocityControl.v_Vehicle_kmhIn4;
            connect fAS_Input.leverUp_b3 -> limiter.leverUp_bIn4;
            connect fAS_Input.v_Vehicle_kmh5 -> limiter.v_Vehicle_kmhIn3;
            connect fAS_Input.limiter_b3 -> limiter.limiter_bIn1;
            connect fAS_Input.leverDown_b3 -> limiter.leverDown_bIn5;
            connect fAS_Input.cruiseControl_b -> tempomat.cruiseControl_bIn3;
            connect fAS_Input.v_Vehicle_kmh -> tempomat.v_Vehicle_kmhIn4;
            connect fAS_Input.leverDown_b -> tempomat.leverDown_bIn7;
            connect fAS_Input.leverUp_b -> tempomat.leverUp_bIn6;
            connect fAS_Input.brakeForce_pedal_pc -> tempomat.brakeForce_pedal_pcIn2;
            connect fAS_Input.parkingBrake_b -> tempomat.parkingBrake_bIn1;
            connect unitDelay.valueOut -> limiter.in1;
            connect unitDelay.valueOut -> limiter.in2;
            connect unitDelay.valueOut -> limiter.in3;
            connect unitDelay.valueOut -> limiter.in4;
            connect unitDelay.valueOut -> limiter.in5;
            connect unitDelay.valueOut -> limiter.in6;
            connect unitDelay.valueOut -> limiter.in7;
            connect limiter.out1 -> unitDelay.valueIn;
            connect unitDelay1.valueOut -> tempomat.in1;
            connect unitDelay1.valueOut -> tempomat.in2;
            connect unitDelay1.valueOut -> tempomat.in3;
            connect unitDelay1.valueOut -> tempomat.in4;
            connect unitDelay1.valueOut -> tempomat.in5;
            connect unitDelay1.valueOut -> tempomat.in6;
            connect unitDelay1.valueOut -> tempomat.in7;
            connect unitDelay1.valueOut -> tempomat.in8;
            connect unitDelay1.valueOut -> tempomat.in9;
            connect unitDelay1.valueOut -> tempomat.in10;
            connect unitDelay1.valueOut -> tempomat.in11;
            connect unitDelay1.valueOut -> tempomat.in12;
            connect unitDelay1.valueOut -> tempomat.in13;
            connect tempomat.out1 -> unitDelay1.valueIn;
            connect tempomat.cC_active_bOut2 -> cC_active_b;
            connect velocityControl.acceleration_pcOut1 -> acceleration_pc;
            connect unitDelay1.valueOut -> cCSetValue_kmh;
            connect unitDelay.valueOut -> limiterSetValue_kmh;
            connect limiter.limiter_active_bOut2 -> limiter_active_b;
            connect parkingBrake_b -> fAS_Input.parkingBrake_b1;
            connect parkingBrake_b1 -> fAS_Input.parkingBrake_b2;
            connect brakeForce_pedal_pc -> fAS_Input.brakeForce_pedal_pc1;
            connect brakeForce_pedal_pc1 -> fAS_Input.brakeForce_pedal_pc2;
            connect acceleration_pedal_pc -> fAS_Input.acceleration_pedal_pc1;
            connect acceleration_pedal_pc1 -> fAS_Input.acceleration_pedal_pc2;
            connect cruiseControl_b -> fAS_Input.cruiseControl_b1;
            connect cruiseControl_b1 -> fAS_Input.cruiseControl_b2;
            connect limiter_b -> fAS_Input.limiter_b1;
            connect limiter_b1 -> fAS_Input.limiter_b2;
            connect leverUp_b -> fAS_Input.leverUp_b1;
            connect leverUp_b1 -> fAS_Input.leverUp_b2;
            connect leverDown_b -> fAS_Input.leverDown_b1;
            connect leverDown_b1 -> fAS_Input.leverDown_b2;
            connect v_Vehicle_kmh -> fAS_Input.v_Vehicle_kmh2;
            connect v_Vehicle_kmh1 -> fAS_Input.v_Vehicle_kmh3;
            connect v_Vehicle_kmh2 -> fAS_Input.v_Vehicle_kmh4;
          }
          <<Type="SubSystem">> component DEMO_FAS_Input {
            ports
              in Boolean parkingBrake_bIn1,
              in Double brakeForce_pedal_pcIn2,
              in Double _Acceleration_pedal_pcIn3,
              in Boolean cruiseControl_bIn4,
              in Boolean limiter_bIn5,
              in Boolean leverUp_bIn6,
              in Boolean leverDown_bIn7,
              in Double v_Vehicle_bIn8,
              out Boolean parkingBrake_b,
              out Boolean parkingBrake_b1,
              out Double brakeForce_pedal_pc,
              out Double brakeForce_pedal_pc1,
              out Double acceleration_pedal_pc,
              out Double acceleration_pedal_pc1,
              out Boolean cruiseControl_b,
              out Boolean cruiseControl_b1,
              out Boolean limiter_b,
              out Boolean limiter_b1,
              out Boolean leverUp_b,
              out Boolean leverUp_b1,
              out Boolean leverDown_b,
              out Boolean leverDown_b1,
              out Double v_Vehicle_kmh,
              out Double v_Vehicle_kmh1,
              out Double v_Vehicle_kmh2;
            connect parkingBrake_bIn1 -> parkingBrake_b;
            connect parkingBrake_bIn1 -> parkingBrake_b1;
            connect brakeForce_pedal_pcIn2 -> brakeForce_pedal_pc;
            connect brakeForce_pedal_pcIn2 -> brakeForce_pedal_pc1;
            connect _Acceleration_pedal_pcIn3 -> acceleration_pedal_pc;
            connect _Acceleration_pedal_pcIn3 -> acceleration_pedal_pc1;
            connect cruiseControl_bIn4 -> cruiseControl_b;
            connect cruiseControl_bIn4 -> cruiseControl_b1;
            connect limiter_bIn5 -> limiter_b;
            connect limiter_bIn5 -> limiter_b1;
            connect leverUp_bIn6 -> leverUp_b;
            connect leverUp_bIn6 -> leverUp_b1;
            connect leverDown_bIn7 -> leverDown_b;
            connect leverDown_bIn7 -> leverDown_b1;
            connect v_Vehicle_bIn8 -> v_Vehicle_kmh;
            connect v_Vehicle_bIn8 -> v_Vehicle_kmh1;
            connect v_Vehicle_bIn8 -> v_Vehicle_kmh2;
          }
          <<Type="SubSystem">> component DEMO_FAS_Output {
            ports
              out Boolean cC_active_bOut1,
              out Double acceleration_pcOut2,
              out Double cCSetValue_kmhOut3,
              out Double limiterSetValue_kmhOut4,
              out Boolean limiter_active_bOut5,
              in Boolean cC_active_b,
              in Double acceleration_pc,
              in Double cCSetValue_kmh,
              in Double limiterSetValue_kmh,
              in Boolean limiter_active_b;
            connect cC_active_b -> cC_active_bOut1;
            connect acceleration_pc -> acceleration_pcOut2;
            connect cCSetValue_kmh -> cCSetValue_kmhOut3;
            connect limiterSetValue_kmh -> limiterSetValue_kmhOut4;
            connect limiter_active_b -> limiter_active_bOut5;
          }
          component DEMO_FAS_Funktion dEMO_FAS_Funktion;
          component DEMO_FAS_Input dEMO_FAS_Input;
          component DEMO_FAS_Output dEMO_FAS_Output;
          connect dEMO_FAS_Funktion.cC_active_b -> dEMO_FAS_Output.cC_active_b;
          connect dEMO_FAS_Funktion.acceleration_pc -> dEMO_FAS_Output.acceleration_pc;
          connect dEMO_FAS_Funktion.cCSetValue_kmh -> dEMO_FAS_Output.cCSetValue_kmh;
          connect dEMO_FAS_Funktion.limiterSetValue_kmh -> dEMO_FAS_Output.limiterSetValue_kmh;
          connect dEMO_FAS_Funktion.limiter_active_b -> dEMO_FAS_Output.limiter_active_b;
          connect dEMO_FAS_Input.parkingBrake_b -> dEMO_FAS_Funktion.parkingBrake_b;
          connect dEMO_FAS_Input.parkingBrake_b1 -> dEMO_FAS_Funktion.parkingBrake_b1;
          connect dEMO_FAS_Input.brakeForce_pedal_pc -> dEMO_FAS_Funktion.brakeForce_pedal_pc;
          connect dEMO_FAS_Input.brakeForce_pedal_pc1 -> dEMO_FAS_Funktion.brakeForce_pedal_pc1;
          connect dEMO_FAS_Input.acceleration_pedal_pc -> dEMO_FAS_Funktion.acceleration_pedal_pc;
          connect dEMO_FAS_Input.acceleration_pedal_pc1 -> dEMO_FAS_Funktion.acceleration_pedal_pc1;
          connect dEMO_FAS_Input.cruiseControl_b -> dEMO_FAS_Funktion.cruiseControl_b;
          connect dEMO_FAS_Input.cruiseControl_b1 -> dEMO_FAS_Funktion.cruiseControl_b1;
          connect dEMO_FAS_Input.limiter_b -> dEMO_FAS_Funktion.limiter_b;
          connect dEMO_FAS_Input.limiter_b1 -> dEMO_FAS_Funktion.limiter_b1;
          connect dEMO_FAS_Input.leverUp_b -> dEMO_FAS_Funktion.leverUp_b;
          connect dEMO_FAS_Input.leverUp_b1 -> dEMO_FAS_Funktion.leverUp_b1;
          connect dEMO_FAS_Input.leverDown_b -> dEMO_FAS_Funktion.leverDown_b;
          connect dEMO_FAS_Input.leverDown_b1 -> dEMO_FAS_Funktion.leverDown_b1;
          connect dEMO_FAS_Input.v_Vehicle_kmh -> dEMO_FAS_Funktion.v_Vehicle_kmh;
          connect dEMO_FAS_Input.v_Vehicle_kmh1 -> dEMO_FAS_Funktion.v_Vehicle_kmh1;
          connect dEMO_FAS_Input.v_Vehicle_kmh2 -> dEMO_FAS_Funktion.v_Vehicle_kmh2;
          connect dEMO_FAS_Output.limiter_active_bOut5 -> _Limiter_active_bOut5;
          connect dEMO_FAS_Output.limiterSetValue_kmhOut4 -> _LimiterSetValue_kmhOut4;
          connect dEMO_FAS_Output.cCSetValue_kmhOut3 -> _CCSetValue_kmhOut3;
          connect dEMO_FAS_Output.cC_active_bOut1 -> _CC_active_bOut1;
          connect dEMO_FAS_Output.acceleration_pcOut2 -> _Acceleration_pcOut2;
          connect _BrakeForce_pedal_pcIn2 -> dEMO_FAS_Input.brakeForce_pedal_pcIn2;
          connect _ParkingBrake_bIn1 -> dEMO_FAS_Input.parkingBrake_bIn1;
          connect _CruiseControl_bIn4 -> dEMO_FAS_Input.cruiseControl_bIn4;
          connect _Acceleration_pedal_pcIn3 -> dEMO_FAS_Input._Acceleration_pedal_pcIn3;
          connect _Limiter_bIn5 -> dEMO_FAS_Input.limiter_bIn5;
          connect _LeverUp_bIn6 -> dEMO_FAS_Input.leverUp_bIn6;
          connect _LeverDown_bIn7 -> dEMO_FAS_Input.leverDown_bIn7;
          connect _V_Vehicle_kmhIn8 -> dEMO_FAS_Input.v_Vehicle_bIn8;
        }
        component DEMO_FAS dEMO_FAS;
        connect dEMO_FAS._Limiter_active_bOut5 -> _Limiter_active_bOut5;
        connect dEMO_FAS._LimiterSetValue_kmhOut4 -> _LimiterSetValue_kmhOut4;
        connect dEMO_FAS._CCSetValue_kmhOut3 -> _CCSetValue_kmhOut3;
        connect dEMO_FAS._Acceleration_pcOut2 -> _Acceleration_pcOut2;
        connect dEMO_FAS._CC_active_bOut1 -> _CC_active_bOut1;
        connect _V_Vehicle_kmhIn8 -> dEMO_FAS._V_Vehicle_kmhIn8;
        connect _LeverDown_bIn7 -> dEMO_FAS._LeverDown_bIn7;
        connect _LeverUp_bIn6 -> dEMO_FAS._LeverUp_bIn6;
        connect _Limiter_bIn5 -> dEMO_FAS._Limiter_bIn5;
        connect _CruiseControl_bIn4 -> dEMO_FAS._CruiseControl_bIn4;
        connect _Acceleration_pedal_pcIn3 -> dEMO_FAS._Acceleration_pedal_pcIn3;
        connect _BrakeForce_pedal_pcIn2 -> dEMO_FAS._BrakeForce_pedal_pcIn2;
        connect _ParkingBrake_bIn1 -> dEMO_FAS._ParkingBrake_bIn1;
      }
      component Subsystem subsystem;
      connect subsystem._Limiter_active_bOut5 -> _Limiter_active_bOut5;
      connect subsystem._LimiterSetValue_kmhOut4 -> _LimiterSetValue_kmhOut4;
      connect subsystem._CCSetValue_kmhOut3 -> _CCSetValue_kmhOut3;
      connect subsystem._Acceleration_pcOut2 -> _Acceleration_pcOut2;
      connect subsystem._CC_active_bOut1 -> _CC_active_bOut1;
      connect _V_Vehicle_kmhIn8 -> subsystem._V_Vehicle_kmhIn8;
      connect _LeverDown_bIn7 -> subsystem._LeverDown_bIn7;
      connect _LeverUp_bIn6 -> subsystem._LeverUp_bIn6;
      connect _Limiter_bIn5 -> subsystem._Limiter_bIn5;
      connect _CruiseControl_bIn4 -> subsystem._CruiseControl_bIn4;
      connect _Acceleration_pedal_pcIn3 -> subsystem._Acceleration_pedal_pcIn3;
      connect _BrakeForce_pedal_pcIn2 -> subsystem._BrakeForce_pedal_pcIn2;
      connect _ParkingBrake_bIn1 -> subsystem._ParkingBrake_bIn1;
    }
    component DEMO_FAS dEMO_FAS;
    connect v_Vehicle_kmh -> dEMO_FAS._V_Vehicle_kmhIn8;
    connect leverDown_b -> dEMO_FAS._LeverDown_bIn7;
    connect leverUp_b -> dEMO_FAS._LeverUp_bIn6;
    connect limiter_b -> dEMO_FAS._Limiter_bIn5;
    connect cruiseControl_b -> dEMO_FAS._CruiseControl_bIn4;
    connect acceleration_pedal_pc -> dEMO_FAS._Acceleration_pedal_pcIn3;
    connect brakeForce_pedal_pc -> dEMO_FAS._BrakeForce_pedal_pcIn2;
    connect parkingBrake_b -> dEMO_FAS._ParkingBrake_bIn1;
    connect dEMO_FAS._CC_active_bOut1 -> cC_active_b;
    connect dEMO_FAS._CCSetValue_kmhOut3 -> cCSetValue_kmh;
    connect dEMO_FAS._LimiterSetValue_kmhOut4 -> limiterSetValue_kmh;
    connect dEMO_FAS._Limiter_active_bOut5 -> limiter_active_b;
    connect dEMO_FAS._Acceleration_pcOut2 -> acceleration_pc;
  }
  <<Type="SubSystem">> component SimToRealTime {
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="1">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator1 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator2 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator3 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator4 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator5 {
      ports
        in Double in1;
    }
    <<Type="SubSystem">> component VAPS_TimeConfiguration {
      ports
        in Double timeConfigIn1,
        in Double maxSpeedIn2,
        out Double factor_SimTimeOut1,
        out Double timeConfig_ConsumerOut2,
        out Double timeConfig_ProducerOut3,
        out Double tsSimOut4,
        out Double tsRTimeOut5,
        out Double tsRTimeEffOut6;
      <<Type="Constant",Value="0">> component Constant1 {
        ports
          out Double out1;
      }
      <<Type="Constant",Value="2">> component Constant10 {
        ports
          out Double out1;
      }
      <<Type="Constant",Value="0">> component Constant4 {
        ports
          out Double out1;
      }
      <<Type="SubSystem">> component RealTimeTimerAdapter {
        ports
          in Double in1In1,
          out Double out1Out1;
        <<Type="Constant",Value="-10">> component Constant1 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant10 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant11 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="-1">> component Constant12 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant13 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1.1">> component Constant15 {
          ports
            out Double out1;
        }
        <<Type="Product",Inputs="*/">> component Div1 {
          ports
            in Double in1,
            in Double in2,
            out Double out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="AND",Type="Logic">> component LogOp1 {
          ports
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">",Type="RelationalOperator">> component RelOp2 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="<=",Type="RelationalOperator">> component RelOp3 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">=",Type="RelationalOperator">> component RelOp4 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Sum",ListOfSigns="+-">> component Sum {
          ports
            in Double in1,
            in Double in2,
            out Double out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        component Constant1 constant1;
        component Constant10 constant10;
        component Constant11 constant11;
        component Constant12 constant12;
        component Constant13 constant13;
        component Constant15 constant15;
        component Div1 div1;
        component LogOp1 logOp1;
        component RelOp2 relOp2;
        component RelOp3 relOp3;
        component RelOp4 relOp4;
        component Sum sum;
        component SwitchBlock2 switchBlock2;
        component SwitchBlock3 switchBlock3;
        component Condition condition;
        component Condition1 condition1;
        connect condition1.out1 -> switchBlock2.condition;
        connect condition.out1 -> switchBlock3.condition;
        connect logOp1.out1 -> condition.in1;
        connect relOp2.out1 -> condition1.in1;
        connect in1In1 -> relOp2.in1;
        connect in1In1 -> switchBlock2.ifIn;
        connect in1In1 -> relOp3.in1;
        connect in1In1 -> relOp4.in1;
        connect in1In1 -> div1.in1;
        connect constant10.out1 -> relOp2.in2;
        connect switchBlock2.out1 -> out1Out1;
        connect constant11.out1 -> relOp3.in2;
        connect switchBlock3.out1 -> switchBlock2.elseIn;
        connect constant12.out1 -> relOp4.in2;
        connect relOp3.out1 -> logOp1.in1;
        connect relOp4.out1 -> logOp1.in2;
        connect constant13.out1 -> switchBlock3.ifIn;
        connect sum.out1 -> switchBlock3.elseIn;
        connect constant1.out1 -> div1.in2;
        connect constant15.out1 -> sum.in1;
        connect div1.out1 -> sum.in2;
      }
      <<Type="SubSystem">> component RealTimeTimerAdapter_Value {
        ports
          in Double in1In1,
          out Double out1Out1;
        <<Type="Constant",Value="1">> component Constant11 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="-1">> component Constant12 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="0">> component Constant13 {
          ports
            out Double out1;
        }
        <<Operator="AND",Type="Logic">> component LogOp1 {
          ports
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="<=",Type="RelationalOperator">> component RelOp3 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">=",Type="RelationalOperator">> component RelOp4 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        component Constant11 constant11;
        component Constant12 constant12;
        component Constant13 constant13;
        component LogOp1 logOp1;
        component RelOp3 relOp3;
        component RelOp4 relOp4;
        component SwitchBlock3 switchBlock3;
        component Condition condition;
        connect condition.out1 -> switchBlock3.condition;
        connect logOp1.out1 -> condition.in1;
        connect constant13.out1 -> switchBlock3.ifIn;
        connect relOp4.out1 -> logOp1.in2;
        connect relOp3.out1 -> logOp1.in1;
        connect constant12.out1 -> relOp4.in2;
        connect constant11.out1 -> relOp3.in2;
        connect switchBlock3.out1 -> out1Out1;
        connect in1In1 -> switchBlock3.elseIn;
        connect in1In1 -> relOp4.in1;
        connect in1In1 -> relOp3.in1;
      }
      <<Operator="==",Type="RelationalOperator">> component RelOp4 {
        ports
          in Double in1,
          in Double in2,
          out Boolean out1;
        effect in1 -> out1;
        effect in2 -> out1;
      }
      <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
        ports
          in Double ifIn,
          in Boolean condition,
          in Double elseIn,
          out Double out1;
        effect ifIn -> out1;
        effect condition -> out1;
        effect elseIn -> out1;
      }
      <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
        ports
          in Double ifIn,
          in Boolean condition,
          in Double elseIn,
          out Double out1;
        effect ifIn -> out1;
        effect condition -> out1;
        effect elseIn -> out1;
      }
      <<Type="SubSystem">> component SysInit1 {
        ports
          out Double yOut1;
        <<Type="UnitDelay",InitialCondition="1">> component Memory_Init {
          ports
            in Double in1,
            out Double out1;
          effect in1 -> out1;
        }
        <<Type="Constant",Value="0">> component Zero_Init {
          ports
            out Double out1;
        }
        component Memory_Init memory_Init;
        component Zero_Init zero_Init;
        connect memory_Init.out1 -> yOut1;
        connect zero_Init.out1 -> memory_Init.in1;
      }
      <<Type="SubSystem">> component VAPSRealTimeTimer {
        ports
          in Double timeFactorIn1,
          out Double tsSimOut1,
          out Double tsRTimeOut2,
          out Double tsRTimeEffectiveOut3;
        <<Type="M-S-Function">> component Level2MfileSFunction {
          ports
            in Double in1,
            out Double tsSimOut1,
            out Double tsRTimeOut2,
            out Double tsRTimeEffectiveOut3;
          effect in1 -> tsSimOut1;
          effect in1 -> tsRTimeOut2;
          effect in1 -> tsRTimeEffectiveOut3;
        }
        component Level2MfileSFunction level2MfileSFunction;
        connect timeFactorIn1 -> level2MfileSFunction.in1;
        connect level2MfileSFunction.tsSimOut1 -> tsSimOut1;
        connect level2MfileSFunction.tsRTimeOut2 -> tsRTimeOut2;
        connect level2MfileSFunction.tsRTimeEffectiveOut3 -> tsRTimeEffectiveOut3;
      }
      <<Condition="u2 >= 1",Type="Condition">> component Condition {
        ports
          in Boolean in1,
          out Boolean out1;
      }
      <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
        ports
          in Double in1,
          out Boolean out1;
      }
      component Constant1 constant1;
      component Constant10 constant10;
      component Constant4 constant4;
      component RealTimeTimerAdapter realTimeTimerAdapter;
      component RealTimeTimerAdapter_Value realTimeTimerAdapter_Value;
      component RelOp4 relOp4;
      component SwitchBlock1 switchBlock1;
      component SwitchBlock2 switchBlock2;
      component SysInit1 sysInit1;
      component VAPSRealTimeTimer vAPSRealTimeTimer;
      component Condition condition;
      component Condition1 condition1;
      connect condition.out1 -> switchBlock1.condition;
      connect condition1.out1 -> switchBlock2.condition;
      connect relOp4.out1 -> condition.in1;
      connect sysInit1.yOut1 -> condition1.in1;
      connect constant1.out1 -> switchBlock1.ifIn;
      connect maxSpeedIn2 -> relOp4.in1;
      connect switchBlock1.out1 -> vAPSRealTimeTimer.timeFactorIn1;
      connect constant10.out1 -> relOp4.in2;
      connect vAPSRealTimeTimer.tsRTimeEffectiveOut3 -> tsRTimeEffOut6;
      connect vAPSRealTimeTimer.tsRTimeOut2 -> tsRTimeOut5;
      connect vAPSRealTimeTimer.tsSimOut1 -> tsSimOut4;
      connect realTimeTimerAdapter_Value.out1Out1 -> timeConfig_ProducerOut3;
      connect realTimeTimerAdapter_Value.out1Out1 -> timeConfig_ConsumerOut2;
      connect realTimeTimerAdapter.out1Out1 -> switchBlock1.elseIn;
      connect realTimeTimerAdapter.out1Out1 -> factor_SimTimeOut1;
      connect timeConfigIn1 -> switchBlock2.elseIn;
      connect constant4.out1 -> switchBlock2.ifIn;
      connect switchBlock2.out1 -> realTimeTimerAdapter_Value.in1In1;
      connect switchBlock2.out1 -> realTimeTimerAdapter.in1In1;
    }
    component Constant constant;
    component Constant1 constant1;
    component Terminator terminator;
    component Terminator1 terminator1;
    component Terminator2 terminator2;
    component Terminator3 terminator3;
    component Terminator4 terminator4;
    component Terminator5 terminator5;
    component VAPS_TimeConfiguration vAPS_TimeConfiguration;
    connect vAPS_TimeConfiguration.timeConfig_ProducerOut3 -> terminator2.in1;
    connect vAPS_TimeConfiguration.tsSimOut4 -> terminator3.in1;
    connect vAPS_TimeConfiguration.tsRTimeOut5 -> terminator4.in1;
    connect vAPS_TimeConfiguration.tsRTimeEffOut6 -> terminator5.in1;
    connect vAPS_TimeConfiguration.timeConfig_ConsumerOut2 -> terminator1.in1;
    connect vAPS_TimeConfiguration.factor_SimTimeOut1 -> terminator.in1;
    connect constant1.out1 -> vAPS_TimeConfiguration.maxSpeedIn2;
    connect constant.out1 -> vAPS_TimeConfiguration.timeConfigIn1;
  }
  <<Type="SubSystem">> component Umgebung {
    ports
      out Double v_Vehicle_kmh,
      out Boolean parkingBrake_b,
      out Double brakeForce_pedal_pc,
      out Double acceleration_pedal_pc,
      out Boolean cruiseControl_b,
      out Boolean limiter_b,
      out Boolean leverUp_b,
      out Boolean leverDown_b,
      in Boolean cC_active_b,
      in Double cCSetValue_kmh,
      in Double limiterSetValue_kmh,
      in Boolean limiter_active_b,
      in Double acceleration_pc;
    <<Type="Display">> component CCSetValue {
      ports
        in Double cCSetValue_kmhIn1;
    }
    <<Type="Display">> component CC_active {
      ports
        in Boolean cC_active_bIn1;
    }
    <<Type="Constant",Value="1">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="1">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="SubSystem">> component FalseBlock {
      ports
        out Double yOut1;
      <<Type="Constant",Value="0">> component Zero {
        ports
          out Double out1;
      }
      component Zero zero;
      connect zero.out1 -> yOut1;
    }
    <<Type="SubSystem">> component FalseBlock2 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="0">> component Zero {
        ports
          out Double out1;
      }
      component Zero zero;
      connect zero.out1 -> yOut1;
    }
    <<Type="SubSystem">> component FalseBlock3 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="0">> component Zero {
        ports
          out Double out1;
      }
      component Zero zero;
      connect zero.out1 -> yOut1;
    }
    <<Type="SubSystem">> component FalseBlock4 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="0">> component Zero {
        ports
          out Double out1;
      }
      component Zero zero;
      connect zero.out1 -> yOut1;
    }
    <<Type="SubSystem">> component FalseBlock5 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="0">> component Zero {
        ports
          out Double out1;
      }
      component Zero zero;
      connect zero.out1 -> yOut1;
    }
    <<Type="Display">> component LimiterSetValue {
      ports
        in Double limiterSetValue_kmhIn1;
    }
    <<Type="Display">> component Limiter_active {
      ports
        in Boolean limiter_active_bIn1;
    }
    <<Type="ManualSwitch">> component ManualSwitch {
      ports
        in Double in1,
        in Double in2,
        out Boolean parkingBrake_bOut1;
      effect in1 -> parkingBrake_bOut1;
      effect in2 -> parkingBrake_bOut1;
    }
    <<Type="ManualSwitch">> component ManualSwitch2 {
      ports
        in Double in1,
        in Double in2,
        out Boolean cruiseControl_bOut1;
      effect in1 -> cruiseControl_bOut1;
      effect in2 -> cruiseControl_bOut1;
    }
    <<Type="ManualSwitch">> component ManualSwitch3 {
      ports
        in Double in1,
        in Double in2,
        out Boolean leverUp_bOut1;
      effect in1 -> leverUp_bOut1;
      effect in2 -> leverUp_bOut1;
    }
    <<Type="ManualSwitch">> component ManualSwitch4 {
      ports
        in Double in1,
        in Double in2,
        out Boolean leverDown_bOut1;
      effect in1 -> leverDown_bOut1;
      effect in2 -> leverDown_bOut1;
    }
    <<Type="ManualSwitch">> component ManualSwitch5 {
      ports
        in Double in1,
        in Double in2,
        out Boolean limiter_bOut1;
      effect in1 -> limiter_bOut1;
      effect in2 -> limiter_bOut1;
    }
    <<Type="Memory">> component Memory {
      ports
        in Double brakeForce_pedal_pcIn1,
        out Double brakeForce_pedal_pcOut1;
      effect brakeForce_pedal_pcIn1 -> brakeForce_pedal_pcOut1;
    }
    <<Type="SubSystem">> component SliderGain {
      ports
        in Double uIn1,
        out Double yOut1;
      <<Type="Gain",Gain="gain">> component SliderGain {
        ports
          in Double in1,
          out Double out1;
        effect in1 -> out1;
      }
      component SliderGain sliderGain;
      connect sliderGain.out1 -> yOut1;
      connect uIn1 -> sliderGain.in1;
    }
    <<Type="SubSystem">> component SliderGain1 {
      ports
        in Double uIn1,
        out Double yOut1;
      <<Type="Gain",Gain="gain">> component SliderGain {
        ports
          in Double in1,
          out Double out1;
        effect in1 -> out1;
      }
      component SliderGain sliderGain;
      connect sliderGain.out1 -> yOut1;
      connect uIn1 -> sliderGain.in1;
    }
    <<Type="SubSystem">> component TrueBlock {
      ports
        out Double yOut1;
      <<Type="Constant",Value="1">> component One {
        ports
          out Double out1;
      }
      component One one;
      connect one.out1 -> yOut1;
    }
    <<Type="SubSystem">> component TrueBlock2 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="1">> component One {
        ports
          out Double out1;
      }
      component One one;
      connect one.out1 -> yOut1;
    }
    <<Type="SubSystem">> component TrueBlock3 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="1">> component One {
        ports
          out Double out1;
      }
      component One one;
      connect one.out1 -> yOut1;
    }
    <<Type="SubSystem">> component TrueBlock4 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="1">> component One {
        ports
          out Double out1;
      }
      component One one;
      connect one.out1 -> yOut1;
    }
    <<Type="SubSystem">> component TrueBlock5 {
      ports
        out Double yOut1;
      <<Type="Constant",Value="1">> component One {
        ports
          out Double out1;
      }
      component One one;
      connect one.out1 -> yOut1;
    }
    <<Type="Display">> component V_Vehicle_kmh {
      ports
        in Double v_Vehicle_kmhIn1;
    }
    <<Type="SubSystem">> component Vehicle {
      ports
        out Double v_Vehicle_kmhOut1,
        in Double brakeForce_pedal_pc,
        in Double brakeForce_pedal_pc1,
        in Double acceleration_pc;
      <<Type="SubSystem">> component DiscreteTransferFcnwithinitialstates {
        ports
          in Double in1In1,
          out Double out1Out1;
        <<Type="DiscreteStateSpace">> component DiscreteStateSpace {
          ports
            in Double in1,
            out Double out1;
          effect in1 -> out1;
        }
        component DiscreteStateSpace discreteStateSpace;
        connect discreteStateSpace.out1 -> out1Out1;
        connect in1In1 -> discreteStateSpace.in1;
      }
      <<Type="Gain",Gain="-2">> component Gain {
        ports
          in Double brakeForce_pedal_pcIn1,
          out Double out1;
        effect brakeForce_pedal_pcIn1 -> out1;
      }
      <<Type="Saturate",LowerLimit="0",UpperLimit="250">> component Saturation {
        ports
          in Double in1,
          out Double v_Vehicle_kmhOut1;
        effect in1 -> v_Vehicle_kmhOut1;
      }
      <<Type="Sum",ListOfSigns="+--">> component Sum1 {
        ports
          in Double v_Vehicle_kmhIn1,
          in Double in2,
          in Double in3,
          out Double out1;
        effect v_Vehicle_kmhIn1 -> out1;
        effect in2 -> out1;
        effect in3 -> out1;
      }
      <<Type="Switch",Criteria="u2 > Threshold",Threshold="0">> component SwitchBlock {
        ports
          in Double ifIn,
          in Boolean condition,
          in Double elseIn,
          out Double out1;
        effect ifIn -> out1;
        effect condition -> out1;
        effect elseIn -> out1;
      }
      <<Type="UniformRandomNumber">> component UniformRandomNumber {
        ports
          out Double out1;
      }
      <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
        ports
          in Double v_Vehicle_kmhIn1,
          out Double out1;
        effect v_Vehicle_kmhIn1 -> out1;
      }
      <<Type="Lookup">> component Widerstand {
        ports
          in Double in1,
          out Double out1;
        effect in1 -> out1;
      }
      <<Condition="u2 > 0",Type="Condition">> component Condition {
        ports
          in Double in1,
          out Boolean out1;
      }
      component DiscreteTransferFcnwithinitialstates discreteTransferFcnwithinitialstates;
      component Gain gain;
      component Saturation saturation;
      component Sum1 sum1;
      component SwitchBlock switchBlock;
      component UniformRandomNumber uniformRandomNumber;
      component UnitDelay unitDelay;
      component Widerstand widerstand;
      component Condition condition;
      connect acceleration_pc -> switchBlock.elseIn;
      connect brakeForce_pedal_pc -> gain.brakeForce_pedal_pcIn1;
      connect condition.out1 -> switchBlock.condition;
      connect brakeForce_pedal_pc1 -> condition.in1;
      connect uniformRandomNumber.out1 -> sum1.in3;
      connect unitDelay.out1 -> widerstand.in1;
      connect widerstand.out1 -> sum1.in2;
      connect discreteTransferFcnwithinitialstates.out1Out1 -> sum1.v_Vehicle_kmhIn1;
      connect sum1.out1 -> saturation.in1;
      connect switchBlock.out1 -> discreteTransferFcnwithinitialstates.in1In1;
      connect gain.out1 -> switchBlock.ifIn;
      connect saturation.v_Vehicle_kmhOut1 -> v_Vehicle_kmhOut1;
      connect saturation.v_Vehicle_kmhOut1 -> unitDelay.v_Vehicle_kmhIn1;
    }
    component CCSetValue cCSetValue;
    component CC_active cC_active;
    component Constant constant;
    component Constant1 constant1;
    component FalseBlock falseBlock;
    component FalseBlock2 falseBlock2;
    component FalseBlock3 falseBlock3;
    component FalseBlock4 falseBlock4;
    component FalseBlock5 falseBlock5;
    component LimiterSetValue limiterSetValue;
    component Limiter_active limiter_active;
    component ManualSwitch manualSwitch;
    component ManualSwitch2 manualSwitch2;
    component ManualSwitch3 manualSwitch3;
    component ManualSwitch4 manualSwitch4;
    component ManualSwitch5 manualSwitch5;
    component Memory memory;
    component SliderGain sliderGain;
    component SliderGain1 sliderGain1;
    component TrueBlock trueBlock;
    component TrueBlock2 trueBlock2;
    component TrueBlock3 trueBlock3;
    component TrueBlock4 trueBlock4;
    component TrueBlock5 trueBlock5;
    component V_Vehicle_kmh v_Vehicle_kmh;
    component Vehicle vehicle;
    connect vehicle.v_Vehicle_kmhOut1 -> v_Vehicle_kmh;
    connect limiter_active_b -> limiter_active.limiter_active_bIn1;
    connect limiterSetValue_kmh -> limiterSetValue.limiterSetValue_kmhIn1;
    connect cCSetValue_kmh -> cCSetValue.cCSetValue_kmhIn1;
    connect cC_active_b -> cC_active.cC_active_bIn1;
    connect manualSwitch.parkingBrake_bOut1 -> parkingBrake_b;
    connect sliderGain1.yOut1 -> brakeForce_pedal_pc;
    connect sliderGain.yOut1 -> acceleration_pedal_pc;
    connect manualSwitch2.cruiseControl_bOut1 -> cruiseControl_b;
    connect manualSwitch5.limiter_bOut1 -> limiter_b;
    connect manualSwitch3.leverUp_bOut1 -> leverUp_b;
    connect manualSwitch4.leverDown_bOut1 -> leverDown_b;
    connect memory.brakeForce_pedal_pcOut1 -> vehicle.brakeForce_pedal_pc;
    connect memory.brakeForce_pedal_pcOut1 -> vehicle.brakeForce_pedal_pc1;
    connect acceleration_pc -> vehicle.acceleration_pc;
    connect constant1.out1 -> sliderGain1.uIn1;
    connect constant.out1 -> sliderGain.uIn1;
    connect vehicle.v_Vehicle_kmhOut1 -> v_Vehicle_kmh.v_Vehicle_kmhIn1;
    connect falseBlock5.yOut1 -> manualSwitch5.in2;
    connect trueBlock5.yOut1 -> manualSwitch5.in1;
    connect falseBlock4.yOut1 -> manualSwitch4.in2;
    connect trueBlock4.yOut1 -> manualSwitch4.in1;
    connect falseBlock3.yOut1 -> manualSwitch3.in2;
    connect trueBlock3.yOut1 -> manualSwitch3.in1;
    connect falseBlock2.yOut1 -> manualSwitch2.in2;
    connect trueBlock2.yOut1 -> manualSwitch2.in1;
    connect sliderGain1.yOut1 -> memory.brakeForce_pedal_pcIn1;
    connect falseBlock.yOut1 -> manualSwitch.in2;
    connect trueBlock.yOut1 -> manualSwitch.in1;
  }
  <<Type="Memory">> component Memory1 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory2 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory3 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory4 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory5 {
    ports
      in Double in1,
      out Double out1;
  }
  component DEMO_FAS dEMO_FAS;
  component SimToRealTime simToRealTime;
  component Umgebung umgebung;
  component Memory1 memory1;
  component Memory2 memory2;
  component Memory3 memory3;
  component Memory4 memory4;
  component Memory5 memory5;
  connect umgebung.parkingBrake_b -> dEMO_FAS.parkingBrake_b;
  connect umgebung.brakeForce_pedal_pc -> dEMO_FAS.brakeForce_pedal_pc;
  connect umgebung.acceleration_pedal_pc -> dEMO_FAS.acceleration_pedal_pc;
  connect umgebung.cruiseControl_b -> dEMO_FAS.cruiseControl_b;
  connect umgebung.limiter_b -> dEMO_FAS.limiter_b;
  connect umgebung.leverUp_b -> dEMO_FAS.leverUp_b;
  connect umgebung.leverDown_b -> dEMO_FAS.leverDown_b;
  connect umgebung.v_Vehicle_kmh -> dEMO_FAS.v_Vehicle_kmh;
  connect memory1.out1 -> umgebung.cC_active_b;
  connect memory2.out1 -> umgebung.cCSetValue_kmh;
  connect memory3.out1 -> umgebung.limiterSetValue_kmh;
  connect memory4.out1 -> umgebung.limiter_active_b;
  connect memory5.out1 -> umgebung.acceleration_pc;
  connect dEMO_FAS.cC_active_b -> memory1.in1;
  connect dEMO_FAS.cCSetValue_kmh -> memory2.in1;
  connect dEMO_FAS.limiterSetValue_kmh -> memory3.in1;
  connect dEMO_FAS.limiter_active_b -> memory4.in1;
  connect dEMO_FAS.acceleration_pc -> memory5.in1;
}
