# A Systematic Cross-Domain Mapping Study on the Software Engineering of Digital Twins

Manuela Dalibor, Nico Jansen, Bernhard Rumpe, David Schmalzing, Louis Wachtmeister, Manuel Wimmer, and Andreas Wortmann

<p>
Digital Twins are currently investigated as the technological backbone for providing an enhanced understanding and management
of existing systems as well as for designing new systems in various domains, e.g., ranging from single manufacturing components
such as sensors to large-scale systems such as smart cities. Given the diverse application domains of Digital Twins, it is not
surprising that the characterization of the term Digital Twin, as well as the needs for developing and operating Digital Twins are
multi-faceted. Providing a better understanding what the commonalities and differences of Digital Twins in different contexts
are, may allow to build reusable support for developing, running, and managing Digital Twins by providing dedicated concepts,
techniques, and tool support. In this paper, we aim to uncover the nature of Digital Twins based on a systematic mapping study
which is not limited to a particular application domain or technological space. We systematically retrieved a set of 1471 unique
publications of which 529 were identified as potentially relevant and of which finally 356 were selected for further investigation.
In particular, we analyzed the types of research and contributions made for Digital Twins, the expected properties Digital Twins
have to fulfill, how Digital Twins are realized and operated, as well as how Digital Twins are finally evaluated. Based on this
analysis, we also contribute a novel feature model for Digital Twins as well as several observations to further guide future software
engineering research in this area.
</p>

For better transparency and replicability, this directory provides
* a list of the [initial 2002 publications](2021-DTSMS-InitialSearch.xlsx) and
* a list of the [356 included publications](2021-DTSMS-Included.xlsx).
