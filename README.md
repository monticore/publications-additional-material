<!-- (c) https://github.com/MontiCore/monticore -->
# Additional Material For MontiCore related Publications

This  MontiCore project contains a number of directories with individual
artifacts (software, data, etc.) that are published as
accompanying material for publications of the
Software Engineering Chair at the RWTH Aachen
(https://www.se-rwth.de/).

The original papers, where these artifacts belong to can be found under:

  * https://www.se-rwth.de/publications/

For further information you can look at the individual websites:

  * https://www.se-rwth.de/materials/cd2alloy/
  * https://www.se-rwth.de/materials/cncviews/
  * https://www.se-rwth.de/materials/cncviewscasestudy/
  * https://www.se-rwth.de/materials/deeplearning/
  * https://www.se-rwth.de/materials/ema_compiler/
  * https://www.se-rwth.de/materials/ema_tutorial/
  * https://www.se-rwth.de/materials/embeddedmontiarc/
  * https://www.se-rwth.de/materials/iserveu/
  * https://www.se-rwth.de/materials/mbse4ind/
  * https://www.se-rwth.de/materials/mbtcc/
  * https://www.se-rwth.de/materials/mcvisitors/
  * https://www.se-rwth.de/materials/middleware/
  * https://www.se-rwth.de/materials/mod/
  * https://www.se-rwth.de/materials/rcl/
  * https://www.se-rwth.de/materials/semdiff/
  * https://www.se-rwth.de/materials/semvar/


# History: 

In the times before Zenodo, this material was simply put on the
web and thus made publicly available.
In the future, we will probably add newer artifacts of
such kind directly as Zenodo submissions, like e.g.

  * https://zenodo.org/record/1412345#.XLzplqTgo-U
  * https://zenodo.org/record/1314370#.XLzplKTgo-U
  * https://zenodo.org/record/1319653#.XLzpn6Tgo-U
  * InviDas Privacy Policy of Smart Watches Class Diagram: https://doi.org/10.5281/zenodo.5898204


# Liability and license

This project is free software and other artifacts;
you can redistribute it and/or modify it under the terms of the LGPL;
either version 3.0 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

