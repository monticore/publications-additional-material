/* (c) https://github.com/MontiCore/monticore */
package oeffentlicher_Demonstrator_AL;
import java.lang.*;
import java.util.*;
<<Type="SubSystem">> component Oeffentlicher_Demonstrator_AL {
  <<Type="SubSystem">> component AbbiegelichtLinks {
    ports
      in Double aBLIn1;
    <<Type="Constant",Value="0.1">> component Constant2 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0">> component Constant3 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.01">> component Constant4 {
      ports
        out Double out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Product",Inputs="2">> component Product {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Operator=">=",Type="RelationalOperator">> component RelationalOperator1 {
      ports
        in Double in1,
        in Double in2,
        out Boolean out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
      ports
        in Double ifIn,
        in Boolean condition,
        in Double elseIn,
        out Double out1;
      effect ifIn -> out1;
      effect condition -> out1;
      effect elseIn -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Condition="u2 >= 1",Type="Condition">> component Condition {
      ports
        in Boolean in1,
        out Boolean out1;
    }
    component Constant2 constant2;
    component Constant3 constant3;
    component Constant4 constant4;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component Product product;
    component RGB rGB;
    component RelationalOperator1 relationalOperator1;
    component SwitchBlock1 switchBlock1;
    component Terminator terminator;
    component Condition condition;
    connect condition.out1 -> switchBlock1.condition;
    connect relationalOperator1.out1 -> condition.in1;
    connect switchBlock1.out1 -> gain2.in1;
    connect switchBlock1.out1 -> gain.in1;
    connect switchBlock1.out1 -> gain1.in1;
    connect aBLIn1 -> product.in1;
    connect product.out1 -> switchBlock1.ifIn;
    connect product.out1 -> relationalOperator1.in1;
    connect constant4.out1 -> product.in2;
    connect constant3.out1 -> switchBlock1.elseIn;
    connect constant2.out1 -> relationalOperator1.in2;
    connect rGB.out1 -> terminator.in1;
    connect gain2.out1 -> rGB.in1;
    connect gain.out1 -> rGB.in2;
    connect gain1.out1 -> rGB.in3;
  }
  <<Type="SubSystem">> component AbbiegelichtRechts {
    ports
      in Double aBRIn1;
    <<Type="Constant",Value="0.1">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.01">> component Constant4 {
      ports
        out Double out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Product",Inputs="2">> component Product {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Operator=">=",Type="RelationalOperator">> component RelationalOperator {
      ports
        in Double in1,
        in Double in2,
        out Boolean out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
      ports
        in Double ifIn,
        in Boolean condition,
        in Double elseIn,
        out Double out1;
      effect ifIn -> out1;
      effect condition -> out1;
      effect elseIn -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Condition="u2 >= 1",Type="Condition">> component Condition {
      ports
        in Boolean in1,
        out Boolean out1;
    }
    component Constant constant;
    component Constant1 constant1;
    component Constant4 constant4;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component Product product;
    component RGB rGB;
    component RelationalOperator relationalOperator;
    component SwitchBlock switchBlock;
    component Terminator terminator;
    component Condition condition;
    connect condition.out1 -> switchBlock.condition;
    connect relationalOperator.out1 -> condition.in1;
    connect product.out1 -> switchBlock.ifIn;
    connect product.out1 -> relationalOperator.in1;
    connect aBRIn1 -> product.in1;
    connect constant4.out1 -> product.in2;
    connect switchBlock.out1 -> gain1.in1;
    connect switchBlock.out1 -> gain.in1;
    connect switchBlock.out1 -> gain2.in1;
    connect constant1.out1 -> switchBlock.elseIn;
    connect constant.out1 -> relationalOperator.in2;
    connect rGB.out1 -> terminator.in1;
    connect gain2.out1 -> rGB.in1;
    connect gain.out1 -> rGB.in2;
    connect gain1.out1 -> rGB.in3;
  }
  <<Type="SubSystem">> component AbblendlichtLinks {
    ports
      in Double abblendlichtLIn1;
    <<Type="Constant",Value="100">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Product",Inputs="*/">> component Divide {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component Divide divide;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component RGB rGB;
    component Terminator terminator;
    connect divide.out1 -> gain2.in1;
    connect divide.out1 -> gain1.in1;
    connect divide.out1 -> gain.in1;
    connect constant.out1 -> divide.in2;
    connect abblendlichtLIn1 -> divide.in1;
    connect gain1.out1 -> rGB.in3;
    connect gain.out1 -> rGB.in2;
    connect gain2.out1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component AbblendlichtRechts {
    ports
      in Double abblendlichtRIn1;
    <<Type="Constant",Value="100">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Product",Inputs="*/">> component Divide {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component Divide divide;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> divide.in2;
    connect abblendlichtRIn1 -> divide.in1;
    connect divide.out1 -> gain.in1;
    connect divide.out1 -> gain2.in1;
    connect divide.out1 -> gain1.in1;
    connect gain1.out1 -> rGB.in3;
    connect gain.out1 -> rGB.in2;
    connect gain2.out1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Aussenhelligkeitlx {
    ports
      in Double uIn1,
      out Double yOut1;
    <<Type="Gain",Gain="gain">> component SliderGain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    component SliderGain sliderGain;
    connect sliderGain.out1 -> yOut1;
    connect uIn1 -> sliderGain.in1;
  }
  <<Type="Constant",Value="1">> component Constant1 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant18 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant19 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant2 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant20 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant21 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant22 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant23 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant24 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant25 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant26 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant27 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant28 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant29 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant3 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant4 {
    ports
      out Double out1;
  }
  <<Type="Constant",Value="1">> component Constant5 {
    ports
      out Double out1;
  }
  <<Type="SubSystem">> component DEMO_Aussenlicht {
    ports
      out Double fRAVL_b,
      out Double fRAHL_b,
      out Double fRAAL_b,
      out Double fRAVR_b,
      out Double fRAHR_b,
      out Double fRAAR_b,
      out Double abblendlichtLinks_pc,
      out Double abblendlichtRechts_pc,
      out Double abbiegelichtRechts_pc,
      out Double abbiegelichtLinks_pc,
      out Double ausleuchtungLinks_m,
      out Double ausleuchtungRechts_m,
      out Boolean fLLinksDefekt_b,
      out Boolean abblLLinksDefekt_b,
      out Boolean abbiLLinksDefekt_b,
      out Boolean fAVLinksDefekt_b,
      out Boolean fAALinksDefekt_b,
      out Boolean fAHLinksDefekt_b,
      out Boolean fLRechtsDefekt_b,
      out Boolean abblLRechtsDefekt_b,
      out Boolean abbiLRechtsDefekt_b,
      out Boolean fAVRechtsDefekt_b,
      out Boolean fAARechtsDefekt_b,
      out Boolean fAHRechtsDefekt_b,
      in Boolean rBLinks_b,
      in Boolean rBRechts_b,
      in Boolean warnblinken_b,
      in Double schluessel_st,
      in Double lichtdrehschalter_st,
      in Double aussenhelligkeit_lx,
      in Boolean entriegelt_b,
      in Boolean tuer_b,
      in Double fzggeschw_kmh,
      in Boolean entFahrzeug_b,
      in Boolean fernlicht_b,
      in Double spannung_V,
      in Double fLLinks_st,
      in Double abbleLLinks_st,
      in Double abbiLLinks_st,
      in Double fAVLinks_st,
      in Double fAALinks_st,
      in Double fAHLinks_st,
      in Double fLRechts_st,
      in Double abbleLRechts_st,
      in Double abbiLRechts_st,
      in Double fAVRechts_st,
      in Double fAARechts_st,
      in Double fAHRechts_st,
      in Double dunkeltaster_st;
    <<Type="SubSystem">> component DEMO_Aussenlicht {
      ports
        in Boolean _RBLinks_bIn1,
        in Boolean _RBRechts_bIn2,
        in Boolean _Warnblinken_bIn3,
        in Double _Schluessel_stIn4,
        in Double _Lichtdrehschalter_stIn5,
        in Double _Aussenhelligkeit_lxIn6,
        in Boolean _Entriegelt_bIn7,
        in Boolean _Tuer_bIn8,
        in Double _Fzggeschw_kmhIn9,
        in Boolean _EntFahrzeug_bIn10,
        in Boolean _Fernlicht_bIn11,
        in Double _Spannung_VIn12,
        in Double _FLLinks_stIn13,
        in Double _AbbleLLinks_stIn14,
        in Double _AbbiLLinks_stIn15,
        in Double _FAVLinks_stIn16,
        in Double _FAALinks_stIn17,
        in Double _FAHLinks_stIn18,
        in Double _FLRechts_stIn19,
        in Double _AbbleLRechts_stIn20,
        in Double _AbbiLRechts_stIn21,
        in Double _FAVRechts_stIn22,
        in Double _FAARechts_stIn23,
        in Double _FAHRechts_stIn24,
        in Double _Dunkeltaster_stIn25,
        out Double _FRAVL_bOut1,
        out Double _FRAHL_bOut2,
        out Double _FRAAL_bOut3,
        out Double _FRAVR_bOut4,
        out Double _FRAHR_bOut5,
        out Double _FRAAR_bOut6,
        out Double _AbblendlichtLinks_pcOut7,
        out Double _AbblendlichtRechts_pcOut8,
        out Double _AbbiegelichtRechts_pcOut9,
        out Double _AbbiegelichtsLinks_pcOut10,
        out Double _AusleuchtungLinks_mOut11,
        out Double _AusleuchtungRechts_mOut12,
        out Boolean _FLLinksDefekt_bOut13,
        out Boolean _AbbleLLinksDefekt_bOut14,
        out Boolean _AbbiLLinksDefekt_bOut15,
        out Boolean _FAVLinksDefekt_bOut16,
        out Boolean _FAALinksDefekt_bOut17,
        out Boolean _FAHLinksDefekt_bOut18,
        out Boolean _FLRechtsDefekt_bOut19,
        out Boolean _AbblLRechtsDefekt_bOut20,
        out Boolean _AbbiLRechtsDefekt_bOut21,
        out Boolean _FAVRechtsDefekt_bOut22,
        out Boolean _FAARechtsDefekt_bOut23,
        out Boolean _FAHRechtsDefekt_bOut24;
      <<Type="SubSystem">> component Subsystem {
        ports
          in Boolean _RBLinks_bIn1,
          in Boolean _RBRechts_bIn2,
          in Boolean _Warnblinken_bIn3,
          in Double _Schluessel_stIn4,
          in Double _Lichtdrehschalter_stIn5,
          in Double _Aussenhelligkeit_lxIn6,
          in Boolean _Entriegelt_bIn7,
          in Boolean _Tuer_bIn8,
          in Double _Fzggeschw_kmhIn9,
          in Boolean _EntFahrzeug_bIn10,
          in Boolean _Fernlicht_bIn11,
          in Double _Spannung_VIn12,
          in Double _FLLinks_stIn13,
          in Double _AbbleLLinks_stIn14,
          in Double _AbbiLLinks_stIn15,
          in Double _FAVLinks_stIn16,
          in Double _FAALinks_stIn17,
          in Double _FAHLinks_stIn18,
          in Double _FLRechts_stIn19,
          in Double _AbbleLRechts_stIn20,
          in Double _AbbiLRechts_stIn21,
          in Double _FAVRechts_stIn22,
          in Double _FAARechts_stIn23,
          in Double _FAHRechts_stIn24,
          in Double _Dunkeltaster_stIn25,
          out Double _FRAVL_bOut1,
          out Double _FRAHL_bOut2,
          out Double _FRAAL_bOut3,
          out Double _FRAVR_bOut4,
          out Double _FRAHR_bOut5,
          out Double _FRAAR_bOut6,
          out Double _AbblendlichtLinks_pcOut7,
          out Double _AbblendlichtRechts_pcOut8,
          out Double _AbbiegelichtRechts_pcOut9,
          out Double _AbbiegelichtsLinks_pcOut10,
          out Double _AusleuchtungLinks_mOut11,
          out Double _AusleuchtungRechts_mOut12,
          out Boolean _FLLinksDefekt_bOut13,
          out Boolean _AbbleLLinksDefekt_bOut14,
          out Boolean _AbbiLLinksDefekt_bOut15,
          out Boolean _FAVLinksDefekt_bOut16,
          out Boolean _FAALinksDefekt_bOut17,
          out Boolean _FAHLinksDefekt_bOut18,
          out Boolean _FLRechtsDefekt_bOut19,
          out Boolean _AbblLRechtsDefekt_bOut20,
          out Boolean _AbbiLRechtsDefekt_bOut21,
          out Boolean _FAVRechtsDefekt_bOut22,
          out Boolean _FAARechtsDefekt_bOut23,
          out Boolean _FAHRechtsDefekt_bOut24;
        <<Type="SubSystem">> component DEMO_Aussenlicht {
          ports
            in Boolean _RBLinks_bIn1,
            in Boolean _RBRechts_bIn2,
            in Boolean _Warnblinken_bIn3,
            in Double _Schluessel_stIn4,
            in Double _Lichtdrehschalter_stIn5,
            in Double _Aussenhelligkeit_lxIn6,
            in Boolean _Entriegelt_bIn7,
            in Boolean _Tuer_bIn8,
            in Double _Fzggeschw_kmhIn9,
            in Boolean _EntFahrzeug_bIn10,
            in Boolean _Fernlicht_bIn11,
            in Double _Spannung_VIn12,
            in Double _FLLinks_stIn13,
            in Double _AbbleLLinks_stIn14,
            in Double _AbbiLLinks_stIn15,
            in Double _FAVLinks_stIn16,
            in Double _FAALinks_stIn17,
            in Double _FAHLinks_stIn18,
            in Double _FLRechts_stIn19,
            in Double _AbbleLRechts_stIn20,
            in Double _AbbiLRechts_stIn21,
            in Double _FAVRechts_stIn22,
            in Double _FAARechts_stIn23,
            in Double _FAHRechts_stIn24,
            in Double _Dunkeltaster_stIn25,
            out Double _FRAVL_bOut1,
            out Double _FRAHL_bOut2,
            out Double _FRAAL_bOut3,
            out Double _FRAVR_bOut4,
            out Double _FRAHR_bOut5,
            out Double _FRAAR_bOut6,
            out Double _AbblendlichtLinks_pcOut7,
            out Double _AbblendlichtRechts_pcOut8,
            out Double _AbbiegelichtRechts_pcOut9,
            out Double _AbbiegelichtsLinks_pcOut10,
            out Double _AusleuchtungLinks_mOut11,
            out Double _AusleuchtungRechts_mOut12,
            out Boolean _FLLinksDefekt_bOut13,
            out Boolean _AbbleLLinksDefekt_bOut14,
            out Boolean _AbbiLLinksDefekt_bOut15,
            out Boolean _FAVLinksDefekt_bOut16,
            out Boolean _FAALinksDefekt_bOut17,
            out Boolean _FAHLinksDefekt_bOut18,
            out Boolean _FLRechtsDefekt_bOut19,
            out Boolean _AbblLRechtsDefekt_bOut20,
            out Boolean _AbbiLRechtsDefekt_bOut21,
            out Boolean _FAVRechtsDefekt_bOut22,
            out Boolean _FAARechtsDefekt_bOut23,
            out Boolean _FAHRechtsDefekt_bOut24;
          <<Type="SubSystem">> component DEMO_Aussenlicht_Funktion {
            ports
              in Boolean rBLinks_b,
              in Boolean rBRechts_b,
              in Boolean warnblinken_b,
              in Double schluessel_st,
              in Double schluessel_st1,
              in Double lichtdrehschalter_st,
              in Double aussenhelligkeit_lx,
              in Boolean entriegelt_b,
              in Boolean tuer_b,
              in Double fzggeschw_kmh,
              in Boolean entFahrzeug_b,
              in Boolean fernlicht_b,
              in Double spannung_V,
              in Double fLLinks_st,
              in Double abbleLLinks_st,
              in Double abbiLLinks_st,
              in Double fAVLinks_st,
              in Double fAALinks_st,
              in Double fAHLinks_st,
              in Double fLRechts_st,
              in Double abbleLRechts_st,
              in Double abbiLRechts_st,
              in Double fAVRechts_st,
              in Double fAARechts_st,
              in Double fAHRechts_st,
              in Double dunkeltaster_st,
              out Double fRAVL_b,
              out Double fRAHL_b,
              out Double fRAAL_b,
              out Double fRAVR_b,
              out Double fRAHR_b,
              out Double fRAAR_b,
              out Double abblendlichtLinks_pc,
              out Double abblendlichtRechts_pc,
              out Double abbiegelichtRechts_pc,
              out Double abbiegelichtLinks_pc,
              out Double ausleuchtungLinks_m,
              out Double ausleuchtungRechts_m,
              out Boolean fLLinksDefekt_b,
              out Boolean abblLLinksDefekt_b,
              out Boolean abbiLLinksDefekt_b,
              out Boolean fAVLinksDefekt_b,
              out Boolean fAALinksDefekt_b,
              out Boolean fAHLinksDefekt_b,
              out Boolean fLRechtsDefekt_b,
              out Boolean abblLRechtsDefekt_b,
              out Boolean abbiLRechtsDefekt_b,
              out Boolean fAVRechtsDefekt_b,
              out Boolean fAARechtsDefekt_b,
              out Boolean fAHRechtsDefekt_b;
            <<Type="SubSystem">> component AL_Input {
              ports
                out Double schluessel_st,
                in Boolean rBLinks_b,
                in Boolean rBRechts_b,
                in Boolean warnblinken_b,
                in Double schluessel_st1,
                in Double schluessel_st2,
                in Double lichtdrehschalter_st,
                in Double aussenhelligkeit_lx,
                in Boolean entriegelt_b,
                in Boolean tuer_b,
                in Double fzggeschw_kmh,
                in Boolean entFahrzeug_b,
                in Boolean fernlicht_b,
                in Double spannung_V,
                in Double fLLinks_st,
                in Double abbleLLinks_st,
                in Double abbiLLinks_st,
                in Double fAVLinks_st,
                in Double fAALinks_st,
                in Double fAHLinks_st,
                in Double fLRechts_st,
                in Double abbleLRechts_st,
                in Double abbiLRechts_st,
                in Double fAVRechts_st,
                in Double fAARechts_st,
                in Double fAHRechts_st,
                in Double dunkeltaster_st,
                out Double lichtdrehschalter_st1,
                out Boolean aussenhelligkeit_b,
                out Boolean entriegelt_b1,
                out Boolean tuer_b1,
                out Double fzggeschw_kmh1,
                out Boolean entFahrzeug_b1,
                out Boolean fernlicht_b1,
                out Double spannung_V1,
                out Double dunkeltaster_st1,
                out Double fLLinks_st1,
                out Double abbleLLinks_st1,
                out Double abbiLLinks_st1,
                out Double fAVLinks_st1,
                out Double fAALinks_st1,
                out Double fAHLinks_st1,
                out Double fLRechts_st1,
                out Double abbleLRechts_st1,
                out Double abbiLRechts_st1,
                out Double fAVRechts_st1,
                out Double fAARechts_st1,
                out Double fAHRechts_st1,
                out Boolean rBLinks_b1,
                out Boolean rBRechts_b1,
                out Boolean warnblinken_b1;
              <<Type="Constant",Value="100">> component DEMO_AUSSEN_Min_UmgebungsHell {
                ports
                  out Double out1;
              }
              <<Operator="<=",Type="RelationalOperator">> component RelOp2 {
                ports
                  in Double aussenhelligkeit_lxIn1,
                  in Double in2,
                  out Boolean aussenhelligkeit_bOut1;
                effect aussenhelligkeit_lxIn1 -> aussenhelligkeit_bOut1;
                effect in2 -> aussenhelligkeit_bOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component DEMO_AUSSEN_Min_UmgebungsHell dEMO_AUSSEN_Min_UmgebungsHell;
              component RelOp2 relOp2;
              component VERSION_INFO vERSION_INFO;
              connect aussenhelligkeit_lx -> relOp2.aussenhelligkeit_lxIn1;
              connect schluessel_st2 -> schluessel_st;
              connect lichtdrehschalter_st -> lichtdrehschalter_st1;
              connect relOp2.aussenhelligkeit_bOut1 -> aussenhelligkeit_b;
              connect entriegelt_b -> entriegelt_b1;
              connect tuer_b -> tuer_b1;
              connect fzggeschw_kmh -> fzggeschw_kmh1;
              connect entFahrzeug_b -> entFahrzeug_b1;
              connect fernlicht_b -> fernlicht_b1;
              connect spannung_V -> spannung_V1;
              connect dunkeltaster_st -> dunkeltaster_st1;
              connect fLLinks_st -> fLLinks_st1;
              connect abbleLLinks_st -> abbleLLinks_st1;
              connect abbiLLinks_st -> abbiLLinks_st1;
              connect fAVLinks_st -> fAVLinks_st1;
              connect fAALinks_st -> fAALinks_st1;
              connect fAHLinks_st -> fAHLinks_st1;
              connect fLRechts_st -> fLRechts_st1;
              connect abbleLRechts_st -> abbleLRechts_st1;
              connect abbiLRechts_st -> abbiLRechts_st1;
              connect fAVRechts_st -> fAVRechts_st1;
              connect fAARechts_st -> fAARechts_st1;
              connect fAHRechts_st -> fAHRechts_st1;
              connect rBLinks_b -> rBLinks_b1;
              connect rBRechts_b -> rBRechts_b1;
              connect warnblinken_b -> warnblinken_b1;
              connect dEMO_AUSSEN_Min_UmgebungsHell.out1 -> relOp2.in2;
            }
            <<Type="SubSystem">> component Abschaltung {
              ports
                out Double fRAVL_bOut1,
                out Double fRAHL_bOut2,
                out Double fRAAL_bOut3,
                out Double fRAVR_bOut4,
                out Double fRAHR_bOut5,
                out Double fRAAR_bOut6,
                out Double abblendlichtLinks_pcOut7,
                out Double abblendlichtRechts_pcOut8,
                out Double ausleuchtungRechts_mOut9,
                out Double ausleuchtungLinks_mOut10,
                out Double abbiegelichtRechts_pcOut11,
                out Double abbiegelichtLinks_pcOut12,
                in Double fRAVL_b,
                in Double fRAHL_b,
                in Double fRAAL_b,
                in Double abblendlichtRechts_pc,
                in Double ausleuchtungRechts_m,
                in Double abbiegelichtRechts_pc,
                in Double fRAVR_b,
                in Double fRAHR_b,
                in Double fRAAR_b,
                in Boolean abblLRechtsDefekt_b,
                in Boolean fLRechtsDefekt_b,
                in Boolean abbiLRechtsDefekt_b,
                in Boolean abblLLinksDefekt_b,
                in Boolean fLLinksDefekt_b,
                in Boolean abbiLLinksDefekt_b,
                in Double abblendlichtLinks_pc,
                in Double ausleuchtungLinks_m,
                in Double abbiegelichtLinks_pc,
                in Boolean fAVLinksDefekt_b,
                in Boolean fAHLinksDefekt_b,
                in Boolean fAALinksDefekt_b,
                in Boolean fAVRechtsDefekt_b,
                in Boolean fAHRechtsDefekt_b,
                in Boolean fAARechtsDefekt_b;
              <<Type="Constant",Value="0">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant10 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant11 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant12 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant2 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant3 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant4 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant5 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant6 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant7 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant8 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant9 {
                ports
                  out Double out1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAVL_bOut1;
                effect ifIn -> fRAVL_bOut1;
                effect condition -> fRAVL_bOut1;
                effect elseIn -> fRAVL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAHL_bOut1;
                effect ifIn -> fRAHL_bOut1;
                effect condition -> fRAHL_bOut1;
                effect elseIn -> fRAHL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock10 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double ausleuchtungLinks_mOut1;
                effect ifIn -> ausleuchtungLinks_mOut1;
                effect condition -> ausleuchtungLinks_mOut1;
                effect elseIn -> ausleuchtungLinks_mOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock11 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double abbiegelichtLinks_pcOut1;
                effect ifIn -> abbiegelichtLinks_pcOut1;
                effect condition -> abbiegelichtLinks_pcOut1;
                effect elseIn -> abbiegelichtLinks_pcOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAAL_bOut1;
                effect ifIn -> fRAAL_bOut1;
                effect condition -> fRAAL_bOut1;
                effect elseIn -> fRAAL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAVR_bOut1;
                effect ifIn -> fRAVR_bOut1;
                effect condition -> fRAVR_bOut1;
                effect elseIn -> fRAVR_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock4 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAHR_bOut1;
                effect ifIn -> fRAHR_bOut1;
                effect condition -> fRAHR_bOut1;
                effect elseIn -> fRAHR_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock5 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAAR_bOut1;
                effect ifIn -> fRAAR_bOut1;
                effect condition -> fRAAR_bOut1;
                effect elseIn -> fRAAR_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock6 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double abblendlichtRechts_pcOut1;
                effect ifIn -> abblendlichtRechts_pcOut1;
                effect condition -> abblendlichtRechts_pcOut1;
                effect elseIn -> abblendlichtRechts_pcOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock7 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double ausleuchtungRechts_mOut1;
                effect ifIn -> ausleuchtungRechts_mOut1;
                effect condition -> ausleuchtungRechts_mOut1;
                effect elseIn -> ausleuchtungRechts_mOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock8 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double abbiegelichtRechts_pcOut1;
                effect ifIn -> abbiegelichtRechts_pcOut1;
                effect condition -> abbiegelichtRechts_pcOut1;
                effect elseIn -> abbiegelichtRechts_pcOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock9 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double abblendlichtLinks_pcOut1;
                effect ifIn -> abblendlichtLinks_pcOut1;
                effect condition -> abblendlichtLinks_pcOut1;
                effect elseIn -> abblendlichtLinks_pcOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition2 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition3 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition4 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition5 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition6 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition7 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition8 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition9 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition10 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition11 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              component Constant1 constant1;
              component Constant10 constant10;
              component Constant11 constant11;
              component Constant12 constant12;
              component Constant2 constant2;
              component Constant3 constant3;
              component Constant4 constant4;
              component Constant5 constant5;
              component Constant6 constant6;
              component Constant7 constant7;
              component Constant8 constant8;
              component Constant9 constant9;
              component SwitchBlock switchBlock;
              component SwitchBlock1 switchBlock1;
              component SwitchBlock10 switchBlock10;
              component SwitchBlock11 switchBlock11;
              component SwitchBlock2 switchBlock2;
              component SwitchBlock3 switchBlock3;
              component SwitchBlock4 switchBlock4;
              component SwitchBlock5 switchBlock5;
              component SwitchBlock6 switchBlock6;
              component SwitchBlock7 switchBlock7;
              component SwitchBlock8 switchBlock8;
              component SwitchBlock9 switchBlock9;
              component VERSION_INFO vERSION_INFO;
              component Condition condition;
              component Condition1 condition1;
              component Condition2 condition2;
              component Condition3 condition3;
              component Condition4 condition4;
              component Condition5 condition5;
              component Condition6 condition6;
              component Condition7 condition7;
              component Condition8 condition8;
              component Condition9 condition9;
              component Condition10 condition10;
              component Condition11 condition11;
              connect condition5.out1 -> switchBlock11.condition;
              connect condition1.out1 -> switchBlock10.condition;
              connect condition11.out1 -> switchBlock9.condition;
              connect condition10.out1 -> switchBlock8.condition;
              connect condition4.out1 -> switchBlock7.condition;
              connect condition2.out1 -> switchBlock6.condition;
              connect abbiegelichtLinks_pc -> switchBlock11.elseIn;
              connect ausleuchtungLinks_m -> switchBlock10.elseIn;
              connect abblendlichtLinks_pc -> switchBlock9.elseIn;
              connect abbiegelichtRechts_pc -> switchBlock8.elseIn;
              connect ausleuchtungRechts_m -> switchBlock7.elseIn;
              connect abblendlichtRechts_pc -> switchBlock6.elseIn;
              connect condition8.out1 -> switchBlock5.condition;
              connect condition6.out1 -> switchBlock4.condition;
              connect condition3.out1 -> switchBlock3.condition;
              connect fRAAR_b -> switchBlock5.elseIn;
              connect fRAHR_b -> switchBlock4.elseIn;
              connect fRAVR_b -> switchBlock3.elseIn;
              connect condition.out1 -> switchBlock2.condition;
              connect fRAAL_b -> switchBlock2.elseIn;
              connect condition9.out1 -> switchBlock1.condition;
              connect fRAHL_b -> switchBlock1.elseIn;
              connect condition7.out1 -> switchBlock.condition;
              connect fRAVL_b -> switchBlock.elseIn;
              connect fAALinksDefekt_b -> condition.in1;
              connect fLLinksDefekt_b -> condition1.in1;
              connect abblLRechtsDefekt_b -> condition2.in1;
              connect fAVRechtsDefekt_b -> condition3.in1;
              connect fLRechtsDefekt_b -> condition4.in1;
              connect abbiLLinksDefekt_b -> condition5.in1;
              connect fAHRechtsDefekt_b -> condition6.in1;
              connect fAVLinksDefekt_b -> condition7.in1;
              connect fAARechtsDefekt_b -> condition8.in1;
              connect fAHLinksDefekt_b -> condition9.in1;
              connect abbiLRechtsDefekt_b -> condition10.in1;
              connect abblLLinksDefekt_b -> condition11.in1;
              connect constant12.out1 -> switchBlock11.ifIn;
              connect constant11.out1 -> switchBlock10.ifIn;
              connect constant10.out1 -> switchBlock9.ifIn;
              connect constant8.out1 -> switchBlock8.ifIn;
              connect constant7.out1 -> switchBlock7.ifIn;
              connect constant6.out1 -> switchBlock6.ifIn;
              connect constant5.out1 -> switchBlock5.ifIn;
              connect constant4.out1 -> switchBlock4.ifIn;
              connect constant3.out1 -> switchBlock3.ifIn;
              connect constant2.out1 -> switchBlock2.ifIn;
              connect constant1.out1 -> switchBlock1.ifIn;
              connect constant9.out1 -> switchBlock.ifIn;
              connect switchBlock11.abbiegelichtLinks_pcOut1 -> abbiegelichtLinks_pcOut12;
              connect switchBlock10.ausleuchtungLinks_mOut1 -> ausleuchtungLinks_mOut10;
              connect switchBlock9.abblendlichtLinks_pcOut1 -> abblendlichtLinks_pcOut7;
              connect switchBlock8.abbiegelichtRechts_pcOut1 -> abbiegelichtRechts_pcOut11;
              connect switchBlock7.ausleuchtungRechts_mOut1 -> ausleuchtungRechts_mOut9;
              connect switchBlock6.abblendlichtRechts_pcOut1 -> abblendlichtRechts_pcOut8;
              connect switchBlock5.fRAAR_bOut1 -> fRAAR_bOut6;
              connect switchBlock4.fRAHR_bOut1 -> fRAHR_bOut5;
              connect switchBlock3.fRAVR_bOut1 -> fRAVR_bOut4;
              connect switchBlock2.fRAAL_bOut1 -> fRAAL_bOut3;
              connect switchBlock1.fRAHL_bOut1 -> fRAHL_bOut2;
              connect switchBlock.fRAVL_bOut1 -> fRAVL_bOut1;
            }
            <<Type="SubSystem">> component Blinken {
              ports
                in Boolean rBLinks_bIn1,
                in Boolean rBRechts_bIn2,
                in Boolean warnblinken_bIn3,
                in Boolean schluessel_bIn4,
                out Boolean rBLinksAktiv_bOut1,
                out Boolean rBRechtsAktiv_bOut2,
                out Boolean warnblinkenAktiv_bOut3,
                out Boolean blinkenRechtsAktiv_bOut4,
                out Boolean blinkenLinksAktiv_bOut5;
              <<Type="SubSystem">> component Chart {
                ports
                  in Boolean timerAbgelaufen_bIn1,
                  in Boolean tippblinkenLinks_bIn2,
                  in Boolean richtungsblinkenLinks_bIn3,
                  in Boolean warnblinken_bIn4,
                  in Boolean tippblinkenRechts_bIn5,
                  in Boolean richtungsblinkenRechts_bIn6,
                  in Boolean blinkAnfrage_bIn7,
                  out Boolean warnblinkenAktiv_bOut1,
                  out Boolean blinkenLinksAktiv_bOut2,
                  out Boolean blinkenRechtsAktiv_bOut3;
                <<Type="Demux">> component Demux {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="S-Function">> component SFunction {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    in Boolean in3,
                    in Boolean in4,
                    in Boolean in5,
                    in Boolean in6,
                    in Boolean in7,
                    out Double out1,
                    out Boolean warnblinkenAktiv_bOut2,
                    out Boolean blinkenLinksAktiv_bOut3,
                    out Boolean blinkenRechtsAktiv_bOut4;
                  effect in1 -> out1;
                  effect in1 -> warnblinkenAktiv_bOut2;
                  effect in1 -> blinkenLinksAktiv_bOut3;
                  effect in1 -> blinkenRechtsAktiv_bOut4;
                  effect in2 -> out1;
                  effect in2 -> warnblinkenAktiv_bOut2;
                  effect in2 -> blinkenLinksAktiv_bOut3;
                  effect in2 -> blinkenRechtsAktiv_bOut4;
                  effect in3 -> out1;
                  effect in3 -> warnblinkenAktiv_bOut2;
                  effect in3 -> blinkenLinksAktiv_bOut3;
                  effect in3 -> blinkenRechtsAktiv_bOut4;
                  effect in4 -> out1;
                  effect in4 -> warnblinkenAktiv_bOut2;
                  effect in4 -> blinkenLinksAktiv_bOut3;
                  effect in4 -> blinkenRechtsAktiv_bOut4;
                  effect in5 -> out1;
                  effect in5 -> warnblinkenAktiv_bOut2;
                  effect in5 -> blinkenLinksAktiv_bOut3;
                  effect in5 -> blinkenRechtsAktiv_bOut4;
                  effect in6 -> out1;
                  effect in6 -> warnblinkenAktiv_bOut2;
                  effect in6 -> blinkenLinksAktiv_bOut3;
                  effect in6 -> blinkenRechtsAktiv_bOut4;
                  effect in7 -> out1;
                  effect in7 -> warnblinkenAktiv_bOut2;
                  effect in7 -> blinkenLinksAktiv_bOut3;
                  effect in7 -> blinkenRechtsAktiv_bOut4;
                }
                <<Type="Terminator">> component Terminator {
                  ports
                    in Double in1;
                }
                component Demux demux;
                component SFunction sFunction;
                component Terminator terminator;
                connect sFunction.out1 -> demux.in1;
                connect demux.out1 -> terminator.in1;
                connect sFunction.blinkenRechtsAktiv_bOut4 -> blinkenRechtsAktiv_bOut3;
                connect sFunction.blinkenLinksAktiv_bOut3 -> blinkenLinksAktiv_bOut2;
                connect sFunction.warnblinkenAktiv_bOut2 -> warnblinkenAktiv_bOut1;
                connect blinkAnfrage_bIn7 -> sFunction.in7;
                connect richtungsblinkenRechts_bIn6 -> sFunction.in6;
                connect tippblinkenRechts_bIn5 -> sFunction.in5;
                connect warnblinken_bIn4 -> sFunction.in4;
                connect richtungsblinkenLinks_bIn3 -> sFunction.in3;
                connect tippblinkenLinks_bIn2 -> sFunction.in2;
                connect timerAbgelaufen_bIn1 -> sFunction.in1;
              }
              <<Type="Constant",Value="0.9">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.7">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.5">> component Constant2 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.2">> component Constant4 {
                ports
                  out Double out1;
              }
              <<Type="SubSystem">> component EdgeRising {
                ports
                  in Boolean uIn1,
                  in Boolean rIn2,
                  in Boolean iVIn3,
                  out Boolean yOut1;
                <<Operator="AND",Type="Logic">> component LogOp_A {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator="NOT",Type="Logic">> component LogOp_N {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component LogOp_A logOp_A;
                component LogOp_N logOp_N;
                component Memory_U memory_U;
                component Switch_R switch_R;
                component Condition condition;
                connect condition.out1 -> switch_R.condition;
                connect rIn2 -> condition.in1;
                connect logOp_N.out1 -> logOp_A.in2;
                connect logOp_A.out1 -> yOut1;
                connect uIn1 -> logOp_A.in1;
                connect uIn1 -> memory_U.in1;
                connect switch_R.out1 -> logOp_N.in1;
                connect memory_U.out1 -> switch_R.elseIn;
                connect iVIn3 -> switch_R.ifIn;
              }
              <<Type="SubSystem">> component FalseBlock2 {
                ports
                  out Boolean yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Boolean out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="Constant",Value="0.5">> component Konstante {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.5">> component Konstante1 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Konstante2 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.5">> component Konstante3 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0.5">> component Konstante4 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Konstante5 {
                ports
                  out Double out1;
              }
              <<Operator="NOT",Type="Logic">> component LogOp {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp1 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean tippblinkenLinks_bOut1;
                effect in1 -> tippblinkenLinks_bOut1;
                effect in2 -> tippblinkenLinks_bOut1;
              }
              <<Operator="AND",Type="Logic">> component LogOp10 {
                ports
                  in Boolean in1,
                  in Boolean blinkenLinksAktiv_bIn2,
                  out Boolean rBLinksAktiv_bOut1;
                effect in1 -> rBLinksAktiv_bOut1;
                effect blinkenLinksAktiv_bIn2 -> rBLinksAktiv_bOut1;
              }
              <<Operator="NOT",Type="Logic">> component LogOp11 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp12 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp13 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean warnblinkenAktiv_bOut1;
                effect in1 -> warnblinkenAktiv_bOut1;
                effect in2 -> warnblinkenAktiv_bOut1;
              }
              <<Operator="NOT",Type="Logic">> component LogOp14 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="NOT",Type="Logic">> component LogOp2 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp3 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean tippblinkenRechts_bOut1;
                effect in1 -> tippblinkenRechts_bOut1;
                effect in2 -> tippblinkenRechts_bOut1;
              }
              <<Operator="OR",Type="Logic">> component LogOp4 {
                ports
                  in Boolean tippblinkenLinks_bIn1,
                  in Boolean richtungsblinkenLinks_bIn2,
                  in Boolean in3,
                  in Boolean tippblinkenRechts_bIn4,
                  in Boolean richtungsblinkenRechts_bIn5,
                  out Boolean blinkanfrage_bOut1;
                effect tippblinkenLinks_bIn1 -> blinkanfrage_bOut1;
                effect richtungsblinkenLinks_bIn2 -> blinkanfrage_bOut1;
                effect in3 -> blinkanfrage_bOut1;
                effect tippblinkenRechts_bIn4 -> blinkanfrage_bOut1;
                effect richtungsblinkenRechts_bIn5 -> blinkanfrage_bOut1;
              }
              <<Operator="AND",Type="Logic">> component LogOp5 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="OR",Type="Logic">> component LogOp6 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="NOT",Type="Logic">> component LogOp7 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="OR",Type="Logic">> component LogOp8 {
                ports
                  in Boolean in1,
                  in Boolean blinkenLinksAktiv_bIn2,
                  in Boolean blinkenRechtsAktiv_bIn3,
                  out Boolean out1;
                effect in1 -> out1;
                effect blinkenLinksAktiv_bIn2 -> out1;
                effect blinkenRechtsAktiv_bIn3 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogOp9 {
                ports
                  in Boolean in1,
                  in Boolean blinkenRechtsAktiv_bIn2,
                  out Boolean rBRechtsAktiv_bOut1;
                effect in1 -> rBRechtsAktiv_bOut1;
                effect blinkenRechtsAktiv_bIn2 -> rBRechtsAktiv_bOut1;
              }
              <<Operator="<",Type="RelationalOperator">> component RelOp {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">",Type="RelationalOperator">> component RelOp1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp2 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean richtungsblinkenLinks_bOut1;
                effect in1 -> richtungsblinkenLinks_bOut1;
                effect in2 -> richtungsblinkenLinks_bOut1;
              }
              <<Operator="<",Type="RelationalOperator">> component RelOp3 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">",Type="RelationalOperator">> component RelOp4 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp5 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean richtungsblinkenRechts_bOut1;
                effect in1 -> richtungsblinkenRechts_bOut1;
                effect in2 -> richtungsblinkenRechts_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp6 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">",Type="RelationalOperator">> component RelOp8 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Type="SubSystem">> component StopWatch_RE1 {
                ports
                  in Boolean eIn1,
                  in Boolean rIn2,
                  out Double yOut1;
                <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                  ports
                    out Double out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Sum",ListOfSigns="++">> component Sum_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Constant",Value="0">> component Zero_R {
                  ports
                    out Double out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Const_dTeff const_dTeff;
                component Memory_C memory_C;
                component Sum_C sum_C;
                component Switch_C switch_C;
                component Switch_R switch_R;
                component Zero_R zero_R;
                component Condition condition;
                component Condition1 condition1;
                connect condition1.out1 -> switch_R.condition;
                connect condition.out1 -> switch_C.condition;
                connect eIn1 -> condition.in1;
                connect rIn2 -> condition1.in1;
                connect const_dTeff.out1 -> sum_C.in2;
                connect switch_R.out1 -> yOut1;
                connect switch_R.out1 -> memory_C.in1;
                connect switch_C.out1 -> switch_R.elseIn;
                connect zero_R.out1 -> switch_R.ifIn;
                connect memory_C.out1 -> switch_C.elseIn;
                connect memory_C.out1 -> sum_C.in1;
                connect sum_C.out1 -> switch_C.ifIn;
              }
              <<Type="SubSystem">> component StopWatch_RE2 {
                ports
                  in Boolean eIn1,
                  in Boolean rIn2,
                  out Double yOut1;
                <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                  ports
                    out Double out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Sum",ListOfSigns="++">> component Sum_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Constant",Value="0">> component Zero_R {
                  ports
                    out Double out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Const_dTeff const_dTeff;
                component Memory_C memory_C;
                component Sum_C sum_C;
                component Switch_C switch_C;
                component Switch_R switch_R;
                component Zero_R zero_R;
                component Condition condition;
                component Condition1 condition1;
                connect condition.out1 -> switch_R.condition;
                connect condition1.out1 -> switch_C.condition;
                connect rIn2 -> condition.in1;
                connect eIn1 -> condition1.in1;
                connect const_dTeff.out1 -> sum_C.in2;
                connect switch_R.out1 -> yOut1;
                connect switch_R.out1 -> memory_C.in1;
                connect switch_C.out1 -> switch_R.elseIn;
                connect zero_R.out1 -> switch_R.ifIn;
                connect memory_C.out1 -> switch_C.elseIn;
                connect memory_C.out1 -> sum_C.in1;
                connect sum_C.out1 -> switch_C.ifIn;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
                effect ifIn -> out1;
                effect condition -> out1;
                effect elseIn -> out1;
              }
              <<Type="SubSystem">> component Timer_REM {
                ports
                  in Boolean eIn1,
                  in Boolean rIn2,
                  in Double iVIn3,
                  out Boolean yOut1,
                  out Double mOut2;
                <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                  ports
                    out Double out1;
                }
                <<Operator="AND",Type="Logic">> component LogOp_C {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator="AND",Type="Logic">> component LogOp_R {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                  ports
                    out Double out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Const_dTeff const_dTeff;
                component LogOp_C logOp_C;
                component LogOp_R logOp_R;
                component Memory_C memory_C;
                component RelOp_C relOp_C;
                component RelOp_R relOp_R;
                component RelOp_Y relOp_Y;
                component Sum_C sum_C;
                component Switch_C switch_C;
                component Switch_R switch_R;
                component Zero_C zero_C;
                component Zero_R zero_R;
                component Zero_Y zero_Y;
                component Condition condition;
                component Condition1 condition1;
                connect condition.out1 -> switch_R.condition;
                connect condition1.out1 -> switch_C.condition;
                connect logOp_R.out1 -> condition.in1;
                connect logOp_C.out1 -> condition1.in1;
                connect rIn2 -> logOp_R.in2;
                connect relOp_R.out1 -> logOp_R.in1;
                connect memory_C.out1 -> relOp_R.in1;
                connect memory_C.out1 -> switch_C.elseIn;
                connect memory_C.out1 -> sum_C.in1;
                connect memory_C.out1 -> relOp_C.in1;
                connect zero_R.out1 -> relOp_R.in2;
                connect iVIn3 -> switch_R.ifIn;
                connect relOp_C.out1 -> logOp_C.in2;
                connect zero_C.out1 -> relOp_C.in2;
                connect eIn1 -> logOp_C.in1;
                connect switch_C.out1 -> switch_R.elseIn;
                connect sum_C.out1 -> switch_C.ifIn;
                connect const_dTeff.out1 -> sum_C.in2;
                connect relOp_Y.out1 -> yOut1;
                connect switch_R.out1 -> memory_C.in1;
                connect switch_R.out1 -> relOp_Y.in1;
                connect switch_R.out1 -> mOut2;
                connect zero_Y.out1 -> relOp_Y.in2;
              }
              <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              component Chart chart;
              component Constant constant;
              component Constant1 constant1;
              component Constant2 constant2;
              component Constant4 constant4;
              component EdgeRising edgeRising;
              component FalseBlock2 falseBlock2;
              component Konstante konstante;
              component Konstante1 konstante1;
              component Konstante2 konstante2;
              component Konstante3 konstante3;
              component Konstante4 konstante4;
              component Konstante5 konstante5;
              component LogOp logOp;
              component LogOp1 logOp1;
              component LogOp10 logOp10;
              component LogOp11 logOp11;
              component LogOp12 logOp12;
              component LogOp13 logOp13;
              component LogOp14 logOp14;
              component LogOp2 logOp2;
              component LogOp3 logOp3;
              component LogOp4 logOp4;
              component LogOp5 logOp5;
              component LogOp6 logOp6;
              component LogOp7 logOp7;
              component LogOp8 logOp8;
              component LogOp9 logOp9;
              component RelOp relOp;
              component RelOp1 relOp1;
              component RelOp2 relOp2;
              component RelOp3 relOp3;
              component RelOp4 relOp4;
              component RelOp5 relOp5;
              component RelOp6 relOp6;
              component RelOp8 relOp8;
              component StopWatch_RE1 stopWatch_RE1;
              component StopWatch_RE2 stopWatch_RE2;
              component SwitchBlock1 switchBlock1;
              component Timer_REM timer_REM;
              component UnitDelay unitDelay;
              component UnitDelay1 unitDelay1;
              component VERSION_INFO vERSION_INFO;
              component Condition condition;
              connect condition.out1 -> switchBlock1.condition;
              connect logOp12.out1 -> condition.in1;
              connect timer_REM.mOut2 -> relOp8.in1;
              connect timer_REM.mOut2 -> relOp6.in1;
              connect konstante4.out1 -> relOp5.in2;
              connect relOp3.out1 -> logOp3.in2;
              connect relOp4.out1 -> logOp3.in1;
              connect konstante5.out1 -> relOp4.in2;
              connect konstante3.out1 -> relOp3.in2;
              connect stopWatch_RE2.yOut1 -> relOp5.in1;
              connect stopWatch_RE2.yOut1 -> relOp3.in1;
              connect stopWatch_RE2.yOut1 -> relOp4.in1;
              connect logOp14.out1 -> logOp12.in1;
              connect constant2.out1 -> switchBlock1.elseIn;
              connect constant1.out1 -> switchBlock1.ifIn;
              connect logOp13.warnblinkenAktiv_bOut1 -> warnblinkenAktiv_bOut3;
              connect schluessel_bIn4 -> logOp14.in1;
              connect switchBlock1.out1 -> relOp6.in2;
              connect unitDelay1.out1 -> chart.timerAbgelaufen_bIn1;
              connect logOp11.out1 -> unitDelay1.in1;
              connect relOp8.out1 -> logOp7.in1;
              connect constant4.out1 -> relOp8.in2;
              connect logOp7.out1 -> unitDelay.in1;
              connect logOp10.rBLinksAktiv_bOut1 -> rBLinksAktiv_bOut1;
              connect logOp9.rBRechtsAktiv_bOut1 -> rBRechtsAktiv_bOut2;
              connect chart.blinkenRechtsAktiv_bOut3 -> logOp9.blinkenRechtsAktiv_bIn2;
              connect chart.blinkenRechtsAktiv_bOut3 -> logOp8.blinkenRechtsAktiv_bIn3;
              connect chart.blinkenRechtsAktiv_bOut3 -> blinkenRechtsAktiv_bOut4;
              connect chart.blinkenLinksAktiv_bOut2 -> logOp10.blinkenLinksAktiv_bIn2;
              connect chart.blinkenLinksAktiv_bOut2 -> blinkenLinksAktiv_bOut5;
              connect chart.blinkenLinksAktiv_bOut2 -> logOp8.blinkenLinksAktiv_bIn2;
              connect chart.warnblinkenAktiv_bOut1 -> logOp8.in1;
              connect chart.warnblinkenAktiv_bOut1 -> logOp12.in2;
              connect chart.warnblinkenAktiv_bOut1 -> logOp13.in2;
              connect logOp8.out1 -> edgeRising.uIn1;
              connect logOp8.out1 -> timer_REM.eIn1;
              connect logOp8.out1 -> logOp5.in2;
              connect timer_REM.yOut1 -> logOp11.in1;
              connect unitDelay.out1 -> logOp5.in1;
              connect relOp6.out1 -> logOp10.in1;
              connect relOp6.out1 -> logOp9.in1;
              connect relOp6.out1 -> logOp13.in1;
              connect constant.out1 -> timer_REM.iVIn3;
              connect edgeRising.yOut1 -> logOp6.in2;
              connect logOp5.out1 -> logOp6.in1;
              connect falseBlock2.yOut1 -> edgeRising.iVIn3;
              connect falseBlock2.yOut1 -> edgeRising.rIn2;
              connect logOp6.out1 -> timer_REM.rIn2;
              connect logOp4.blinkanfrage_bOut1 -> chart.blinkAnfrage_bIn7;
              connect rBRechts_bIn2 -> stopWatch_RE2.eIn1;
              connect rBRechts_bIn2 -> logOp2.in1;
              connect relOp5.richtungsblinkenRechts_bOut1 -> logOp4.richtungsblinkenRechts_bIn5;
              connect relOp5.richtungsblinkenRechts_bOut1 -> chart.richtungsblinkenRechts_bIn6;
              connect logOp3.tippblinkenRechts_bOut1 -> logOp4.tippblinkenRechts_bIn4;
              connect logOp3.tippblinkenRechts_bOut1 -> chart.tippblinkenRechts_bIn5;
              connect logOp2.out1 -> stopWatch_RE2.rIn2;
              connect warnblinken_bIn3 -> chart.warnblinken_bIn4;
              connect warnblinken_bIn3 -> logOp4.in3;
              connect relOp2.richtungsblinkenLinks_bOut1 -> logOp4.richtungsblinkenLinks_bIn2;
              connect relOp2.richtungsblinkenLinks_bOut1 -> chart.richtungsblinkenLinks_bIn3;
              connect logOp1.tippblinkenLinks_bOut1 -> logOp4.tippblinkenLinks_bIn1;
              connect logOp1.tippblinkenLinks_bOut1 -> chart.tippblinkenLinks_bIn2;
              connect konstante1.out1 -> relOp2.in2;
              connect relOp.out1 -> logOp1.in2;
              connect relOp1.out1 -> logOp1.in1;
              connect konstante2.out1 -> relOp1.in2;
              connect konstante.out1 -> relOp.in2;
              connect stopWatch_RE1.yOut1 -> relOp.in1;
              connect stopWatch_RE1.yOut1 -> relOp1.in1;
              connect stopWatch_RE1.yOut1 -> relOp2.in1;
              connect logOp.out1 -> stopWatch_RE1.rIn2;
              connect rBLinks_bIn1 -> stopWatch_RE1.eIn1;
              connect rBLinks_bIn1 -> logOp.in1;
            }
            <<Type="SubSystem">> component Defekt_Erkennung {
              ports
                in Double fLLinks_stIn1,
                in Double abbleLLinks_stIn2,
                in Double abbiLLinks_stIn3,
                in Double fAVLinks_stIn4,
                in Double fAALinks_stIn5,
                in Double fAHLinks_stIn6,
                in Double fLRechts_stIn7,
                in Double abbleLRechts_stIn8,
                in Double abbiLRechts_stIn9,
                in Double fAVRechts_stIn10,
                in Double fAARechts_stIn11,
                in Double fAHRechts_stIn12,
                out Boolean fLLinksDefekt_bOut1,
                out Boolean abbleLLinksDefekt_bOut2,
                out Boolean abbiLLinksDefekt_bOut3,
                out Boolean fAVLinksDefekt_bOut4,
                out Boolean fAALinksDefekt_bOut5,
                out Boolean fAHLinksDefekt_bOut6,
                out Boolean fLRechtsDefekt_bOut7,
                out Boolean abblLRechtsDefekt_bOut8,
                out Boolean abbiLRechtsDefekt_bOut9,
                out Boolean fAVRechtsDefekt_bOut10,
                out Boolean fAARechtsDefekt_bOut11,
                out Boolean fAHRechtsDefekt_bOut12;
              <<Type="Constant",Value="2">> component Constant {
                ports
                  out Double out1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fLLinksDefekt_bOut1;
                effect in1 -> fLLinksDefekt_bOut1;
                effect in2 -> fLLinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean abblLLinksDefekt_bOut1;
                effect in1 -> abblLLinksDefekt_bOut1;
                effect in2 -> abblLLinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp10 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAARechtsDefekt_bOut1;
                effect in1 -> fAARechtsDefekt_bOut1;
                effect in2 -> fAARechtsDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp11 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAHRechtsDefekt_bOut1;
                effect in1 -> fAHRechtsDefekt_bOut1;
                effect in2 -> fAHRechtsDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp2 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean abbiLLinksDefekt_bOut1;
                effect in1 -> abbiLLinksDefekt_bOut1;
                effect in2 -> abbiLLinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp3 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAVLinksDefekt_bOut1;
                effect in1 -> fAVLinksDefekt_bOut1;
                effect in2 -> fAVLinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp4 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAALinksDefekt_bOut1;
                effect in1 -> fAALinksDefekt_bOut1;
                effect in2 -> fAALinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp5 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAHLinksDefekt_bOut1;
                effect in1 -> fAHLinksDefekt_bOut1;
                effect in2 -> fAHLinksDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp6 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fLRechtsDefekt_bOut1;
                effect in1 -> fLRechtsDefekt_bOut1;
                effect in2 -> fLRechtsDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp7 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean abblLRechtsDefekt_bOut1;
                effect in1 -> abblLRechtsDefekt_bOut1;
                effect in2 -> abblLRechtsDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp8 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean abbiLRechtsDefekt_bOut1;
                effect in1 -> abbiLRechtsDefekt_bOut1;
                effect in2 -> abbiLRechtsDefekt_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp9 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean fAVRechtsDefekt_bOut1;
                effect in1 -> fAVRechtsDefekt_bOut1;
                effect in2 -> fAVRechtsDefekt_bOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component Constant constant;
              component RelOp relOp;
              component RelOp1 relOp1;
              component RelOp10 relOp10;
              component RelOp11 relOp11;
              component RelOp2 relOp2;
              component RelOp3 relOp3;
              component RelOp4 relOp4;
              component RelOp5 relOp5;
              component RelOp6 relOp6;
              component RelOp7 relOp7;
              component RelOp8 relOp8;
              component RelOp9 relOp9;
              component VERSION_INFO vERSION_INFO;
              connect relOp4.fAALinksDefekt_bOut1 -> fAALinksDefekt_bOut5;
              connect relOp5.fAHLinksDefekt_bOut1 -> fAHLinksDefekt_bOut6;
              connect relOp6.fLRechtsDefekt_bOut1 -> fLRechtsDefekt_bOut7;
              connect relOp7.abblLRechtsDefekt_bOut1 -> abblLRechtsDefekt_bOut8;
              connect relOp8.abbiLRechtsDefekt_bOut1 -> abbiLRechtsDefekt_bOut9;
              connect relOp9.fAVRechtsDefekt_bOut1 -> fAVRechtsDefekt_bOut10;
              connect relOp10.fAARechtsDefekt_bOut1 -> fAARechtsDefekt_bOut11;
              connect relOp11.fAHRechtsDefekt_bOut1 -> fAHRechtsDefekt_bOut12;
              connect relOp3.fAVLinksDefekt_bOut1 -> fAVLinksDefekt_bOut4;
              connect relOp2.abbiLLinksDefekt_bOut1 -> abbiLLinksDefekt_bOut3;
              connect relOp1.abblLLinksDefekt_bOut1 -> abbleLLinksDefekt_bOut2;
              connect constant.out1 -> relOp7.in2;
              connect constant.out1 -> relOp5.in2;
              connect constant.out1 -> relOp.in2;
              connect constant.out1 -> relOp1.in2;
              connect constant.out1 -> relOp2.in2;
              connect constant.out1 -> relOp3.in2;
              connect constant.out1 -> relOp4.in2;
              connect constant.out1 -> relOp6.in2;
              connect constant.out1 -> relOp8.in2;
              connect constant.out1 -> relOp9.in2;
              connect constant.out1 -> relOp10.in2;
              connect constant.out1 -> relOp11.in2;
              connect relOp.fLLinksDefekt_bOut1 -> fLLinksDefekt_bOut1;
              connect fAHLinks_stIn6 -> relOp5.in1;
              connect abbleLLinks_stIn2 -> relOp1.in1;
              connect fAVLinks_stIn4 -> relOp3.in1;
              connect fAALinks_stIn5 -> relOp4.in1;
              connect abbiLLinks_stIn3 -> relOp2.in1;
              connect abbleLRechts_stIn8 -> relOp7.in1;
              connect abbiLRechts_stIn9 -> relOp8.in1;
              connect fAHRechts_stIn12 -> relOp11.in1;
              connect fLRechts_stIn7 -> relOp6.in1;
              connect fAVRechts_stIn10 -> relOp9.in1;
              connect fAARechts_stIn11 -> relOp10.in1;
              connect fLLinks_stIn1 -> relOp.in1;
            }
            <<Type="SubSystem">> component Fahrrichtungsanzeiger {
              ports
                in Boolean rBLinksAktiv_bIn1,
                in Boolean rBRechtsAktiv_bIn2,
                in Boolean warnblinkenAktiv_bIn3,
                out Double fRAVL_bOut1,
                out Double fRAHL_bOut2,
                out Double fRAAL_bOut3,
                out Double fRAVR_bOut4,
                out Double fRAHR_bOut5,
                out Double fRAAR_bOut6;
              <<Type="SubSystem">> component FalseBlock {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="SubSystem">> component FalseBlock1 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="SubSystem">> component FalseBlock2 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="SubSystem">> component FalseBlock3 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="SubSystem">> component FalseBlock4 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Type="SubSystem">> component FalseBlock5 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Double out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Operator="OR",Type="Logic">> component LogOp {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="OR",Type="Logic">> component LogOp1 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAVL_bOut1;
                effect ifIn -> fRAVL_bOut1;
                effect condition -> fRAVL_bOut1;
                effect elseIn -> fRAVL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAAL_bOut1;
                effect ifIn -> fRAAL_bOut1;
                effect condition -> fRAAL_bOut1;
                effect elseIn -> fRAAL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAHL_bOut1;
                effect ifIn -> fRAHL_bOut1;
                effect condition -> fRAHL_bOut1;
                effect elseIn -> fRAHL_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock4 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAHR_bOut1;
                effect ifIn -> fRAHR_bOut1;
                effect condition -> fRAHR_bOut1;
                effect elseIn -> fRAHR_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock5 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAVR_bOut1;
                effect ifIn -> fRAVR_bOut1;
                effect condition -> fRAVR_bOut1;
                effect elseIn -> fRAVR_bOut1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock6 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double fRAAR_bOut1;
                effect ifIn -> fRAAR_bOut1;
                effect condition -> fRAAR_bOut1;
                effect elseIn -> fRAAR_bOut1;
              }
              <<Type="SubSystem">> component TrueBlock {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component TrueBlock1 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component TrueBlock2 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component TrueBlock3 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component TrueBlock4 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component TrueBlock5 {
                ports
                  out Double yOut1;
                <<Type="Constant",Value="1">> component One {
                  ports
                    out Double out1;
                }
                component One one;
                connect one.out1 -> yOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition2 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition3 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition4 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition5 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              component FalseBlock falseBlock;
              component FalseBlock1 falseBlock1;
              component FalseBlock2 falseBlock2;
              component FalseBlock3 falseBlock3;
              component FalseBlock4 falseBlock4;
              component FalseBlock5 falseBlock5;
              component LogOp logOp;
              component LogOp1 logOp1;
              component SwitchBlock1 switchBlock1;
              component SwitchBlock2 switchBlock2;
              component SwitchBlock3 switchBlock3;
              component SwitchBlock4 switchBlock4;
              component SwitchBlock5 switchBlock5;
              component SwitchBlock6 switchBlock6;
              component TrueBlock trueBlock;
              component TrueBlock1 trueBlock1;
              component TrueBlock2 trueBlock2;
              component TrueBlock3 trueBlock3;
              component TrueBlock4 trueBlock4;
              component TrueBlock5 trueBlock5;
              component VERSION_INFO vERSION_INFO;
              component Condition condition;
              component Condition1 condition1;
              component Condition2 condition2;
              component Condition3 condition3;
              component Condition4 condition4;
              component Condition5 condition5;
              connect condition4.out1 -> switchBlock5.condition;
              connect condition3.out1 -> switchBlock4.condition;
              connect condition5.out1 -> switchBlock6.condition;
              connect condition1.out1 -> switchBlock3.condition;
              connect condition.out1 -> switchBlock1.condition;
              connect condition2.out1 -> switchBlock2.condition;
              connect logOp.out1 -> condition.in1;
              connect logOp.out1 -> condition1.in1;
              connect logOp.out1 -> condition2.in1;
              connect logOp1.out1 -> condition3.in1;
              connect logOp1.out1 -> condition4.in1;
              connect logOp1.out1 -> condition5.in1;
              connect switchBlock4.fRAHR_bOut1 -> fRAHR_bOut5;
              connect switchBlock6.fRAAR_bOut1 -> fRAAR_bOut6;
              connect switchBlock5.fRAVR_bOut1 -> fRAVR_bOut4;
              connect switchBlock2.fRAAL_bOut1 -> fRAAL_bOut3;
              connect switchBlock3.fRAHL_bOut1 -> fRAHL_bOut2;
              connect switchBlock1.fRAVL_bOut1 -> fRAVL_bOut1;
              connect trueBlock5.yOut1 -> switchBlock6.ifIn;
              connect falseBlock5.yOut1 -> switchBlock6.elseIn;
              connect trueBlock4.yOut1 -> switchBlock5.ifIn;
              connect falseBlock4.yOut1 -> switchBlock5.elseIn;
              connect trueBlock3.yOut1 -> switchBlock4.ifIn;
              connect falseBlock3.yOut1 -> switchBlock4.elseIn;
              connect trueBlock2.yOut1 -> switchBlock2.ifIn;
              connect falseBlock2.yOut1 -> switchBlock2.elseIn;
              connect trueBlock1.yOut1 -> switchBlock1.ifIn;
              connect falseBlock1.yOut1 -> switchBlock1.elseIn;
              connect trueBlock.yOut1 -> switchBlock3.ifIn;
              connect falseBlock.yOut1 -> switchBlock3.elseIn;
              connect warnblinkenAktiv_bIn3 -> logOp.in2;
              connect warnblinkenAktiv_bIn3 -> logOp1.in1;
              connect rBRechtsAktiv_bIn2 -> logOp1.in2;
              connect rBLinksAktiv_bIn1 -> logOp.in1;
            }
            <<Type="SubSystem">> component Scheinwerfer {
              ports
                in Boolean blinkenLinksAktiv_bIn1,
                in Boolean blinkenRechtsAktiv_bIn2,
                in Boolean motor_bIn3,
                in Boolean schluessel_bIn4,
                in Double lichtdrehschalter_stIn5,
                in Boolean aussenhelligkeit_bIn6,
                in Boolean entriegelt_bIn7,
                in Boolean tuer_bIn8,
                in Double fzggeschw_kmhIn9,
                in Boolean entFahrzeug_bIn10,
                in Boolean fernlicht_bIn11,
                in Double spannung_VIn12,
                in Double dunkeltaster_stIn13,
                out Double abblendlichtLinks_pcOut1,
                out Double abblendlichtRechts_pcOut2,
                out Double ausleuchtungRechts_mOut3,
                out Double ausleuchtungLinks_mOut4,
                out Double abbiegelichtRechts_pcOut5,
                out Double abbiegelichtLinks_pcOut6;
              <<Type="SubSystem">> component Abbiegelicht {
                ports
                  in Boolean abblendlicht_bIn1,
                  in Double fahrzeuggeschwindigkeit_kmhIn2,
                  in Boolean rBLinks_bIn3,
                  in Boolean rBRechts_bIn4,
                  in Double spannung_VIn5,
                  out Double abbiegelichtRechts_pcOut1,
                  out Double abbiegelichtLinks_pcOut2;
                <<Type="SubSystem">> component Abbiegelicht_Acitve {
                  ports
                    in Boolean abblendlicht_bIn1,
                    in Double fahrzeuggeschwindigkeit_kmhIn2,
                    in Boolean rBLinks_bIn3,
                    in Boolean rBRechts_bIn4,
                    out Double abbiegelichtRechts_pcOut1,
                    out Double abbiegelichtLinks_pcOut2;
                  <<Type="Constant",Value="10">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="10">> component Constant1 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant10 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant2 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant3 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="1">> component Constant4 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant5 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="10">> component Constant6 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant7 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant8 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="1">> component Constant9 {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component EdgeFalling1 {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect iVIn3 -> switch_R.ifIn;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect logOp_A.out1 -> yOut1;
                    connect logOp_N.out1 -> logOp_A.in1;
                    connect uIn1 -> memory_U.in1;
                    connect uIn1 -> logOp_N.in1;
                    connect switch_R.out1 -> logOp_A.in2;
                  }
                  <<Type="SubSystem">> component EdgeFalling2 {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect switch_R.out1 -> logOp_A.in2;
                    connect uIn1 -> logOp_N.in1;
                    connect uIn1 -> memory_U.in1;
                    connect logOp_N.out1 -> logOp_A.in1;
                    connect logOp_A.out1 -> yOut1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component FalseBlock1 {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Type="SubSystem">> component FalseBlock2 {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="AND",Type="Logic">> component LogOp {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                    effect in3 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogOp1 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                    effect in3 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp3 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp4 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp5 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp6 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogOp7 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Function="max",Type="MinMax">> component MinMax {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abbiegelichtRechts_pcOut1;
                    effect in1 -> abbiegelichtRechts_pcOut1;
                    effect in2 -> abbiegelichtRechts_pcOut1;
                  }
                  <<Function="max",Type="MinMax">> component MinMax1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abbiegelichtLinks_pcOut1;
                    effect in1 -> abbiegelichtLinks_pcOut1;
                    effect in2 -> abbiegelichtLinks_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="SubSystem">> component RSFlipFlop {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_S.condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect sIn1 -> condition1.in1;
                    connect memory_Q.out1 -> switch_S.elseIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect logOp_N.out1 -> nOT_QOut2;
                  }
                  <<Type="SubSystem">> component RSFlipFlop1 {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_S.condition;
                    connect sIn1 -> condition.in1;
                    connect rIn2 -> condition1.in1;
                    connect logOp_N.out1 -> nOT_QOut2;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect memory_Q.out1 -> switch_S.elseIn;
                  }
                  <<Type="SubSystem">> component RSFlipFlop2 {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition.out1 -> switch_R.condition;
                    connect condition1.out1 -> switch_S.condition;
                    connect rIn2 -> condition.in1;
                    connect sIn1 -> condition1.in1;
                    connect logOp_N.out1 -> nOT_QOut2;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect memory_Q.out1 -> switch_S.elseIn;
                  }
                  <<Type="SubSystem">> component RSFlipFlop3 {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_S.condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect sIn1 -> condition1.in1;
                    connect memory_Q.out1 -> switch_S.elseIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect logOp_N.out1 -> nOT_QOut2;
                  }
                  <<Operator="<=",Type="RelationalOperator">> component RelOp {
                    ports
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                    effect ifIn -> out1;
                    effect condition -> out1;
                    effect elseIn -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                    effect ifIn -> out1;
                    effect condition -> out1;
                    effect elseIn -> out1;
                  }
                  <<Type="Terminator">> component Terminator {
                    ports
                      in Boolean in1;
                  }
                  <<Type="Terminator">> component Terminator1 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="Terminator">> component Terminator2 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="Terminator">> component Terminator3 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="SubSystem">> component Timer_RE {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_C.condition;
                    connect condition.out1 -> switch_R.condition;
                    connect logOp_R.out1 -> condition.in1;
                    connect logOp_C.out1 -> condition1.in1;
                    connect zero_Y.out1 -> relOp_Y.in2;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect relOp_Y.out1 -> yOut1;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect eIn1 -> logOp_C.in1;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect rIn2 -> logOp_R.in2;
                  }
                  <<Type="SubSystem">> component Timer_RE1 {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_C.condition;
                    connect condition.out1 -> switch_R.condition;
                    connect logOp_R.out1 -> condition.in1;
                    connect logOp_C.out1 -> condition1.in1;
                    connect zero_Y.out1 -> relOp_Y.in2;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect relOp_Y.out1 -> yOut1;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect eIn1 -> logOp_C.in1;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect rIn2 -> logOp_R.in2;
                  }
                  <<Type="SubSystem">> component Timer_REM {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1,
                      out Double mOut2;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition.out1 -> switch_R.condition;
                    connect condition1.out1 -> switch_C.condition;
                    connect logOp_R.out1 -> condition.in1;
                    connect logOp_C.out1 -> condition1.in1;
                    connect rIn2 -> logOp_R.in2;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect eIn1 -> logOp_C.in1;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect relOp_Y.out1 -> yOut1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> mOut2;
                    connect zero_Y.out1 -> relOp_Y.in2;
                  }
                  <<Type="SubSystem">> component Timer_REM1 {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1,
                      out Double mOut2;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_C.condition;
                    connect logOp_C.out1 -> condition.in1;
                    connect logOp_R.out1 -> condition1.in1;
                    connect rIn2 -> logOp_R.in2;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect eIn1 -> logOp_C.in1;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect relOp_Y.out1 -> yOut1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> mOut2;
                    connect zero_Y.out1 -> relOp_Y.in2;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay3 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component Constant constant;
                  component Constant1 constant1;
                  component Constant10 constant10;
                  component Constant2 constant2;
                  component Constant3 constant3;
                  component Constant4 constant4;
                  component Constant5 constant5;
                  component Constant6 constant6;
                  component Constant7 constant7;
                  component Constant8 constant8;
                  component Constant9 constant9;
                  component EdgeFalling1 edgeFalling1;
                  component EdgeFalling2 edgeFalling2;
                  component FalseBlock1 falseBlock1;
                  component FalseBlock2 falseBlock2;
                  component LogOp logOp;
                  component LogOp1 logOp1;
                  component LogOp2 logOp2;
                  component LogOp3 logOp3;
                  component LogOp4 logOp4;
                  component LogOp5 logOp5;
                  component LogOp6 logOp6;
                  component LogOp7 logOp7;
                  component MinMax minMax;
                  component MinMax1 minMax1;
                  component Mul mul;
                  component Mul1 mul1;
                  component RSFlipFlop rSFlipFlop;
                  component RSFlipFlop1 rSFlipFlop1;
                  component RSFlipFlop2 rSFlipFlop2;
                  component RSFlipFlop3 rSFlipFlop3;
                  component RelOp relOp;
                  component SwitchBlock switchBlock;
                  component SwitchBlock2 switchBlock2;
                  component Terminator terminator;
                  component Terminator1 terminator1;
                  component Terminator2 terminator2;
                  component Terminator3 terminator3;
                  component Timer_RE timer_RE;
                  component Timer_RE1 timer_RE1;
                  component Timer_REM timer_REM;
                  component Timer_REM1 timer_REM1;
                  component UnitDelay unitDelay;
                  component UnitDelay1 unitDelay1;
                  component UnitDelay2 unitDelay2;
                  component UnitDelay3 unitDelay3;
                  component VERSION_INFO vERSION_INFO;
                  component Condition condition;
                  component Condition1 condition1;
                  connect condition.out1 -> switchBlock2.condition;
                  connect condition1.out1 -> switchBlock.condition;
                  connect logOp1.out1 -> condition.in1;
                  connect logOp.out1 -> condition1.in1;
                  connect mul1.out1 -> minMax1.in2;
                  connect mul.out1 -> minMax.in2;
                  connect minMax.abbiegelichtRechts_pcOut1 -> abbiegelichtRechts_pcOut1;
                  connect switchBlock.out1 -> minMax.in1;
                  connect constant5.out1 -> mul.in1;
                  connect constant10.out1 -> mul1.in1;
                  connect timer_REM1.mOut2 -> mul1.in2;
                  connect timer_REM.mOut2 -> mul.in2;
                  connect edgeFalling2.yOut1 -> rSFlipFlop.sIn1;
                  connect minMax1.abbiegelichtLinks_pcOut1 -> abbiegelichtLinks_pcOut2;
                  connect switchBlock2.out1 -> minMax1.in1;
                  connect unitDelay3.out1 -> rSFlipFlop3.rIn2;
                  connect logOp7.out1 -> unitDelay3.in1;
                  connect timer_REM1.yOut1 -> logOp7.in1;
                  connect constant9.out1 -> timer_REM1.iVIn3;
                  connect rSFlipFlop3.qOut1 -> timer_REM1.eIn1;
                  connect rSFlipFlop3.nOT_QOut2 -> terminator3.in1;
                  connect constant8.out1 -> switchBlock2.elseIn;
                  connect constant7.out1 -> switchBlock2.ifIn;
                  connect logOp6.out1 -> rSFlipFlop3.sIn1;
                  connect logOp6.out1 -> unitDelay2.in1;
                  connect unitDelay2.out1 -> rSFlipFlop2.rIn2;
                  connect timer_RE1.yOut1 -> logOp6.in1;
                  connect constant6.out1 -> timer_RE1.iVIn3;
                  connect logOp5.out1 -> timer_RE1.rIn2;
                  connect rSFlipFlop2.qOut1 -> timer_RE1.eIn1;
                  connect rSFlipFlop2.qOut1 -> logOp5.in1;
                  connect rSFlipFlop2.nOT_QOut2 -> terminator2.in1;
                  connect edgeFalling1.yOut1 -> rSFlipFlop2.sIn1;
                  connect falseBlock2.yOut1 -> edgeFalling1.iVIn3;
                  connect falseBlock2.yOut1 -> edgeFalling1.rIn2;
                  connect logOp1.out1 -> edgeFalling1.uIn1;
                  connect logOp1.out1 -> timer_REM1.rIn2;
                  connect unitDelay1.out1 -> rSFlipFlop1.rIn2;
                  connect logOp4.out1 -> unitDelay1.in1;
                  connect timer_REM.yOut1 -> logOp4.in1;
                  connect constant4.out1 -> timer_REM.iVIn3;
                  connect rSFlipFlop1.qOut1 -> timer_REM.eIn1;
                  connect rSFlipFlop1.nOT_QOut2 -> terminator1.in1;
                  connect constant3.out1 -> switchBlock.elseIn;
                  connect constant2.out1 -> switchBlock.ifIn;
                  connect logOp3.out1 -> unitDelay.in1;
                  connect logOp3.out1 -> rSFlipFlop1.sIn1;
                  connect unitDelay.out1 -> rSFlipFlop.rIn2;
                  connect timer_RE.yOut1 -> logOp3.in1;
                  connect constant1.out1 -> timer_RE.iVIn3;
                  connect logOp2.out1 -> timer_RE.rIn2;
                  connect rSFlipFlop.qOut1 -> timer_RE.eIn1;
                  connect rSFlipFlop.qOut1 -> logOp2.in1;
                  connect rSFlipFlop.nOT_QOut2 -> terminator.in1;
                  connect falseBlock1.yOut1 -> edgeFalling2.iVIn3;
                  connect falseBlock1.yOut1 -> edgeFalling2.rIn2;
                  connect logOp.out1 -> timer_REM.rIn2;
                  connect logOp.out1 -> edgeFalling2.uIn1;
                  connect abblendlicht_bIn1 -> logOp.in2;
                  connect abblendlicht_bIn1 -> logOp1.in1;
                  connect rBLinks_bIn3 -> logOp1.in3;
                  connect relOp.out1 -> logOp1.in2;
                  connect relOp.out1 -> logOp.in3;
                  connect fahrzeuggeschwindigkeit_kmhIn2 -> relOp.in1;
                  connect constant.out1 -> relOp.in2;
                  connect rBRechts_bIn4 -> logOp.in1;
                }
                <<Type="SubSystem">> component Abbiegelicht_Deactive {
                  ports
                    out Double abbiegelichtRechts_pcOut1,
                    out Double abbiegelichtLinks_pcOut2;
                  <<Type="Constant",Value="0">> component Constant {
                    ports
                      out Double abbiegelichtRechts_pcOut1;
                  }
                  <<Type="Constant",Value="0">> component Constant1 {
                    ports
                      out Double abbiegelichtLinks_pcOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant1 constant1;
                  component VERSION_INFO vERSION_INFO;
                  connect constant.abbiegelichtRechts_pcOut1 -> abbiegelichtRechts_pcOut1;
                  connect constant1.abbiegelichtLinks_pcOut1 -> abbiegelichtLinks_pcOut2;
                }
                <<Type="SubSystem">> component Abbiegelicht_Disabled {
                  ports
                    out Double abbiegelichtRechts_pcOut1,
                    out Double abbiegelichtLinks_pcOut2;
                  <<Type="Constant",Value="0">> component Constant {
                    ports
                      out Double abbiegelichtRechts_pcOut1;
                  }
                  <<Type="Constant",Value="0">> component Constant1 {
                    ports
                      out Double abbiegelichtLinks_pcOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant1 constant1;
                  component VERSION_INFO vERSION_INFO;
                  connect constant1.abbiegelichtLinks_pcOut1 -> abbiegelichtLinks_pcOut2;
                  connect constant.abbiegelichtRechts_pcOut1 -> abbiegelichtRechts_pcOut1;
                }
                <<Type="Constant",Value="1">> component DEMO_AUSSEN_Abbiegelicht {
                  ports
                    out Double out1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Type="Switch">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2>=8.5",Type="Condition">> component Condition {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2<8.5",Type="Condition">> component Condition1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock2 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 & u2>=8.5)&&!(u1 & u2<8.5)",Type="Condition">> component Condition2 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock3 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2>=8.5",Type="Condition">> component Condition3 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock4 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2<8.5",Type="Condition">> component Condition4 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock5 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 & u2>=8.5)&&!(u1 & u2<8.5)",Type="Condition">> component Condition5 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                component Abbiegelicht_Acitve abbiegelicht_Acitve;
                component Abbiegelicht_Deactive abbiegelicht_Deactive;
                component Abbiegelicht_Disabled abbiegelicht_Disabled;
                component DEMO_AUSSEN_Abbiegelicht dEMO_AUSSEN_Abbiegelicht;
                component VERSION_INFO vERSION_INFO;
                component SwitchBlock switchBlock;
                component Condition condition;
                component SwitchBlock1 switchBlock1;
                component Condition1 condition1;
                component SwitchBlock2 switchBlock2;
                component Condition2 condition2;
                component UnitDelay unitDelay;
                component SwitchBlock3 switchBlock3;
                component Condition3 condition3;
                component SwitchBlock4 switchBlock4;
                component Condition4 condition4;
                component SwitchBlock5 switchBlock5;
                component Condition5 condition5;
                component UnitDelay1 unitDelay1;
                connect switchBlock.out1 -> abbiegelichtRechts_pcOut1;
                connect switchBlock3.out1 -> abbiegelichtLinks_pcOut2;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition.in1;
                connect spannung_VIn5 -> condition.in2;
                connect condition.out1 -> switchBlock.condition;
                connect abbiegelicht_Acitve.abbiegelichtRechts_pcOut1 -> switchBlock.ifIn;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition1.in1;
                connect spannung_VIn5 -> condition1.in2;
                connect condition1.out1 -> switchBlock1.condition;
                connect abbiegelicht_Deactive.abbiegelichtRechts_pcOut1 -> switchBlock1.ifIn;
                connect switchBlock1.out1 -> switchBlock.elseIn;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition2.in1;
                connect spannung_VIn5 -> condition2.in2;
                connect condition2.out1 -> switchBlock2.condition;
                connect abbiegelicht_Disabled.abbiegelichtRechts_pcOut1 -> switchBlock2.ifIn;
                connect switchBlock2.out1 -> switchBlock1.elseIn;
                connect switchBlock.out1 -> unitDelay.valueIn;
                connect unitDelay.valueOut -> switchBlock2.elseIn;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition3.in1;
                connect spannung_VIn5 -> condition3.in2;
                connect condition3.out1 -> switchBlock3.condition;
                connect abbiegelicht_Acitve.abbiegelichtLinks_pcOut2 -> switchBlock3.ifIn;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition4.in1;
                connect spannung_VIn5 -> condition4.in2;
                connect condition4.out1 -> switchBlock4.condition;
                connect abbiegelicht_Deactive.abbiegelichtLinks_pcOut2 -> switchBlock4.ifIn;
                connect switchBlock4.out1 -> switchBlock3.elseIn;
                connect dEMO_AUSSEN_Abbiegelicht.out1 -> condition5.in1;
                connect spannung_VIn5 -> condition5.in2;
                connect condition5.out1 -> switchBlock5.condition;
                connect abbiegelicht_Disabled.abbiegelichtLinks_pcOut2 -> switchBlock5.ifIn;
                connect switchBlock5.out1 -> switchBlock4.elseIn;
                connect switchBlock3.out1 -> unitDelay1.valueIn;
                connect unitDelay1.valueOut -> switchBlock5.elseIn;
                connect rBLinks_bIn3 -> abbiegelicht_Acitve.rBLinks_bIn3;
                connect fahrzeuggeschwindigkeit_kmhIn2 -> abbiegelicht_Acitve.fahrzeuggeschwindigkeit_kmhIn2;
                connect rBRechts_bIn4 -> abbiegelicht_Acitve.rBRechts_bIn4;
                connect abblendlicht_bIn1 -> abbiegelicht_Acitve.abblendlicht_bIn1;
              }
              <<Type="SubSystem">> component AdaptivesFernlicht {
                ports
                  in Double lichtdrehschalter_stIn1,
                  in Boolean entFahrzeug_bIn2,
                  in Boolean fernlicht_bIn3,
                  in Double fzggeschw_kmhIn4,
                  in Double spannung_VIn5,
                  out Double ausleuchtungRechts_mOut1,
                  out Double ausleuchtungLinks_mOut2;
                <<Type="Constant",Value="1">> component DEMO_AUSSEN_AdaptivesFernlicht {
                  ports
                    out Double out1;
                }
                <<Type="SubSystem">> component Fernlicht_Adaptive {
                  ports
                    in Double fzggeschw_kmhIn1,
                    in Boolean fernlicht_bIn2,
                    in Boolean entFahrzeug_bIn3,
                    out Double ausleuchtungRechts_mOut1,
                    out Double ausleuchtungLinks_mOut2;
                  <<Type="Constant",Value="-0.5">> component Constant1 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0.5">> component Constant2 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="65">> component Constant3 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="-2">> component Constant4 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="1.2">> component Constant5 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="2">> component Constant6 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="1">> component Constant7 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant8 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant9 {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component EdgeRising {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect logOp_N.out1 -> logOp_A.in2;
                    connect logOp_A.out1 -> yOut1;
                    connect uIn1 -> logOp_A.in1;
                    connect uIn1 -> memory_U.in1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component EdgeRising1 {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect iVIn3 -> switch_R.ifIn;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect uIn1 -> memory_U.in1;
                    connect uIn1 -> logOp_A.in1;
                    connect logOp_A.out1 -> yOut1;
                    connect logOp_N.out1 -> logOp_A.in2;
                  }
                  <<Type="SubSystem">> component FalseBlock {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Type="SubSystem">> component FalseBlock1 {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator3 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator4 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<BreakpointsForDimension1="[30,60,90,120,170,250]",Type="Lookup_n-D",NumberOfTableDimensions="1",Table="[100,120,140,190,300,300]",BreakpointsSpecification="Explicit values">> component LookUpTable {
                    ports
                      in Double in1,
                      out Double out1;
                    effect in1 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul2 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul3 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Sum",ListOfSigns="+-">> component Sum {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Sum",ListOfSigns="++">> component Sum1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Sum",ListOfSigns="+-">> component Sum2 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Sum",ListOfSigns="++">> component Sum4 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double ausleuchtungRechts_mOut1;
                    effect ifIn -> ausleuchtungRechts_mOut1;
                    effect condition -> ausleuchtungRechts_mOut1;
                    effect elseIn -> ausleuchtungRechts_mOut1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double out1;
                    effect ifIn -> out1;
                    effect condition -> out1;
                    effect elseIn -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double ausleuchtungLinks_mOut1;
                    effect ifIn -> ausleuchtungLinks_mOut1;
                    effect condition -> ausleuchtungLinks_mOut1;
                    effect elseIn -> ausleuchtungLinks_mOut1;
                  }
                  <<Type="Terminator">> component Terminator {
                    ports
                      in Boolean in1;
                  }
                  <<Type="Terminator">> component Terminator1 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="SubSystem">> component Timer_REM {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1,
                      out Double mOut2;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_C.condition;
                    connect logOp_C.out1 -> condition.in1;
                    connect logOp_R.out1 -> condition1.in1;
                    connect rIn2 -> logOp_R.in2;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect eIn1 -> logOp_C.in1;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect relOp_Y.out1 -> yOut1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> mOut2;
                    connect zero_Y.out1 -> relOp_Y.in2;
                  }
                  <<Type="SubSystem">> component Timer_REM1 {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1,
                      out Double mOut2;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_C.condition;
                    connect logOp_C.out1 -> condition.in1;
                    connect logOp_R.out1 -> condition1.in1;
                    connect rIn2 -> logOp_R.in2;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect eIn1 -> logOp_C.in1;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect relOp_Y.out1 -> yOut1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> mOut2;
                    connect zero_Y.out1 -> relOp_Y.in2;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition2 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component Constant1 constant1;
                  component Constant2 constant2;
                  component Constant3 constant3;
                  component Constant4 constant4;
                  component Constant5 constant5;
                  component Constant6 constant6;
                  component Constant7 constant7;
                  component Constant8 constant8;
                  component Constant9 constant9;
                  component EdgeRising edgeRising;
                  component EdgeRising1 edgeRising1;
                  component FalseBlock falseBlock;
                  component FalseBlock1 falseBlock1;
                  component LogicalOperator logicalOperator;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator2 logicalOperator2;
                  component LogicalOperator3 logicalOperator3;
                  component LogicalOperator4 logicalOperator4;
                  component LookUpTable lookUpTable;
                  component Mul mul;
                  component Mul1 mul1;
                  component Mul2 mul2;
                  component Mul3 mul3;
                  component Sum sum;
                  component Sum1 sum1;
                  component Sum2 sum2;
                  component Sum4 sum4;
                  component SwitchBlock1 switchBlock1;
                  component SwitchBlock2 switchBlock2;
                  component SwitchBlock3 switchBlock3;
                  component Terminator terminator;
                  component Terminator1 terminator1;
                  component Timer_REM timer_REM;
                  component Timer_REM1 timer_REM1;
                  component VERSION_INFO vERSION_INFO;
                  component Condition condition;
                  component Condition1 condition1;
                  component Condition2 condition2;
                  connect condition2.out1 -> switchBlock1.condition;
                  connect condition1.out1 -> switchBlock3.condition;
                  connect condition.out1 -> switchBlock2.condition;
                  connect entFahrzeug_bIn3 -> condition.in1;
                  connect fernlicht_bIn2 -> condition1.in1;
                  connect fernlicht_bIn2 -> condition2.in1;
                  connect fernlicht_bIn2 -> edgeRising.uIn1;
                  connect fernlicht_bIn2 -> logicalOperator2.in1;
                  connect constant9.out1 -> switchBlock3.elseIn;
                  connect switchBlock3.ausleuchtungLinks_mOut1 -> ausleuchtungLinks_mOut2;
                  connect logicalOperator4.out1 -> timer_REM1.eIn1;
                  connect logicalOperator.out1 -> timer_REM1.rIn2;
                  connect constant8.out1 -> switchBlock1.elseIn;
                  connect switchBlock1.ausleuchtungRechts_mOut1 -> ausleuchtungRechts_mOut1;
                  connect switchBlock2.out1 -> switchBlock1.ifIn;
                  connect switchBlock2.out1 -> switchBlock3.ifIn;
                  connect edgeRising1.yOut1 -> logicalOperator.in2;
                  connect falseBlock1.yOut1 -> edgeRising1.iVIn3;
                  connect logicalOperator1.out1 -> edgeRising1.rIn2;
                  connect fzggeschw_kmhIn1 -> lookUpTable.in1;
                  connect edgeRising.yOut1 -> logicalOperator.in1;
                  connect falseBlock.yOut1 -> edgeRising.iVIn3;
                  connect logicalOperator2.out1 -> edgeRising.rIn2;
                  connect entFahrzeug_bIn3 -> timer_REM.eIn1;
                  connect entFahrzeug_bIn3 -> logicalOperator3.in1;
                  connect entFahrzeug_bIn3 -> logicalOperator1.in1;
                  connect entFahrzeug_bIn3 -> logicalOperator4.in1;
                  connect entFahrzeug_bIn3 -> edgeRising1.uIn1;
                  connect mul3.out1 -> switchBlock2.elseIn;
                  connect sum2.out1 -> switchBlock2.ifIn;
                  connect sum4.out1 -> mul3.in2;
                  connect timer_REM1.mOut2 -> mul2.in1;
                  connect constant7.out1 -> sum4.in1;
                  connect mul2.out1 -> sum4.in2;
                  connect constant1.out1 -> mul2.in2;
                  connect timer_REM1.yOut1 -> terminator1.in1;
                  connect constant6.out1 -> timer_REM1.iVIn3;
                  connect timer_REM.yOut1 -> terminator.in1;
                  connect sum.out1 -> mul1.in1;
                  connect mul1.out1 -> sum2.in2;
                  connect sum1.out1 -> mul1.in2;
                  connect constant5.out1 -> sum1.in1;
                  connect mul.out1 -> sum1.in2;
                  connect constant4.out1 -> mul.in2;
                  connect constant3.out1 -> sum.in2;
                  connect lookUpTable.out1 -> sum.in1;
                  connect lookUpTable.out1 -> sum2.in1;
                  connect lookUpTable.out1 -> mul3.in1;
                  connect timer_REM.mOut2 -> mul.in1;
                  connect constant2.out1 -> timer_REM.iVIn3;
                  connect logicalOperator3.out1 -> timer_REM.rIn2;
                }
                <<Type="SubSystem">> component Fernlicht_Deactive {
                  ports
                    out Double ausleuchtungRechts_mOut1,
                    out Double ausleuchtungLinks_mOut2;
                  <<Type="Constant",Value="0">> component Constant {
                    ports
                      out Double ausleuchtungRechts_mOut1;
                  }
                  <<Type="Constant",Value="0">> component Constant2 {
                    ports
                      out Double ausleuchtungLinks_mOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant2 constant2;
                  component VERSION_INFO vERSION_INFO;
                  connect constant2.ausleuchtungLinks_mOut1 -> ausleuchtungLinks_mOut2;
                  connect constant.ausleuchtungRechts_mOut1 -> ausleuchtungRechts_mOut1;
                }
                <<Type="SubSystem">> component Fernlicht_Manuell {
                  ports
                    out Double ausleuchtungRechts_mOut1,
                    out Double ausleuchtungLinks_mOut2;
                  <<Type="Constant",Value="220">> component Constant {
                    ports
                      out Double ausleuchtungRechts_mOut1;
                  }
                  <<Type="Constant",Value="220">> component Constant2 {
                    ports
                      out Double ausleuchtungLinks_mOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant2 constant2;
                  component VERSION_INFO vERSION_INFO;
                  connect constant2.ausleuchtungLinks_mOut1 -> ausleuchtungLinks_mOut2;
                  connect constant.ausleuchtungRechts_mOut1 -> ausleuchtungRechts_mOut1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Type="Switch">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2 & u3>=30 & u4>=1 & u4<2 & u5>8.5",Type="Condition">> component Condition {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u2 & (u4<1 | u4 >=2 | u5 <= 8.5)",Type="Condition">> component Condition1 {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock2 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 & u2 & u3>=30 & u4>=1 & u4<2 & u5>8.5)&&!(u2 & (u4<1 | u4 >=2 | u5 <= 8.5))",Type="Condition">> component Condition2 {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock3 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 & u2 & u3>=30 & u4>=1 & u4<2 & u5>8.5",Type="Condition">> component Condition3 {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock4 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u2 & (u4<1 | u4 >=2 | u5 <= 8.5)",Type="Condition">> component Condition4 {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock5 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 & u2 & u3>=30 & u4>=1 & u4<2 & u5>8.5)&&!(u2 & (u4<1 | u4 >=2 | u5 <= 8.5))",Type="Condition">> component Condition5 {
                  ports
                    in Double in1,
                    in Boolean in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                component DEMO_AUSSEN_AdaptivesFernlicht dEMO_AUSSEN_AdaptivesFernlicht;
                component Fernlicht_Adaptive fernlicht_Adaptive;
                component Fernlicht_Deactive fernlicht_Deactive;
                component Fernlicht_Manuell fernlicht_Manuell;
                component VERSION_INFO vERSION_INFO;
                component SwitchBlock switchBlock;
                component Condition condition;
                component SwitchBlock1 switchBlock1;
                component Condition1 condition1;
                component SwitchBlock2 switchBlock2;
                component Condition2 condition2;
                component UnitDelay unitDelay;
                component SwitchBlock3 switchBlock3;
                component Condition3 condition3;
                component SwitchBlock4 switchBlock4;
                component Condition4 condition4;
                component SwitchBlock5 switchBlock5;
                component Condition5 condition5;
                component UnitDelay1 unitDelay1;
                connect switchBlock.out1 -> ausleuchtungRechts_mOut1;
                connect switchBlock3.out1 -> ausleuchtungLinks_mOut2;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition.in1;
                connect fernlicht_bIn3 -> condition.in2;
                connect fzggeschw_kmhIn4 -> condition.in3;
                connect lichtdrehschalter_stIn1 -> condition.in4;
                connect spannung_VIn5 -> condition.in5;
                connect condition.out1 -> switchBlock.condition;
                connect fernlicht_Adaptive.ausleuchtungRechts_mOut1 -> switchBlock.ifIn;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition1.in1;
                connect fernlicht_bIn3 -> condition1.in2;
                connect fzggeschw_kmhIn4 -> condition1.in3;
                connect lichtdrehschalter_stIn1 -> condition1.in4;
                connect spannung_VIn5 -> condition1.in5;
                connect condition1.out1 -> switchBlock1.condition;
                connect fernlicht_Manuell.ausleuchtungRechts_mOut1 -> switchBlock1.ifIn;
                connect switchBlock1.out1 -> switchBlock.elseIn;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition2.in1;
                connect fernlicht_bIn3 -> condition2.in2;
                connect fzggeschw_kmhIn4 -> condition2.in3;
                connect lichtdrehschalter_stIn1 -> condition2.in4;
                connect spannung_VIn5 -> condition2.in5;
                connect condition2.out1 -> switchBlock2.condition;
                connect fernlicht_Deactive.ausleuchtungRechts_mOut1 -> switchBlock2.ifIn;
                connect switchBlock2.out1 -> switchBlock1.elseIn;
                connect switchBlock.out1 -> unitDelay.valueIn;
                connect unitDelay.valueOut -> switchBlock2.elseIn;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition3.in1;
                connect fernlicht_bIn3 -> condition3.in2;
                connect fzggeschw_kmhIn4 -> condition3.in3;
                connect lichtdrehschalter_stIn1 -> condition3.in4;
                connect spannung_VIn5 -> condition3.in5;
                connect condition3.out1 -> switchBlock3.condition;
                connect fernlicht_Adaptive.ausleuchtungLinks_mOut2 -> switchBlock3.ifIn;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition4.in1;
                connect fernlicht_bIn3 -> condition4.in2;
                connect fzggeschw_kmhIn4 -> condition4.in3;
                connect lichtdrehschalter_stIn1 -> condition4.in4;
                connect spannung_VIn5 -> condition4.in5;
                connect condition4.out1 -> switchBlock4.condition;
                connect fernlicht_Manuell.ausleuchtungLinks_mOut2 -> switchBlock4.ifIn;
                connect switchBlock4.out1 -> switchBlock3.elseIn;
                connect dEMO_AUSSEN_AdaptivesFernlicht.out1 -> condition5.in1;
                connect fernlicht_bIn3 -> condition5.in2;
                connect fzggeschw_kmhIn4 -> condition5.in3;
                connect lichtdrehschalter_stIn1 -> condition5.in4;
                connect spannung_VIn5 -> condition5.in5;
                connect condition5.out1 -> switchBlock5.condition;
                connect fernlicht_Deactive.ausleuchtungLinks_mOut2 -> switchBlock5.ifIn;
                connect switchBlock5.out1 -> switchBlock4.elseIn;
                connect switchBlock3.out1 -> unitDelay1.valueIn;
                connect unitDelay1.valueOut -> switchBlock5.elseIn;
                connect fzggeschw_kmhIn4 -> fernlicht_Adaptive.fzggeschw_kmhIn1;
                connect fernlicht_bIn3 -> fernlicht_Adaptive.fernlicht_bIn2;
                connect entFahrzeug_bIn2 -> fernlicht_Adaptive.entFahrzeug_bIn3;
              }
              <<Type="Constant",Value="1">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant10 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="3">> component Constant11 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="1">> component Constant5 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="100">> component Constant6 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component Constant7 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="2">> component Constant8 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="100">> component Constant9 {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="0">> component DEMO_AUSSEN_Sonderschutz {
                ports
                  out Boolean out1;
              }
              <<Type="Constant",Value="0">> component DEMO_AUSSEN_Sonderschutz1 {
                ports
                  out Boolean out1;
              }
              <<Type="SubSystem">> component EdgeFalling1 {
                ports
                  in Boolean uIn1,
                  in Boolean rIn2,
                  in Boolean iVIn3,
                  out Boolean yOut1;
                <<Operator="AND",Type="Logic">> component LogOp_A {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator="NOT",Type="Logic">> component LogOp_N {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component LogOp_A logOp_A;
                component LogOp_N logOp_N;
                component Memory_U memory_U;
                component Switch_R switch_R;
                component Condition condition;
                connect condition.out1 -> switch_R.condition;
                connect rIn2 -> condition.in1;
                connect switch_R.out1 -> logOp_A.in2;
                connect uIn1 -> logOp_N.in1;
                connect uIn1 -> memory_U.in1;
                connect logOp_N.out1 -> logOp_A.in1;
                connect logOp_A.out1 -> yOut1;
                connect memory_U.out1 -> switch_R.elseIn;
                connect iVIn3 -> switch_R.ifIn;
              }
              <<Type="SubSystem">> component FalseBlock5 {
                ports
                  out Boolean yOut1;
                <<Type="Constant",Value="0">> component Zero {
                  ports
                    out Boolean out1;
                }
                component Zero zero;
                connect zero.out1 -> yOut1;
              }
              <<Operator="OR",Type="Logic">> component LogOp {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean abblendlicht_bOut1;
                effect in1 -> abblendlicht_bOut1;
                effect in2 -> abblendlicht_bOut1;
              }
              <<Operator="AND",Type="Logic">> component LogicalOperator {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="NOT",Type="Logic">> component LogicalOperator1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="NOT",Type="Logic">> component LogicalOperator13 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Operator="OR",Type="Logic">> component LogicalOperator14 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
                effect in3 -> out1;
              }
              <<Operator="AND",Type="Logic">> component LogicalOperator2 {
                ports
                  in Boolean in1,
                  in Boolean in2,
                  in Boolean in3,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
                effect in3 -> out1;
              }
              <<Function="max",Type="MinMax">> component MinMax {
                ports
                  in Double in1,
                  in Double in2,
                  in Double in3,
                  out Double abblendlichtLinks_pcOut1;
                effect in1 -> abblendlichtLinks_pcOut1;
                effect in2 -> abblendlichtLinks_pcOut1;
                effect in3 -> abblendlichtLinks_pcOut1;
              }
              <<Function="max",Type="MinMax">> component MinMax1 {
                ports
                  in Double in1,
                  in Double in2,
                  in Double in3,
                  out Double abblendlichtRechts_pcOut1;
                effect in1 -> abblendlichtRechts_pcOut1;
                effect in2 -> abblendlichtRechts_pcOut1;
                effect in3 -> abblendlichtRechts_pcOut1;
              }
              <<Function="min",Type="MinMax">> component MinMax2 {
                ports
                  in Double abblendlichtRechts_pcIn1,
                  in Double in2,
                  out Double abblendlichtRechts_pcOut1;
                effect abblendlichtRechts_pcIn1 -> abblendlichtRechts_pcOut1;
                effect in2 -> abblendlichtRechts_pcOut1;
              }
              <<Function="min",Type="MinMax">> component MinMax3 {
                ports
                  in Double abblendlichtLinks_pcIn1,
                  in Double in2,
                  out Double abblendlichtLinks_pcOut1;
                effect abblendlichtLinks_pcIn1 -> abblendlichtLinks_pcOut1;
                effect in2 -> abblendlichtLinks_pcOut1;
              }
              <<Function="min",Type="MinMax">> component MinMax4 {
                ports
                  in Double in1,
                  in Double in2,
                  out Double abbiegelichtRechts_pcOut1;
                effect in1 -> abbiegelichtRechts_pcOut1;
                effect in2 -> abbiegelichtRechts_pcOut1;
              }
              <<Function="min",Type="MinMax">> component MinMax5 {
                ports
                  in Double in1,
                  in Double in2,
                  out Double abbiegelichtLinks_pcOut1;
                effect in1 -> abbiegelichtLinks_pcOut1;
                effect in2 -> abbiegelichtLinks_pcOut1;
              }
              <<Type="SubSystem">> component RSFlipFlop4 {
                ports
                  in Boolean sIn1,
                  in Boolean rIn2,
                  out Boolean qOut1,
                  out Boolean nOT_QOut2;
                <<Operator="NOT",Type="Logic">> component LogOp_N {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="Constant",Value="1">> component One_S {
                  ports
                    out Boolean out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Constant",Value="0">> component Zero_R {
                  ports
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component LogOp_N logOp_N;
                component Memory_Q memory_Q;
                component One_S one_S;
                component Switch_R switch_R;
                component Switch_S switch_S;
                component Zero_R zero_R;
                component Condition condition;
                component Condition1 condition1;
                connect condition.out1 -> switch_R.condition;
                connect condition1.out1 -> switch_S.condition;
                connect rIn2 -> condition.in1;
                connect sIn1 -> condition1.in1;
                connect logOp_N.out1 -> nOT_QOut2;
                connect zero_R.out1 -> switch_R.ifIn;
                connect switch_R.out1 -> logOp_N.in1;
                connect switch_R.out1 -> qOut1;
                connect switch_R.out1 -> memory_Q.in1;
                connect one_S.out1 -> switch_S.ifIn;
                connect switch_S.out1 -> switch_R.elseIn;
                connect memory_Q.out1 -> switch_S.elseIn;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Operator="==",Type="RelationalOperator">> component RelationalOperator {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                effect in1 -> out1;
                effect in2 -> out1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
                effect ifIn -> out1;
                effect condition -> out1;
                effect elseIn -> out1;
              }
              <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                ports
                  in Double ifIn,
                  in Boolean condition,
                  in Double elseIn,
                  out Double out1;
                effect ifIn -> out1;
                effect condition -> out1;
                effect elseIn -> out1;
              }
              <<Type="SubSystem">> component Tagfahrlicht {
                ports
                  in Boolean motor_bIn1,
                  in Boolean schluessel_bIn2,
                  in Boolean blinkenLinksAktiv_bIn3,
                  in Boolean blinkenRechtsAktiv_bIn4,
                  out Double tagfahrlichtLinks_pcOut1,
                  out Double tagfahrlichtRechts_pcOut2,
                  out Boolean tagfahrlicht_bOut3;
                <<Type="Constant",Value="0">> component Constant {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="100">> component Constant1 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="0.5">> component Constant2 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="0.5">> component Constant3 {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="0">> component DEMO_AUSSEN_DimmbaresTFL {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="1">> component DEMO_AUSSEN_Tagfahrlicht {
                  ports
                    out Double out1;
                }
                <<Type="SubSystem">> component FalseBlock8 {
                  ports
                    out Boolean yOut1;
                  <<Type="Constant",Value="0">> component Zero {
                    ports
                      out Boolean out1;
                  }
                  component Zero zero;
                  connect zero.out1 -> yOut1;
                }
                <<Operator="NOT",Type="Logic">> component LogicalOperator3 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Type="Product",Inputs="**">> component Mul {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Product",Inputs="**">> component Mul1 {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="SubSystem">> component RSFlipFlop {
                  ports
                    in Boolean sIn1,
                    in Boolean rIn2,
                    out Boolean qOut1,
                    out Boolean nOT_QOut2;
                  <<Operator="NOT",Type="Logic">> component LogOp_N {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="Constant",Value="1">> component One_S {
                    ports
                      out Boolean out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                    effect ifIn -> out1;
                    effect condition -> out1;
                    effect elseIn -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                    ports
                      in Boolean ifIn,
                      in Boolean condition,
                      in Boolean elseIn,
                      out Boolean out1;
                    effect ifIn -> out1;
                    effect condition -> out1;
                    effect elseIn -> out1;
                  }
                  <<Type="Constant",Value="0">> component Zero_R {
                    ports
                      out Boolean out1;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component LogOp_N logOp_N;
                  component Memory_Q memory_Q;
                  component One_S one_S;
                  component Switch_R switch_R;
                  component Switch_S switch_S;
                  component Zero_R zero_R;
                  component Condition condition;
                  component Condition1 condition1;
                  connect condition1.out1 -> switch_S.condition;
                  connect condition.out1 -> switch_R.condition;
                  connect rIn2 -> condition.in1;
                  connect sIn1 -> condition1.in1;
                  connect memory_Q.out1 -> switch_S.elseIn;
                  connect switch_S.out1 -> switch_R.elseIn;
                  connect one_S.out1 -> switch_S.ifIn;
                  connect switch_R.out1 -> memory_Q.in1;
                  connect switch_R.out1 -> qOut1;
                  connect switch_R.out1 -> logOp_N.in1;
                  connect zero_R.out1 -> switch_R.ifIn;
                  connect logOp_N.out1 -> nOT_QOut2;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double tagfahrlichtLinks_pcOut1;
                  effect ifIn -> tagfahrlichtLinks_pcOut1;
                  effect condition -> tagfahrlichtLinks_pcOut1;
                  effect elseIn -> tagfahrlichtLinks_pcOut1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock4 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock5 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double tagfahrlichtRechts_pcOut1;
                  effect ifIn -> tagfahrlichtRechts_pcOut1;
                  effect condition -> tagfahrlichtRechts_pcOut1;
                  effect elseIn -> tagfahrlichtRechts_pcOut1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock6 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Terminator">> component Terminator {
                  ports
                    in Boolean in1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition2 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition3 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition4 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition5 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                component Constant constant;
                component Constant1 constant1;
                component Constant2 constant2;
                component Constant3 constant3;
                component DEMO_AUSSEN_DimmbaresTFL dEMO_AUSSEN_DimmbaresTFL;
                component DEMO_AUSSEN_Tagfahrlicht dEMO_AUSSEN_Tagfahrlicht;
                component FalseBlock8 falseBlock8;
                component LogicalOperator3 logicalOperator3;
                component Mul mul;
                component Mul1 mul1;
                component RSFlipFlop rSFlipFlop;
                component SwitchBlock1 switchBlock1;
                component SwitchBlock2 switchBlock2;
                component SwitchBlock3 switchBlock3;
                component SwitchBlock4 switchBlock4;
                component SwitchBlock5 switchBlock5;
                component SwitchBlock6 switchBlock6;
                component Terminator terminator;
                component VERSION_INFO vERSION_INFO;
                component Condition condition;
                component Condition1 condition1;
                component Condition2 condition2;
                component Condition3 condition3;
                component Condition4 condition4;
                component Condition5 condition5;
                connect condition4.out1 -> switchBlock6.condition;
                connect condition1.out1 -> switchBlock4.condition;
                connect condition5.out1 -> switchBlock3.condition;
                connect condition2.out1 -> switchBlock5.condition;
                connect condition.out1 -> switchBlock1.condition;
                connect condition3.out1 -> switchBlock2.condition;
                connect switchBlock2.out1 -> condition.in1;
                connect blinkenRechtsAktiv_bIn4 -> condition1.in1;
                connect dEMO_AUSSEN_DimmbaresTFL.out1 -> condition2.in1;
                connect dEMO_AUSSEN_Tagfahrlicht.out1 -> condition3.in1;
                connect blinkenLinksAktiv_bIn3 -> condition4.in1;
                connect dEMO_AUSSEN_DimmbaresTFL.out1 -> condition5.in1;
                connect switchBlock3.tagfahrlichtLinks_pcOut1 -> tagfahrlichtLinks_pcOut1;
                connect switchBlock4.out1 -> switchBlock5.ifIn;
                connect switchBlock6.out1 -> switchBlock3.ifIn;
                connect mul1.out1 -> switchBlock4.ifIn;
                connect constant3.out1 -> mul1.in2;
                connect mul.out1 -> switchBlock6.ifIn;
                connect constant2.out1 -> mul.in2;
                connect switchBlock5.tagfahrlichtRechts_pcOut1 -> tagfahrlichtRechts_pcOut2;
                connect switchBlock1.out1 -> mul.in1;
                connect switchBlock1.out1 -> switchBlock6.elseIn;
                connect switchBlock1.out1 -> mul1.in1;
                connect switchBlock1.out1 -> switchBlock4.elseIn;
                connect switchBlock1.out1 -> switchBlock5.elseIn;
                connect switchBlock1.out1 -> switchBlock3.elseIn;
                connect switchBlock2.out1 -> tagfahrlicht_bOut3;
                connect constant1.out1 -> switchBlock1.ifIn;
                connect constant.out1 -> switchBlock1.elseIn;
                connect falseBlock8.yOut1 -> switchBlock2.elseIn;
                connect rSFlipFlop.qOut1 -> switchBlock2.ifIn;
                connect motor_bIn1 -> rSFlipFlop.sIn1;
                connect rSFlipFlop.nOT_QOut2 -> terminator.in1;
                connect logicalOperator3.out1 -> rSFlipFlop.rIn2;
                connect schluessel_bIn2 -> logicalOperator3.in1;
              }
              <<Type="Terminator">> component Terminator4 {
                ports
                  in Boolean in1;
              }
              <<Type="SubSystem">> component TimerRetrigger_RE {
                ports
                  in Boolean eIn1,
                  in Boolean rIn2,
                  in Double iVIn3,
                  out Boolean yOut1;
                <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                  ports
                    out Double out1;
                }
                <<Operator="AND",Type="Logic">> component LogOp_C {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                  ports
                    in Double in1,
                    in Double in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                  ports
                    in Double in1,
                    in Double in2,
                    out Double out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                  effect ifIn -> out1;
                  effect condition -> out1;
                  effect elseIn -> out1;
                }
                <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                  ports
                    out Double out1;
                }
                <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                  ports
                    out Double out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                component Const_dTeff const_dTeff;
                component LogOp_C logOp_C;
                component Memory_C memory_C;
                component RelOp_C relOp_C;
                component RelOp_Y relOp_Y;
                component Sum_C sum_C;
                component Switch_C switch_C;
                component Switch_R switch_R;
                component Zero_C zero_C;
                component Zero_Y zero_Y;
                component Condition condition;
                component Condition1 condition1;
                connect condition1.out1 -> switch_R.condition;
                connect condition.out1 -> switch_C.condition;
                connect logOp_C.out1 -> condition.in1;
                connect rIn2 -> condition1.in1;
                connect memory_C.out1 -> relOp_C.in1;
                connect memory_C.out1 -> sum_C.in1;
                connect memory_C.out1 -> switch_C.elseIn;
                connect const_dTeff.out1 -> sum_C.in2;
                connect sum_C.out1 -> switch_C.ifIn;
                connect switch_C.out1 -> switch_R.elseIn;
                connect eIn1 -> logOp_C.in1;
                connect zero_C.out1 -> relOp_C.in2;
                connect relOp_C.out1 -> logOp_C.in2;
                connect zero_Y.out1 -> relOp_Y.in2;
                connect switch_R.out1 -> relOp_Y.in1;
                connect switch_R.out1 -> memory_C.in1;
                connect relOp_Y.out1 -> yOut1;
                connect iVIn3 -> switch_R.ifIn;
              }
              <<Type="SubSystem">> component Ueberspannungsschutz {
                ports
                  in Double abblendlichtLinks_pcIn1,
                  in Double abblendlichtRechts_pcIn2,
                  in Double ausleuchtungRechts_mIn3,
                  in Double ausleuchtungLinks_mIn4,
                  in Double abbiegelichtRechts_pcIn5,
                  in Double abbiegelichtLinks_pcIn6,
                  in Double spannung_VIn7,
                  out Double abblendlichtLinks_pcOut1,
                  out Double abblendlichtRechts_pcOut2,
                  out Double ausleuchtungRechts_mOut3,
                  out Double ausleuchtungLinks_mOut4,
                  out Double abbiegelichtRechts_pcOut5,
                  out Double abbiegelichtLinks_pcOut6;
                <<Type="Gain",Gain="1">> component Abbiegelicht_Links_Helligkeit {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="Gain",Gain="1">> component Abbiegelicht_Rechts_Helligkeit {
                  ports
                    in Double in1,
                    out Double out1;
                  effect in1 -> out1;
                }
                <<Type="SubSystem">> component Keine_Ueberspannung {
                  ports
                    in Double abblendlichtLinks_pcIn1,
                    in Double abblendlichtRechts_pcIn2,
                    in Double ausleuchtungRechts_mIn3,
                    in Double ausleuchtungLinks_mIn4,
                    in Double abbiegelichtRechts_pcIn5,
                    in Double abbiegelichtLinks_pcIn6,
                    out Double abblendlichtLinks_pcOut1,
                    out Double abblendlichtRechts_pcOut2,
                    out Double ausleuchtung_mOut3,
                    out Double ausleuchtungRechts_mOut4,
                    out Double abbiegelichtRechts_pcOut5,
                    out Double abbiegelichtLinks_pcOut6;
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component VERSION_INFO vERSION_INFO;
                  connect abblendlichtLinks_pcIn1 -> abblendlichtLinks_pcOut1;
                  connect ausleuchtungLinks_mIn4 -> ausleuchtungRechts_mOut4;
                  connect abbiegelichtLinks_pcIn6 -> abbiegelichtLinks_pcOut6;
                  connect abbiegelichtRechts_pcIn5 -> abbiegelichtRechts_pcOut5;
                  connect ausleuchtungRechts_mIn3 -> ausleuchtung_mOut3;
                  connect abblendlichtRechts_pcIn2 -> abblendlichtRechts_pcOut2;
                }
                <<Type="Terminator">> component Terminator {
                  ports
                    in Double in1;
                }
                <<Type="Terminator">> component Terminator1 {
                  ports
                    in Double in1;
                }
                <<Type="SubSystem">> component Ueberspannung {
                  ports
                    in Double spannung_VIn1,
                    in Double abblendlichtLinks_pcIn2,
                    in Double abblendlichtRechts_pcIn3,
                    in Double ausleuchtung_mIn4,
                    in Double ausleuchtungLinks_mIn5,
                    in Double abbiegelichtRechts_pcIn6,
                    in Double abbiegelichtLinks_pcIn7,
                    out Double abblendlichtLinks_pcOut1,
                    out Double abblendlichtRechts_pcOut2,
                    out Double ausleuchtungRechts_mOut3,
                    out Double ausleuchtungLinks_mOut4,
                    out Double abbiegelichtRechts_pcOut5,
                    out Double abbiegelichtLinks_pcOut6;
                  <<Type="Constant",Value="100">> component Constant {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="14.5">> component Constant1 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="20">> component Constant2 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant4 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant5 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant6 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant7 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant8 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant9 {
                    ports
                      out Double out1;
                  }
                  <<Type="Product",Inputs="*/">> component Div1 {
                    ports
                      in Double abblendlichtLinks_pcIn1,
                      in Double in2,
                      out Double abblendlichtLinks_pcOut1;
                    effect abblendlichtLinks_pcIn1 -> abblendlichtLinks_pcOut1;
                    effect in2 -> abblendlichtLinks_pcOut1;
                  }
                  <<Type="Product",Inputs="*/">> component Div2 {
                    ports
                      in Double abblendlichtRechts_pcIn1,
                      in Double in2,
                      out Double abblendlichtRechts_pcOut1;
                    effect abblendlichtRechts_pcIn1 -> abblendlichtRechts_pcOut1;
                    effect in2 -> abblendlichtRechts_pcOut1;
                  }
                  <<Type="Product",Inputs="*/">> component Div3 {
                    ports
                      in Double ausleuchtungRechts_mIn1,
                      in Double in2,
                      out Double ausleuchtungRechts_mOut1;
                    effect ausleuchtungRechts_mIn1 -> ausleuchtungRechts_mOut1;
                    effect in2 -> ausleuchtungRechts_mOut1;
                  }
                  <<Type="Product",Inputs="*/">> component Div4 {
                    ports
                      in Double ausleuchtungLinks_mIn1,
                      in Double in2,
                      out Double ausleuchtungLinks_mOut1;
                    effect ausleuchtungLinks_mIn1 -> ausleuchtungLinks_mOut1;
                    effect in2 -> ausleuchtungLinks_mOut1;
                  }
                  <<Type="Product",Inputs="*/">> component Div5 {
                    ports
                      in Double abbiegelichtRechts_pcIn1,
                      in Double in2,
                      out Double abbiegelichtRechts_pcOut1;
                    effect abbiegelichtRechts_pcIn1 -> abbiegelichtRechts_pcOut1;
                    effect in2 -> abbiegelichtRechts_pcOut1;
                  }
                  <<Type="Product",Inputs="*/">> component Div6 {
                    ports
                      in Double abbiegelichtLinks_pcIn1,
                      in Double in2,
                      out Double abbiegelichtLinks_pcOut1;
                    effect abbiegelichtLinks_pcIn1 -> abbiegelichtLinks_pcOut1;
                    effect in2 -> abbiegelichtLinks_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Product",Inputs="**">> component Mul1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abblendlichtLinks_pcOut1;
                    effect in1 -> abblendlichtLinks_pcOut1;
                    effect in2 -> abblendlichtLinks_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul2 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abblendlichtRechts_pcOut1;
                    effect in1 -> abblendlichtRechts_pcOut1;
                    effect in2 -> abblendlichtRechts_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul3 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double ausleuchtungRechts_mOut1;
                    effect in1 -> ausleuchtungRechts_mOut1;
                    effect in2 -> ausleuchtungRechts_mOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul4 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abbiegelichtRechts_pcOut1;
                    effect in1 -> abbiegelichtRechts_pcOut1;
                    effect in2 -> abbiegelichtRechts_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul5 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double abbiegelichtLinks_pcOut1;
                    effect in1 -> abbiegelichtLinks_pcOut1;
                    effect in2 -> abbiegelichtLinks_pcOut1;
                  }
                  <<Type="Product",Inputs="**">> component Mul6 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double ausleuchtungLinks_mOut1;
                    effect in1 -> ausleuchtungLinks_mOut1;
                    effect in2 -> ausleuchtungLinks_mOut1;
                  }
                  <<Type="Sum",ListOfSigns="+-">> component Sum {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Sum",ListOfSigns="+-">> component Sum1 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Double out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant constant;
                  component Constant1 constant1;
                  component Constant2 constant2;
                  component Constant4 constant4;
                  component Constant5 constant5;
                  component Constant6 constant6;
                  component Constant7 constant7;
                  component Constant8 constant8;
                  component Constant9 constant9;
                  component Div1 div1;
                  component Div2 div2;
                  component Div3 div3;
                  component Div4 div4;
                  component Div5 div5;
                  component Div6 div6;
                  component Mul mul;
                  component Mul1 mul1;
                  component Mul2 mul2;
                  component Mul3 mul3;
                  component Mul4 mul4;
                  component Mul5 mul5;
                  component Mul6 mul6;
                  component Sum sum;
                  component Sum1 sum1;
                  component VERSION_INFO vERSION_INFO;
                  connect div6.abbiegelichtLinks_pcOut1 -> abbiegelichtLinks_pcOut6;
                  connect mul5.abbiegelichtLinks_pcOut1 -> div6.abbiegelichtLinks_pcIn1;
                  connect constant9.out1 -> div6.in2;
                  connect div5.abbiegelichtRechts_pcOut1 -> abbiegelichtRechts_pcOut5;
                  connect mul4.abbiegelichtRechts_pcOut1 -> div5.abbiegelichtRechts_pcIn1;
                  connect constant8.out1 -> div5.in2;
                  connect div4.ausleuchtungLinks_mOut1 -> ausleuchtungLinks_mOut4;
                  connect mul6.ausleuchtungLinks_mOut1 -> div4.ausleuchtungLinks_mIn1;
                  connect constant7.out1 -> div4.in2;
                  connect div3.ausleuchtungRechts_mOut1 -> ausleuchtungRechts_mOut3;
                  connect mul3.ausleuchtungRechts_mOut1 -> div3.ausleuchtungRechts_mIn1;
                  connect constant6.out1 -> div3.in2;
                  connect div2.abblendlichtRechts_pcOut1 -> abblendlichtRechts_pcOut2;
                  connect mul2.abblendlichtRechts_pcOut1 -> div2.abblendlichtRechts_pcIn1;
                  connect constant5.out1 -> div2.in2;
                  connect div1.abblendlichtLinks_pcOut1 -> abblendlichtLinks_pcOut1;
                  connect mul1.abblendlichtLinks_pcOut1 -> div1.abblendlichtLinks_pcIn1;
                  connect constant4.out1 -> div1.in2;
                  connect ausleuchtungLinks_mIn5 -> mul6.in2;
                  connect sum.out1 -> mul1.in1;
                  connect sum.out1 -> mul2.in1;
                  connect sum.out1 -> mul3.in1;
                  connect sum.out1 -> mul4.in1;
                  connect sum.out1 -> mul5.in1;
                  connect sum.out1 -> mul6.in1;
                  connect abbiegelichtLinks_pcIn7 -> mul5.in2;
                  connect abbiegelichtRechts_pcIn6 -> mul4.in2;
                  connect ausleuchtung_mIn4 -> mul3.in2;
                  connect abblendlichtRechts_pcIn3 -> mul2.in2;
                  connect abblendlichtLinks_pcIn2 -> mul1.in2;
                  connect mul.out1 -> sum.in2;
                  connect sum1.out1 -> mul.in2;
                  connect constant2.out1 -> mul.in1;
                  connect spannung_VIn1 -> sum1.in1;
                  connect constant1.out1 -> sum1.in2;
                  connect constant.out1 -> sum.in1;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Type="Switch">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition1 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock2 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition2 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock3 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition3 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock4 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition4 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock5 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition5 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay2 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock6 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition6 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock7 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition7 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay3 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock8 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition8 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock9 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition9 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay4 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock10 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1 > 14.5",Type="Condition">> component Condition10 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock11 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1 > 14.5)",Type="Condition">> component Condition11 {
                  ports
                    in Double in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay5 {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                component Abbiegelicht_Links_Helligkeit abbiegelicht_Links_Helligkeit;
                component Abbiegelicht_Rechts_Helligkeit abbiegelicht_Rechts_Helligkeit;
                component Keine_Ueberspannung keine_Ueberspannung;
                component Terminator terminator;
                component Terminator1 terminator1;
                component Ueberspannung ueberspannung;
                component VERSION_INFO vERSION_INFO;
                component SwitchBlock switchBlock;
                component Condition condition;
                component SwitchBlock1 switchBlock1;
                component Condition1 condition1;
                component UnitDelay unitDelay;
                component SwitchBlock2 switchBlock2;
                component Condition2 condition2;
                component SwitchBlock3 switchBlock3;
                component Condition3 condition3;
                component UnitDelay1 unitDelay1;
                component SwitchBlock4 switchBlock4;
                component Condition4 condition4;
                component SwitchBlock5 switchBlock5;
                component Condition5 condition5;
                component UnitDelay2 unitDelay2;
                component SwitchBlock6 switchBlock6;
                component Condition6 condition6;
                component SwitchBlock7 switchBlock7;
                component Condition7 condition7;
                component UnitDelay3 unitDelay3;
                component SwitchBlock8 switchBlock8;
                component Condition8 condition8;
                component SwitchBlock9 switchBlock9;
                component Condition9 condition9;
                component UnitDelay4 unitDelay4;
                component SwitchBlock10 switchBlock10;
                component Condition10 condition10;
                component SwitchBlock11 switchBlock11;
                component Condition11 condition11;
                component UnitDelay5 unitDelay5;
                connect switchBlock.out1 -> abblendlichtLinks_pcOut1;
                connect switchBlock2.out1 -> abblendlichtRechts_pcOut2;
                connect switchBlock4.out1 -> ausleuchtungRechts_mOut3;
                connect switchBlock6.out1 -> ausleuchtungLinks_mOut4;
                connect switchBlock8.out1 -> abbiegelichtRechts_pcOut5;
                connect switchBlock10.out1 -> abbiegelichtLinks_pcOut6;
                connect spannung_VIn7 -> condition.in1;
                connect condition.out1 -> switchBlock.condition;
                connect ueberspannung.abblendlichtLinks_pcOut1 -> switchBlock.ifIn;
                connect spannung_VIn7 -> condition1.in1;
                connect condition1.out1 -> switchBlock1.condition;
                connect keine_Ueberspannung.abblendlichtLinks_pcOut1 -> switchBlock1.ifIn;
                connect switchBlock1.out1 -> switchBlock.elseIn;
                connect switchBlock.out1 -> unitDelay.valueIn;
                connect unitDelay.valueOut -> switchBlock1.elseIn;
                connect spannung_VIn7 -> condition2.in1;
                connect condition2.out1 -> switchBlock2.condition;
                connect ueberspannung.abblendlichtRechts_pcOut2 -> switchBlock2.ifIn;
                connect spannung_VIn7 -> condition3.in1;
                connect condition3.out1 -> switchBlock3.condition;
                connect keine_Ueberspannung.abblendlichtRechts_pcOut2 -> switchBlock3.ifIn;
                connect switchBlock3.out1 -> switchBlock2.elseIn;
                connect switchBlock2.out1 -> unitDelay1.valueIn;
                connect unitDelay1.valueOut -> switchBlock3.elseIn;
                connect spannung_VIn7 -> condition4.in1;
                connect condition4.out1 -> switchBlock4.condition;
                connect ueberspannung.ausleuchtungRechts_mOut3 -> switchBlock4.ifIn;
                connect spannung_VIn7 -> condition5.in1;
                connect condition5.out1 -> switchBlock5.condition;
                connect keine_Ueberspannung.ausleuchtung_mOut3 -> switchBlock5.ifIn;
                connect switchBlock5.out1 -> switchBlock4.elseIn;
                connect switchBlock4.out1 -> unitDelay2.valueIn;
                connect unitDelay2.valueOut -> switchBlock5.elseIn;
                connect spannung_VIn7 -> condition6.in1;
                connect condition6.out1 -> switchBlock6.condition;
                connect ueberspannung.ausleuchtungLinks_mOut4 -> switchBlock6.ifIn;
                connect spannung_VIn7 -> condition7.in1;
                connect condition7.out1 -> switchBlock7.condition;
                connect keine_Ueberspannung.ausleuchtungRechts_mOut4 -> switchBlock7.ifIn;
                connect switchBlock7.out1 -> switchBlock6.elseIn;
                connect switchBlock6.out1 -> unitDelay3.valueIn;
                connect unitDelay3.valueOut -> switchBlock7.elseIn;
                connect spannung_VIn7 -> condition8.in1;
                connect condition8.out1 -> switchBlock8.condition;
                connect ueberspannung.abbiegelichtRechts_pcOut5 -> switchBlock8.ifIn;
                connect spannung_VIn7 -> condition9.in1;
                connect condition9.out1 -> switchBlock9.condition;
                connect keine_Ueberspannung.abbiegelichtRechts_pcOut5 -> switchBlock9.ifIn;
                connect switchBlock9.out1 -> switchBlock8.elseIn;
                connect switchBlock8.out1 -> unitDelay4.valueIn;
                connect unitDelay4.valueOut -> switchBlock9.elseIn;
                connect spannung_VIn7 -> condition10.in1;
                connect condition10.out1 -> switchBlock10.condition;
                connect ueberspannung.abbiegelichtLinks_pcOut6 -> switchBlock10.ifIn;
                connect spannung_VIn7 -> condition11.in1;
                connect condition11.out1 -> switchBlock11.condition;
                connect keine_Ueberspannung.abbiegelichtLinks_pcOut6 -> switchBlock11.ifIn;
                connect switchBlock11.out1 -> switchBlock10.elseIn;
                connect switchBlock10.out1 -> unitDelay5.valueIn;
                connect unitDelay5.valueOut -> switchBlock11.elseIn;
                connect abbiegelicht_Links_Helligkeit.out1 -> terminator1.in1;
                connect abbiegelicht_Rechts_Helligkeit.out1 -> terminator.in1;
                connect ausleuchtungLinks_mIn4 -> ueberspannung.ausleuchtungLinks_mIn5;
                connect ausleuchtungLinks_mIn4 -> keine_Ueberspannung.ausleuchtungLinks_mIn4;
                connect abbiegelichtLinks_pcIn6 -> abbiegelicht_Links_Helligkeit.in1;
                connect abbiegelichtLinks_pcIn6 -> keine_Ueberspannung.abbiegelichtLinks_pcIn6;
                connect abbiegelichtLinks_pcIn6 -> ueberspannung.abbiegelichtLinks_pcIn7;
                connect abbiegelichtRechts_pcIn5 -> abbiegelicht_Rechts_Helligkeit.in1;
                connect abbiegelichtRechts_pcIn5 -> ueberspannung.abbiegelichtRechts_pcIn6;
                connect abbiegelichtRechts_pcIn5 -> keine_Ueberspannung.abbiegelichtRechts_pcIn5;
                connect ausleuchtungRechts_mIn3 -> ueberspannung.ausleuchtung_mIn4;
                connect ausleuchtungRechts_mIn3 -> keine_Ueberspannung.ausleuchtungRechts_mIn3;
                connect abblendlichtRechts_pcIn2 -> ueberspannung.abblendlichtRechts_pcIn3;
                connect abblendlichtRechts_pcIn2 -> keine_Ueberspannung.abblendlichtRechts_pcIn2;
                connect abblendlichtLinks_pcIn1 -> ueberspannung.abblendlichtLinks_pcIn2;
                connect abblendlichtLinks_pcIn1 -> keine_Ueberspannung.abblendlichtLinks_pcIn1;
                connect spannung_VIn7 -> ueberspannung.spannung_VIn1;
              }
              <<Type="SubSystem">> component Umfeldbeleuchtung {
                ports
                  in Boolean schluessel_bIn1,
                  in Boolean motor_bIn2,
                  in Boolean entriegelt_bIn3,
                  in Boolean tuer_bIn4,
                  in Boolean aussenhelligkeit_bIn5,
                  out Double umfeldbeleuchtung_pcOut1,
                  out Boolean umfeldbeleuchtung_bOut2;
                <<Type="Constant",Value="0">> component DEMO_AUSSEN_Sonderschutz {
                  ports
                    out Boolean out1;
                }
                <<Type="Constant",Value="1">> component DEMO_AUSSEN_Umfeldbeleuchtung {
                  ports
                    out Boolean out1;
                }
                <<Operator="NOT",Type="Logic">> component LogicalOperator10 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                  effect in1 -> out1;
                }
                <<Operator="AND",Type="Logic">> component LogicalOperator13 {
                  ports
                    in Boolean in1,
                    in Boolean in2,
                    out Boolean out1;
                  effect in1 -> out1;
                  effect in2 -> out1;
                }
                <<Type="SubSystem">> component Umfeldbeleuchtung_Active {
                  ports
                    in Boolean schluessel_bIn1,
                    in Boolean motor_bIn2,
                    in Boolean entriegelt_bIn3,
                    in Boolean tuer_bIn4,
                    in Boolean aussenhelligkeit_bIn5,
                    out Double umfeldbeleuchtung_pcOut1,
                    out Boolean umfeldbeleuchtung_bOut2;
                  <<Type="Constant",Value="30">> component Constant2 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="0">> component Constant3 {
                    ports
                      out Double out1;
                  }
                  <<Type="Constant",Value="100">> component Constant4 {
                    ports
                      out Double out1;
                  }
                  <<Type="SubSystem">> component EdgeFalling2 {
                    ports
                      in Boolean uIn1,
                      in Boolean rIn2,
                      in Boolean iVIn3,
                      out Boolean yOut1;
                    <<Operator="AND",Type="Logic">> component LogOp_A {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_U {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_A logOp_A;
                    component LogOp_N logOp_N;
                    component Memory_U memory_U;
                    component Switch_R switch_R;
                    component Condition condition;
                    connect condition.out1 -> switch_R.condition;
                    connect rIn2 -> condition.in1;
                    connect switch_R.out1 -> logOp_A.in2;
                    connect uIn1 -> logOp_N.in1;
                    connect uIn1 -> memory_U.in1;
                    connect logOp_N.out1 -> logOp_A.in1;
                    connect logOp_A.out1 -> yOut1;
                    connect memory_U.out1 -> switch_R.elseIn;
                    connect iVIn3 -> switch_R.ifIn;
                  }
                  <<Type="SubSystem">> component FalseBlock1 {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator1 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator11 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator16 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator2 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean umfeldbeleuchtung_bOut1;
                    effect in1 -> umfeldbeleuchtung_bOut1;
                    effect in2 -> umfeldbeleuchtung_bOut1;
                  }
                  <<Operator="OR",Type="Logic">> component LogicalOperator4 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator5 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator6 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator7 {
                    ports
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator8 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Operator="NOT",Type="Logic">> component LogicalOperator9 {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="SubSystem">> component RSFlipFlop1 {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_S.condition;
                    connect sIn1 -> condition.in1;
                    connect rIn2 -> condition1.in1;
                    connect logOp_N.out1 -> nOT_QOut2;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect memory_Q.out1 -> switch_S.elseIn;
                  }
                  <<Type="SubSystem">> component RSFlipFlop2 {
                    ports
                      in Boolean sIn1,
                      in Boolean rIn2,
                      out Boolean qOut1,
                      out Boolean nOT_QOut2;
                    <<Operator="NOT",Type="Logic">> component LogOp_N {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_Q {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                      effect in1 -> out1;
                    }
                    <<Type="Constant",Value="1">> component One_S {
                      ports
                        out Boolean out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_S {
                      ports
                        in Boolean ifIn,
                        in Boolean condition,
                        in Boolean elseIn,
                        out Boolean out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="0">> component Zero_R {
                      ports
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component LogOp_N logOp_N;
                    component Memory_Q memory_Q;
                    component One_S one_S;
                    component Switch_R switch_R;
                    component Switch_S switch_S;
                    component Zero_R zero_R;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition.out1 -> switch_S.condition;
                    connect condition1.out1 -> switch_R.condition;
                    connect sIn1 -> condition.in1;
                    connect rIn2 -> condition1.in1;
                    connect memory_Q.out1 -> switch_S.elseIn;
                    connect switch_S.out1 -> switch_R.elseIn;
                    connect one_S.out1 -> switch_S.ifIn;
                    connect switch_R.out1 -> memory_Q.in1;
                    connect switch_R.out1 -> qOut1;
                    connect switch_R.out1 -> logOp_N.in1;
                    connect zero_R.out1 -> switch_R.ifIn;
                    connect logOp_N.out1 -> nOT_QOut2;
                  }
                  <<Operator="<",Type="RelationalOperator">> component RelOp3 {
                    ports
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    effect in1 -> out1;
                    effect in2 -> out1;
                  }
                  <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
                    ports
                      in Double ifIn,
                      in Boolean condition,
                      in Double elseIn,
                      out Double umfeldbeleuchtung_pcOut1;
                    effect ifIn -> umfeldbeleuchtung_pcOut1;
                    effect condition -> umfeldbeleuchtung_pcOut1;
                    effect elseIn -> umfeldbeleuchtung_pcOut1;
                  }
                  <<Type="Terminator">> component Terminator1 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="Terminator">> component Terminator2 {
                    ports
                      in Boolean in1;
                  }
                  <<Type="SubSystem">> component Timer_REM {
                    ports
                      in Boolean eIn1,
                      in Boolean rIn2,
                      in Double iVIn3,
                      out Boolean yOut1,
                      out Double mOut2;
                    <<Type="Constant",Value="dT_eff">> component Const_dTeff {
                      ports
                        out Double out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_C {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="AND",Type="Logic">> component LogOp_R {
                      ports
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="UnitDelay",InitialCondition="0">> component Memory_C {
                      ports
                        in Double in1,
                        out Double out1;
                      effect in1 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator="<=",Type="RelationalOperator">> component RelOp_R {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Operator=">",Type="RelationalOperator">> component RelOp_Y {
                      ports
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Sum",ListOfSigns="+-">> component Sum_C {
                      ports
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      effect in1 -> out1;
                      effect in2 -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_C {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component Switch_R {
                      ports
                        in Double ifIn,
                        in Boolean condition,
                        in Double elseIn,
                        out Double out1;
                      effect ifIn -> out1;
                      effect condition -> out1;
                      effect elseIn -> out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_C {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_R {
                      ports
                        out Double out1;
                    }
                    <<Type="Constant",Value="2.2204e-11">> component Zero_Y {
                      ports
                        out Double out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                      ports
                        in Boolean in1,
                        out Boolean out1;
                    }
                    component Const_dTeff const_dTeff;
                    component LogOp_C logOp_C;
                    component LogOp_R logOp_R;
                    component Memory_C memory_C;
                    component RelOp_C relOp_C;
                    component RelOp_R relOp_R;
                    component RelOp_Y relOp_Y;
                    component Sum_C sum_C;
                    component Switch_C switch_C;
                    component Switch_R switch_R;
                    component Zero_C zero_C;
                    component Zero_R zero_R;
                    component Zero_Y zero_Y;
                    component Condition condition;
                    component Condition1 condition1;
                    connect condition1.out1 -> switch_R.condition;
                    connect condition.out1 -> switch_C.condition;
                    connect logOp_C.out1 -> condition.in1;
                    connect logOp_R.out1 -> condition1.in1;
                    connect rIn2 -> logOp_R.in2;
                    connect relOp_R.out1 -> logOp_R.in1;
                    connect memory_C.out1 -> relOp_R.in1;
                    connect memory_C.out1 -> switch_C.elseIn;
                    connect memory_C.out1 -> sum_C.in1;
                    connect memory_C.out1 -> relOp_C.in1;
                    connect zero_R.out1 -> relOp_R.in2;
                    connect iVIn3 -> switch_R.ifIn;
                    connect relOp_C.out1 -> logOp_C.in2;
                    connect zero_C.out1 -> relOp_C.in2;
                    connect eIn1 -> logOp_C.in1;
                    connect switch_C.out1 -> switch_R.elseIn;
                    connect sum_C.out1 -> switch_C.ifIn;
                    connect const_dTeff.out1 -> sum_C.in2;
                    connect relOp_Y.out1 -> yOut1;
                    connect switch_R.out1 -> memory_C.in1;
                    connect switch_R.out1 -> relOp_Y.in1;
                    connect switch_R.out1 -> mOut2;
                    connect zero_Y.out1 -> relOp_Y.in2;
                  }
                  <<Type="SubSystem">> component TrueBlock {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="1">> component One {
                      ports
                        out Boolean out1;
                    }
                    component One one;
                    connect one.out1 -> yOut1;
                  }
                  <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                    effect in1 -> out1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  <<Condition="u2 >= 1",Type="Condition">> component Condition {
                    ports
                      in Boolean in1,
                      out Boolean out1;
                  }
                  component Constant2 constant2;
                  component Constant3 constant3;
                  component Constant4 constant4;
                  component EdgeFalling2 edgeFalling2;
                  component FalseBlock1 falseBlock1;
                  component LogicalOperator1 logicalOperator1;
                  component LogicalOperator11 logicalOperator11;
                  component LogicalOperator16 logicalOperator16;
                  component LogicalOperator2 logicalOperator2;
                  component LogicalOperator4 logicalOperator4;
                  component LogicalOperator5 logicalOperator5;
                  component LogicalOperator6 logicalOperator6;
                  component LogicalOperator7 logicalOperator7;
                  component LogicalOperator8 logicalOperator8;
                  component LogicalOperator9 logicalOperator9;
                  component RSFlipFlop1 rSFlipFlop1;
                  component RSFlipFlop2 rSFlipFlop2;
                  component RelOp3 relOp3;
                  component SwitchBlock3 switchBlock3;
                  component Terminator1 terminator1;
                  component Terminator2 terminator2;
                  component Timer_REM timer_REM;
                  component TrueBlock trueBlock;
                  component UnitDelay unitDelay;
                  component VERSION_INFO vERSION_INFO;
                  component Condition condition;
                  connect condition.out1 -> switchBlock3.condition;
                  connect logicalOperator1.out1 -> condition.in1;
                  connect relOp3.out1 -> logicalOperator1.in1;
                  connect trueBlock.yOut1 -> logicalOperator2.in2;
                  connect logicalOperator2.umfeldbeleuchtung_bOut1 -> umfeldbeleuchtung_bOut2;
                  connect schluessel_bIn1 -> logicalOperator5.in1;
                  connect logicalOperator1.out1 -> logicalOperator2.in1;
                  connect logicalOperator11.out1 -> logicalOperator1.in2;
                  connect switchBlock3.umfeldbeleuchtung_pcOut1 -> umfeldbeleuchtung_pcOut1;
                  connect constant4.out1 -> switchBlock3.ifIn;
                  connect constant3.out1 -> switchBlock3.elseIn;
                  connect tuer_bIn4 -> logicalOperator11.in2;
                  connect aussenhelligkeit_bIn5 -> logicalOperator11.in1;
                  connect logicalOperator16.out1 -> logicalOperator7.in2;
                  connect entriegelt_bIn3 -> logicalOperator16.in1;
                  connect logicalOperator9.out1 -> rSFlipFlop2.rIn2;
                  connect logicalOperator9.out1 -> logicalOperator4.in2;
                  connect unitDelay.out1 -> logicalOperator9.in1;
                  connect timer_REM.yOut1 -> unitDelay.in1;
                  connect timer_REM.mOut2 -> relOp3.in1;
                  connect constant2.out1 -> timer_REM.iVIn3;
                  connect constant2.out1 -> relOp3.in2;
                  connect logicalOperator8.out1 -> timer_REM.rIn2;
                  connect logicalOperator7.out1 -> timer_REM.eIn1;
                  connect logicalOperator7.out1 -> logicalOperator8.in1;
                  connect rSFlipFlop2.qOut1 -> logicalOperator7.in1;
                  connect rSFlipFlop2.nOT_QOut2 -> terminator2.in1;
                  connect logicalOperator6.out1 -> rSFlipFlop2.sIn1;
                  connect rSFlipFlop1.nOT_QOut2 -> terminator1.in1;
                  connect logicalOperator5.out1 -> logicalOperator6.in1;
                  connect rSFlipFlop1.qOut1 -> logicalOperator6.in2;
                  connect edgeFalling2.yOut1 -> rSFlipFlop1.sIn1;
                  connect logicalOperator4.out1 -> rSFlipFlop1.rIn2;
                  connect falseBlock1.yOut1 -> edgeFalling2.iVIn3;
                  connect falseBlock1.yOut1 -> edgeFalling2.rIn2;
                  connect motor_bIn2 -> logicalOperator4.in1;
                  connect motor_bIn2 -> edgeFalling2.uIn1;
                }
                <<Type="SubSystem">> component Umfeldbeleuchtung_Disabled {
                  ports
                    out Double umfeldbeleuchtung_pcOut1,
                    out Boolean umfeldbeleuchtung_bOut2;
                  <<Type="Constant",Value="100">> component Constant2 {
                    ports
                      out Double umfeldbeleuchtung_pcOut1;
                  }
                  <<Type="SubSystem">> component FalseBlock {
                    ports
                      out Boolean yOut1;
                    <<Type="Constant",Value="0">> component Zero {
                      ports
                        out Boolean out1;
                    }
                    component Zero zero;
                    connect zero.out1 -> yOut1;
                  }
                  <<Operator="AND",Type="Logic">> component LogicalOperator {
                    ports
                      in Boolean umfeldbeleuchtung_bIn1,
                      in Boolean umfeldbeleuchtung_bIn2,
                      out Boolean umfeldbeleuchtung_bOut1;
                    effect umfeldbeleuchtung_bIn1 -> umfeldbeleuchtung_bOut1;
                    effect umfeldbeleuchtung_bIn2 -> umfeldbeleuchtung_bOut1;
                  }
                  <<Type="SubSystem">> component VERSION_INFO {
                    <<Type="SubSystem">> component Copyright {
                    }
                    component Copyright copyright;
                  }
                  component Constant2 constant2;
                  component FalseBlock falseBlock;
                  component LogicalOperator logicalOperator;
                  component VERSION_INFO vERSION_INFO;
                  connect constant2.umfeldbeleuchtung_pcOut1 -> umfeldbeleuchtung_pcOut1;
                  connect falseBlock.yOut1 -> logicalOperator.umfeldbeleuchtung_bIn1;
                  connect falseBlock.yOut1 -> logicalOperator.umfeldbeleuchtung_bIn2;
                  connect logicalOperator.umfeldbeleuchtung_bOut1 -> umfeldbeleuchtung_bOut2;
                }
                <<Type="SubSystem">> component VERSION_INFO {
                  <<Type="SubSystem">> component Copyright {
                  }
                  component Copyright copyright;
                }
                <<Type="Switch">> component SwitchBlock {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="u1",Type="Condition">> component Condition {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock1 {
                  ports
                    in Double ifIn,
                    in Boolean condition,
                    in Double elseIn,
                    out Double out1;
                }
                <<Condition="!(u1)",Type="Condition">> component Condition1 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay {
                  ports
                    in Double valueIn,
                    out Double valueOut;
                }
                <<Type="Switch">> component SwitchBlock2 {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                }
                <<Condition="u1",Type="Condition">> component Condition2 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="Switch">> component SwitchBlock3 {
                  ports
                    in Boolean ifIn,
                    in Boolean condition,
                    in Boolean elseIn,
                    out Boolean out1;
                }
                <<Condition="!(u1)",Type="Condition">> component Condition3 {
                  ports
                    in Boolean in1,
                    out Boolean out1;
                }
                <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                  ports
                    in Boolean valueIn,
                    out Boolean valueOut;
                }
                component DEMO_AUSSEN_Sonderschutz dEMO_AUSSEN_Sonderschutz;
                component DEMO_AUSSEN_Umfeldbeleuchtung dEMO_AUSSEN_Umfeldbeleuchtung;
                component LogicalOperator10 logicalOperator10;
                component LogicalOperator13 logicalOperator13;
                component Umfeldbeleuchtung_Active umfeldbeleuchtung_Active;
                component Umfeldbeleuchtung_Disabled umfeldbeleuchtung_Disabled;
                component VERSION_INFO vERSION_INFO;
                component SwitchBlock switchBlock;
                component Condition condition;
                component SwitchBlock1 switchBlock1;
                component Condition1 condition1;
                component UnitDelay unitDelay;
                component SwitchBlock2 switchBlock2;
                component Condition2 condition2;
                component SwitchBlock3 switchBlock3;
                component Condition3 condition3;
                component UnitDelay1 unitDelay1;
                connect switchBlock.out1 -> umfeldbeleuchtung_pcOut1;
                connect switchBlock2.out1 -> umfeldbeleuchtung_bOut2;
                connect logicalOperator13.out1 -> condition.in1;
                connect condition.out1 -> switchBlock.condition;
                connect umfeldbeleuchtung_Active.umfeldbeleuchtung_pcOut1 -> switchBlock.ifIn;
                connect logicalOperator13.out1 -> condition1.in1;
                connect condition1.out1 -> switchBlock1.condition;
                connect umfeldbeleuchtung_Disabled.umfeldbeleuchtung_pcOut1 -> switchBlock1.ifIn;
                connect switchBlock1.out1 -> switchBlock.elseIn;
                connect switchBlock.out1 -> unitDelay.valueIn;
                connect unitDelay.valueOut -> switchBlock1.elseIn;
                connect logicalOperator13.out1 -> condition2.in1;
                connect condition2.out1 -> switchBlock2.condition;
                connect umfeldbeleuchtung_Active.umfeldbeleuchtung_bOut2 -> switchBlock2.ifIn;
                connect logicalOperator13.out1 -> condition3.in1;
                connect condition3.out1 -> switchBlock3.condition;
                connect umfeldbeleuchtung_Disabled.umfeldbeleuchtung_bOut2 -> switchBlock3.ifIn;
                connect switchBlock3.out1 -> switchBlock2.elseIn;
                connect switchBlock2.out1 -> unitDelay1.valueIn;
                connect unitDelay1.valueOut -> switchBlock3.elseIn;
                connect schluessel_bIn1 -> umfeldbeleuchtung_Active.schluessel_bIn1;
                connect entriegelt_bIn3 -> umfeldbeleuchtung_Active.entriegelt_bIn3;
                connect tuer_bIn4 -> umfeldbeleuchtung_Active.tuer_bIn4;
                connect aussenhelligkeit_bIn5 -> umfeldbeleuchtung_Active.aussenhelligkeit_bIn5;
                connect motor_bIn2 -> umfeldbeleuchtung_Active.motor_bIn2;
                connect logicalOperator10.out1 -> logicalOperator13.in1;
                connect dEMO_AUSSEN_Umfeldbeleuchtung.out1 -> logicalOperator13.in2;
                connect dEMO_AUSSEN_Sonderschutz.out1 -> logicalOperator10.in1;
              }
              <<Type="UnitDelay",InitialCondition="0">> component UnitDelay1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
                effect in1 -> out1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
                ports
                  in Boolean in1,
                  out Boolean out1;
              }
              component Abbiegelicht abbiegelicht;
              component AdaptivesFernlicht adaptivesFernlicht;
              component Constant1 constant1;
              component Constant10 constant10;
              component Constant11 constant11;
              component Constant5 constant5;
              component Constant6 constant6;
              component Constant7 constant7;
              component Constant8 constant8;
              component Constant9 constant9;
              component DEMO_AUSSEN_Sonderschutz dEMO_AUSSEN_Sonderschutz;
              component DEMO_AUSSEN_Sonderschutz1 dEMO_AUSSEN_Sonderschutz1;
              component EdgeFalling1 edgeFalling1;
              component FalseBlock5 falseBlock5;
              component LogOp logOp;
              component LogicalOperator logicalOperator;
              component LogicalOperator1 logicalOperator1;
              component LogicalOperator13 logicalOperator13;
              component LogicalOperator14 logicalOperator14;
              component LogicalOperator2 logicalOperator2;
              component MinMax minMax;
              component MinMax1 minMax1;
              component MinMax2 minMax2;
              component MinMax3 minMax3;
              component MinMax4 minMax4;
              component MinMax5 minMax5;
              component RSFlipFlop4 rSFlipFlop4;
              component RelOp relOp;
              component RelOp1 relOp1;
              component RelationalOperator relationalOperator;
              component SwitchBlock1 switchBlock1;
              component SwitchBlock2 switchBlock2;
              component Tagfahrlicht tagfahrlicht;
              component Terminator4 terminator4;
              component TimerRetrigger_RE timerRetrigger_RE;
              component Ueberspannungsschutz ueberspannungsschutz;
              component Umfeldbeleuchtung umfeldbeleuchtung;
              component UnitDelay1 unitDelay1;
              component VERSION_INFO vERSION_INFO;
              component Condition condition;
              component Condition1 condition1;
              connect condition.out1 -> switchBlock1.condition;
              connect condition1.out1 -> switchBlock2.condition;
              connect logicalOperator14.out1 -> condition.in1;
              connect logicalOperator.out1 -> condition1.in1;
              connect constant10.out1 -> switchBlock1.elseIn;
              connect constant9.out1 -> switchBlock1.ifIn;
              connect constant11.out1 -> timerRetrigger_RE.iVIn3;
              connect constant1.out1 -> relOp1.in2;
              connect constant8.out1 -> relOp.in2;
              connect ueberspannungsschutz.abblendlichtLinks_pcOut1 -> abblendlichtLinks_pcOut1;
              connect tagfahrlicht.tagfahrlichtLinks_pcOut1 -> minMax.in2;
              connect logicalOperator1.out1 -> logicalOperator2.in3;
              connect dunkeltaster_stIn13 -> relationalOperator.in1;
              connect switchBlock2.out1 -> minMax3.in2;
              connect switchBlock2.out1 -> minMax2.in2;
              connect switchBlock2.out1 -> minMax4.in2;
              connect switchBlock2.out1 -> minMax5.in2;
              connect constant7.out1 -> switchBlock2.ifIn;
              connect constant6.out1 -> switchBlock2.elseIn;
              connect dEMO_AUSSEN_Sonderschutz1.out1 -> logicalOperator.in1;
              connect relationalOperator.out1 -> logicalOperator.in2;
              connect constant5.out1 -> relationalOperator.in2;
              connect minMax3.abblendlichtLinks_pcOut1 -> ueberspannungsschutz.abblendlichtLinks_pcIn1;
              connect minMax5.abbiegelichtLinks_pcOut1 -> ueberspannungsschutz.abbiegelichtLinks_pcIn6;
              connect minMax4.abbiegelichtRechts_pcOut1 -> ueberspannungsschutz.abbiegelichtRechts_pcIn5;
              connect minMax2.abblendlichtRechts_pcOut1 -> ueberspannungsschutz.abblendlichtRechts_pcIn2;
              connect minMax1.abblendlichtRechts_pcOut1 -> minMax2.abblendlichtRechts_pcIn1;
              connect ueberspannungsschutz.ausleuchtungLinks_mOut4 -> ausleuchtungLinks_mOut4;
              connect adaptivesFernlicht.ausleuchtungLinks_mOut2 -> ueberspannungsschutz.ausleuchtungLinks_mIn4;
              connect ueberspannungsschutz.abblendlichtRechts_pcOut2 -> abblendlichtRechts_pcOut2;
              connect ueberspannungsschutz.ausleuchtungRechts_mOut3 -> ausleuchtungRechts_mOut3;
              connect ueberspannungsschutz.abbiegelichtRechts_pcOut5 -> abbiegelichtRechts_pcOut5;
              connect ueberspannungsschutz.abbiegelichtLinks_pcOut6 -> abbiegelichtLinks_pcOut6;
              connect adaptivesFernlicht.ausleuchtungRechts_mOut1 -> ueberspannungsschutz.ausleuchtungRechts_mIn3;
              connect spannung_VIn12 -> abbiegelicht.spannung_VIn5;
              connect spannung_VIn12 -> adaptivesFernlicht.spannung_VIn5;
              connect spannung_VIn12 -> ueberspannungsschutz.spannung_VIn7;
              connect lichtdrehschalter_stIn5 -> relOp.in1;
              connect lichtdrehschalter_stIn5 -> relOp1.in1;
              connect lichtdrehschalter_stIn5 -> adaptivesFernlicht.lichtdrehschalter_stIn1;
              connect abbiegelicht.abbiegelichtLinks_pcOut2 -> minMax5.in1;
              connect abbiegelicht.abbiegelichtRechts_pcOut1 -> minMax4.in1;
              connect dEMO_AUSSEN_Sonderschutz.out1 -> logicalOperator1.in1;
              connect tagfahrlicht.tagfahrlichtRechts_pcOut2 -> minMax1.in2;
              connect switchBlock1.out1 -> minMax.in1;
              connect switchBlock1.out1 -> minMax1.in1;
              connect minMax.abblendlichtLinks_pcOut1 -> minMax3.abblendlichtLinks_pcIn1;
              connect relOp.out1 -> logicalOperator14.in1;
              connect umfeldbeleuchtung.umfeldbeleuchtung_pcOut1 -> minMax1.in3;
              connect umfeldbeleuchtung.umfeldbeleuchtung_pcOut1 -> minMax.in3;
              connect tagfahrlicht.tagfahrlicht_bOut3 -> logOp.in1;
              connect umfeldbeleuchtung.umfeldbeleuchtung_bOut2 -> logOp.in2;
              connect logOp.abblendlicht_bOut1 -> abbiegelicht.abblendlicht_bIn1;
              connect fzggeschw_kmhIn9 -> adaptivesFernlicht.fzggeschw_kmhIn4;
              connect fzggeschw_kmhIn9 -> abbiegelicht.fahrzeuggeschwindigkeit_kmhIn2;
              connect fernlicht_bIn11 -> adaptivesFernlicht.fernlicht_bIn3;
              connect entFahrzeug_bIn10 -> adaptivesFernlicht.entFahrzeug_bIn2;
              connect entriegelt_bIn7 -> umfeldbeleuchtung.entriegelt_bIn3;
              connect blinkenLinksAktiv_bIn1 -> tagfahrlicht.blinkenLinksAktiv_bIn3;
              connect blinkenLinksAktiv_bIn1 -> abbiegelicht.rBLinks_bIn3;
              connect blinkenRechtsAktiv_bIn2 -> tagfahrlicht.blinkenRechtsAktiv_bIn4;
              connect blinkenRechtsAktiv_bIn2 -> abbiegelicht.rBRechts_bIn4;
              connect schluessel_bIn4 -> tagfahrlicht.schluessel_bIn2;
              connect schluessel_bIn4 -> umfeldbeleuchtung.schluessel_bIn1;
              connect tuer_bIn8 -> umfeldbeleuchtung.tuer_bIn4;
              connect logicalOperator13.out1 -> rSFlipFlop4.rIn2;
              connect unitDelay1.out1 -> logicalOperator13.in1;
              connect timerRetrigger_RE.yOut1 -> logicalOperator14.in3;
              connect timerRetrigger_RE.yOut1 -> unitDelay1.in1;
              connect rSFlipFlop4.qOut1 -> timerRetrigger_RE.eIn1;
              connect edgeFalling1.yOut1 -> rSFlipFlop4.sIn1;
              connect rSFlipFlop4.nOT_QOut2 -> terminator4.in1;
              connect logicalOperator2.out1 -> logicalOperator14.in2;
              connect logicalOperator2.out1 -> timerRetrigger_RE.rIn2;
              connect logicalOperator2.out1 -> edgeFalling1.uIn1;
              connect falseBlock5.yOut1 -> edgeFalling1.rIn2;
              connect falseBlock5.yOut1 -> edgeFalling1.iVIn3;
              connect aussenhelligkeit_bIn6 -> logicalOperator2.in2;
              connect aussenhelligkeit_bIn6 -> umfeldbeleuchtung.aussenhelligkeit_bIn5;
              connect motor_bIn3 -> tagfahrlicht.motor_bIn1;
              connect motor_bIn3 -> umfeldbeleuchtung.motor_bIn2;
              connect relOp1.out1 -> logicalOperator2.in1;
            }
            <<Type="SubSystem">> component Schluessel {
              ports
                in Double schluessel_stIn1,
                out Boolean schluessel_bOut1,
                out Boolean motor_bOut2;
              <<Type="Constant",Value="1">> component Constant {
                ports
                  out Double out1;
              }
              <<Type="Constant",Value="3">> component Constant1 {
                ports
                  out Double out1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean motor_bOut1;
                effect in1 -> motor_bOut1;
                effect in2 -> motor_bOut1;
              }
              <<Operator=">=",Type="RelationalOperator">> component RelOp1 {
                ports
                  in Double in1,
                  in Double in2,
                  out Boolean schluessel_bOut1;
                effect in1 -> schluessel_bOut1;
                effect in2 -> schluessel_bOut1;
              }
              <<Type="SubSystem">> component VERSION_INFO {
                <<Type="SubSystem">> component Copyright {
                }
                component Copyright copyright;
              }
              component Constant constant;
              component Constant1 constant1;
              component RelOp relOp;
              component RelOp1 relOp1;
              component VERSION_INFO vERSION_INFO;
              connect constant1.out1 -> relOp.in2;
              connect constant.out1 -> relOp1.in2;
              connect relOp1.schluessel_bOut1 -> schluessel_bOut1;
              connect schluessel_stIn1 -> relOp.in1;
              connect schluessel_stIn1 -> relOp1.in1;
              connect relOp.motor_bOut1 -> motor_bOut2;
            }
            <<Type="SubSystem">> component VERSION_INFO {
              <<Type="SubSystem">> component Copyright {
              }
              component Copyright copyright;
            }
            component AL_Input aL_Input;
            component Abschaltung abschaltung;
            component Blinken blinken;
            component Defekt_Erkennung defekt_Erkennung;
            component Fahrrichtungsanzeiger fahrrichtungsanzeiger;
            component Scheinwerfer scheinwerfer;
            component Schluessel schluessel;
            component VERSION_INFO vERSION_INFO;
            connect aL_Input.dunkeltaster_st1 -> scheinwerfer.dunkeltaster_stIn13;
            connect aL_Input.fAVRechts_st1 -> defekt_Erkennung.fAVRechts_stIn10;
            connect aL_Input.abbiLRechts_st1 -> defekt_Erkennung.abbiLRechts_stIn9;
            connect aL_Input.fAARechts_st1 -> defekt_Erkennung.fAARechts_stIn11;
            connect aL_Input.fAHRechts_st1 -> defekt_Erkennung.fAHRechts_stIn12;
            connect aL_Input.fLRechts_st1 -> defekt_Erkennung.fLRechts_stIn7;
            connect aL_Input.fLLinks_st1 -> defekt_Erkennung.fLLinks_stIn1;
            connect aL_Input.abbiLLinks_st1 -> defekt_Erkennung.abbiLLinks_stIn3;
            connect aL_Input.abbleLLinks_st1 -> defekt_Erkennung.abbleLLinks_stIn2;
            connect aL_Input.fAVLinks_st1 -> defekt_Erkennung.fAVLinks_stIn4;
            connect aL_Input.fAALinks_st1 -> defekt_Erkennung.fAALinks_stIn5;
            connect aL_Input.fAHLinks_st1 -> defekt_Erkennung.fAHLinks_stIn6;
            connect aL_Input.abbleLRechts_st1 -> defekt_Erkennung.abbleLRechts_stIn8;
            connect aL_Input.spannung_V1 -> scheinwerfer.spannung_VIn12;
            connect blinken.blinkenLinksAktiv_bOut5 -> scheinwerfer.blinkenLinksAktiv_bIn1;
            connect blinken.blinkenRechtsAktiv_bOut4 -> scheinwerfer.blinkenRechtsAktiv_bIn2;
            connect aL_Input.entFahrzeug_b1 -> scheinwerfer.entFahrzeug_bIn10;
            connect aL_Input.fernlicht_b1 -> scheinwerfer.fernlicht_bIn11;
            connect aL_Input.fzggeschw_kmh1 -> scheinwerfer.fzggeschw_kmhIn9;
            connect schluessel.schluessel_bOut1 -> scheinwerfer.schluessel_bIn4;
            connect blinken.warnblinkenAktiv_bOut3 -> fahrrichtungsanzeiger.warnblinkenAktiv_bIn3;
            connect blinken.rBRechtsAktiv_bOut2 -> fahrrichtungsanzeiger.rBRechtsAktiv_bIn2;
            connect blinken.rBLinksAktiv_bOut1 -> fahrrichtungsanzeiger.rBLinksAktiv_bIn1;
            connect aL_Input.tuer_b1 -> scheinwerfer.tuer_bIn8;
            connect aL_Input.entriegelt_b1 -> scheinwerfer.entriegelt_bIn7;
            connect aL_Input.aussenhelligkeit_b -> scheinwerfer.aussenhelligkeit_bIn6;
            connect aL_Input.lichtdrehschalter_st1 -> scheinwerfer.lichtdrehschalter_stIn5;
            connect schluessel.motor_bOut2 -> scheinwerfer.motor_bIn3;
            connect aL_Input.rBLinks_b1 -> blinken.rBLinks_bIn1;
            connect aL_Input.rBRechts_b1 -> blinken.rBRechts_bIn2;
            connect aL_Input.warnblinken_b1 -> blinken.warnblinken_bIn3;
            connect schluessel.schluessel_bOut1 -> blinken.schluessel_bIn4;
            connect aL_Input.schluessel_st -> schluessel.schluessel_stIn1;
            connect rBLinks_b -> aL_Input.rBLinks_b;
            connect rBRechts_b -> aL_Input.rBRechts_b;
            connect warnblinken_b -> aL_Input.warnblinken_b;
            connect schluessel_st -> aL_Input.schluessel_st1;
            connect schluessel_st1 -> aL_Input.schluessel_st2;
            connect lichtdrehschalter_st -> aL_Input.lichtdrehschalter_st;
            connect aussenhelligkeit_lx -> aL_Input.aussenhelligkeit_lx;
            connect entriegelt_b -> aL_Input.entriegelt_b;
            connect tuer_b -> aL_Input.tuer_b;
            connect fzggeschw_kmh -> aL_Input.fzggeschw_kmh;
            connect entFahrzeug_b -> aL_Input.entFahrzeug_b;
            connect fernlicht_b -> aL_Input.fernlicht_b;
            connect spannung_V -> aL_Input.spannung_V;
            connect fLLinks_st -> aL_Input.fLLinks_st;
            connect abbleLLinks_st -> aL_Input.abbleLLinks_st;
            connect abbiLLinks_st -> aL_Input.abbiLLinks_st;
            connect fAVLinks_st -> aL_Input.fAVLinks_st;
            connect fAALinks_st -> aL_Input.fAALinks_st;
            connect fAHLinks_st -> aL_Input.fAHLinks_st;
            connect fLRechts_st -> aL_Input.fLRechts_st;
            connect abbleLRechts_st -> aL_Input.abbleLRechts_st;
            connect abbiLRechts_st -> aL_Input.abbiLRechts_st;
            connect fAVRechts_st -> aL_Input.fAVRechts_st;
            connect fAARechts_st -> aL_Input.fAARechts_st;
            connect fAHRechts_st -> aL_Input.fAHRechts_st;
            connect dunkeltaster_st -> aL_Input.dunkeltaster_st;
            connect fahrrichtungsanzeiger.fRAVL_bOut1 -> abschaltung.fRAVL_b;
            connect fahrrichtungsanzeiger.fRAHL_bOut2 -> abschaltung.fRAHL_b;
            connect fahrrichtungsanzeiger.fRAAL_bOut3 -> abschaltung.fRAAL_b;
            connect scheinwerfer.abblendlichtRechts_pcOut2 -> abschaltung.abblendlichtRechts_pc;
            connect scheinwerfer.ausleuchtungRechts_mOut3 -> abschaltung.ausleuchtungRechts_m;
            connect scheinwerfer.abbiegelichtRechts_pcOut5 -> abschaltung.abbiegelichtRechts_pc;
            connect fahrrichtungsanzeiger.fRAVR_bOut4 -> abschaltung.fRAVR_b;
            connect fahrrichtungsanzeiger.fRAHR_bOut5 -> abschaltung.fRAHR_b;
            connect fahrrichtungsanzeiger.fRAAR_bOut6 -> abschaltung.fRAAR_b;
            connect defekt_Erkennung.abblLRechtsDefekt_bOut8 -> abschaltung.abblLRechtsDefekt_b;
            connect defekt_Erkennung.fLRechtsDefekt_bOut7 -> abschaltung.fLRechtsDefekt_b;
            connect defekt_Erkennung.abbiLRechtsDefekt_bOut9 -> abschaltung.abbiLRechtsDefekt_b;
            connect defekt_Erkennung.abbleLLinksDefekt_bOut2 -> abschaltung.abblLLinksDefekt_b;
            connect defekt_Erkennung.fLLinksDefekt_bOut1 -> abschaltung.fLLinksDefekt_b;
            connect defekt_Erkennung.abbiLLinksDefekt_bOut3 -> abschaltung.abbiLLinksDefekt_b;
            connect scheinwerfer.abblendlichtLinks_pcOut1 -> abschaltung.abblendlichtLinks_pc;
            connect scheinwerfer.ausleuchtungLinks_mOut4 -> abschaltung.ausleuchtungLinks_m;
            connect scheinwerfer.abbiegelichtLinks_pcOut6 -> abschaltung.abbiegelichtLinks_pc;
            connect defekt_Erkennung.fAVLinksDefekt_bOut4 -> abschaltung.fAVLinksDefekt_b;
            connect defekt_Erkennung.fAHLinksDefekt_bOut6 -> abschaltung.fAHLinksDefekt_b;
            connect defekt_Erkennung.fAALinksDefekt_bOut5 -> abschaltung.fAALinksDefekt_b;
            connect defekt_Erkennung.fAVRechtsDefekt_bOut10 -> abschaltung.fAVRechtsDefekt_b;
            connect defekt_Erkennung.fAHRechtsDefekt_bOut12 -> abschaltung.fAHRechtsDefekt_b;
            connect defekt_Erkennung.fAARechtsDefekt_bOut11 -> abschaltung.fAARechtsDefekt_b;
            connect abschaltung.fRAVL_bOut1 -> fRAVL_b;
            connect abschaltung.fRAHL_bOut2 -> fRAHL_b;
            connect abschaltung.fRAAL_bOut3 -> fRAAL_b;
            connect abschaltung.fRAVR_bOut4 -> fRAVR_b;
            connect abschaltung.fRAHR_bOut5 -> fRAHR_b;
            connect abschaltung.fRAAR_bOut6 -> fRAAR_b;
            connect abschaltung.abblendlichtLinks_pcOut7 -> abblendlichtLinks_pc;
            connect abschaltung.abblendlichtRechts_pcOut8 -> abblendlichtRechts_pc;
            connect abschaltung.abbiegelichtRechts_pcOut11 -> abbiegelichtRechts_pc;
            connect abschaltung.abbiegelichtLinks_pcOut12 -> abbiegelichtLinks_pc;
            connect abschaltung.ausleuchtungLinks_mOut10 -> ausleuchtungLinks_m;
            connect abschaltung.ausleuchtungRechts_mOut9 -> ausleuchtungRechts_m;
            connect defekt_Erkennung.fLLinksDefekt_bOut1 -> fLLinksDefekt_b;
            connect defekt_Erkennung.abbleLLinksDefekt_bOut2 -> abblLLinksDefekt_b;
            connect defekt_Erkennung.abbiLLinksDefekt_bOut3 -> abbiLLinksDefekt_b;
            connect defekt_Erkennung.fAVLinksDefekt_bOut4 -> fAVLinksDefekt_b;
            connect defekt_Erkennung.fAALinksDefekt_bOut5 -> fAALinksDefekt_b;
            connect defekt_Erkennung.fAHLinksDefekt_bOut6 -> fAHLinksDefekt_b;
            connect defekt_Erkennung.fLRechtsDefekt_bOut7 -> fLRechtsDefekt_b;
            connect defekt_Erkennung.abblLRechtsDefekt_bOut8 -> abblLRechtsDefekt_b;
            connect defekt_Erkennung.abbiLRechtsDefekt_bOut9 -> abbiLRechtsDefekt_b;
            connect defekt_Erkennung.fAVRechtsDefekt_bOut10 -> fAVRechtsDefekt_b;
            connect defekt_Erkennung.fAARechtsDefekt_bOut11 -> fAARechtsDefekt_b;
            connect defekt_Erkennung.fAHRechtsDefekt_bOut12 -> fAHRechtsDefekt_b;
          }
          <<Type="SubSystem">> component DEMO_Aussenlicht_Input {
            ports
              in Boolean rBLinks_bIn1,
              in Boolean rBRechts_bIn2,
              in Boolean warnblinken_bIn3,
              in Double schluessel_stIn4,
              in Double lichtdrehschalter_stIn5,
              in Double aussenhelligkeit_lxIn6,
              in Boolean entriegelt_bIn7,
              in Boolean tuer_bIn8,
              in Double fzggeschw_kmhIn9,
              in Boolean entFahrzeug_bIn10,
              in Boolean fernlicht_bIn11,
              in Double spannung_VIn12,
              in Double fLLinks_stIn13,
              in Double abbleLLinks_stIn14,
              in Double abbiLLinks_stIn15,
              in Double fAVLinks_stIn16,
              in Double fAALinks_stIn17,
              in Double fAHLinks_stIn18,
              in Double fLRechts_stIn19,
              in Double abbleLRechts_stIn20,
              in Double abbiLRechts_stIn21,
              in Double fAVRechts_stIn22,
              in Double fAARechts_stIn23,
              in Double fAHRechts_stIn24,
              in Double dunkeltaster_stIn25,
              out Boolean rBLinks_b,
              out Boolean rBRechts_b,
              out Boolean warnblinken_b,
              out Double schluessel_st,
              out Double schluessel_st1,
              out Double lichtdrehschalter_st,
              out Double aussenhelligkeit_lx,
              out Boolean entriegelt_b,
              out Boolean tuer_b,
              out Double fzggeschw_kmh,
              out Boolean entFahrzeug_b,
              out Boolean fernlicht_b,
              out Double spannung_V,
              out Double fLLinks_st,
              out Double abbleLLinks_st,
              out Double abbiLLinks_st,
              out Double fAVLinks_st,
              out Double fAALinks_st,
              out Double fAHLinks_st,
              out Double fLRechts_st,
              out Double abbleLRechts_st,
              out Double abbiLRechts_st,
              out Double fAVRechts_st,
              out Double fAARechts_st,
              out Double fAHRechts_st,
              out Double dunkeltaster_st;
            connect rBLinks_bIn1 -> rBLinks_b;
            connect rBRechts_bIn2 -> rBRechts_b;
            connect warnblinken_bIn3 -> warnblinken_b;
            connect schluessel_stIn4 -> schluessel_st;
            connect schluessel_stIn4 -> schluessel_st1;
            connect lichtdrehschalter_stIn5 -> lichtdrehschalter_st;
            connect aussenhelligkeit_lxIn6 -> aussenhelligkeit_lx;
            connect entriegelt_bIn7 -> entriegelt_b;
            connect tuer_bIn8 -> tuer_b;
            connect fzggeschw_kmhIn9 -> fzggeschw_kmh;
            connect entFahrzeug_bIn10 -> entFahrzeug_b;
            connect fernlicht_bIn11 -> fernlicht_b;
            connect spannung_VIn12 -> spannung_V;
            connect fLLinks_stIn13 -> fLLinks_st;
            connect abbleLLinks_stIn14 -> abbleLLinks_st;
            connect abbiLLinks_stIn15 -> abbiLLinks_st;
            connect fAVLinks_stIn16 -> fAVLinks_st;
            connect fAALinks_stIn17 -> fAALinks_st;
            connect fAHLinks_stIn18 -> fAHLinks_st;
            connect fLRechts_stIn19 -> fLRechts_st;
            connect abbleLRechts_stIn20 -> abbleLRechts_st;
            connect abbiLRechts_stIn21 -> abbiLRechts_st;
            connect fAVRechts_stIn22 -> fAVRechts_st;
            connect fAARechts_stIn23 -> fAARechts_st;
            connect fAHRechts_stIn24 -> fAHRechts_st;
            connect dunkeltaster_stIn25 -> dunkeltaster_st;
          }
          <<Type="SubSystem">> component DEMO_Aussenlicht_Output {
            ports
              out Double fRAVL_bOut1,
              out Double fRAHL_bOut2,
              out Double fRAAL_bOut3,
              out Double fRAVR_bOut4,
              out Double fRAHR_bOut5,
              out Double fRAAR_bOut6,
              out Double abblendlichLinks_pcOut7,
              out Double abblendlichRechts_pcOut8,
              out Double abbiegelichtRechts_pcOut9,
              out Double abbiegelichtLinks_pcOut10,
              out Double ausleuchtungLinks_mOut11,
              out Double ausleuchtungRechts_mOut12,
              out Boolean fLLinksDefekt_bOut13,
              out Boolean abbleLLinksDefekt_bOut14,
              out Boolean abbiLLinksDefekt_bOut15,
              out Boolean fAVLinksDefekt_bOut16,
              out Boolean fAALinksDefekt_bOut17,
              out Boolean fAHLinksDefekt_bOut18,
              out Boolean fLRechtsDefekt_bOut19,
              out Boolean abblLRechtsDefekt_bOut20,
              out Boolean abbiLRechtsDefekt_bOut21,
              out Boolean fAVRechtsDefekt_bOut22,
              out Boolean fAARechtsDefekt_bOut23,
              out Boolean fAHRechtsDefekt_bOut24,
              in Double fRAVL_b,
              in Double fRAHL_b,
              in Double fRAAL_b,
              in Double fRAVR_b,
              in Double fRAHR_b,
              in Double fRAAR_b,
              in Double abblendlichtLinks_pc,
              in Double abblendlichtRechts_pc,
              in Double abbiegelichtRechts_pc,
              in Double abbiegelichtLinks_pc,
              in Double ausleuchtungLinks_m,
              in Double ausleuchtungRechts_m,
              in Boolean fLLinksDefekt_b,
              in Boolean abblLLinksDefekt_b,
              in Boolean abbiLLinksDefekt_b,
              in Boolean fAVLinksDefekt_b,
              in Boolean fAALinksDefekt_b,
              in Boolean fAHLinksDefekt_b,
              in Boolean fLRechtsDefekt_b,
              in Boolean abblLRechtsDefekt_b,
              in Boolean abbiLRechtsDefekt_b,
              in Boolean fAVRechtsDefekt_b,
              in Boolean fAARechtsDefekt_b,
              in Boolean fAHRechtsDefekt_b;
            connect fRAVL_b -> fRAVL_bOut1;
            connect fRAHL_b -> fRAHL_bOut2;
            connect fRAAL_b -> fRAAL_bOut3;
            connect fRAVR_b -> fRAVR_bOut4;
            connect fRAHR_b -> fRAHR_bOut5;
            connect fRAAR_b -> fRAAR_bOut6;
            connect abblendlichtLinks_pc -> abblendlichLinks_pcOut7;
            connect abblendlichtRechts_pc -> abblendlichRechts_pcOut8;
            connect abbiegelichtRechts_pc -> abbiegelichtRechts_pcOut9;
            connect abbiegelichtLinks_pc -> abbiegelichtLinks_pcOut10;
            connect ausleuchtungLinks_m -> ausleuchtungLinks_mOut11;
            connect ausleuchtungRechts_m -> ausleuchtungRechts_mOut12;
            connect fLLinksDefekt_b -> fLLinksDefekt_bOut13;
            connect abblLLinksDefekt_b -> abbleLLinksDefekt_bOut14;
            connect abbiLLinksDefekt_b -> abbiLLinksDefekt_bOut15;
            connect fAVLinksDefekt_b -> fAVLinksDefekt_bOut16;
            connect fAALinksDefekt_b -> fAALinksDefekt_bOut17;
            connect fAHLinksDefekt_b -> fAHLinksDefekt_bOut18;
            connect fLRechtsDefekt_b -> fLRechtsDefekt_bOut19;
            connect abblLRechtsDefekt_b -> abblLRechtsDefekt_bOut20;
            connect abbiLRechtsDefekt_b -> abbiLRechtsDefekt_bOut21;
            connect fAVRechtsDefekt_b -> fAVRechtsDefekt_bOut22;
            connect fAARechtsDefekt_b -> fAARechtsDefekt_bOut23;
            connect fAHRechtsDefekt_b -> fAHRechtsDefekt_bOut24;
          }
          component DEMO_Aussenlicht_Funktion dEMO_Aussenlicht_Funktion;
          component DEMO_Aussenlicht_Input dEMO_Aussenlicht_Input;
          component DEMO_Aussenlicht_Output dEMO_Aussenlicht_Output;
          connect dEMO_Aussenlicht_Input.rBLinks_b -> dEMO_Aussenlicht_Funktion.rBLinks_b;
          connect dEMO_Aussenlicht_Input.rBRechts_b -> dEMO_Aussenlicht_Funktion.rBRechts_b;
          connect dEMO_Aussenlicht_Input.warnblinken_b -> dEMO_Aussenlicht_Funktion.warnblinken_b;
          connect dEMO_Aussenlicht_Input.schluessel_st -> dEMO_Aussenlicht_Funktion.schluessel_st;
          connect dEMO_Aussenlicht_Input.schluessel_st1 -> dEMO_Aussenlicht_Funktion.schluessel_st1;
          connect dEMO_Aussenlicht_Input.lichtdrehschalter_st -> dEMO_Aussenlicht_Funktion.lichtdrehschalter_st;
          connect dEMO_Aussenlicht_Input.aussenhelligkeit_lx -> dEMO_Aussenlicht_Funktion.aussenhelligkeit_lx;
          connect dEMO_Aussenlicht_Input.entriegelt_b -> dEMO_Aussenlicht_Funktion.entriegelt_b;
          connect dEMO_Aussenlicht_Input.tuer_b -> dEMO_Aussenlicht_Funktion.tuer_b;
          connect dEMO_Aussenlicht_Input.fzggeschw_kmh -> dEMO_Aussenlicht_Funktion.fzggeschw_kmh;
          connect dEMO_Aussenlicht_Input.entFahrzeug_b -> dEMO_Aussenlicht_Funktion.entFahrzeug_b;
          connect dEMO_Aussenlicht_Input.fernlicht_b -> dEMO_Aussenlicht_Funktion.fernlicht_b;
          connect dEMO_Aussenlicht_Input.spannung_V -> dEMO_Aussenlicht_Funktion.spannung_V;
          connect dEMO_Aussenlicht_Input.fLLinks_st -> dEMO_Aussenlicht_Funktion.fLLinks_st;
          connect dEMO_Aussenlicht_Input.abbleLLinks_st -> dEMO_Aussenlicht_Funktion.abbleLLinks_st;
          connect dEMO_Aussenlicht_Input.abbiLLinks_st -> dEMO_Aussenlicht_Funktion.abbiLLinks_st;
          connect dEMO_Aussenlicht_Input.fAVLinks_st -> dEMO_Aussenlicht_Funktion.fAVLinks_st;
          connect dEMO_Aussenlicht_Input.fAALinks_st -> dEMO_Aussenlicht_Funktion.fAALinks_st;
          connect dEMO_Aussenlicht_Input.fAHLinks_st -> dEMO_Aussenlicht_Funktion.fAHLinks_st;
          connect dEMO_Aussenlicht_Input.fLRechts_st -> dEMO_Aussenlicht_Funktion.fLRechts_st;
          connect dEMO_Aussenlicht_Input.abbleLRechts_st -> dEMO_Aussenlicht_Funktion.abbleLRechts_st;
          connect dEMO_Aussenlicht_Input.abbiLRechts_st -> dEMO_Aussenlicht_Funktion.abbiLRechts_st;
          connect dEMO_Aussenlicht_Input.fAVRechts_st -> dEMO_Aussenlicht_Funktion.fAVRechts_st;
          connect dEMO_Aussenlicht_Input.fAARechts_st -> dEMO_Aussenlicht_Funktion.fAARechts_st;
          connect dEMO_Aussenlicht_Input.fAHRechts_st -> dEMO_Aussenlicht_Funktion.fAHRechts_st;
          connect dEMO_Aussenlicht_Input.dunkeltaster_st -> dEMO_Aussenlicht_Funktion.dunkeltaster_st;
          connect dEMO_Aussenlicht_Funktion.fRAVL_b -> dEMO_Aussenlicht_Output.fRAVL_b;
          connect dEMO_Aussenlicht_Funktion.fRAHL_b -> dEMO_Aussenlicht_Output.fRAHL_b;
          connect dEMO_Aussenlicht_Funktion.fRAAL_b -> dEMO_Aussenlicht_Output.fRAAL_b;
          connect dEMO_Aussenlicht_Funktion.fRAVR_b -> dEMO_Aussenlicht_Output.fRAVR_b;
          connect dEMO_Aussenlicht_Funktion.fRAHR_b -> dEMO_Aussenlicht_Output.fRAHR_b;
          connect dEMO_Aussenlicht_Funktion.fRAAR_b -> dEMO_Aussenlicht_Output.fRAAR_b;
          connect dEMO_Aussenlicht_Funktion.abblendlichtLinks_pc -> dEMO_Aussenlicht_Output.abblendlichtLinks_pc;
          connect dEMO_Aussenlicht_Funktion.abblendlichtRechts_pc -> dEMO_Aussenlicht_Output.abblendlichtRechts_pc;
          connect dEMO_Aussenlicht_Funktion.abbiegelichtRechts_pc -> dEMO_Aussenlicht_Output.abbiegelichtRechts_pc;
          connect dEMO_Aussenlicht_Funktion.abbiegelichtLinks_pc -> dEMO_Aussenlicht_Output.abbiegelichtLinks_pc;
          connect dEMO_Aussenlicht_Funktion.ausleuchtungLinks_m -> dEMO_Aussenlicht_Output.ausleuchtungLinks_m;
          connect dEMO_Aussenlicht_Funktion.ausleuchtungRechts_m -> dEMO_Aussenlicht_Output.ausleuchtungRechts_m;
          connect dEMO_Aussenlicht_Funktion.fLLinksDefekt_b -> dEMO_Aussenlicht_Output.fLLinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.abblLLinksDefekt_b -> dEMO_Aussenlicht_Output.abblLLinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.abbiLLinksDefekt_b -> dEMO_Aussenlicht_Output.abbiLLinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAVLinksDefekt_b -> dEMO_Aussenlicht_Output.fAVLinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAALinksDefekt_b -> dEMO_Aussenlicht_Output.fAALinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAHLinksDefekt_b -> dEMO_Aussenlicht_Output.fAHLinksDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fLRechtsDefekt_b -> dEMO_Aussenlicht_Output.fLRechtsDefekt_b;
          connect dEMO_Aussenlicht_Funktion.abblLRechtsDefekt_b -> dEMO_Aussenlicht_Output.abblLRechtsDefekt_b;
          connect dEMO_Aussenlicht_Funktion.abbiLRechtsDefekt_b -> dEMO_Aussenlicht_Output.abbiLRechtsDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAVRechtsDefekt_b -> dEMO_Aussenlicht_Output.fAVRechtsDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAARechtsDefekt_b -> dEMO_Aussenlicht_Output.fAARechtsDefekt_b;
          connect dEMO_Aussenlicht_Funktion.fAHRechtsDefekt_b -> dEMO_Aussenlicht_Output.fAHRechtsDefekt_b;
          connect _Dunkeltaster_stIn25 -> dEMO_Aussenlicht_Input.dunkeltaster_stIn25;
          connect dEMO_Aussenlicht_Output.ausleuchtungRechts_mOut12 -> _AusleuchtungRechts_mOut12;
          connect dEMO_Aussenlicht_Output.fAHRechtsDefekt_bOut24 -> _FAHRechtsDefekt_bOut24;
          connect dEMO_Aussenlicht_Output.fAARechtsDefekt_bOut23 -> _FAARechtsDefekt_bOut23;
          connect dEMO_Aussenlicht_Output.fAVRechtsDefekt_bOut22 -> _FAVRechtsDefekt_bOut22;
          connect dEMO_Aussenlicht_Output.abbiLRechtsDefekt_bOut21 -> _AbbiLRechtsDefekt_bOut21;
          connect dEMO_Aussenlicht_Output.abblLRechtsDefekt_bOut20 -> _AbblLRechtsDefekt_bOut20;
          connect dEMO_Aussenlicht_Output.fLRechtsDefekt_bOut19 -> _FLRechtsDefekt_bOut19;
          connect dEMO_Aussenlicht_Output.fAHLinksDefekt_bOut18 -> _FAHLinksDefekt_bOut18;
          connect dEMO_Aussenlicht_Output.fAALinksDefekt_bOut17 -> _FAALinksDefekt_bOut17;
          connect dEMO_Aussenlicht_Output.fAVLinksDefekt_bOut16 -> _FAVLinksDefekt_bOut16;
          connect dEMO_Aussenlicht_Output.abbiLLinksDefekt_bOut15 -> _AbbiLLinksDefekt_bOut15;
          connect dEMO_Aussenlicht_Output.abbleLLinksDefekt_bOut14 -> _AbbleLLinksDefekt_bOut14;
          connect dEMO_Aussenlicht_Output.fLLinksDefekt_bOut13 -> _FLLinksDefekt_bOut13;
          connect _FAHLinks_stIn18 -> dEMO_Aussenlicht_Input.fAHLinks_stIn18;
          connect _FAHRechts_stIn24 -> dEMO_Aussenlicht_Input.fAHRechts_stIn24;
          connect _AbbleLRechts_stIn20 -> dEMO_Aussenlicht_Input.abbleLRechts_stIn20;
          connect _FAARechts_stIn23 -> dEMO_Aussenlicht_Input.fAARechts_stIn23;
          connect _FLLinks_stIn13 -> dEMO_Aussenlicht_Input.fLLinks_stIn13;
          connect _FLRechts_stIn19 -> dEMO_Aussenlicht_Input.fLRechts_stIn19;
          connect _AbbleLLinks_stIn14 -> dEMO_Aussenlicht_Input.abbleLLinks_stIn14;
          connect _FAVRechts_stIn22 -> dEMO_Aussenlicht_Input.fAVRechts_stIn22;
          connect _FAVLinks_stIn16 -> dEMO_Aussenlicht_Input.fAVLinks_stIn16;
          connect _AbbiLLinks_stIn15 -> dEMO_Aussenlicht_Input.abbiLLinks_stIn15;
          connect _AbbiLRechts_stIn21 -> dEMO_Aussenlicht_Input.abbiLRechts_stIn21;
          connect _FAALinks_stIn17 -> dEMO_Aussenlicht_Input.fAALinks_stIn17;
          connect _Spannung_VIn12 -> dEMO_Aussenlicht_Input.spannung_VIn12;
          connect dEMO_Aussenlicht_Output.abblendlichRechts_pcOut8 -> _AbblendlichtRechts_pcOut8;
          connect dEMO_Aussenlicht_Output.ausleuchtungLinks_mOut11 -> _AusleuchtungLinks_mOut11;
          connect dEMO_Aussenlicht_Output.abbiegelichtLinks_pcOut10 -> _AbbiegelichtsLinks_pcOut10;
          connect dEMO_Aussenlicht_Output.abbiegelichtRechts_pcOut9 -> _AbbiegelichtRechts_pcOut9;
          connect dEMO_Aussenlicht_Output.abblendlichLinks_pcOut7 -> _AbblendlichtLinks_pcOut7;
          connect dEMO_Aussenlicht_Output.fRAAR_bOut6 -> _FRAAR_bOut6;
          connect dEMO_Aussenlicht_Output.fRAHR_bOut5 -> _FRAHR_bOut5;
          connect dEMO_Aussenlicht_Output.fRAVR_bOut4 -> _FRAVR_bOut4;
          connect dEMO_Aussenlicht_Output.fRAAL_bOut3 -> _FRAAL_bOut3;
          connect dEMO_Aussenlicht_Output.fRAHL_bOut2 -> _FRAHL_bOut2;
          connect dEMO_Aussenlicht_Output.fRAVL_bOut1 -> _FRAVL_bOut1;
          connect _Fernlicht_bIn11 -> dEMO_Aussenlicht_Input.fernlicht_bIn11;
          connect _EntFahrzeug_bIn10 -> dEMO_Aussenlicht_Input.entFahrzeug_bIn10;
          connect _Fzggeschw_kmhIn9 -> dEMO_Aussenlicht_Input.fzggeschw_kmhIn9;
          connect _Tuer_bIn8 -> dEMO_Aussenlicht_Input.tuer_bIn8;
          connect _Entriegelt_bIn7 -> dEMO_Aussenlicht_Input.entriegelt_bIn7;
          connect _Aussenhelligkeit_lxIn6 -> dEMO_Aussenlicht_Input.aussenhelligkeit_lxIn6;
          connect _Lichtdrehschalter_stIn5 -> dEMO_Aussenlicht_Input.lichtdrehschalter_stIn5;
          connect _Schluessel_stIn4 -> dEMO_Aussenlicht_Input.schluessel_stIn4;
          connect _Warnblinken_bIn3 -> dEMO_Aussenlicht_Input.warnblinken_bIn3;
          connect _RBRechts_bIn2 -> dEMO_Aussenlicht_Input.rBRechts_bIn2;
          connect _RBLinks_bIn1 -> dEMO_Aussenlicht_Input.rBLinks_bIn1;
        }
        component DEMO_Aussenlicht dEMO_Aussenlicht;
        connect dEMO_Aussenlicht._FAHRechtsDefekt_bOut24 -> _FAHRechtsDefekt_bOut24;
        connect dEMO_Aussenlicht._FAARechtsDefekt_bOut23 -> _FAARechtsDefekt_bOut23;
        connect dEMO_Aussenlicht._FAVRechtsDefekt_bOut22 -> _FAVRechtsDefekt_bOut22;
        connect dEMO_Aussenlicht._AbbiLRechtsDefekt_bOut21 -> _AbbiLRechtsDefekt_bOut21;
        connect dEMO_Aussenlicht._AbblLRechtsDefekt_bOut20 -> _AbblLRechtsDefekt_bOut20;
        connect dEMO_Aussenlicht._FLRechtsDefekt_bOut19 -> _FLRechtsDefekt_bOut19;
        connect dEMO_Aussenlicht._FAHLinksDefekt_bOut18 -> _FAHLinksDefekt_bOut18;
        connect dEMO_Aussenlicht._FAALinksDefekt_bOut17 -> _FAALinksDefekt_bOut17;
        connect dEMO_Aussenlicht._FAVLinksDefekt_bOut16 -> _FAVLinksDefekt_bOut16;
        connect dEMO_Aussenlicht._AbbiLLinksDefekt_bOut15 -> _AbbiLLinksDefekt_bOut15;
        connect dEMO_Aussenlicht._AbbleLLinksDefekt_bOut14 -> _AbbleLLinksDefekt_bOut14;
        connect dEMO_Aussenlicht._FLLinksDefekt_bOut13 -> _FLLinksDefekt_bOut13;
        connect dEMO_Aussenlicht._AusleuchtungRechts_mOut12 -> _AusleuchtungRechts_mOut12;
        connect dEMO_Aussenlicht._AusleuchtungLinks_mOut11 -> _AusleuchtungLinks_mOut11;
        connect dEMO_Aussenlicht._AbbiegelichtsLinks_pcOut10 -> _AbbiegelichtsLinks_pcOut10;
        connect dEMO_Aussenlicht._AbbiegelichtRechts_pcOut9 -> _AbbiegelichtRechts_pcOut9;
        connect dEMO_Aussenlicht._AbblendlichtRechts_pcOut8 -> _AbblendlichtRechts_pcOut8;
        connect dEMO_Aussenlicht._AbblendlichtLinks_pcOut7 -> _AbblendlichtLinks_pcOut7;
        connect dEMO_Aussenlicht._FRAAR_bOut6 -> _FRAAR_bOut6;
        connect dEMO_Aussenlicht._FRAHR_bOut5 -> _FRAHR_bOut5;
        connect dEMO_Aussenlicht._FRAVR_bOut4 -> _FRAVR_bOut4;
        connect dEMO_Aussenlicht._FRAAL_bOut3 -> _FRAAL_bOut3;
        connect dEMO_Aussenlicht._FRAHL_bOut2 -> _FRAHL_bOut2;
        connect dEMO_Aussenlicht._FRAVL_bOut1 -> _FRAVL_bOut1;
        connect _Dunkeltaster_stIn25 -> dEMO_Aussenlicht._Dunkeltaster_stIn25;
        connect _FAHRechts_stIn24 -> dEMO_Aussenlicht._FAHRechts_stIn24;
        connect _FAARechts_stIn23 -> dEMO_Aussenlicht._FAARechts_stIn23;
        connect _FAVRechts_stIn22 -> dEMO_Aussenlicht._FAVRechts_stIn22;
        connect _AbbiLRechts_stIn21 -> dEMO_Aussenlicht._AbbiLRechts_stIn21;
        connect _AbbleLRechts_stIn20 -> dEMO_Aussenlicht._AbbleLRechts_stIn20;
        connect _FLRechts_stIn19 -> dEMO_Aussenlicht._FLRechts_stIn19;
        connect _FAHLinks_stIn18 -> dEMO_Aussenlicht._FAHLinks_stIn18;
        connect _FAALinks_stIn17 -> dEMO_Aussenlicht._FAALinks_stIn17;
        connect _FAVLinks_stIn16 -> dEMO_Aussenlicht._FAVLinks_stIn16;
        connect _AbbiLLinks_stIn15 -> dEMO_Aussenlicht._AbbiLLinks_stIn15;
        connect _AbbleLLinks_stIn14 -> dEMO_Aussenlicht._AbbleLLinks_stIn14;
        connect _FLLinks_stIn13 -> dEMO_Aussenlicht._FLLinks_stIn13;
        connect _Spannung_VIn12 -> dEMO_Aussenlicht._Spannung_VIn12;
        connect _Fernlicht_bIn11 -> dEMO_Aussenlicht._Fernlicht_bIn11;
        connect _EntFahrzeug_bIn10 -> dEMO_Aussenlicht._EntFahrzeug_bIn10;
        connect _Fzggeschw_kmhIn9 -> dEMO_Aussenlicht._Fzggeschw_kmhIn9;
        connect _Tuer_bIn8 -> dEMO_Aussenlicht._Tuer_bIn8;
        connect _Entriegelt_bIn7 -> dEMO_Aussenlicht._Entriegelt_bIn7;
        connect _Aussenhelligkeit_lxIn6 -> dEMO_Aussenlicht._Aussenhelligkeit_lxIn6;
        connect _Lichtdrehschalter_stIn5 -> dEMO_Aussenlicht._Lichtdrehschalter_stIn5;
        connect _Schluessel_stIn4 -> dEMO_Aussenlicht._Schluessel_stIn4;
        connect _Warnblinken_bIn3 -> dEMO_Aussenlicht._Warnblinken_bIn3;
        connect _RBRechts_bIn2 -> dEMO_Aussenlicht._RBRechts_bIn2;
        connect _RBLinks_bIn1 -> dEMO_Aussenlicht._RBLinks_bIn1;
      }
      component Subsystem subsystem;
      connect subsystem._FAHRechtsDefekt_bOut24 -> _FAHRechtsDefekt_bOut24;
      connect subsystem._FAARechtsDefekt_bOut23 -> _FAARechtsDefekt_bOut23;
      connect subsystem._FAVRechtsDefekt_bOut22 -> _FAVRechtsDefekt_bOut22;
      connect subsystem._AbbiLRechtsDefekt_bOut21 -> _AbbiLRechtsDefekt_bOut21;
      connect subsystem._AbblLRechtsDefekt_bOut20 -> _AbblLRechtsDefekt_bOut20;
      connect subsystem._FLRechtsDefekt_bOut19 -> _FLRechtsDefekt_bOut19;
      connect subsystem._FAHLinksDefekt_bOut18 -> _FAHLinksDefekt_bOut18;
      connect subsystem._FAALinksDefekt_bOut17 -> _FAALinksDefekt_bOut17;
      connect subsystem._FAVLinksDefekt_bOut16 -> _FAVLinksDefekt_bOut16;
      connect subsystem._AbbiLLinksDefekt_bOut15 -> _AbbiLLinksDefekt_bOut15;
      connect subsystem._AbbleLLinksDefekt_bOut14 -> _AbbleLLinksDefekt_bOut14;
      connect subsystem._FLLinksDefekt_bOut13 -> _FLLinksDefekt_bOut13;
      connect subsystem._AusleuchtungRechts_mOut12 -> _AusleuchtungRechts_mOut12;
      connect subsystem._AusleuchtungLinks_mOut11 -> _AusleuchtungLinks_mOut11;
      connect subsystem._AbbiegelichtsLinks_pcOut10 -> _AbbiegelichtsLinks_pcOut10;
      connect subsystem._AbbiegelichtRechts_pcOut9 -> _AbbiegelichtRechts_pcOut9;
      connect subsystem._AbblendlichtRechts_pcOut8 -> _AbblendlichtRechts_pcOut8;
      connect subsystem._AbblendlichtLinks_pcOut7 -> _AbblendlichtLinks_pcOut7;
      connect subsystem._FRAAR_bOut6 -> _FRAAR_bOut6;
      connect subsystem._FRAHR_bOut5 -> _FRAHR_bOut5;
      connect subsystem._FRAVR_bOut4 -> _FRAVR_bOut4;
      connect subsystem._FRAAL_bOut3 -> _FRAAL_bOut3;
      connect subsystem._FRAHL_bOut2 -> _FRAHL_bOut2;
      connect subsystem._FRAVL_bOut1 -> _FRAVL_bOut1;
      connect _Dunkeltaster_stIn25 -> subsystem._Dunkeltaster_stIn25;
      connect _FAHRechts_stIn24 -> subsystem._FAHRechts_stIn24;
      connect _FAARechts_stIn23 -> subsystem._FAARechts_stIn23;
      connect _FAVRechts_stIn22 -> subsystem._FAVRechts_stIn22;
      connect _AbbiLRechts_stIn21 -> subsystem._AbbiLRechts_stIn21;
      connect _AbbleLRechts_stIn20 -> subsystem._AbbleLRechts_stIn20;
      connect _FLRechts_stIn19 -> subsystem._FLRechts_stIn19;
      connect _FAHLinks_stIn18 -> subsystem._FAHLinks_stIn18;
      connect _FAALinks_stIn17 -> subsystem._FAALinks_stIn17;
      connect _FAVLinks_stIn16 -> subsystem._FAVLinks_stIn16;
      connect _AbbiLLinks_stIn15 -> subsystem._AbbiLLinks_stIn15;
      connect _AbbleLLinks_stIn14 -> subsystem._AbbleLLinks_stIn14;
      connect _FLLinks_stIn13 -> subsystem._FLLinks_stIn13;
      connect _Spannung_VIn12 -> subsystem._Spannung_VIn12;
      connect _Fernlicht_bIn11 -> subsystem._Fernlicht_bIn11;
      connect _EntFahrzeug_bIn10 -> subsystem._EntFahrzeug_bIn10;
      connect _Fzggeschw_kmhIn9 -> subsystem._Fzggeschw_kmhIn9;
      connect _Tuer_bIn8 -> subsystem._Tuer_bIn8;
      connect _Entriegelt_bIn7 -> subsystem._Entriegelt_bIn7;
      connect _Aussenhelligkeit_lxIn6 -> subsystem._Aussenhelligkeit_lxIn6;
      connect _Lichtdrehschalter_stIn5 -> subsystem._Lichtdrehschalter_stIn5;
      connect _Schluessel_stIn4 -> subsystem._Schluessel_stIn4;
      connect _Warnblinken_bIn3 -> subsystem._Warnblinken_bIn3;
      connect _RBRechts_bIn2 -> subsystem._RBRechts_bIn2;
      connect _RBLinks_bIn1 -> subsystem._RBLinks_bIn1;
    }
    component DEMO_Aussenlicht dEMO_Aussenlicht;
    connect dunkeltaster_st -> dEMO_Aussenlicht._Dunkeltaster_stIn25;
    connect fLLinks_st -> dEMO_Aussenlicht._FLLinks_stIn13;
    connect abbleLRechts_st -> dEMO_Aussenlicht._AbbleLRechts_stIn20;
    connect fLRechts_st -> dEMO_Aussenlicht._FLRechts_stIn19;
    connect abbiLRechts_st -> dEMO_Aussenlicht._AbbiLRechts_stIn21;
    connect fAHLinks_st -> dEMO_Aussenlicht._FAHLinks_stIn18;
    connect fAARechts_st -> dEMO_Aussenlicht._FAARechts_stIn23;
    connect fAVRechts_st -> dEMO_Aussenlicht._FAVRechts_stIn22;
    connect abbleLLinks_st -> dEMO_Aussenlicht._AbbleLLinks_stIn14;
    connect fAVLinks_st -> dEMO_Aussenlicht._FAVLinks_stIn16;
    connect abbiLLinks_st -> dEMO_Aussenlicht._AbbiLLinks_stIn15;
    connect fAHRechts_st -> dEMO_Aussenlicht._FAHRechts_stIn24;
    connect fAALinks_st -> dEMO_Aussenlicht._FAALinks_stIn17;
    connect spannung_V -> dEMO_Aussenlicht._Spannung_VIn12;
    connect fernlicht_b -> dEMO_Aussenlicht._Fernlicht_bIn11;
    connect entFahrzeug_b -> dEMO_Aussenlicht._EntFahrzeug_bIn10;
    connect fzggeschw_kmh -> dEMO_Aussenlicht._Fzggeschw_kmhIn9;
    connect tuer_b -> dEMO_Aussenlicht._Tuer_bIn8;
    connect entriegelt_b -> dEMO_Aussenlicht._Entriegelt_bIn7;
    connect aussenhelligkeit_lx -> dEMO_Aussenlicht._Aussenhelligkeit_lxIn6;
    connect lichtdrehschalter_st -> dEMO_Aussenlicht._Lichtdrehschalter_stIn5;
    connect schluessel_st -> dEMO_Aussenlicht._Schluessel_stIn4;
    connect warnblinken_b -> dEMO_Aussenlicht._Warnblinken_bIn3;
    connect rBRechts_b -> dEMO_Aussenlicht._RBRechts_bIn2;
    connect rBLinks_b -> dEMO_Aussenlicht._RBLinks_bIn1;
    connect dEMO_Aussenlicht._FRAVL_bOut1 -> fRAVL_b;
    connect dEMO_Aussenlicht._FRAHL_bOut2 -> fRAHL_b;
    connect dEMO_Aussenlicht._FRAAL_bOut3 -> fRAAL_b;
    connect dEMO_Aussenlicht._FRAVR_bOut4 -> fRAVR_b;
    connect dEMO_Aussenlicht._FRAHR_bOut5 -> fRAHR_b;
    connect dEMO_Aussenlicht._FRAAR_bOut6 -> fRAAR_b;
    connect dEMO_Aussenlicht._AbblendlichtLinks_pcOut7 -> abblendlichtLinks_pc;
    connect dEMO_Aussenlicht._AbblendlichtRechts_pcOut8 -> abblendlichtRechts_pc;
    connect dEMO_Aussenlicht._AbbiegelichtRechts_pcOut9 -> abbiegelichtRechts_pc;
    connect dEMO_Aussenlicht._AbbiegelichtsLinks_pcOut10 -> abbiegelichtLinks_pc;
    connect dEMO_Aussenlicht._AusleuchtungLinks_mOut11 -> ausleuchtungLinks_m;
    connect dEMO_Aussenlicht._AusleuchtungRechts_mOut12 -> ausleuchtungRechts_m;
    connect dEMO_Aussenlicht._FLLinksDefekt_bOut13 -> fLLinksDefekt_b;
    connect dEMO_Aussenlicht._AbbleLLinksDefekt_bOut14 -> abblLLinksDefekt_b;
    connect dEMO_Aussenlicht._AbbiLLinksDefekt_bOut15 -> abbiLLinksDefekt_b;
    connect dEMO_Aussenlicht._FAVLinksDefekt_bOut16 -> fAVLinksDefekt_b;
    connect dEMO_Aussenlicht._FAALinksDefekt_bOut17 -> fAALinksDefekt_b;
    connect dEMO_Aussenlicht._FAHLinksDefekt_bOut18 -> fAHLinksDefekt_b;
    connect dEMO_Aussenlicht._FLRechtsDefekt_bOut19 -> fLRechtsDefekt_b;
    connect dEMO_Aussenlicht._AbblLRechtsDefekt_bOut20 -> abblLRechtsDefekt_b;
    connect dEMO_Aussenlicht._AbbiLRechtsDefekt_bOut21 -> abbiLRechtsDefekt_b;
    connect dEMO_Aussenlicht._FAVRechtsDefekt_bOut22 -> fAVRechtsDefekt_b;
    connect dEMO_Aussenlicht._FAARechtsDefekt_bOut23 -> fAARechtsDefekt_b;
    connect dEMO_Aussenlicht._FAHRechtsDefekt_bOut24 -> fAHRechtsDefekt_b;
  }
  <<Type="SubSystem">> component Defekt_AbbiegelichtLinks {
    ports
      in Boolean aBLLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect aBLLDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component Defekt_AbbiegelichtRechts {
    ports
      in Boolean aBLRDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect aBLRDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_AbblendlichtLinks {
    ports
      in Boolean aLLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect aLLDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component Defekt_AbblendlichtRechts {
    ports
      in Boolean aLRDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect aLRDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerAussenspiegelLinks {
    ports
      in Boolean fAALDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAALDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerAussenspiegelRechts {
    ports
      in Boolean fAARDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect fAARDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerHintenLinks {
    ports
      in Boolean fAHLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAHLDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerHintenRechts {
    ports
      in Boolean fAHRDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect fAHRDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerVorneLinks {
    ports
      in Boolean fAVLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAVLDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component Defekt_FahrreichtungsanzeigerVorneRechts {
    ports
      in Boolean fAVRDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect fAVRDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_FernlichRechts {
    ports
      in Boolean fLRDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
    connect fLRDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component Defekt_FernlichtLinks {
    ports
      in Boolean fLLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Boolean in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fLLDefektIn1 -> rGB.in1;
    connect constant.out1 -> rGB.in3;
    connect constant.out1 -> rGB.in2;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerAussenspiegelLinks {
    ports
      in Double fAALIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in3;
    connect fAALIn1 -> rGB.in2;
    connect fAALIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerAussenspiegelRechts {
    ports
      in Double fAARIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAARIn1 -> rGB.in1;
    connect fAARIn1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerHintenLinks {
    ports
      in Double fAHLIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAHLIn1 -> rGB.in1;
    connect fAHLIn1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerHintenRechts {
    ports
      in Double aLLDefektIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in3;
    connect aLLDefektIn1 -> rGB.in2;
    connect aLLDefektIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerVorneLinks {
    ports
      in Double fAVLIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect constant.out1 -> rGB.in3;
    connect fAVLIn1 -> rGB.in2;
    connect fAVLIn1 -> rGB.in1;
    connect rGB.out1 -> terminator.in1;
  }
  <<Type="SubSystem">> component FahrrichtungsanzeigerVorneRechts {
    ports
      in Double fAVRIn1;
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    component Constant constant;
    component RGB rGB;
    component Terminator terminator;
    connect rGB.out1 -> terminator.in1;
    connect fAVRIn1 -> rGB.in1;
    connect fAVRIn1 -> rGB.in2;
    connect constant.out1 -> rGB.in3;
  }
  <<Type="SubSystem">> component FalseBlock {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock1 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock10 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock2 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock3 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock4 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock5 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock6 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock7 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock8 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FalseBlock9 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="0">> component Zero {
      ports
        out Double out1;
    }
    component Zero zero;
    connect zero.out1 -> yOut1;
  }
  <<Type="SubSystem">> component FernlichtLinks {
    ports
      in Double ausleuchtungLinksIn1;
    <<Type="Sum",ListOfSigns="++">> component Add {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Constant",Value="0">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="1">> component Constant2 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.49">> component Constant3 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.0017">> component Constant4 {
      ports
        out Double out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Product",Inputs="2">> component Product {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Operator=">=",Type="RelationalOperator">> component RelationalOperator {
      ports
        in Double in1,
        in Double in2,
        out Boolean out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
      ports
        in Double ifIn,
        in Boolean condition,
        in Double elseIn,
        out Double out1;
      effect ifIn -> out1;
      effect condition -> out1;
      effect elseIn -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Condition="u2 >= 1",Type="Condition">> component Condition {
      ports
        in Boolean in1,
        out Boolean out1;
    }
    component Add add;
    component Constant1 constant1;
    component Constant2 constant2;
    component Constant3 constant3;
    component Constant4 constant4;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component Product product;
    component RGB rGB;
    component RelationalOperator relationalOperator;
    component SwitchBlock switchBlock;
    component Terminator terminator;
    component Condition condition;
    connect condition.out1 -> switchBlock.condition;
    connect relationalOperator.out1 -> condition.in1;
    connect constant4.out1 -> product.in2;
    connect constant3.out1 -> add.in2;
    connect add.out1 -> switchBlock.ifIn;
    connect product.out1 -> add.in1;
    connect ausleuchtungLinksIn1 -> product.in1;
    connect ausleuchtungLinksIn1 -> relationalOperator.in1;
    connect constant2.out1 -> relationalOperator.in2;
    connect rGB.out1 -> terminator.in1;
    connect constant1.out1 -> switchBlock.elseIn;
    connect gain2.out1 -> rGB.in1;
    connect gain.out1 -> rGB.in2;
    connect gain1.out1 -> rGB.in3;
    connect switchBlock.out1 -> gain1.in1;
    connect switchBlock.out1 -> gain2.in1;
    connect switchBlock.out1 -> gain.in1;
  }
  <<Type="SubSystem">> component FernlichtRechts {
    ports
      in Double ausleuchtungRechtsIn1;
    <<Type="Sum",ListOfSigns="++">> component Add {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Constant",Value="0">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="1">> component Constant2 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.49">> component Constant3 {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="0.0017">> component Constant4 {
      ports
        out Double out1;
    }
    <<Type="Gain",Gain="0.94">> component Gain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="1">> component Gain1 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Gain",Gain="0.84">> component Gain2 {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    <<Type="Product",Inputs="2">> component Product {
      ports
        in Double in1,
        in Double in2,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="M-S-Function">> component RGB {
      ports
        in Double in1,
        in Double in2,
        in Double in3,
        out Double out1;
      effect in1 -> out1;
      effect in2 -> out1;
      effect in3 -> out1;
    }
    <<Operator=">=",Type="RelationalOperator">> component RelationalOperator {
      ports
        in Double in1,
        in Double in2,
        out Boolean out1;
      effect in1 -> out1;
      effect in2 -> out1;
    }
    <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock {
      ports
        in Double ifIn,
        in Boolean condition,
        in Double elseIn,
        out Double out1;
      effect ifIn -> out1;
      effect condition -> out1;
      effect elseIn -> out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Condition="u2 >= 1",Type="Condition">> component Condition {
      ports
        in Boolean in1,
        out Boolean out1;
    }
    component Add add;
    component Constant1 constant1;
    component Constant2 constant2;
    component Constant3 constant3;
    component Constant4 constant4;
    component Gain gain;
    component Gain1 gain1;
    component Gain2 gain2;
    component Product product;
    component RGB rGB;
    component RelationalOperator relationalOperator;
    component SwitchBlock switchBlock;
    component Terminator terminator;
    component Condition condition;
    connect condition.out1 -> switchBlock.condition;
    connect relationalOperator.out1 -> condition.in1;
    connect constant4.out1 -> product.in2;
    connect constant3.out1 -> add.in2;
    connect add.out1 -> switchBlock.ifIn;
    connect product.out1 -> add.in1;
    connect ausleuchtungRechtsIn1 -> product.in1;
    connect ausleuchtungRechtsIn1 -> relationalOperator.in1;
    connect constant2.out1 -> relationalOperator.in2;
    connect rGB.out1 -> terminator.in1;
    connect constant1.out1 -> switchBlock.elseIn;
    connect gain2.out1 -> rGB.in1;
    connect gain.out1 -> rGB.in2;
    connect gain1.out1 -> rGB.in3;
    connect switchBlock.out1 -> gain1.in1;
    connect switchBlock.out1 -> gain2.in1;
    connect switchBlock.out1 -> gain.in1;
  }
  <<Type="ManualSwitch">> component ManualSwitch {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch1 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch10 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch2 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch3 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch4 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch5 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch6 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch7 {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component ManualSwitch8 {
    ports
      in Double in1,
      in Double in2,
      out Double out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="ManualSwitch">> component SW_Entriegelt {
    ports
      in Double in1,
      in Double in2,
      out Boolean out1;
    effect in1 -> out1;
    effect in2 -> out1;
  }
  <<Type="SubSystem">> component SimToRealTime {
    <<Type="Constant",Value="0">> component Constant {
      ports
        out Double out1;
    }
    <<Type="Constant",Value="1">> component Constant1 {
      ports
        out Double out1;
    }
    <<Type="Terminator">> component Terminator {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator1 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator2 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator3 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator4 {
      ports
        in Double in1;
    }
    <<Type="Terminator">> component Terminator5 {
      ports
        in Double in1;
    }
    <<Type="SubSystem">> component VAPS_TimeConfiguration {
      ports
        in Double timeConfigIn1,
        in Double maxSpeedIn2,
        out Double factor_SimTimeOut1,
        out Double timeConfig_ConsumerOut2,
        out Double timeConfig_ProducerOut3,
        out Double tsSimOut4,
        out Double tsRTimeOut5,
        out Double tsRTimeEffOut6;
      <<Type="Constant",Value="0">> component Constant1 {
        ports
          out Double out1;
      }
      <<Type="Constant",Value="2">> component Constant10 {
        ports
          out Double out1;
      }
      <<Type="Constant",Value="0">> component Constant4 {
        ports
          out Double out1;
      }
      <<Type="SubSystem">> component RealTimeTimerAdapter {
        ports
          in Double in1In1,
          out Double out1Out1;
        <<Type="Constant",Value="-10">> component Constant1 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant10 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant11 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="-1">> component Constant12 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1">> component Constant13 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="1.1">> component Constant15 {
          ports
            out Double out1;
        }
        <<Type="Product",Inputs="*/">> component Div1 {
          ports
            in Double in1,
            in Double in2,
            out Double out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="AND",Type="Logic">> component LogOp1 {
          ports
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">",Type="RelationalOperator">> component RelOp2 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="<=",Type="RelationalOperator">> component RelOp3 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">=",Type="RelationalOperator">> component RelOp4 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Sum",ListOfSigns="+-">> component Sum {
          ports
            in Double in1,
            in Double in2,
            out Double out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        component Constant1 constant1;
        component Constant10 constant10;
        component Constant11 constant11;
        component Constant12 constant12;
        component Constant13 constant13;
        component Constant15 constant15;
        component Div1 div1;
        component LogOp1 logOp1;
        component RelOp2 relOp2;
        component RelOp3 relOp3;
        component RelOp4 relOp4;
        component Sum sum;
        component SwitchBlock2 switchBlock2;
        component SwitchBlock3 switchBlock3;
        component Condition condition;
        component Condition1 condition1;
        connect condition.out1 -> switchBlock2.condition;
        connect condition1.out1 -> switchBlock3.condition;
        connect relOp2.out1 -> condition.in1;
        connect logOp1.out1 -> condition1.in1;
        connect in1In1 -> relOp2.in1;
        connect in1In1 -> switchBlock2.ifIn;
        connect in1In1 -> relOp3.in1;
        connect in1In1 -> relOp4.in1;
        connect in1In1 -> div1.in1;
        connect constant10.out1 -> relOp2.in2;
        connect switchBlock2.out1 -> out1Out1;
        connect constant11.out1 -> relOp3.in2;
        connect switchBlock3.out1 -> switchBlock2.elseIn;
        connect constant12.out1 -> relOp4.in2;
        connect relOp3.out1 -> logOp1.in1;
        connect relOp4.out1 -> logOp1.in2;
        connect constant13.out1 -> switchBlock3.ifIn;
        connect sum.out1 -> switchBlock3.elseIn;
        connect constant1.out1 -> div1.in2;
        connect constant15.out1 -> sum.in1;
        connect div1.out1 -> sum.in2;
      }
      <<Type="SubSystem">> component RealTimeTimerAdapter_Value {
        ports
          in Double in1In1,
          out Double out1Out1;
        <<Type="Constant",Value="1">> component Constant11 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="-1">> component Constant12 {
          ports
            out Double out1;
        }
        <<Type="Constant",Value="0">> component Constant13 {
          ports
            out Double out1;
        }
        <<Operator="AND",Type="Logic">> component LogOp1 {
          ports
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator="<=",Type="RelationalOperator">> component RelOp3 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Operator=">=",Type="RelationalOperator">> component RelOp4 {
          ports
            in Double in1,
            in Double in2,
            out Boolean out1;
          effect in1 -> out1;
          effect in2 -> out1;
        }
        <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock3 {
          ports
            in Double ifIn,
            in Boolean condition,
            in Double elseIn,
            out Double out1;
          effect ifIn -> out1;
          effect condition -> out1;
          effect elseIn -> out1;
        }
        <<Condition="u2 >= 1",Type="Condition">> component Condition {
          ports
            in Boolean in1,
            out Boolean out1;
        }
        component Constant11 constant11;
        component Constant12 constant12;
        component Constant13 constant13;
        component LogOp1 logOp1;
        component RelOp3 relOp3;
        component RelOp4 relOp4;
        component SwitchBlock3 switchBlock3;
        component Condition condition;
        connect condition.out1 -> switchBlock3.condition;
        connect logOp1.out1 -> condition.in1;
        connect constant13.out1 -> switchBlock3.ifIn;
        connect relOp4.out1 -> logOp1.in2;
        connect relOp3.out1 -> logOp1.in1;
        connect constant12.out1 -> relOp4.in2;
        connect constant11.out1 -> relOp3.in2;
        connect switchBlock3.out1 -> out1Out1;
        connect in1In1 -> switchBlock3.elseIn;
        connect in1In1 -> relOp4.in1;
        connect in1In1 -> relOp3.in1;
      }
      <<Operator="==",Type="RelationalOperator">> component RelOp4 {
        ports
          in Double in1,
          in Double in2,
          out Boolean out1;
        effect in1 -> out1;
        effect in2 -> out1;
      }
      <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock1 {
        ports
          in Double ifIn,
          in Boolean condition,
          in Double elseIn,
          out Double out1;
        effect ifIn -> out1;
        effect condition -> out1;
        effect elseIn -> out1;
      }
      <<Type="Switch",Criteria="u2 >= Threshold",Threshold="1">> component SwitchBlock2 {
        ports
          in Double ifIn,
          in Boolean condition,
          in Double elseIn,
          out Double out1;
        effect ifIn -> out1;
        effect condition -> out1;
        effect elseIn -> out1;
      }
      <<Type="SubSystem">> component SysInit1 {
        ports
          out Double yOut1;
        <<Type="UnitDelay",InitialCondition="0">> component Memory_Init {
          ports
            in Double in1,
            out Double out1;
          effect in1 -> out1;
        }
        <<Type="Constant",Value="0">> component Zero_Init {
          ports
            out Double out1;
        }
        component Memory_Init memory_Init;
        component Zero_Init zero_Init;
        connect memory_Init.out1 -> yOut1;
        connect zero_Init.out1 -> memory_Init.in1;
      }
      <<Type="SubSystem">> component VAPSRealTimeTimer {
        ports
          in Double timeFactorIn1,
          out Double tsSimOut1,
          out Double tsRTimeOut2,
          out Double tsRTimeEffectiveOut3;
        <<Type="M-S-Function">> component Level2MfileSFunction {
          ports
            in Double in1,
            out Double tsSimOut1,
            out Double tsRTimeOut2,
            out Double tsRTimeEffectiveOut3;
          effect in1 -> tsSimOut1;
          effect in1 -> tsRTimeOut2;
          effect in1 -> tsRTimeEffectiveOut3;
        }
        component Level2MfileSFunction level2MfileSFunction;
        connect timeFactorIn1 -> level2MfileSFunction.in1;
        connect level2MfileSFunction.tsSimOut1 -> tsSimOut1;
        connect level2MfileSFunction.tsRTimeOut2 -> tsRTimeOut2;
        connect level2MfileSFunction.tsRTimeEffectiveOut3 -> tsRTimeEffectiveOut3;
      }
      <<Condition="u2 >= 1",Type="Condition">> component Condition {
        ports
          in Boolean in1,
          out Boolean out1;
      }
      <<Condition="u2 >= 1",Type="Condition">> component Condition1 {
        ports
          in Double in1,
          out Boolean out1;
      }
      component Constant1 constant1;
      component Constant10 constant10;
      component Constant4 constant4;
      component RealTimeTimerAdapter realTimeTimerAdapter;
      component RealTimeTimerAdapter_Value realTimeTimerAdapter_Value;
      component RelOp4 relOp4;
      component SwitchBlock1 switchBlock1;
      component SwitchBlock2 switchBlock2;
      component SysInit1 sysInit1;
      component VAPSRealTimeTimer vAPSRealTimeTimer;
      component Condition condition;
      component Condition1 condition1;
      connect condition.out1 -> switchBlock1.condition;
      connect condition1.out1 -> switchBlock2.condition;
      connect relOp4.out1 -> condition.in1;
      connect sysInit1.yOut1 -> condition1.in1;
      connect constant1.out1 -> switchBlock1.ifIn;
      connect maxSpeedIn2 -> relOp4.in1;
      connect switchBlock1.out1 -> vAPSRealTimeTimer.timeFactorIn1;
      connect constant10.out1 -> relOp4.in2;
      connect vAPSRealTimeTimer.tsRTimeEffectiveOut3 -> tsRTimeEffOut6;
      connect vAPSRealTimeTimer.tsRTimeOut2 -> tsRTimeOut5;
      connect vAPSRealTimeTimer.tsSimOut1 -> tsSimOut4;
      connect realTimeTimerAdapter_Value.out1Out1 -> timeConfig_ProducerOut3;
      connect realTimeTimerAdapter_Value.out1Out1 -> timeConfig_ConsumerOut2;
      connect realTimeTimerAdapter.out1Out1 -> switchBlock1.elseIn;
      connect realTimeTimerAdapter.out1Out1 -> factor_SimTimeOut1;
      connect timeConfigIn1 -> switchBlock2.elseIn;
      connect constant4.out1 -> switchBlock2.ifIn;
      connect switchBlock2.out1 -> realTimeTimerAdapter_Value.in1In1;
      connect switchBlock2.out1 -> realTimeTimerAdapter.in1In1;
    }
    component Constant constant;
    component Constant1 constant1;
    component Terminator terminator;
    component Terminator1 terminator1;
    component Terminator2 terminator2;
    component Terminator3 terminator3;
    component Terminator4 terminator4;
    component Terminator5 terminator5;
    component VAPS_TimeConfiguration vAPS_TimeConfiguration;
    connect vAPS_TimeConfiguration.timeConfig_ProducerOut3 -> terminator2.in1;
    connect vAPS_TimeConfiguration.tsSimOut4 -> terminator3.in1;
    connect vAPS_TimeConfiguration.tsRTimeOut5 -> terminator4.in1;
    connect vAPS_TimeConfiguration.tsRTimeEffOut6 -> terminator5.in1;
    connect vAPS_TimeConfiguration.timeConfig_ConsumerOut2 -> terminator1.in1;
    connect vAPS_TimeConfiguration.factor_SimTimeOut1 -> terminator.in1;
    connect constant1.out1 -> vAPS_TimeConfiguration.maxSpeedIn2;
    connect constant.out1 -> vAPS_TimeConfiguration.timeConfigIn1;
  }
  <<Type="SubSystem">> component SliderGain {
    ports
      in Double uIn1,
      out Double yOut1;
    <<Type="Gain",Gain="gain">> component SliderGain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    component SliderGain sliderGain;
    connect sliderGain.out1 -> yOut1;
    connect uIn1 -> sliderGain.in1;
  }
  <<Type="SubSystem">> component SliderGain2 {
    ports
      in Double uIn1,
      out Double yOut1;
    <<Type="Gain",Gain="gain">> component SliderGain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    component SliderGain sliderGain;
    connect sliderGain.out1 -> yOut1;
    connect uIn1 -> sliderGain.in1;
  }
  <<Type="SubSystem">> component Subsystem {
  }
  <<Type="SubSystem">> component TrueBlock {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock1 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock10 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock2 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock3 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock4 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock5 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock6 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock7 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock8 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component TrueBlock9 {
    ports
      out Double yOut1;
    <<Type="Constant",Value="1">> component One {
      ports
        out Double out1;
    }
    component One one;
    connect one.out1 -> yOut1;
  }
  <<Type="SubSystem">> component Umgebung {
    ports
      in Boolean blinkerLinksIn2,
      in Boolean blinkerRechtsIn3,
      in Boolean warnblinkerIn4,
      in Double schluesselIn5,
      in Double lichtdrehschalterIn6,
      in Double aussenhelligkeitIn7,
      in Boolean entriegeltIn8,
      in Boolean fahrzeugtuerHLIn9,
      in Boolean fahrzeugtuerHRIn10,
      in Boolean fahrzeugtuerVLIn11,
      in Boolean fahrzeugtuerVRIn12,
      in Double fahrzeuggeschwindigkeitIn13,
      in Boolean ent_FahrzeugIn14,
      in Boolean fernlichtIn15,
      in Double bordnetzspannungIn16,
      in Double status_FernlichtLinksIn17,
      in Double status_AbblendlichtLinksIn18,
      in Double status_AbbiegelichtLinksIn19,
      in Double status_FahrrichtungsanzeigerVorneLinksIn20,
      in Double status_FahrrichtungsanzeigerAussenspiegelLinksIn21,
      in Double status_FahrrichtungsanzeigerHintenLinksIn22,
      in Double status_FernlichtRechtsIn23,
      in Double status_AbblendlichtRechtsIn24,
      in Double status_AbbiegelichtRechtsIn25,
      in Double status_FahrrichtungsanzeigerVorneRechtsIn26,
      in Double status_FahrrichtungsanzeigerAussenspiegelRechtsIn27,
      in Double status_FahrrichtungsanzeigerHintenRechtsIn28,
      in Double dunkeltasterIn29,
      out Double fAVLOut2,
      out Double fAHLOut3,
      out Double fAALOut4,
      out Double fAVROut5,
      out Double fAHROut6,
      out Double fAAROut7,
      out Double abblendlichtLOut8,
      out Double abblendlichtROut9,
      out Double aBROut10,
      out Double aBLOut11,
      out Double ausleuchtungLinksOut12,
      out Double ausleuchtungRechtsOut13,
      out Boolean fLLDefektOut14,
      out Boolean aLLDefektOut15,
      out Boolean aBLLDefektOut16,
      out Boolean fAVLDefektOut17,
      out Boolean fAALDefektOut18,
      out Boolean fAHLDefektOut19,
      out Boolean fLRDefektOut20,
      out Boolean aLRDefektOut21,
      out Boolean aBLRDefektOut22,
      out Boolean fAVRDefektOut23,
      out Boolean fAARDefektOut24,
      out Boolean fAHRDefektOut25,
      in Double fRAVL_b,
      in Double fRAVL_b1,
      in Double fRAHL_b,
      in Double fRAHL_b1,
      in Double fRAAL_b,
      in Double fRAAL_b1,
      in Double fRAVR_b,
      in Double fRAVR_b1,
      in Double fRAHR_b,
      in Double fRAHR_b1,
      in Double fRAAR_b,
      in Double fRAAR_b1,
      in Double abblendlichtLinks_pc,
      in Double abblendlichtLinks_pc1,
      in Double abblendlichtRechts_pc,
      in Double abblendlichtRechts_pc1,
      in Double abbiegelichtRechts_pc,
      in Double abbiegelichtRechts_pc1,
      in Double abbiegelichtLinks_pc,
      in Double abbiegelichtLinks_pc1,
      in Double ausleuchtungLinks_m,
      in Double ausleuchtungLinks_m1,
      in Double ausleuchtungRechts_m,
      in Double ausleuchtungRechts_m1,
      in Boolean fLLinksDefekt_b,
      in Boolean fLLinksDefekt_b1,
      in Boolean abblLLinksDefekt_b,
      in Boolean abblLLinksDefekt_b1,
      in Boolean abbiLLinksDefekt_b,
      in Boolean abbiLLinksDefekt_b1,
      in Boolean fAVLinksDefekt_b,
      in Boolean fAVLinksDefekt_b1,
      in Boolean fAALinksDefekt_b,
      in Boolean fAALinksDefekt_b1,
      in Boolean fAHLinksDefekt_b,
      in Boolean fAHLinksDefekt_b1,
      in Boolean fLRechtsDefekt_b,
      in Boolean fLRechtsDefekt_b1,
      in Boolean abblLRechtsDefekt_b,
      in Boolean abblLRechtsDefekt_b1,
      in Boolean abbiLRechtsDefekt_b,
      in Boolean abbiLRechtsDefekt_b1,
      in Boolean fAVRechtsDefekt_b,
      in Boolean fAVRechtsDefekt_b1,
      in Boolean fAARechtsDefekt_b,
      in Boolean fAARechtsDefekt_b1,
      in Boolean fAHRechtsDefekt_b,
      in Boolean fAHRechtsDefekt_b1,
      out Boolean rBLinks_b,
      out Boolean rBRechts_b,
      out Boolean warnblinken_b,
      out Double schluessel_st,
      out Double lichtdrehschalter_st,
      out Double aussenhelligkeit_lx,
      out Boolean entriegelt_b,
      out Boolean tuer_b,
      out Double fzggeschw_kmh,
      out Boolean entFahrzeug_b,
      out Boolean fernlicht_b,
      out Double spannung_V,
      out Double fLLinks_st,
      out Double abbleLLinks_st,
      out Double abbiLLinks_st,
      out Double fAVLinks_st,
      out Double fAALinks_st,
      out Double fAHLinks_st,
      out Double fLRechts_st,
      out Double abbleLRechts_st,
      out Double abbiLRechts_st,
      out Double fAVRechts_st,
      out Double fAARechts_st,
      out Double fAHRechts_st,
      out Double dunkeltaster_st;
    <<Operator="OR",Type="Logic">> component LogicalOperator {
      ports
        in Boolean in1,
        in Boolean in2,
        in Boolean in3,
        in Boolean in4,
        out Boolean tuer_bOut1;
      effect in1 -> tuer_bOut1;
      effect in2 -> tuer_bOut1;
      effect in3 -> tuer_bOut1;
      effect in4 -> tuer_bOut1;
    }
    <<Operator="AND",Type="Logic">> component LogicalOperator2 {
      ports
        in Boolean in1,
        in Boolean in2,
        out Boolean rBLinks_bOut1;
      effect in1 -> rBLinks_bOut1;
      effect in2 -> rBLinks_bOut1;
    }
    <<Operator="AND",Type="Logic">> component LogicalOperator3 {
      ports
        in Boolean in1,
        in Boolean in2,
        out Boolean rBRechts_bOut1;
      effect in1 -> rBRechts_bOut1;
      effect in2 -> rBRechts_bOut1;
    }
    <<Operator="NOT",Type="Logic">> component LogicalOperator4 {
      ports
        in Boolean in1,
        out Boolean out1;
      effect in1 -> out1;
    }
    <<Operator="NOT",Type="Logic">> component LogicalOperator5 {
      ports
        in Boolean in1,
        out Boolean out1;
      effect in1 -> out1;
    }
    <<Type="Scope">> component Scope {
      ports
        in Double fRAVL_bIn1;
    }
    <<Type="Scope">> component Scope1 {
      ports
        in Double fRAHL_bIn1;
    }
    <<Type="Scope">> component Scope10 {
      ports
        in Double abblendlichtRechts_pcIn1;
    }
    <<Type="Scope">> component Scope11 {
      ports
        in Boolean fLLinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope12 {
      ports
        in Boolean abblLLinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope13 {
      ports
        in Boolean abbiLLinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope14 {
      ports
        in Boolean fAVLinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope15 {
      ports
        in Boolean fAALinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope16 {
      ports
        in Boolean fAHLinksDefekt_bIn1;
    }
    <<Type="Scope">> component Scope17 {
      ports
        in Boolean fLRechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope18 {
      ports
        in Boolean abblLRechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope19 {
      ports
        in Boolean abbiLRechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope2 {
      ports
        in Double fRAAL_bIn1;
    }
    <<Type="Scope">> component Scope20 {
      ports
        in Boolean fAVRechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope21 {
      ports
        in Boolean fAARechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope22 {
      ports
        in Boolean fAHRechtsDefekt_bIn1;
    }
    <<Type="Scope">> component Scope23 {
      ports
        in Double ausleuchtungRechts_mIn1;
    }
    <<Type="Scope">> component Scope3 {
      ports
        in Double fRAVR_bIn1;
    }
    <<Type="Scope">> component Scope4 {
      ports
        in Double fRAHR_bIn1;
    }
    <<Type="Scope">> component Scope5 {
      ports
        in Double fRAAR_bIn1;
    }
    <<Type="Scope">> component Scope6 {
      ports
        in Double abblendlichtLinks_pcIn1;
    }
    <<Type="Scope">> component Scope7 {
      ports
        in Double abbiegelichtRechts_pcIn1;
    }
    <<Type="Scope">> component Scope8 {
      ports
        in Double abbiegelichtLinks_pcIn1;
    }
    <<Type="Scope">> component Scope9 {
      ports
        in Double ausleuchtungLinks_mIn1;
    }
    component LogicalOperator logicalOperator;
    component LogicalOperator2 logicalOperator2;
    component LogicalOperator3 logicalOperator3;
    component LogicalOperator4 logicalOperator4;
    component LogicalOperator5 logicalOperator5;
    component Scope scope;
    component Scope1 scope1;
    component Scope10 scope10;
    component Scope11 scope11;
    component Scope12 scope12;
    component Scope13 scope13;
    component Scope14 scope14;
    component Scope15 scope15;
    component Scope16 scope16;
    component Scope17 scope17;
    component Scope18 scope18;
    component Scope19 scope19;
    component Scope2 scope2;
    component Scope20 scope20;
    component Scope21 scope21;
    component Scope22 scope22;
    component Scope23 scope23;
    component Scope3 scope3;
    component Scope4 scope4;
    component Scope5 scope5;
    component Scope6 scope6;
    component Scope7 scope7;
    component Scope8 scope8;
    component Scope9 scope9;
    connect fRAVL_b1 -> fAVLOut2;
    connect fRAHL_b1 -> fAHLOut3;
    connect fRAAL_b1 -> fAALOut4;
    connect fRAVR_b1 -> fAVROut5;
    connect fRAHR_b -> fAHROut6;
    connect fRAAR_b1 -> fAAROut7;
    connect abblendlichtLinks_pc -> abblendlichtLOut8;
    connect abblendlichtRechts_pc1 -> abblendlichtROut9;
    connect abbiegelichtRechts_pc -> aBROut10;
    connect abbiegelichtLinks_pc1 -> aBLOut11;
    connect ausleuchtungLinks_m -> ausleuchtungLinksOut12;
    connect ausleuchtungRechts_m1 -> ausleuchtungRechtsOut13;
    connect fLLinksDefekt_b -> fLLDefektOut14;
    connect abblLLinksDefekt_b -> aLLDefektOut15;
    connect abbiLLinksDefekt_b -> aBLLDefektOut16;
    connect fAVLinksDefekt_b1 -> fAVLDefektOut17;
    connect fAALinksDefekt_b -> fAALDefektOut18;
    connect fAHLinksDefekt_b1 -> fAHLDefektOut19;
    connect fLRechtsDefekt_b1 -> fLRDefektOut20;
    connect abblLRechtsDefekt_b -> aLRDefektOut21;
    connect abbiLRechtsDefekt_b -> aBLRDefektOut22;
    connect fAVRechtsDefekt_b1 -> fAVRDefektOut23;
    connect fAARechtsDefekt_b -> fAARDefektOut24;
    connect fAHRechtsDefekt_b1 -> fAHRDefektOut25;
    connect ausleuchtungRechts_m -> scope23.ausleuchtungRechts_mIn1;
    connect fAHRechtsDefekt_b -> scope22.fAHRechtsDefekt_bIn1;
    connect fAARechtsDefekt_b1 -> scope21.fAARechtsDefekt_bIn1;
    connect fAVRechtsDefekt_b -> scope20.fAVRechtsDefekt_bIn1;
    connect abbiLRechtsDefekt_b1 -> scope19.abbiLRechtsDefekt_bIn1;
    connect abblLRechtsDefekt_b1 -> scope18.abblLRechtsDefekt_bIn1;
    connect fLRechtsDefekt_b -> scope17.fLRechtsDefekt_bIn1;
    connect fAHLinksDefekt_b -> scope16.fAHLinksDefekt_bIn1;
    connect fAALinksDefekt_b1 -> scope15.fAALinksDefekt_bIn1;
    connect fAVLinksDefekt_b -> scope14.fAVLinksDefekt_bIn1;
    connect abbiLLinksDefekt_b1 -> scope13.abbiLLinksDefekt_bIn1;
    connect abblLLinksDefekt_b1 -> scope12.abblLLinksDefekt_bIn1;
    connect fLLinksDefekt_b1 -> scope11.fLLinksDefekt_bIn1;
    connect abblendlichtRechts_pc -> scope10.abblendlichtRechts_pcIn1;
    connect ausleuchtungLinks_m1 -> scope9.ausleuchtungLinks_mIn1;
    connect abbiegelichtLinks_pc -> scope8.abbiegelichtLinks_pcIn1;
    connect abbiegelichtRechts_pc1 -> scope7.abbiegelichtRechts_pcIn1;
    connect abblendlichtLinks_pc1 -> scope6.abblendlichtLinks_pcIn1;
    connect fRAAR_b -> scope5.fRAAR_bIn1;
    connect fRAHR_b1 -> scope4.fRAHR_bIn1;
    connect fRAVR_b -> scope3.fRAVR_bIn1;
    connect fRAAL_b -> scope2.fRAAL_bIn1;
    connect fRAHL_b -> scope1.fRAHL_bIn1;
    connect fRAVL_b -> scope.fRAVL_bIn1;
    connect logicalOperator2.rBLinks_bOut1 -> rBLinks_b;
    connect logicalOperator3.rBRechts_bOut1 -> rBRechts_b;
    connect warnblinkerIn4 -> warnblinken_b;
    connect schluesselIn5 -> schluessel_st;
    connect lichtdrehschalterIn6 -> lichtdrehschalter_st;
    connect aussenhelligkeitIn7 -> aussenhelligkeit_lx;
    connect entriegeltIn8 -> entriegelt_b;
    connect logicalOperator.tuer_bOut1 -> tuer_b;
    connect fahrzeuggeschwindigkeitIn13 -> fzggeschw_kmh;
    connect ent_FahrzeugIn14 -> entFahrzeug_b;
    connect fernlichtIn15 -> fernlicht_b;
    connect bordnetzspannungIn16 -> spannung_V;
    connect status_FernlichtLinksIn17 -> fLLinks_st;
    connect status_AbblendlichtLinksIn18 -> abbleLLinks_st;
    connect status_AbbiegelichtLinksIn19 -> abbiLLinks_st;
    connect status_FahrrichtungsanzeigerVorneLinksIn20 -> fAVLinks_st;
    connect status_FahrrichtungsanzeigerAussenspiegelLinksIn21 -> fAALinks_st;
    connect status_FahrrichtungsanzeigerHintenLinksIn22 -> fAHLinks_st;
    connect status_FernlichtRechtsIn23 -> fLRechts_st;
    connect status_AbblendlichtRechtsIn24 -> abbleLRechts_st;
    connect status_AbbiegelichtRechtsIn25 -> abbiLRechts_st;
    connect status_FahrrichtungsanzeigerVorneRechtsIn26 -> fAVRechts_st;
    connect status_FahrrichtungsanzeigerAussenspiegelRechtsIn27 -> fAARechts_st;
    connect status_FahrrichtungsanzeigerHintenRechtsIn28 -> fAHRechts_st;
    connect dunkeltasterIn29 -> dunkeltaster_st;
    connect logicalOperator5.out1 -> logicalOperator3.in1;
    connect logicalOperator4.out1 -> logicalOperator2.in2;
    connect blinkerRechtsIn3 -> logicalOperator3.in2;
    connect blinkerRechtsIn3 -> logicalOperator4.in1;
    connect blinkerLinksIn2 -> logicalOperator2.in1;
    connect blinkerLinksIn2 -> logicalOperator5.in1;
    connect fahrzeugtuerVRIn12 -> logicalOperator.in4;
    connect fahrzeugtuerVLIn11 -> logicalOperator.in3;
    connect fahrzeugtuerHRIn10 -> logicalOperator.in2;
    connect fahrzeugtuerHLIn9 -> logicalOperator.in1;
  }
  <<Type="SubSystem">> component V {
    ports
      in Double uIn1,
      out Double yOut1;
    <<Type="Gain",Gain="gain">> component SliderGain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    component SliderGain sliderGain;
    connect sliderGain.out1 -> yOut1;
    connect uIn1 -> sliderGain.in1;
  }
  <<Type="SubSystem">> component VERSION_INFO2 {
    <<Type="SubSystem">> component Copyright {
    }
    component Copyright copyright;
  }
  <<Type="SubSystem">> component H {
    ports
      in Double uIn1,
      out Double yOut1;
    <<Type="Gain",Gain="gain">> component SliderGain {
      ports
        in Double in1,
        out Double out1;
      effect in1 -> out1;
    }
    component SliderGain sliderGain;
    connect sliderGain.out1 -> yOut1;
    connect uIn1 -> sliderGain.in1;
  }
  <<Type="Memory">> component Memory1 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory2 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory3 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory4 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory5 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory6 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory7 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory8 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory9 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory10 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory11 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory12 {
    ports
      in Double in1,
      out Double out1;
  }
  <<Type="Memory">> component Memory13 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory14 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory15 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory16 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory17 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory18 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory19 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory20 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory21 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory22 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory23 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  <<Type="Memory">> component Memory24 {
    ports
      in Boolean in1,
      out Boolean out1;
  }
  component AbbiegelichtLinks abbiegelichtLinks;
  component AbbiegelichtRechts abbiegelichtRechts;
  component AbblendlichtLinks abblendlichtLinks;
  component AbblendlichtRechts abblendlichtRechts;
  component Aussenhelligkeitlx aussenhelligkeitlx;
  component Constant1 constant1;
  component Constant18 constant18;
  component Constant19 constant19;
  component Constant2 constant2;
  component Constant20 constant20;
  component Constant21 constant21;
  component Constant22 constant22;
  component Constant23 constant23;
  component Constant24 constant24;
  component Constant25 constant25;
  component Constant26 constant26;
  component Constant27 constant27;
  component Constant28 constant28;
  component Constant29 constant29;
  component Constant3 constant3;
  component Constant4 constant4;
  component Constant5 constant5;
  component DEMO_Aussenlicht dEMO_Aussenlicht;
  component Defekt_AbbiegelichtLinks defekt_AbbiegelichtLinks;
  component Defekt_AbbiegelichtRechts defekt_AbbiegelichtRechts;
  component Defekt_AbblendlichtLinks defekt_AbblendlichtLinks;
  component Defekt_AbblendlichtRechts defekt_AbblendlichtRechts;
  component Defekt_FahrreichtungsanzeigerAussenspiegelLinks defekt_FahrreichtungsanzeigerAussenspiegelLinks;
  component Defekt_FahrreichtungsanzeigerAussenspiegelRechts defekt_FahrreichtungsanzeigerAussenspiegelRechts;
  component Defekt_FahrreichtungsanzeigerHintenLinks defekt_FahrreichtungsanzeigerHintenLinks;
  component Defekt_FahrreichtungsanzeigerHintenRechts defekt_FahrreichtungsanzeigerHintenRechts;
  component Defekt_FahrreichtungsanzeigerVorneLinks defekt_FahrreichtungsanzeigerVorneLinks;
  component Defekt_FahrreichtungsanzeigerVorneRechts defekt_FahrreichtungsanzeigerVorneRechts;
  component Defekt_FernlichRechts defekt_FernlichRechts;
  component Defekt_FernlichtLinks defekt_FernlichtLinks;
  component FahrrichtungsanzeigerAussenspiegelLinks fahrrichtungsanzeigerAussenspiegelLinks;
  component FahrrichtungsanzeigerAussenspiegelRechts fahrrichtungsanzeigerAussenspiegelRechts;
  component FahrrichtungsanzeigerHintenLinks fahrrichtungsanzeigerHintenLinks;
  component FahrrichtungsanzeigerHintenRechts fahrrichtungsanzeigerHintenRechts;
  component FahrrichtungsanzeigerVorneLinks fahrrichtungsanzeigerVorneLinks;
  component FahrrichtungsanzeigerVorneRechts fahrrichtungsanzeigerVorneRechts;
  component FalseBlock falseBlock;
  component FalseBlock1 falseBlock1;
  component FalseBlock10 falseBlock10;
  component FalseBlock2 falseBlock2;
  component FalseBlock3 falseBlock3;
  component FalseBlock4 falseBlock4;
  component FalseBlock5 falseBlock5;
  component FalseBlock6 falseBlock6;
  component FalseBlock7 falseBlock7;
  component FalseBlock8 falseBlock8;
  component FalseBlock9 falseBlock9;
  component FernlichtLinks fernlichtLinks;
  component FernlichtRechts fernlichtRechts;
  component ManualSwitch manualSwitch;
  component ManualSwitch1 manualSwitch1;
  component ManualSwitch10 manualSwitch10;
  component ManualSwitch2 manualSwitch2;
  component ManualSwitch3 manualSwitch3;
  component ManualSwitch4 manualSwitch4;
  component ManualSwitch5 manualSwitch5;
  component ManualSwitch6 manualSwitch6;
  component ManualSwitch7 manualSwitch7;
  component ManualSwitch8 manualSwitch8;
  component SW_Entriegelt sW_Entriegelt;
  component SimToRealTime simToRealTime;
  component SliderGain sliderGain;
  component SliderGain2 sliderGain2;
  component Subsystem subsystem;
  component TrueBlock trueBlock;
  component TrueBlock1 trueBlock1;
  component TrueBlock10 trueBlock10;
  component TrueBlock2 trueBlock2;
  component TrueBlock3 trueBlock3;
  component TrueBlock4 trueBlock4;
  component TrueBlock5 trueBlock5;
  component TrueBlock6 trueBlock6;
  component TrueBlock7 trueBlock7;
  component TrueBlock8 trueBlock8;
  component TrueBlock9 trueBlock9;
  component Umgebung umgebung;
  component V v;
  component VERSION_INFO2 vERSION_INFO2;
  component H h;
  component Memory1 memory1;
  component Memory2 memory2;
  component Memory3 memory3;
  component Memory4 memory4;
  component Memory5 memory5;
  component Memory6 memory6;
  component Memory7 memory7;
  component Memory8 memory8;
  component Memory9 memory9;
  component Memory10 memory10;
  component Memory11 memory11;
  component Memory12 memory12;
  component Memory13 memory13;
  component Memory14 memory14;
  component Memory15 memory15;
  component Memory16 memory16;
  component Memory17 memory17;
  component Memory18 memory18;
  component Memory19 memory19;
  component Memory20 memory20;
  component Memory21 memory21;
  component Memory22 memory22;
  component Memory23 memory23;
  component Memory24 memory24;
  connect memory1.out1 -> umgebung.fRAVL_b;
  connect memory1.out1 -> umgebung.fRAVL_b1;
  connect memory2.out1 -> umgebung.fRAHL_b;
  connect memory2.out1 -> umgebung.fRAHL_b1;
  connect memory3.out1 -> umgebung.fRAAL_b;
  connect memory3.out1 -> umgebung.fRAAL_b1;
  connect memory4.out1 -> umgebung.fRAVR_b;
  connect memory4.out1 -> umgebung.fRAVR_b1;
  connect memory5.out1 -> umgebung.fRAHR_b;
  connect memory5.out1 -> umgebung.fRAHR_b1;
  connect memory6.out1 -> umgebung.fRAAR_b;
  connect memory6.out1 -> umgebung.fRAAR_b1;
  connect memory7.out1 -> umgebung.abblendlichtLinks_pc;
  connect memory7.out1 -> umgebung.abblendlichtLinks_pc1;
  connect memory8.out1 -> umgebung.abblendlichtRechts_pc;
  connect memory8.out1 -> umgebung.abblendlichtRechts_pc1;
  connect memory9.out1 -> umgebung.abbiegelichtRechts_pc;
  connect memory9.out1 -> umgebung.abbiegelichtRechts_pc1;
  connect memory10.out1 -> umgebung.abbiegelichtLinks_pc;
  connect memory10.out1 -> umgebung.abbiegelichtLinks_pc1;
  connect memory11.out1 -> umgebung.ausleuchtungLinks_m;
  connect memory11.out1 -> umgebung.ausleuchtungLinks_m1;
  connect memory12.out1 -> umgebung.ausleuchtungRechts_m;
  connect memory12.out1 -> umgebung.ausleuchtungRechts_m1;
  connect memory13.out1 -> umgebung.fLLinksDefekt_b;
  connect memory13.out1 -> umgebung.fLLinksDefekt_b1;
  connect memory14.out1 -> umgebung.abblLLinksDefekt_b;
  connect memory14.out1 -> umgebung.abblLLinksDefekt_b1;
  connect memory15.out1 -> umgebung.abbiLLinksDefekt_b;
  connect memory15.out1 -> umgebung.abbiLLinksDefekt_b1;
  connect memory16.out1 -> umgebung.fAVLinksDefekt_b;
  connect memory16.out1 -> umgebung.fAVLinksDefekt_b1;
  connect memory17.out1 -> umgebung.fAALinksDefekt_b;
  connect memory17.out1 -> umgebung.fAALinksDefekt_b1;
  connect memory18.out1 -> umgebung.fAHLinksDefekt_b;
  connect memory18.out1 -> umgebung.fAHLinksDefekt_b1;
  connect memory19.out1 -> umgebung.fLRechtsDefekt_b;
  connect memory19.out1 -> umgebung.fLRechtsDefekt_b1;
  connect memory20.out1 -> umgebung.abblLRechtsDefekt_b;
  connect memory20.out1 -> umgebung.abblLRechtsDefekt_b1;
  connect memory21.out1 -> umgebung.abbiLRechtsDefekt_b;
  connect memory21.out1 -> umgebung.abbiLRechtsDefekt_b1;
  connect memory22.out1 -> umgebung.fAVRechtsDefekt_b;
  connect memory22.out1 -> umgebung.fAVRechtsDefekt_b1;
  connect memory23.out1 -> umgebung.fAARechtsDefekt_b;
  connect memory23.out1 -> umgebung.fAARechtsDefekt_b1;
  connect memory24.out1 -> umgebung.fAHRechtsDefekt_b;
  connect memory24.out1 -> umgebung.fAHRechtsDefekt_b1;
  connect dEMO_Aussenlicht.fRAVL_b -> memory1.in1;
  connect dEMO_Aussenlicht.fRAHL_b -> memory2.in1;
  connect dEMO_Aussenlicht.fRAAL_b -> memory3.in1;
  connect dEMO_Aussenlicht.fRAVR_b -> memory4.in1;
  connect dEMO_Aussenlicht.fRAHR_b -> memory5.in1;
  connect dEMO_Aussenlicht.fRAAR_b -> memory6.in1;
  connect dEMO_Aussenlicht.abblendlichtLinks_pc -> memory7.in1;
  connect dEMO_Aussenlicht.abblendlichtRechts_pc -> memory8.in1;
  connect dEMO_Aussenlicht.abbiegelichtRechts_pc -> memory9.in1;
  connect dEMO_Aussenlicht.abbiegelichtLinks_pc -> memory10.in1;
  connect dEMO_Aussenlicht.ausleuchtungLinks_m -> memory11.in1;
  connect dEMO_Aussenlicht.ausleuchtungRechts_m -> memory12.in1;
  connect dEMO_Aussenlicht.fLLinksDefekt_b -> memory13.in1;
  connect dEMO_Aussenlicht.abblLLinksDefekt_b -> memory14.in1;
  connect dEMO_Aussenlicht.abbiLLinksDefekt_b -> memory15.in1;
  connect dEMO_Aussenlicht.fAVLinksDefekt_b -> memory16.in1;
  connect dEMO_Aussenlicht.fAALinksDefekt_b -> memory17.in1;
  connect dEMO_Aussenlicht.fAHLinksDefekt_b -> memory18.in1;
  connect dEMO_Aussenlicht.fLRechtsDefekt_b -> memory19.in1;
  connect dEMO_Aussenlicht.abblLRechtsDefekt_b -> memory20.in1;
  connect dEMO_Aussenlicht.abbiLRechtsDefekt_b -> memory21.in1;
  connect dEMO_Aussenlicht.fAVRechtsDefekt_b -> memory22.in1;
  connect dEMO_Aussenlicht.fAARechtsDefekt_b -> memory23.in1;
  connect dEMO_Aussenlicht.fAHRechtsDefekt_b -> memory24.in1;
  connect umgebung.rBLinks_b -> dEMO_Aussenlicht.rBLinks_b;
  connect umgebung.rBRechts_b -> dEMO_Aussenlicht.rBRechts_b;
  connect umgebung.warnblinken_b -> dEMO_Aussenlicht.warnblinken_b;
  connect umgebung.schluessel_st -> dEMO_Aussenlicht.schluessel_st;
  connect umgebung.lichtdrehschalter_st -> dEMO_Aussenlicht.lichtdrehschalter_st;
  connect umgebung.aussenhelligkeit_lx -> dEMO_Aussenlicht.aussenhelligkeit_lx;
  connect umgebung.entriegelt_b -> dEMO_Aussenlicht.entriegelt_b;
  connect umgebung.tuer_b -> dEMO_Aussenlicht.tuer_b;
  connect umgebung.fzggeschw_kmh -> dEMO_Aussenlicht.fzggeschw_kmh;
  connect umgebung.entFahrzeug_b -> dEMO_Aussenlicht.entFahrzeug_b;
  connect umgebung.fernlicht_b -> dEMO_Aussenlicht.fernlicht_b;
  connect umgebung.spannung_V -> dEMO_Aussenlicht.spannung_V;
  connect umgebung.fLLinks_st -> dEMO_Aussenlicht.fLLinks_st;
  connect umgebung.abbleLLinks_st -> dEMO_Aussenlicht.abbleLLinks_st;
  connect umgebung.abbiLLinks_st -> dEMO_Aussenlicht.abbiLLinks_st;
  connect umgebung.fAVLinks_st -> dEMO_Aussenlicht.fAVLinks_st;
  connect umgebung.fAALinks_st -> dEMO_Aussenlicht.fAALinks_st;
  connect umgebung.fAHLinks_st -> dEMO_Aussenlicht.fAHLinks_st;
  connect umgebung.fLRechts_st -> dEMO_Aussenlicht.fLRechts_st;
  connect umgebung.abbleLRechts_st -> dEMO_Aussenlicht.abbleLRechts_st;
  connect umgebung.abbiLRechts_st -> dEMO_Aussenlicht.abbiLRechts_st;
  connect umgebung.fAVRechts_st -> dEMO_Aussenlicht.fAVRechts_st;
  connect umgebung.fAARechts_st -> dEMO_Aussenlicht.fAARechts_st;
  connect umgebung.fAHRechts_st -> dEMO_Aussenlicht.fAHRechts_st;
  connect umgebung.dunkeltaster_st -> dEMO_Aussenlicht.dunkeltaster_st;
  connect umgebung.fAHROut6 -> fahrrichtungsanzeigerHintenRechts.aLLDefektIn1;
  connect umgebung.fAHLOut3 -> fahrrichtungsanzeigerHintenLinks.fAHLIn1;
  connect umgebung.fAVROut5 -> fahrrichtungsanzeigerVorneRechts.fAVRIn1;
  connect umgebung.fAVLOut2 -> fahrrichtungsanzeigerVorneLinks.fAVLIn1;
  connect umgebung.fAAROut7 -> fahrrichtungsanzeigerAussenspiegelRechts.fAARIn1;
  connect umgebung.fAALOut4 -> fahrrichtungsanzeigerAussenspiegelLinks.fAALIn1;
  connect umgebung.abblendlichtLOut8 -> abblendlichtLinks.abblendlichtLIn1;
  connect umgebung.abblendlichtROut9 -> abblendlichtRechts.abblendlichtRIn1;
  connect umgebung.aBROut10 -> abbiegelichtRechts.aBRIn1;
  connect umgebung.aBLOut11 -> abbiegelichtLinks.aBLIn1;
  connect umgebung.ausleuchtungLinksOut12 -> fernlichtLinks.ausleuchtungLinksIn1;
  connect umgebung.ausleuchtungRechtsOut13 -> fernlichtRechts.ausleuchtungRechtsIn1;
  connect umgebung.fAHRDefektOut25 -> defekt_FahrreichtungsanzeigerHintenRechts.fAHRDefektIn1;
  connect umgebung.fAARDefektOut24 -> defekt_FahrreichtungsanzeigerAussenspiegelRechts.fAARDefektIn1;
  connect umgebung.fAVRDefektOut23 -> defekt_FahrreichtungsanzeigerVorneRechts.fAVRDefektIn1;
  connect umgebung.aBLRDefektOut22 -> defekt_AbbiegelichtRechts.aBLRDefektIn1;
  connect umgebung.aLRDefektOut21 -> defekt_AbblendlichtRechts.aLRDefektIn1;
  connect umgebung.fLRDefektOut20 -> defekt_FernlichRechts.fLRDefektIn1;
  connect umgebung.fAHLDefektOut19 -> defekt_FahrreichtungsanzeigerHintenLinks.fAHLDefektIn1;
  connect umgebung.fAALDefektOut18 -> defekt_FahrreichtungsanzeigerAussenspiegelLinks.fAALDefektIn1;
  connect umgebung.fAVLDefektOut17 -> defekt_FahrreichtungsanzeigerVorneLinks.fAVLDefektIn1;
  connect umgebung.aBLLDefektOut16 -> defekt_AbbiegelichtLinks.aBLLDefektIn1;
  connect umgebung.aLLDefektOut15 -> defekt_AbblendlichtLinks.aLLDefektIn1;
  connect umgebung.fLLDefektOut14 -> defekt_FernlichtLinks.fLLDefektIn1;
  connect constant21.out1 -> umgebung.status_FahrrichtungsanzeigerHintenRechtsIn28;
  connect constant20.out1 -> umgebung.status_FahrrichtungsanzeigerAussenspiegelRechtsIn27;
  connect constant25.out1 -> umgebung.status_FahrrichtungsanzeigerVorneRechtsIn26;
  connect constant24.out1 -> umgebung.status_AbbiegelichtRechtsIn25;
  connect constant23.out1 -> umgebung.status_AbblendlichtRechtsIn24;
  connect constant22.out1 -> umgebung.status_FernlichtRechtsIn23;
  connect constant19.out1 -> umgebung.status_FahrrichtungsanzeigerHintenLinksIn22;
  connect constant18.out1 -> umgebung.status_FahrrichtungsanzeigerAussenspiegelLinksIn21;
  connect constant29.out1 -> umgebung.status_FahrrichtungsanzeigerVorneLinksIn20;
  connect constant28.out1 -> umgebung.status_AbbiegelichtLinksIn19;
  connect constant27.out1 -> umgebung.status_AbblendlichtLinksIn18;
  connect constant26.out1 -> umgebung.status_FernlichtLinksIn17;
  connect manualSwitch6.out1 -> umgebung.fahrzeugtuerVRIn12;
  connect manualSwitch1.out1 -> umgebung.blinkerRechtsIn3;
  connect manualSwitch8.out1 -> umgebung.dunkeltasterIn29;
  connect falseBlock10.yOut1 -> manualSwitch8.in2;
  connect trueBlock10.yOut1 -> manualSwitch8.in1;
  connect manualSwitch7.out1 -> umgebung.fahrzeugtuerHRIn10;
  connect falseBlock8.yOut1 -> manualSwitch7.in2;
  connect trueBlock8.yOut1 -> manualSwitch7.in1;
  connect falseBlock7.yOut1 -> manualSwitch6.in2;
  connect trueBlock7.yOut1 -> manualSwitch6.in1;
  connect manualSwitch5.out1 -> umgebung.ent_FahrzeugIn14;
  connect falseBlock9.yOut1 -> manualSwitch5.in2;
  connect trueBlock9.yOut1 -> manualSwitch5.in1;
  connect manualSwitch3.out1 -> umgebung.warnblinkerIn4;
  connect falseBlock1.yOut1 -> manualSwitch3.in2;
  connect trueBlock1.yOut1 -> manualSwitch3.in1;
  connect falseBlock3.yOut1 -> manualSwitch1.in2;
  connect trueBlock3.yOut1 -> manualSwitch1.in1;
  connect manualSwitch.out1 -> umgebung.fernlichtIn15;
  connect manualSwitch2.out1 -> umgebung.blinkerLinksIn2;
  connect falseBlock2.yOut1 -> manualSwitch2.in2;
  connect trueBlock2.yOut1 -> manualSwitch2.in1;
  connect falseBlock.yOut1 -> manualSwitch.in2;
  connect trueBlock.yOut1 -> manualSwitch.in1;
  connect manualSwitch4.out1 -> umgebung.fahrzeugtuerHLIn9;
  connect falseBlock6.yOut1 -> manualSwitch4.in2;
  connect trueBlock6.yOut1 -> manualSwitch4.in1;
  connect manualSwitch10.out1 -> umgebung.fahrzeugtuerVLIn11;
  connect falseBlock5.yOut1 -> manualSwitch10.in2;
  connect trueBlock5.yOut1 -> manualSwitch10.in1;
  connect sW_Entriegelt.out1 -> umgebung.entriegeltIn8;
  connect falseBlock4.yOut1 -> sW_Entriegelt.in2;
  connect trueBlock4.yOut1 -> sW_Entriegelt.in1;
  connect v.yOut1 -> umgebung.bordnetzspannungIn16;
  connect constant5.out1 -> v.uIn1;
  connect aussenhelligkeitlx.yOut1 -> umgebung.aussenhelligkeitIn7;
  connect constant4.out1 -> aussenhelligkeitlx.uIn1;
  connect sliderGain2.yOut1 -> umgebung.schluesselIn5;
  connect constant3.out1 -> sliderGain2.uIn1;
  connect h.yOut1 -> umgebung.fahrzeuggeschwindigkeitIn13;
  connect constant2.out1 -> h.uIn1;
  connect sliderGain.yOut1 -> umgebung.lichtdrehschalterIn6;
  connect constant1.out1 -> sliderGain.uIn1;
}
