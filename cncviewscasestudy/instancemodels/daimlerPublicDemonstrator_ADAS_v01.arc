/* (c) https://github.com/MontiCore/monticore */
package daimlerPublicDemonstrator_ADAS_v01;
import java.lang.*;
import java.util.*;
import java.lang.*;
import java.util.*;
component /*instance*/ daimlerPublicDemonstrator_ADAS_v01 {
  component /*instance*/ dEMO_FAS_1 {
    port
      out Double acceleration_pc,
      in Double acceleration_pedal_pc,
      in Double brakeForce_pedal_pc,
      out Boolean cC_active_b,
      out Double cCSetValue_kmh,
      in Boolean cruiseControl_b,
      in Boolean leverDown_b,
      in Boolean leverUp_b,
      out Boolean limiter_active_b,
      in Boolean limiter_b,
      out Double limiterSetValue_kmh,
      in Boolean parkingBrake_b,
      in Double v_Vehicle_kmh;
    component /*instance*/ dEMO_FAS_2 {
      port
        out Double _Acceleration_pc,
        in Double _Acceleration_pedal_pc,
        in Double _BrakeForce_pedal_pc,
        out Boolean _CC_active_b,
        out Double _CCSetValue_kmh,
        in Boolean _CruiseControl_b,
        in Boolean _LeverDown_b,
        in Boolean _LeverUp_b,
        out Boolean _Limiter_active_b,
        in Boolean _Limiter_b,
        out Double _LimiterSetValue_kmh,
        in Boolean _ParkingBrake_b,
        in Double _V_Vehicle_kmh;
      component /*instance*/ subsystem {
        port
          out Double _Acceleration_pc,
          in Double _Acceleration_pedal_pc,
          in Double _BrakeForce_pedal_pc,
          out Boolean _CC_active_b,
          out Double _CCSetValue_kmh,
          in Boolean _CruiseControl_b,
          in Boolean _LeverDown_b,
          in Boolean _LeverUp_b,
          out Boolean _Limiter_active_b,
          in Boolean _Limiter_b,
          out Double _LimiterSetValue_kmh,
          in Boolean _ParkingBrake_b,
          in Double _V_Vehicle_kmh;
        component /*instance*/ dEMO_FAS_3 {
          port
            out Double _Acceleration_pc,
            in Double _Acceleration_pedal_pc,
            in Double _BrakeForce_pedal_pc,
            out Boolean _CC_active_b,
            out Double _CCSetValue_kmh,
            in Boolean _CruiseControl_b,
            in Boolean _LeverDown_b,
            in Boolean _LeverUp_b,
            out Boolean _Limiter_active_b,
            in Boolean _Limiter_b,
            out Double _LimiterSetValue_kmh,
            in Boolean _ParkingBrake_b,
            in Double _V_Vehicle_kmh;
          component /*instance*/ dEMO_FAS_Funktion {
            port
              out Double acceleration_pc,
              in Double acceleration_pedal_pc,
              in Double acceleration_pedal_pc1,
              in Double brakeForce_pedal_pc,
              in Double brakeForce_pedal_pc1,
              out Boolean cC_active_b,
              out Double cCSetValue_kmh,
              in Boolean cruiseControl_b,
              in Boolean cruiseControl_b1,
              in Boolean leverDown_b,
              in Boolean leverDown_b1,
              in Boolean leverUp_b,
              in Boolean leverUp_b1,
              out Boolean limiter_active_b,
              in Boolean limiter_b,
              in Boolean limiter_b1,
              out Double limiterSetValue_kmh,
              in Boolean parkingBrake_b,
              in Boolean parkingBrake_b1,
              in Double v_Vehicle_kmh,
              in Double v_Vehicle_kmh1,
              in Double v_Vehicle_kmh2;
            component /*instance*/ fAS_Input {
              port
                out Double acceleration_pedal_pc,
                in Double acceleration_pedal_pc1,
                in Double acceleration_pedal_pc2,
                out Double acceleration_pedal_pc3,
                out Double brakeForce_pedal_pc,
                in Double brakeForce_pedal_pc1,
                in Double brakeForce_pedal_pc2,
                out Boolean cruiseControl_b,
                in Boolean cruiseControl_b1,
                in Boolean cruiseControl_b2,
                out Boolean leverDown_b,
                in Boolean leverDown_b1,
                in Boolean leverDown_b2,
                out Boolean leverDown_b3,
                out Boolean leverUp_b,
                in Boolean leverUp_b1,
                in Boolean leverUp_b2,
                out Boolean leverUp_b3,
                out Boolean limiter_b,
                in Boolean limiter_b1,
                in Boolean limiter_b2,
                out Boolean limiter_b3,
                out Boolean parkingBrake_b,
                in Boolean parkingBrake_b1,
                in Boolean parkingBrake_b2,
                out Double v_Vehicle_kmh,
                out Double v_Vehicle_kmh1,
                in Double v_Vehicle_kmh2,
                in Double v_Vehicle_kmh3,
                in Double v_Vehicle_kmh4,
                out Double v_Vehicle_kmh5;
              component /*instance*/ vERSION_INFO_1 {
                component /*instance*/ copyright_1 {
                }
              }
              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.parkingBrake_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.parkingBrake_b*/
              connect parkingBrake_b1 -> parkingBrake_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.brakeForce_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.brakeForce_pedal_pc*/
              connect brakeForce_pedal_pc1 -> brakeForce_pedal_pc;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.cruiseControl_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.cruiseControl_b*/
              connect cruiseControl_b1 -> cruiseControl_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh*/
              connect v_Vehicle_kmh4 -> v_Vehicle_kmh;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b*/
              connect limiter_b1 -> limiter_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b*/
              connect leverUp_b2 -> leverUp_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b*/
              connect leverDown_b2 -> leverDown_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc*/
              connect acceleration_pedal_pc1 -> acceleration_pedal_pc;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh1*/
              connect v_Vehicle_kmh2 -> v_Vehicle_kmh1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b3*/
              connect limiter_b2 -> limiter_b3;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc3*/
              connect acceleration_pedal_pc2 -> acceleration_pedal_pc3;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh5*/
              connect v_Vehicle_kmh3 -> v_Vehicle_kmh5;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b3*/
              connect leverUp_b1 -> leverUp_b3;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b3*/
              connect leverDown_b1 -> leverDown_b3;

            }
            component /*instance*/ limiter {
              port
                in Double acceleration_pedal_pc,
                in Double in1,
                in Double in2,
                in Double in3,
                in Double in4,
                in Double in5,
                in Double in6,
                in Double in7,
                in Boolean leverDown_b,
                in Boolean leverUp_b,
                out Boolean limiter_active_b,
                in Boolean limiter_b,
                out Double out1,
                in Double v_Vehicle_kmh,
                out Double vMax_kmh;
              component /*instance*/ constant_1 {
                port
                  out Double out1;
              }
              component /*instance*/ limiter_enabled {
                port
                  in Double acceleration_pedal_pc,
                  in Double in1,
                  in Double in2,
                  in Double in3,
                  in Double in4,
                  in Double in5,
                  in Double in6,
                  in Boolean leverDown_b,
                  in Boolean leverUp_b,
                  out Boolean limiter_active_b,
                  in Boolean limiter_b,
                  out Double out1,
                  in Double v_Vehicle_kmh,
                  out Double vMax_kmh;
                component /*instance*/ limiter_Function {
                  port
                    in Double acceleration_pedal_pc,
                    in Double in1,
                    out Boolean limiter_active_b,
                    in Boolean limiter_b,
                    out Double vMax_kmh;
                  component /*instance*/ limiter_Active {
                    port
                      in Double in1,
                      out Boolean limiter_active_b,
                      out Double vMax_kmh;
                    component /*instance*/ gain_1 {
                      port
                        in Double vMax_kmh,
                        out Double vMax_kmhOut1;
                      <<effector>> connect vMax_kmh -> vMax_kmhOut1;
                    }
                    component /*instance*/ logOp_1 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean limiter_active_b;
                      <<effector>> connect in1 -> limiter_active_b;
                      <<effector>> connect in2 -> limiter_active_b;
                    }
                    component /*instance*/ trueBlock_1 {
                      port
                        out Boolean y;
                      component /*instance*/ one_1 {
                        port
                          out Boolean out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.trueBlock.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.trueBlock.yOut1*/
                      connect one_1.out1 -> y;

                    }
                    component /*instance*/ vERSION_INFO_2 {
                      component /*instance*/ copyright_2 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.gain.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.vMax_kmhOut1*/
                    connect gain_1.vMax_kmhOut1 -> vMax_kmh;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.logOp.limiter_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.limiter_active_bOut2*/
                    connect logOp_1.limiter_active_b -> limiter_active_b;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.gain.vMax_kmhIn1*/
                    connect in1 -> gain_1.vMax_kmh;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.trueBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.logOp.in1*/
                    connect trueBlock_1.y -> logOp_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.trueBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.logOp.in2*/
                    connect trueBlock_1.y -> logOp_1.in2;

                  }
                  component /*instance*/ limiter_Deactive {
                    port
                      out Boolean limiter_active_b,
                      out Double vMax_kmh;
                    component /*instance*/ constant_2 {
                      port
                        out Double vMax_kmh;
                    }
                    component /*instance*/ falseBlock_1 {
                      port
                        out Boolean y;
                      component /*instance*/ zero_1 {
                        port
                          out Boolean out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.falseBlock.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.falseBlock.yOut1*/
                      connect zero_1.out1 -> y;

                    }
                    component /*instance*/ gain_2 {
                      port
                        in Double vMax_kmh,
                        out Double vMax_kmhOut1;
                      <<effector>> connect vMax_kmh -> vMax_kmhOut1;
                    }
                    component /*instance*/ logOp_2 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean limiter_active_b;
                      <<effector>> connect in1 -> limiter_active_b;
                      <<effector>> connect in2 -> limiter_active_b;
                    }
                    component /*instance*/ vERSION_INFO_3 {
                      component /*instance*/ copyright_3 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.gain.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.vMax_kmhOut1*/
                    connect gain_2.vMax_kmhOut1 -> vMax_kmh;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.logOp.limiter_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.limiter_active_bOut2*/
                    connect logOp_2.limiter_active_b -> limiter_active_b;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.falseBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.logOp.in1*/
                    connect falseBlock_1.y -> logOp_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.falseBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.logOp.in2*/
                    connect falseBlock_1.y -> logOp_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.constant.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.gain.vMax_kmhIn1*/
                    connect constant_2.vMax_kmh -> gain_2.vMax_kmh;

                  }
                  component /*instance*/ vERSION_INFO_4 {
                    component /*instance*/ copyright_4 {
                    }
                  }
                  component /*instance*/ switchBlock_1 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_1 {
                    port
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ switchBlock1_1 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition1_1 {
                    port
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ unitDelay_1 {
                    port
                      in Double valueIn,
                      out Double valueOut;
                    <<effector>> connect valueIn -> valueOut;
                  }
                  component /*instance*/ switchBlock2_1 {
                    port
                      in Boolean condition,
                      in Boolean elseIn,
                      in Boolean ifIn,
                      out Boolean out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition2_1 {
                    port
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ switchBlock3_1 {
                    port
                      in Boolean condition,
                      in Boolean elseIn,
                      in Boolean ifIn,
                      out Boolean out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition3_1 {
                    port
                      in Double in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ unitDelay1_1 {
                    port
                      in Boolean valueIn,
                      out Boolean valueOut;
                    <<effector>> connect valueIn -> valueOut;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.vMax_kmhOut1*/
                  connect switchBlock_1.out1 -> vMax_kmh;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_active_bOut2*/
                  connect switchBlock2_1.out1 -> limiter_active_b;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.in1*/
                  connect in1 -> limiter_Active.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition.in1*/
                  connect acceleration_pedal_pc -> condition_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition.in2*/
                  connect limiter_b -> condition_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock.condition*/
                  connect condition_1.out1 -> switchBlock_1.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock.ifIn*/
                  connect limiter_Active.vMax_kmh -> switchBlock_1.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition1.in1*/
                  connect acceleration_pedal_pc -> condition1_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition1.in2*/
                  connect limiter_b -> condition1_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock1.condition*/
                  connect condition1_1.out1 -> switchBlock1_1.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock1.ifIn*/
                  connect limiter_Deactive.vMax_kmh -> switchBlock1_1.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock.elseIn*/
                  connect switchBlock1_1.out1 -> switchBlock_1.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.unitDelay.valueIn*/
                  connect switchBlock_1.out1 -> unitDelay_1.valueIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock1.elseIn*/
                  connect unitDelay_1.valueOut -> switchBlock1_1.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition2.in1*/
                  connect acceleration_pedal_pc -> condition2_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition2.in2*/
                  connect limiter_b -> condition2_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock2.condition*/
                  connect condition2_1.out1 -> switchBlock2_1.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Active.limiter_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock2.ifIn*/
                  connect limiter_Active.limiter_active_b -> switchBlock2_1.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition3.in1*/
                  connect acceleration_pedal_pc -> condition3_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition3.in2*/
                  connect limiter_b -> condition3_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.condition3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock3.condition*/
                  connect condition3_1.out1 -> switchBlock3_1.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_Deactive.limiter_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock3.ifIn*/
                  connect limiter_Deactive.limiter_active_b -> switchBlock3_1.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock2.elseIn*/
                  connect switchBlock3_1.out1 -> switchBlock2_1.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.unitDelay1.valueIn*/
                  connect switchBlock2_1.out1 -> unitDelay1_1.valueIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.switchBlock3.elseIn*/
                  connect unitDelay1_1.valueOut -> switchBlock3_1.elseIn;

                }
                component /*instance*/ limiter_InitialSetValue {
                  port
                    out Double out1,
                    out Double out2,
                    out Double out3,
                    out Double out4,
                    out Double out5,
                    out Double out6,
                    in Double v_Vehicle_kmh;
                  component /*instance*/ vERSION_INFO_5 {
                    component /*instance*/ copyright_5 {
                    }
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out1*/
                  connect v_Vehicle_kmh -> out1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out2*/
                  connect v_Vehicle_kmh -> out2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out3*/
                  connect v_Vehicle_kmh -> out3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out4*/
                  connect v_Vehicle_kmh -> out4;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out5*/
                  connect v_Vehicle_kmh -> out5;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out6*/
                  connect v_Vehicle_kmh -> out6;

                }
                component /*instance*/ limiter_SetValue {
                  port
                    in Double in1,
                    in Double in2,
                    in Double in3,
                    in Double in4,
                    in Boolean leverDown_b,
                    in Boolean leverUp_b,
                    out Double out1,
                    out Double out2;
                  component /*instance*/ vERSION_INFO_6 {
                    component /*instance*/ copyright_6 {
                    }
                  }
                  component /*instance*/ v_LimSetValueMinus {
                    port
                      out Double out1,
                      out Double out2,
                      in Double v_LimiterSetValue;
                    component /*instance*/ parameter_1 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ sum_1 {
                      port
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ vERSION_INFO_7 {
                      component /*instance*/ copyright_7 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.out1*/
                    connect sum_1.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.out2*/
                    connect sum_1.out1 -> out2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.parameter.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.sum.in2*/
                    connect parameter_1.out1 -> sum_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.v_LimiterSetValueIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.sum.in1*/
                    connect v_LimiterSetValue -> sum_1.in1;

                  }
                  component /*instance*/ v_LimSetValuePlus {
                    port
                      out Double out1,
                      out Double out2,
                      in Double v_LimiterSetValue;
                    component /*instance*/ parameter_2 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ sum_2 {
                      port
                        in Double in1,
                        in Double in2,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ vERSION_INFO_8 {
                      component /*instance*/ copyright_8 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.out1*/
                    connect sum_2.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.out2*/
                    connect sum_2.out1 -> out2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.parameter.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.sum.in2*/
                    connect parameter_2.out1 -> sum_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.v_LimiterSetValueIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.sum.in1*/
                    connect v_LimiterSetValue -> sum_2.in1;

                  }
                  component /*instance*/ risingEdgeDetector_1 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    component /*instance*/ compareToZero_1 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_1 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_3 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.yOut1*/
                      connect compare_1.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.compare.in1*/
                      connect u -> compare_1.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.compare.in2*/
                      connect constant_3.out1 -> compare_1.in2;

                    }
                    component /*instance*/ compareToZero1_1 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_2 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_4 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.yOut1*/
                      connect compare_2.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.compare.in1*/
                      connect u -> compare_2.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.compare.in2*/
                      connect constant_4.out1 -> compare_2.in2;

                    }
                    component /*instance*/ compareToZero2_1 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_3 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_5 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.yOut1*/
                      connect compare_3.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.compare.in1*/
                      connect u -> compare_3.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.compare.in2*/
                      connect constant_5.out1 -> compare_3.in2;

                    }
                    component /*instance*/ compareToZero3_1 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_4 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_6 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.yOut1*/
                      connect compare_4.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.compare.in1*/
                      connect u -> compare_4.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.compare.in2*/
                      connect constant_6.out1 -> compare_4.in2;

                    }
                    component /*instance*/ compareToZero4_1 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_5 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_7 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.yOut1*/
                      connect compare_5.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.compare.in1*/
                      connect u -> compare_5.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.compare.in2*/
                      connect constant_7.out1 -> compare_5.in2;

                    }
                    component /*instance*/ constant_8 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ constant1_1 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ logicalOperator_1 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                      <<effector>> connect in3 -> out1;
                    }
                    component /*instance*/ logicalOperator1_1 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logicalOperator2_1 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ memory_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory1_1 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory2_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock_2 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_2 {
                      port
                        in Double in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.out1Out1*/
                    connect switchBlock_2.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator.in3*/
                    connect compareToZero4_1.y -> logicalOperator_1.in3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero4.uIn1*/
                    connect memory2_1.out1 -> compareToZero4_1.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator2.in1*/
                    connect logicalOperator_1.out1 -> logicalOperator2_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.switchBlock.elseIn*/
                    connect constant1_1.out1 -> switchBlock_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory1.in1*/
                    connect constant_8.out1 -> memory1_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.switchBlock.condition*/
                    connect condition_2.out1 -> switchBlock_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.switchBlock.ifIn*/
                    connect logicalOperator2_1.out1 -> switchBlock_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory2.in1*/
                    connect logicalOperator2_1.out1 -> memory2_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator2.in2*/
                    connect logicalOperator1_1.out1 -> logicalOperator2_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator1.in2*/
                    connect compareToZero3_1.y -> logicalOperator1_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator1.in1*/
                    connect compareToZero2_1.y -> logicalOperator1_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator.in2*/
                    connect compareToZero1_1.y -> logicalOperator_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.logicalOperator.in1*/
                    connect compareToZero_1.y -> logicalOperator_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero.uIn1*/
                    connect memory_1.out1 -> compareToZero_1.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero2.uIn1*/
                    connect memory_1.out1 -> compareToZero2_1.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory.in1*/
                    connect in1 -> memory_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero1.uIn1*/
                    connect in1 -> compareToZero1_1.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.compareToZero3.uIn1*/
                    connect in1 -> compareToZero3_1.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.condition.in1*/
                    connect memory1_1.out1 -> condition_2.in1;

                  }
                  component /*instance*/ risingEdgeDetector1_1 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    component /*instance*/ compareToZero_2 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_6 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_9 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.yOut1*/
                      connect compare_6.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.compare.in1*/
                      connect u -> compare_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.compare.in2*/
                      connect constant_9.out1 -> compare_6.in2;

                    }
                    component /*instance*/ compareToZero1_2 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_7 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_10 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.yOut1*/
                      connect compare_7.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.compare.in1*/
                      connect u -> compare_7.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.compare.in2*/
                      connect constant_10.out1 -> compare_7.in2;

                    }
                    component /*instance*/ compareToZero2_2 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_8 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_11 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.yOut1*/
                      connect compare_8.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.compare.in1*/
                      connect u -> compare_8.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.compare.in2*/
                      connect constant_11.out1 -> compare_8.in2;

                    }
                    component /*instance*/ compareToZero3_2 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_9 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_12 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.yOut1*/
                      connect compare_9.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.compare.in1*/
                      connect u -> compare_9.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.compare.in2*/
                      connect constant_12.out1 -> compare_9.in2;

                    }
                    component /*instance*/ compareToZero4_2 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_10 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_13 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.yOut1*/
                      connect compare_10.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.compare.in1*/
                      connect u -> compare_10.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.compare.in2*/
                      connect constant_13.out1 -> compare_10.in2;

                    }
                    component /*instance*/ constant_14 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ constant1_2 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ logicalOperator_2 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                      <<effector>> connect in3 -> out1;
                    }
                    component /*instance*/ logicalOperator1_2 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logicalOperator2_2 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ memory_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory1_2 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory2_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock_3 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_3 {
                      port
                        in Double in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.out1Out1*/
                    connect switchBlock_3.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator.in3*/
                    connect compareToZero4_2.y -> logicalOperator_2.in3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero4.uIn1*/
                    connect memory2_2.out1 -> compareToZero4_2.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator2.in1*/
                    connect logicalOperator_2.out1 -> logicalOperator2_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.switchBlock.elseIn*/
                    connect constant1_2.out1 -> switchBlock_3.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory1.in1*/
                    connect constant_14.out1 -> memory1_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.switchBlock.condition*/
                    connect condition_3.out1 -> switchBlock_3.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.switchBlock.ifIn*/
                    connect logicalOperator2_2.out1 -> switchBlock_3.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory2.in1*/
                    connect logicalOperator2_2.out1 -> memory2_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator2.in2*/
                    connect logicalOperator1_2.out1 -> logicalOperator2_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator1.in2*/
                    connect compareToZero3_2.y -> logicalOperator1_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator1.in1*/
                    connect compareToZero2_2.y -> logicalOperator1_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator.in2*/
                    connect compareToZero1_2.y -> logicalOperator_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.logicalOperator.in1*/
                    connect compareToZero_2.y -> logicalOperator_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero.uIn1*/
                    connect memory_2.out1 -> compareToZero_2.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero2.uIn1*/
                    connect memory_2.out1 -> compareToZero2_2.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory.in1*/
                    connect in1 -> memory_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero1.uIn1*/
                    connect in1 -> compareToZero1_2.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.compareToZero3.uIn1*/
                    connect in1 -> compareToZero3_2.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.condition.in1*/
                    connect memory1_2.out1 -> condition_3.in1;

                  }
                  component /*instance*/ switchBlock_4 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_4 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock1_2 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition1_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock2_2 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition2_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock3_2 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition3_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.out1*/
                  connect switchBlock_4.out1 -> out1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.out2*/
                  connect switchBlock2_2.out1 -> out2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.v_LimiterSetValueIn1*/
                  connect in2 -> v_LimSetValueMinus.v_LimiterSetValue;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.v_LimiterSetValueIn1*/
                  connect in1 -> v_LimSetValuePlus.v_LimiterSetValue;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.leverUp_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.in1In1*/
                  connect leverUp_b -> risingEdgeDetector_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.leverDown_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.in1In1*/
                  connect leverDown_b -> risingEdgeDetector1_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition.in1*/
                  connect risingEdgeDetector_1.out1 -> condition_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock.ifIn*/
                  connect v_LimSetValuePlus.out1 -> switchBlock_4.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock.condition*/
                  connect condition_4.out1 -> switchBlock_4.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition1.in1*/
                  connect risingEdgeDetector1_1.out1 -> condition1_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock1.ifIn*/
                  connect v_LimSetValueMinus.out1 -> switchBlock1_2.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock1.condition*/
                  connect condition1_2.out1 -> switchBlock1_2.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock1.elseIn*/
                  connect in3 -> switchBlock1_2.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock.elseIn*/
                  connect switchBlock1_2.out1 -> switchBlock_4.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition2.in1*/
                  connect risingEdgeDetector_1.out1 -> condition2_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValuePlus.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock2.ifIn*/
                  connect v_LimSetValuePlus.out2 -> switchBlock2_2.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock2.condition*/
                  connect condition2_2.out1 -> switchBlock2_2.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition3.in1*/
                  connect risingEdgeDetector1_1.out1 -> condition3_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.v_LimSetValueMinus.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock3.ifIn*/
                  connect v_LimSetValueMinus.out2 -> switchBlock3_2.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.condition3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock3.condition*/
                  connect condition3_2.out1 -> switchBlock3_2.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock3.elseIn*/
                  connect in4 -> switchBlock3_2.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.switchBlock2.elseIn*/
                  connect switchBlock3_2.out1 -> switchBlock2_2.elseIn;

                }
                component /*instance*/ limiter_StartUpSetValue {
                  port
                    out Double out1;
                  component /*instance*/ constant_15 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ vERSION_INFO_9 {
                    component /*instance*/ copyright_9 {
                    }
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_StartUpSetValue.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_StartUpSetValue.out1*/
                  connect constant_15.out1 -> out1;

                }
                component /*instance*/ sysInit_1 {
                  port
                    out Double y;
                  component /*instance*/ memory_Init_1 {
                    port
                      in Double in1,
                      out Double out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ zero_Init_1 {
                    port
                      out Double out1;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.sysInit.memory_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.sysInit.yOut1*/
                  connect memory_Init_1.out1 -> y;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.sysInit.zero_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.sysInit.memory_Init.in1*/
                  connect zero_Init_1.out1 -> memory_Init_1.in1;

                }
                component /*instance*/ vERSION_INFO_10 {
                  component /*instance*/ copyright_10 {
                  }
                }
                component /*instance*/ risingEdgeDetector_2 {
                  port
                    in Double in1,
                    out Boolean out1;
                  component /*instance*/ compareToZero_3 {
                    port
                      in Double u,
                      out Boolean y;
                    component /*instance*/ compare_11 {
                      port
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_16 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.yOut1*/
                    connect compare_11.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.compare.in1*/
                    connect u -> compare_11.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.compare.in2*/
                    connect constant_16.out1 -> compare_11.in2;

                  }
                  component /*instance*/ compareToZero1_3 {
                    port
                      in Double u,
                      out Boolean y;
                    component /*instance*/ compare_12 {
                      port
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_17 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.yOut1*/
                    connect compare_12.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.compare.in1*/
                    connect u -> compare_12.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.compare.in2*/
                    connect constant_17.out1 -> compare_12.in2;

                  }
                  component /*instance*/ compareToZero2_3 {
                    port
                      in Double u,
                      out Boolean y;
                    component /*instance*/ compare_13 {
                      port
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_18 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.yOut1*/
                    connect compare_13.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.compare.in1*/
                    connect u -> compare_13.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.compare.in2*/
                    connect constant_18.out1 -> compare_13.in2;

                  }
                  component /*instance*/ compareToZero3_3 {
                    port
                      in Double u,
                      out Boolean y;
                    component /*instance*/ compare_14 {
                      port
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_19 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.yOut1*/
                    connect compare_14.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.compare.in1*/
                    connect u -> compare_14.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.compare.in2*/
                    connect constant_19.out1 -> compare_14.in2;

                  }
                  component /*instance*/ compareToZero4_3 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_15 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_20 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.yOut1*/
                    connect compare_15.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.compare.in1*/
                    connect u -> compare_15.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.compare.in2*/
                    connect constant_20.out1 -> compare_15.in2;

                  }
                  component /*instance*/ constant_21 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ constant1_3 {
                    port
                      out Boolean out1;
                  }
                  component /*instance*/ logicalOperator_3 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                    <<effector>> connect in3 -> out1;
                  }
                  component /*instance*/ logicalOperator1_3 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ logicalOperator2_3 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ memory_3 {
                    port
                      in Double in1,
                      out Double out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ memory1_3 {
                    port
                      in Double in1,
                      out Double out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ memory2_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock_5 {
                    port
                      in Boolean condition,
                      in Boolean elseIn,
                      in Boolean ifIn,
                      out Boolean out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_5 {
                    port
                      in Double in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.out1Out1*/
                  connect switchBlock_5.out1 -> out1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator.in3*/
                  connect compareToZero4_3.y -> logicalOperator_3.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero4.uIn1*/
                  connect memory2_3.out1 -> compareToZero4_3.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator2.in1*/
                  connect logicalOperator_3.out1 -> logicalOperator2_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.switchBlock.elseIn*/
                  connect constant1_3.out1 -> switchBlock_5.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory1.in1*/
                  connect constant_21.out1 -> memory1_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.switchBlock.condition*/
                  connect condition_5.out1 -> switchBlock_5.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.switchBlock.ifIn*/
                  connect logicalOperator2_3.out1 -> switchBlock_5.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory2.in1*/
                  connect logicalOperator2_3.out1 -> memory2_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator2.in2*/
                  connect logicalOperator1_3.out1 -> logicalOperator2_3.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator1.in2*/
                  connect compareToZero3_3.y -> logicalOperator1_3.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator1.in1*/
                  connect compareToZero2_3.y -> logicalOperator1_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator.in2*/
                  connect compareToZero1_3.y -> logicalOperator_3.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.logicalOperator.in1*/
                  connect compareToZero_3.y -> logicalOperator_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero.uIn1*/
                  connect memory_3.out1 -> compareToZero_3.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero2.uIn1*/
                  connect memory_3.out1 -> compareToZero2_3.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory.in1*/
                  connect in1 -> memory_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero1.uIn1*/
                  connect in1 -> compareToZero1_3.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.compareToZero3.uIn1*/
                  connect in1 -> compareToZero3_3.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.condition.in1*/
                  connect memory1_3.out1 -> condition_5.in1;

                }
                component /*instance*/ risingEdgeDetector1_2 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  component /*instance*/ compareToZero_4 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_16 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_22 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.yOut1*/
                    connect compare_16.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.compare.in1*/
                    connect u -> compare_16.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.compare.in2*/
                    connect constant_22.out1 -> compare_16.in2;

                  }
                  component /*instance*/ compareToZero1_4 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_17 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_23 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.yOut1*/
                    connect compare_17.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.compare.in1*/
                    connect u -> compare_17.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.compare.in2*/
                    connect constant_23.out1 -> compare_17.in2;

                  }
                  component /*instance*/ compareToZero2_4 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_18 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_24 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.yOut1*/
                    connect compare_18.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.compare.in1*/
                    connect u -> compare_18.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.compare.in2*/
                    connect constant_24.out1 -> compare_18.in2;

                  }
                  component /*instance*/ compareToZero3_4 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_19 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_25 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.yOut1*/
                    connect compare_19.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.compare.in1*/
                    connect u -> compare_19.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.compare.in2*/
                    connect constant_25.out1 -> compare_19.in2;

                  }
                  component /*instance*/ compareToZero4_4 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_20 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_26 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.yOut1*/
                    connect compare_20.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.compare.in1*/
                    connect u -> compare_20.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.compare.in2*/
                    connect constant_26.out1 -> compare_20.in2;

                  }
                  component /*instance*/ constant_27 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ constant1_4 {
                    port
                      out Boolean out1;
                  }
                  component /*instance*/ logicalOperator_4 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                    <<effector>> connect in3 -> out1;
                  }
                  component /*instance*/ logicalOperator1_4 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ logicalOperator2_4 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ memory_4 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ memory1_4 {
                    port
                      in Double in1,
                      out Double out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ memory2_4 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock_6 {
                    port
                      in Boolean condition,
                      in Boolean elseIn,
                      in Boolean ifIn,
                      out Boolean out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_6 {
                    port
                      in Double in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1*/
                  connect switchBlock_6.out1 -> out1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator.in3*/
                  connect compareToZero4_4.y -> logicalOperator_4.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero4.uIn1*/
                  connect memory2_4.out1 -> compareToZero4_4.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator2.in1*/
                  connect logicalOperator_4.out1 -> logicalOperator2_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.switchBlock.elseIn*/
                  connect constant1_4.out1 -> switchBlock_6.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory1.in1*/
                  connect constant_27.out1 -> memory1_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.switchBlock.condition*/
                  connect condition_6.out1 -> switchBlock_6.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.switchBlock.ifIn*/
                  connect logicalOperator2_4.out1 -> switchBlock_6.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory2.in1*/
                  connect logicalOperator2_4.out1 -> memory2_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator2.in2*/
                  connect logicalOperator1_4.out1 -> logicalOperator2_4.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator1.in2*/
                  connect compareToZero3_4.y -> logicalOperator1_4.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator1.in1*/
                  connect compareToZero2_4.y -> logicalOperator1_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator.in2*/
                  connect compareToZero1_4.y -> logicalOperator_4.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.logicalOperator.in1*/
                  connect compareToZero_4.y -> logicalOperator_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero.uIn1*/
                  connect memory_4.out1 -> compareToZero_4.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero2.uIn1*/
                  connect memory_4.out1 -> compareToZero2_4.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory.in1*/
                  connect in1 -> memory_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero1.uIn1*/
                  connect in1 -> compareToZero1_4.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.compareToZero3.uIn1*/
                  connect in1 -> compareToZero3_4.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.condition.in1*/
                  connect memory1_4.out1 -> condition_6.in1;

                }
                component /*instance*/ switchBlock_7 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition_7 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock1_3 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition1_3 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock2_3 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition2_3 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock3_3 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition3_3 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock4_1 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition4_1 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock5_1 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition5_1 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock6_1 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition6_1 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock7_1 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition7_1 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                component /*instance*/ switchBlock8_1 {
                  port
                    in Boolean condition,
                    in Double elseIn,
                    in Double ifIn,
                    out Double out1;
                  <<effector>> connect ifIn -> out1;
                  <<effector>> connect condition -> out1;
                  <<effector>> connect elseIn -> out1;
                }
                component /*instance*/ condition8_1 {
                  port
                    in Boolean in1,
                    out Boolean out1;
                  <<effector>> connect in1 -> out1;
                }
                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.vMax_kmhOut1*/
                connect limiter_Function.vMax_kmh -> vMax_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_active_bOut2*/
                connect limiter_Function.limiter_active_b -> limiter_active_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.out1*/
                connect switchBlock5_1.out1 -> out1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.sysInit.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.in1In1*/
                connect sysInit_1.y -> risingEdgeDetector_2.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.in1In1*/
                connect limiter_b -> risingEdgeDetector1_2.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition.in1*/
                connect risingEdgeDetector1_2.out1 -> condition_7.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock.ifIn*/
                connect limiter_InitialSetValue.out1 -> switchBlock_7.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock.condition*/
                connect condition_7.out1 -> switchBlock_7.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock.elseIn*/
                connect in1 -> switchBlock_7.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in1*/
                connect switchBlock_7.out1 -> limiter_SetValue.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition1.in1*/
                connect risingEdgeDetector1_2.out1 -> condition1_3.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock1.ifIn*/
                connect limiter_InitialSetValue.out2 -> switchBlock1_3.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock1.condition*/
                connect condition1_3.out1 -> switchBlock1_3.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock1.elseIn*/
                connect in2 -> switchBlock1_3.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in2*/
                connect switchBlock1_3.out1 -> limiter_SetValue.in2;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition2.in1*/
                connect risingEdgeDetector1_2.out1 -> condition2_3.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock2.ifIn*/
                connect limiter_InitialSetValue.out3 -> switchBlock2_3.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock2.condition*/
                connect condition2_3.out1 -> switchBlock2_3.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock2.elseIn*/
                connect in3 -> switchBlock2_3.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in3*/
                connect switchBlock2_3.out1 -> limiter_SetValue.in3;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition3.in1*/
                connect limiter_b -> condition3_3.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock3.ifIn*/
                connect limiter_SetValue.out1 -> switchBlock3_3.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock3.condition*/
                connect condition3_3.out1 -> switchBlock3_3.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition4.in1*/
                connect risingEdgeDetector1_2.out1 -> condition4_1.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock4.ifIn*/
                connect limiter_InitialSetValue.out4 -> switchBlock4_1.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock4.condition*/
                connect condition4_1.out1 -> switchBlock4_1.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock4.elseIn*/
                connect in4 -> switchBlock4_1.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock3.elseIn*/
                connect switchBlock4_1.out1 -> switchBlock3_3.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.in1*/
                connect switchBlock3_3.out1 -> limiter_Function.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition5.in1*/
                connect risingEdgeDetector_2.out1 -> condition5_1.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_StartUpSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock5.ifIn*/
                connect limiter_StartUpSetValue.out1 -> switchBlock5_1.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock5.condition*/
                connect condition5_1.out1 -> switchBlock5_1.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition6.in1*/
                connect risingEdgeDetector1_2.out1 -> condition6_1.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock6.ifIn*/
                connect limiter_InitialSetValue.out5 -> switchBlock6_1.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock6.condition*/
                connect condition6_1.out1 -> switchBlock6_1.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock6.elseIn*/
                connect in5 -> switchBlock6_1.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.in4*/
                connect switchBlock6_1.out1 -> limiter_SetValue.in4;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition7.in1*/
                connect limiter_b -> condition7_1.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock7.ifIn*/
                connect limiter_SetValue.out2 -> switchBlock7_1.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock7.condition*/
                connect condition7_1.out1 -> switchBlock7_1.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition8.in1*/
                connect risingEdgeDetector1_2.out1 -> condition8_1.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.out6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock8.ifIn*/
                connect limiter_InitialSetValue.out6 -> switchBlock8_1.ifIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.condition8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock8.condition*/
                connect condition8_1.out1 -> switchBlock8_1.condition;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock8.elseIn*/
                connect in6 -> switchBlock8_1.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock7.elseIn*/
                connect switchBlock8_1.out1 -> switchBlock7_1.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.switchBlock5.elseIn*/
                connect switchBlock7_1.out1 -> switchBlock5_1.elseIn;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.acceleration_pedal_pcIn2*/
                connect acceleration_pedal_pc -> limiter_Function.acceleration_pedal_pc;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_Function.limiter_bIn1*/
                connect limiter_b -> limiter_Function.limiter_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.v_Vehicle_kmhIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_InitialSetValue.v_Vehicle_kmhIn1*/
                connect v_Vehicle_kmh -> limiter_InitialSetValue.v_Vehicle_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.leverUp_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.leverUp_bIn1*/
                connect leverUp_b -> limiter_SetValue.leverUp_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.leverDown_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_SetValue.leverDown_bIn2*/
                connect leverDown_b -> limiter_SetValue.leverDown_b;

              }
              component /*instance*/ vERSION_INFO_11 {
                component /*instance*/ copyright_11 {
                }
              }
              component /*instance*/ switchBlock_8 {
                port
                  in Boolean condition,
                  in Double elseIn,
                  in Double ifIn,
                  out Double out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ condition_8 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ condition1_4 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ switchBlock1_4 {
                port
                  in Boolean condition,
                  in Double elseIn,
                  in Double ifIn,
                  out Double out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ constant1_5 {
                port
                  out Double out1;
              }
              component /*instance*/ condition2_4 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ switchBlock2_4 {
                port
                  in Boolean condition,
                  in Boolean elseIn,
                  in Boolean ifIn,
                  out Boolean out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ constant2_1 {
                port
                  out Boolean out1;
              }
              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.vMax_kmhOut1*/
              connect switchBlock1_4.out1 -> vMax_kmh;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_active_bOut2*/
              connect switchBlock2_4.out1 -> limiter_active_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.out1*/
              connect switchBlock_8.out1 -> out1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in1*/
              connect in1 -> limiter_enabled.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in2*/
              connect in2 -> limiter_enabled.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in3*/
              connect in3 -> limiter_enabled.in3;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in4*/
              connect in4 -> limiter_enabled.in4;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in5*/
              connect in5 -> limiter_enabled.in5;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.in6*/
              connect in6 -> limiter_enabled.in6;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition.in1*/
              connect constant_1.out1 -> condition_8.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock.ifIn*/
              connect limiter_enabled.out1 -> switchBlock_8.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock.condition*/
              connect condition_8.out1 -> switchBlock_8.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock.elseIn*/
              connect in7 -> switchBlock_8.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition1.in1*/
              connect constant_1.out1 -> condition1_4.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock1.condition*/
              connect condition1_4.out1 -> switchBlock1_4.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock1.ifIn*/
              connect limiter_enabled.vMax_kmh -> switchBlock1_4.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock1.elseIn*/
              connect constant1_5.out1 -> switchBlock1_4.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition2.in1*/
              connect constant_1.out1 -> condition2_4.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock2.condition*/
              connect condition2_4.out1 -> switchBlock2_4.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock2.ifIn*/
              connect limiter_enabled.limiter_active_b -> switchBlock2_4.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.constant2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.switchBlock2.elseIn*/
              connect constant2_1.out1 -> switchBlock2_4.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.leverDown_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.leverDown_bIn5*/
              connect leverDown_b -> limiter_enabled.leverDown_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.leverUp_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.leverUp_bIn4*/
              connect leverUp_b -> limiter_enabled.leverUp_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.v_Vehicle_kmhIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.v_Vehicle_kmhIn3*/
              connect v_Vehicle_kmh -> limiter_enabled.v_Vehicle_kmh;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.acceleration_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.acceleration_pedal_pcIn2*/
              connect acceleration_pedal_pc -> limiter_enabled.acceleration_pedal_pc;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_enabled.limiter_bIn1*/
              connect limiter_b -> limiter_enabled.limiter_b;

            }
            component /*instance*/ tempomat {
              port
                in Double brakeForce_pedal_pc,
                out Boolean cC_active_b,
                in Boolean cruiseControl_b,
                in Double in1,
                in Double in10,
                in Double in11,
                in Double in12,
                in Double in13,
                in Double in2,
                in Double in3,
                in Double in4,
                in Double in5,
                in Double in6,
                in Double in7,
                in Double in8,
                in Double in9,
                in Boolean leverDown_b,
                in Boolean leverUp_b,
                in Boolean limiter_b,
                out Double out1,
                in Boolean parkingBrake_b,
                out Double v_CC_delta_kmh,
                in Double v_Vehicle_kmh;
              component /*instance*/ constant_28 {
                port
                  out Double out1;
              }
              component /*instance*/ tempomat_Function {
                port
                  in Double brakeForce_pedal_pc,
                  out Boolean cC_active_b,
                  in Boolean cruiseControl_b,
                  in Double in1,
                  in Double in10,
                  in Double in11,
                  in Double in12,
                  in Double in2,
                  in Double in3,
                  in Double in4,
                  in Double in5,
                  in Double in6,
                  in Double in7,
                  in Double in8,
                  in Double in9,
                  in Boolean leverDown_b,
                  in Boolean leverUp_b,
                  in Boolean limiter_b,
                  out Double out1,
                  in Boolean parkingBrake_b,
                  out Double v_CC_delta_kmh,
                  in Double v_Vehicle_kmh;
                component /*instance*/ cC_On_Off {
                  port
                    in Double brakeForce_pedal_pc,
                    out Boolean cC_active_b,
                    in Boolean cruiseControl_b,
                    in Boolean leverDown_b,
                    in Boolean leverUp_b,
                    in Boolean limiter_b,
                    in Boolean parkingBrake_b,
                    in Double v_Vehicle_kmh;
                  component /*instance*/ constant_29 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ constant1_6 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ edgeFalling {
                    port
                      in Boolean iV,
                      in Boolean r,
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ logOp_A_1 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logOp_N_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory_U_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switch_R_1 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_9 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.logOp_A.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.yOut1*/
                    connect logOp_A_1.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.switch_R.condition*/
                    connect condition_9.out1 -> switch_R_1.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.rIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.condition.in1*/
                    connect r -> condition_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.logOp_A.in2*/
                    connect switch_R_1.out1 -> logOp_A_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.logOp_N.in1*/
                    connect u -> logOp_N_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.memory_U.in1*/
                    connect u -> memory_U_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.logOp_N.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.logOp_A.in1*/
                    connect logOp_N_1.out1 -> logOp_A_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.memory_U.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.switch_R.elseIn*/
                    connect memory_U_1.out1 -> switch_R_1.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.iVIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.switch_R.ifIn*/
                    connect iV -> switch_R_1.ifIn;

                  }
                  component /*instance*/ edgeRising_1 {
                    port
                      in Boolean iV,
                      in Boolean r,
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ logOp_A_2 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logOp_N_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory_U_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switch_R_2 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_10 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.logOp_A.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.yOut1*/
                    connect logOp_A_2.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.switch_R.condition*/
                    connect condition_10.out1 -> switch_R_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.rIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.condition.in1*/
                    connect r -> condition_10.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.logOp_N.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.logOp_A.in2*/
                    connect logOp_N_2.out1 -> logOp_A_2.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.logOp_A.in1*/
                    connect u -> logOp_A_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.memory_U.in1*/
                    connect u -> memory_U_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.logOp_N.in1*/
                    connect switch_R_2.out1 -> logOp_N_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.memory_U.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.switch_R.elseIn*/
                    connect memory_U_2.out1 -> switch_R_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.iVIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.switch_R.ifIn*/
                    connect iV -> switch_R_2.ifIn;

                  }
                  component /*instance*/ falseBlock_2 {
                    port
                      out Boolean y;
                    component /*instance*/ zero_2 {
                      port
                        out Boolean out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock.yOut1*/
                    connect zero_2.out1 -> y;

                  }
                  component /*instance*/ falseBlock1 {
                    port
                      out Boolean y;
                    component /*instance*/ zero_3 {
                      port
                        out Boolean out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock1.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock1.yOut1*/
                    connect zero_3.out1 -> y;

                  }
                  component /*instance*/ logicalOperator_5 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      in Boolean in4,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                    <<effector>> connect in3 -> out1;
                    <<effector>> connect in4 -> out1;
                  }
                  component /*instance*/ logicalOperator1_5 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ logicalOperator3_1 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                    <<effector>> connect in3 -> out1;
                  }
                  component /*instance*/ logicalOperator4_1 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ logicalOperator5_1 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ rSFlipFlop_1 {
                    port
                      out Boolean nOT_Q,
                      out Boolean q,
                      in Boolean r,
                      in Boolean s;
                    component /*instance*/ logOp_N_3 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory_Q_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ one_S_1 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ switch_R_3 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ switch_S_1 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ zero_R_1 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ condition_11 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ condition1_5 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.qOut1*/
                    connect switch_R_3.out1 -> q;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.logOp_N.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.nOT_QOut2*/
                    connect logOp_N_3.out1 -> nOT_Q;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_S.condition*/
                    connect condition1_5.out1 -> switch_S_1.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.condition*/
                    connect condition_11.out1 -> switch_R_3.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.rIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.condition.in1*/
                    connect r -> condition_11.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.sIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.condition1.in1*/
                    connect s -> condition1_5.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.memory_Q.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_S.elseIn*/
                    connect memory_Q_1.out1 -> switch_S_1.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_S.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.elseIn*/
                    connect switch_S_1.out1 -> switch_R_3.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.one_S.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_S.ifIn*/
                    connect one_S_1.out1 -> switch_S_1.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.memory_Q.in1*/
                    connect switch_R_3.out1 -> memory_Q_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.logOp_N.in1*/
                    connect switch_R_3.out1 -> logOp_N_3.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.zero_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.switch_R.ifIn*/
                    connect zero_R_1.out1 -> switch_R_3.ifIn;

                  }
                  component /*instance*/ relOp1_1 {
                    port
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ relOp2_1 {
                    port
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ terminator_1 {
                    port
                      in Boolean in1;
                  }
                  component /*instance*/ vERSION_INFO_12 {
                    component /*instance*/ copyright_12 {
                    }
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.qOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cC_active_bOut1*/
                  connect rSFlipFlop_1.q -> cC_active_b;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.iVIn3*/
                  connect falseBlock1.y -> edgeFalling.iV;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator4.in1*/
                  connect logicalOperator5_1.out1 -> logicalOperator4_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator4.in2*/
                  connect logicalOperator3_1.out1 -> logicalOperator4_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator1.in1*/
                  connect logicalOperator4_1.out1 -> logicalOperator1_5.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.uIn1*/
                  connect logicalOperator4_1.out1 -> edgeRising_1.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator5.in1*/
                  connect limiter_b -> logicalOperator5_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator.in1*/
                  connect edgeFalling.y -> logicalOperator_5.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp1.in1*/
                  connect v_Vehicle_kmh -> relOp1_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator3.in3*/
                  connect cruiseControl_b -> logicalOperator3_1.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.uIn1*/
                  connect cruiseControl_b -> edgeFalling.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeFalling.rIn2*/
                  connect cruiseControl_b -> edgeFalling.r;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator3.in1*/
                  connect leverUp_b -> logicalOperator3_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator3.in2*/
                  connect leverDown_b -> logicalOperator3_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator.in4*/
                  connect relOp1_1.out1 -> logicalOperator_5.in4;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp1.in2*/
                  connect constant1_6.out1 -> relOp1_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.falseBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.iVIn3*/
                  connect falseBlock_2.y -> edgeRising_1.iV;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.rIn2*/
                  connect logicalOperator1_5.out1 -> edgeRising_1.r;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.brakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp2.in1*/
                  connect brakeForce_pedal_pc -> relOp2_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.nOT_QOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.terminator.in1*/
                  connect rSFlipFlop_1.nOT_Q -> terminator_1.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.parkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator.in2*/
                  connect parkingBrake_b -> logicalOperator_5.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp2.in2*/
                  connect constant_29.out1 -> relOp2_1.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.relOp2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator.in3*/
                  connect relOp2_1.out1 -> logicalOperator_5.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.rIn2*/
                  connect logicalOperator_5.out1 -> rSFlipFlop_1.r;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.edgeRising.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.rSFlipFlop.sIn1*/
                  connect edgeRising_1.y -> rSFlipFlop_1.s;

                }
                component /*instance*/ cC_SetValue {
                  port
                    in Boolean cC_enabled_b,
                    in Boolean cruiseControl_b,
                    in Double in1,
                    in Double in10,
                    in Double in11,
                    in Double in12,
                    in Double in2,
                    in Double in3,
                    in Double in4,
                    in Double in5,
                    in Double in6,
                    in Double in7,
                    in Double in8,
                    in Double in9,
                    in Boolean leverDown_b,
                    in Boolean leverUp_b,
                    in Boolean limiter_b,
                    out Double out1,
                    out Double out2,
                    in Double v_Vehicle_kmh;
                  component /*instance*/ cC_ChangeSetValue {
                    port
                      in Boolean cCDown_b,
                      in Boolean cCUp_b,
                      in Double in1,
                      in Double in2,
                      in Double in3,
                      in Double in4,
                      in Double in5,
                      in Double in6,
                      in Double in7,
                      out Double out1,
                      out Double out2,
                      out Double out3,
                      out Double out4,
                      out Double out5;
                    component /*instance*/ vERSION_INFO_13 {
                      component /*instance*/ copyright_13 {
                      }
                    }
                    component /*instance*/ v_SetValueMinus {
                      port
                        out Double out1,
                        out Double out2,
                        out Double out3,
                        out Double out4,
                        out Double out5,
                        in Double v_CCSetValue;
                      component /*instance*/ parameter_3 {
                        port
                          out Double out1;
                      }
                      component /*instance*/ sum_3 {
                        port
                          in Double in1,
                          in Double in2,
                          out Double out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ vERSION_INFO_14 {
                        component /*instance*/ copyright_14 {
                        }
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out1*/
                      connect sum_3.out1 -> out1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out2*/
                      connect sum_3.out1 -> out2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out3*/
                      connect sum_3.out1 -> out3;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out4*/
                      connect sum_3.out1 -> out4;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out5*/
                      connect sum_3.out1 -> out5;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.parameter.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.in2*/
                      connect parameter_3.out1 -> sum_3.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.v_CCSetValueIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.sum.in1*/
                      connect v_CCSetValue -> sum_3.in1;

                    }
                    component /*instance*/ v_SetValuePlus {
                      port
                        out Double out1,
                        out Double out2,
                        out Double out3,
                        out Double out4,
                        out Double out5,
                        in Double v_CCSetValue;
                      component /*instance*/ parameter_4 {
                        port
                          out Double out1;
                      }
                      component /*instance*/ sum_4 {
                        port
                          in Double in1,
                          in Double in2,
                          out Double out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ vERSION_INFO_15 {
                        component /*instance*/ copyright_15 {
                        }
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out1*/
                      connect sum_4.out1 -> out1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out2*/
                      connect sum_4.out1 -> out2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out3*/
                      connect sum_4.out1 -> out3;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out4*/
                      connect sum_4.out1 -> out4;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out5*/
                      connect sum_4.out1 -> out5;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.parameter.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.in2*/
                      connect parameter_4.out1 -> sum_4.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.v_CCSetValueIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.sum.in1*/
                      connect v_CCSetValue -> sum_4.in1;

                    }
                    component /*instance*/ risingEdgeDetector_3 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      component /*instance*/ compareToZero_5 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_21 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_30 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.yOut1*/
                        connect compare_21.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.compare.in1*/
                        connect u -> compare_21.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.compare.in2*/
                        connect constant_30.out1 -> compare_21.in2;

                      }
                      component /*instance*/ compareToZero1_5 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_22 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_31 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.yOut1*/
                        connect compare_22.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.compare.in1*/
                        connect u -> compare_22.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.compare.in2*/
                        connect constant_31.out1 -> compare_22.in2;

                      }
                      component /*instance*/ compareToZero2_5 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_23 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_32 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.yOut1*/
                        connect compare_23.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.compare.in1*/
                        connect u -> compare_23.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.compare.in2*/
                        connect constant_32.out1 -> compare_23.in2;

                      }
                      component /*instance*/ compareToZero3_5 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_24 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_33 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.yOut1*/
                        connect compare_24.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.compare.in1*/
                        connect u -> compare_24.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.compare.in2*/
                        connect constant_33.out1 -> compare_24.in2;

                      }
                      component /*instance*/ compareToZero4_5 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_25 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_34 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.yOut1*/
                        connect compare_25.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.compare.in1*/
                        connect u -> compare_25.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.compare.in2*/
                        connect constant_34.out1 -> compare_25.in2;

                      }
                      component /*instance*/ constant_35 {
                        port
                          out Double out1;
                      }
                      component /*instance*/ constant1_7 {
                        port
                          out Boolean out1;
                      }
                      component /*instance*/ logicalOperator_6 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          in Boolean in3,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                        <<effector>> connect in3 -> out1;
                      }
                      component /*instance*/ logicalOperator1_6 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ logicalOperator2_5 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ memory_5 {
                        port
                          in Boolean in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ memory1_5 {
                        port
                          in Double in1,
                          out Double out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ memory2_5 {
                        port
                          in Boolean in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ switchBlock_9 {
                        port
                          in Boolean condition,
                          in Boolean elseIn,
                          in Boolean ifIn,
                          out Boolean out1;
                        <<effector>> connect ifIn -> out1;
                        <<effector>> connect condition -> out1;
                        <<effector>> connect elseIn -> out1;
                      }
                      component /*instance*/ condition_12 {
                        port
                          in Double in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1*/
                      connect switchBlock_9.out1 -> out1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator.in3*/
                      connect compareToZero4_5.y -> logicalOperator_6.in3;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero4.uIn1*/
                      connect memory2_5.out1 -> compareToZero4_5.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator2.in1*/
                      connect logicalOperator_6.out1 -> logicalOperator2_5.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.switchBlock.elseIn*/
                      connect constant1_7.out1 -> switchBlock_9.elseIn;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory1.in1*/
                      connect constant_35.out1 -> memory1_5.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.switchBlock.condition*/
                      connect condition_12.out1 -> switchBlock_9.condition;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.switchBlock.ifIn*/
                      connect logicalOperator2_5.out1 -> switchBlock_9.ifIn;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory2.in1*/
                      connect logicalOperator2_5.out1 -> memory2_5.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator2.in2*/
                      connect logicalOperator1_6.out1 -> logicalOperator2_5.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator1.in2*/
                      connect compareToZero3_5.y -> logicalOperator1_6.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator1.in1*/
                      connect compareToZero2_5.y -> logicalOperator1_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator.in2*/
                      connect compareToZero1_5.y -> logicalOperator_6.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.logicalOperator.in1*/
                      connect compareToZero_5.y -> logicalOperator_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero.uIn1*/
                      connect memory_5.out1 -> compareToZero_5.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero2.uIn1*/
                      connect memory_5.out1 -> compareToZero2_5.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory.in1*/
                      connect in1 -> memory_5.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero1.uIn1*/
                      connect in1 -> compareToZero1_5.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.compareToZero3.uIn1*/
                      connect in1 -> compareToZero3_5.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.condition.in1*/
                      connect memory1_5.out1 -> condition_12.in1;

                    }
                    component /*instance*/ risingEdgeDetector1_3 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      component /*instance*/ compareToZero_6 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_26 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_36 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.yOut1*/
                        connect compare_26.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.compare.in1*/
                        connect u -> compare_26.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.compare.in2*/
                        connect constant_36.out1 -> compare_26.in2;

                      }
                      component /*instance*/ compareToZero1_6 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_27 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_37 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.yOut1*/
                        connect compare_27.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.compare.in1*/
                        connect u -> compare_27.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.compare.in2*/
                        connect constant_37.out1 -> compare_27.in2;

                      }
                      component /*instance*/ compareToZero2_6 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_28 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_38 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.yOut1*/
                        connect compare_28.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.compare.in1*/
                        connect u -> compare_28.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.compare.in2*/
                        connect constant_38.out1 -> compare_28.in2;

                      }
                      component /*instance*/ compareToZero3_6 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_29 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_39 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.yOut1*/
                        connect compare_29.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.compare.in1*/
                        connect u -> compare_29.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.compare.in2*/
                        connect constant_39.out1 -> compare_29.in2;

                      }
                      component /*instance*/ compareToZero4_6 {
                        port
                          in Boolean u,
                          out Boolean y;
                        component /*instance*/ compare_30 {
                          port
                            in Boolean in1,
                            in Double in2,
                            out Boolean out1;
                          <<effector>> connect in1 -> out1;
                          <<effector>> connect in2 -> out1;
                        }
                        component /*instance*/ constant_40 {
                          port
                            out Double out1;
                        }
                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.yOut1*/
                        connect compare_30.out1 -> y;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.compare.in1*/
                        connect u -> compare_30.in1;

                        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.compare.in2*/
                        connect constant_40.out1 -> compare_30.in2;

                      }
                      component /*instance*/ constant_41 {
                        port
                          out Double out1;
                      }
                      component /*instance*/ constant1_8 {
                        port
                          out Boolean out1;
                      }
                      component /*instance*/ logicalOperator_7 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          in Boolean in3,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                        <<effector>> connect in3 -> out1;
                      }
                      component /*instance*/ logicalOperator1_7 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ logicalOperator2_6 {
                        port
                          in Boolean in1,
                          in Boolean in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ memory_6 {
                        port
                          in Boolean in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ memory1_6 {
                        port
                          in Double in1,
                          out Double out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ memory2_6 {
                        port
                          in Boolean in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      component /*instance*/ switchBlock_10 {
                        port
                          in Boolean condition,
                          in Boolean elseIn,
                          in Boolean ifIn,
                          out Boolean out1;
                        <<effector>> connect ifIn -> out1;
                        <<effector>> connect condition -> out1;
                        <<effector>> connect elseIn -> out1;
                      }
                      component /*instance*/ condition_13 {
                        port
                          in Double in1,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1*/
                      connect switchBlock_10.out1 -> out1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator.in3*/
                      connect compareToZero4_6.y -> logicalOperator_7.in3;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero4.uIn1*/
                      connect memory2_6.out1 -> compareToZero4_6.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator2.in1*/
                      connect logicalOperator_7.out1 -> logicalOperator2_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.switchBlock.elseIn*/
                      connect constant1_8.out1 -> switchBlock_10.elseIn;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory1.in1*/
                      connect constant_41.out1 -> memory1_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.switchBlock.condition*/
                      connect condition_13.out1 -> switchBlock_10.condition;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.switchBlock.ifIn*/
                      connect logicalOperator2_6.out1 -> switchBlock_10.ifIn;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory2.in1*/
                      connect logicalOperator2_6.out1 -> memory2_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator2.in2*/
                      connect logicalOperator1_7.out1 -> logicalOperator2_6.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator1.in2*/
                      connect compareToZero3_6.y -> logicalOperator1_7.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator1.in1*/
                      connect compareToZero2_6.y -> logicalOperator1_7.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator.in2*/
                      connect compareToZero1_6.y -> logicalOperator_7.in2;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.logicalOperator.in1*/
                      connect compareToZero_6.y -> logicalOperator_7.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero.uIn1*/
                      connect memory_6.out1 -> compareToZero_6.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero2.uIn1*/
                      connect memory_6.out1 -> compareToZero2_6.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory.in1*/
                      connect in1 -> memory_6.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero1.uIn1*/
                      connect in1 -> compareToZero1_6.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.compareToZero3.uIn1*/
                      connect in1 -> compareToZero3_6.u;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.condition.in1*/
                      connect memory1_6.out1 -> condition_13.in1;

                    }
                    component /*instance*/ switchBlock_11 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_14 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock1_5 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition1_6 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock2_5 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition2_5 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock3_4 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition3_4 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock4_2 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition4_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock5_2 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition5_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock6_2 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition6_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock7_2 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition7_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock8_2 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition8_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock9_1 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition9_1 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out1*/
                    connect switchBlock_11.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out2*/
                    connect switchBlock2_5.out1 -> out2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out3*/
                    connect switchBlock4_2.out1 -> out3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out4*/
                    connect switchBlock6_2.out1 -> out4;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out5*/
                    connect switchBlock8_2.out1 -> out5;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.v_CCSetValueIn1*/
                    connect in1 -> v_SetValuePlus.v_CCSetValue;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.v_CCSetValueIn1*/
                    connect in2 -> v_SetValueMinus.v_CCSetValue;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.cCUp_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.in1In1*/
                    connect cCUp_b -> risingEdgeDetector_3.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.cCDown_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.in1In1*/
                    connect cCDown_b -> risingEdgeDetector1_3.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition.in1*/
                    connect risingEdgeDetector_3.out1 -> condition_14.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock.ifIn*/
                    connect v_SetValuePlus.out1 -> switchBlock_11.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock.condition*/
                    connect condition_14.out1 -> switchBlock_11.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition1.in1*/
                    connect risingEdgeDetector1_3.out1 -> condition1_6.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock1.ifIn*/
                    connect v_SetValueMinus.out1 -> switchBlock1_5.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock1.condition*/
                    connect condition1_6.out1 -> switchBlock1_5.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock1.elseIn*/
                    connect in3 -> switchBlock1_5.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock.elseIn*/
                    connect switchBlock1_5.out1 -> switchBlock_11.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition2.in1*/
                    connect risingEdgeDetector_3.out1 -> condition2_5.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock2.ifIn*/
                    connect v_SetValuePlus.out2 -> switchBlock2_5.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock2.condition*/
                    connect condition2_5.out1 -> switchBlock2_5.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition3.in1*/
                    connect risingEdgeDetector1_3.out1 -> condition3_4.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock3.ifIn*/
                    connect v_SetValueMinus.out2 -> switchBlock3_4.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock3.condition*/
                    connect condition3_4.out1 -> switchBlock3_4.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock3.elseIn*/
                    connect in4 -> switchBlock3_4.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock2.elseIn*/
                    connect switchBlock3_4.out1 -> switchBlock2_5.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition4.in1*/
                    connect risingEdgeDetector_3.out1 -> condition4_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock4.ifIn*/
                    connect v_SetValuePlus.out3 -> switchBlock4_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock4.condition*/
                    connect condition4_2.out1 -> switchBlock4_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition5.in1*/
                    connect risingEdgeDetector1_3.out1 -> condition5_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock5.ifIn*/
                    connect v_SetValueMinus.out3 -> switchBlock5_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock5.condition*/
                    connect condition5_2.out1 -> switchBlock5_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock5.elseIn*/
                    connect in5 -> switchBlock5_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock4.elseIn*/
                    connect switchBlock5_2.out1 -> switchBlock4_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition6.in1*/
                    connect risingEdgeDetector_3.out1 -> condition6_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock6.ifIn*/
                    connect v_SetValuePlus.out4 -> switchBlock6_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock6.condition*/
                    connect condition6_2.out1 -> switchBlock6_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition7.in1*/
                    connect risingEdgeDetector1_3.out1 -> condition7_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock7.ifIn*/
                    connect v_SetValueMinus.out4 -> switchBlock7_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock7.condition*/
                    connect condition7_2.out1 -> switchBlock7_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock7.elseIn*/
                    connect in6 -> switchBlock7_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock6.elseIn*/
                    connect switchBlock7_2.out1 -> switchBlock6_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition8.in1*/
                    connect risingEdgeDetector_3.out1 -> condition8_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValuePlus.out5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock8.ifIn*/
                    connect v_SetValuePlus.out5 -> switchBlock8_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock8.condition*/
                    connect condition8_2.out1 -> switchBlock8_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition9.in1*/
                    connect risingEdgeDetector1_3.out1 -> condition9_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.v_SetValueMinus.out5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock9.ifIn*/
                    connect v_SetValueMinus.out5 -> switchBlock9_1.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.condition9.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock9.condition*/
                    connect condition9_1.out1 -> switchBlock9_1.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock9.elseIn*/
                    connect in7 -> switchBlock9_1.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock9.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.switchBlock8.elseIn*/
                    connect switchBlock9_1.out1 -> switchBlock8_2.elseIn;

                  }
                  component /*instance*/ cC_InitialSetValue {
                    port
                      in Double in1,
                      in Double in2,
                      in Double in3,
                      out Double out1,
                      out Double out2,
                      in Double v_Vehicle_kmh;
                    component /*instance*/ cC_NoInitialSetValue {
                      port
                        out Double out1,
                        out Double out2,
                        in Double v_Vehicle_kmh;
                      component /*instance*/ vERSION_INFO_16 {
                        component /*instance*/ copyright_16 {
                        }
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.out1*/
                      connect v_Vehicle_kmh -> out1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.out2*/
                      connect v_Vehicle_kmh -> out2;

                    }
                    component /*instance*/ constant_42 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ relOp_1 {
                      port
                        in Double in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ vERSION_INFO_17 {
                      component /*instance*/ copyright_17 {
                      }
                    }
                    component /*instance*/ switchBlock_12 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_15 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock1_6 {
                      port
                        in Boolean condition,
                        in Double elseIn,
                        in Double ifIn,
                        out Double out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition1_7 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.out1*/
                    connect switchBlock_12.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.out2*/
                    connect switchBlock1_6.out1 -> out2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.relOp.in1*/
                    connect in1 -> relOp_1.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.relOp.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.condition.in1*/
                    connect relOp_1.out1 -> condition_15.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock.ifIn*/
                    connect cC_NoInitialSetValue.out1 -> switchBlock_12.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock.condition*/
                    connect condition_15.out1 -> switchBlock_12.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock.elseIn*/
                    connect in2 -> switchBlock_12.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.relOp.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.condition1.in1*/
                    connect relOp_1.out1 -> condition1_7.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock1.ifIn*/
                    connect cC_NoInitialSetValue.out2 -> switchBlock1_6.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock1.condition*/
                    connect condition1_7.out1 -> switchBlock1_6.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.switchBlock1.elseIn*/
                    connect in3 -> switchBlock1_6.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.relOp.in2*/
                    connect constant_42.out1 -> relOp_1.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.cC_NoInitialSetValue.v_Vehicle_kmhIn1*/
                    connect v_Vehicle_kmh -> cC_NoInitialSetValue.v_Vehicle_kmh;

                  }
                  component /*instance*/ cC_OnSet_SetValue {
                    port
                      out Double out1,
                      out Double out2,
                      in Double v_Vehicle_kmh;
                    component /*instance*/ vERSION_INFO_18 {
                      component /*instance*/ copyright_18 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.out1*/
                    connect v_Vehicle_kmh -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.out2*/
                    connect v_Vehicle_kmh -> out2;

                  }
                  component /*instance*/ cC_StartUpSetValue {
                    port
                      out Double out1,
                      out Double out2;
                    component /*instance*/ constant_43 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ vERSION_INFO_19 {
                      component /*instance*/ copyright_19 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.out1*/
                    connect constant_43.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.out2*/
                    connect constant_43.out1 -> out2;

                  }
                  component /*instance*/ compareToZero_7 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_31 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_44 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.yOut1*/
                    connect compare_31.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.compare.in1*/
                    connect u -> compare_31.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.compare.in2*/
                    connect constant_44.out1 -> compare_31.in2;

                  }
                  component /*instance*/ compareToZero1_7 {
                    port
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ compare_32 {
                      port
                        in Boolean in1,
                        in Double in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ constant_45 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.yOut1*/
                    connect compare_32.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.compare.in1*/
                    connect u -> compare_32.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.compare.in2*/
                    connect constant_45.out1 -> compare_32.in2;

                  }
                  component /*instance*/ constant_46 {
                    port
                      out Double out1;
                  }
                  component /*instance*/ edgeRising_2 {
                    port
                      in Boolean iV,
                      in Boolean r,
                      in Boolean u,
                      out Boolean y;
                    component /*instance*/ logOp_A_3 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logOp_N_4 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory_U_3 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switch_R_4 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_16 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.logOp_A.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.yOut1*/
                    connect logOp_A_3.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.switch_R.condition*/
                    connect condition_16.out1 -> switch_R_4.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.rIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.condition.in1*/
                    connect r -> condition_16.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.logOp_N.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.logOp_A.in2*/
                    connect logOp_N_4.out1 -> logOp_A_3.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.logOp_A.in1*/
                    connect u -> logOp_A_3.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.memory_U.in1*/
                    connect u -> memory_U_3.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.logOp_N.in1*/
                    connect switch_R_4.out1 -> logOp_N_4.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.memory_U.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.switch_R.elseIn*/
                    connect memory_U_3.out1 -> switch_R_4.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.iVIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.switch_R.ifIn*/
                    connect iV -> switch_R_4.ifIn;

                  }
                  component /*instance*/ falseBlock_3 {
                    port
                      out Boolean y;
                    component /*instance*/ zero_4 {
                      port
                        out Boolean out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.falseBlock.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.falseBlock.yOut1*/
                    connect zero_4.out1 -> y;

                  }
                  component /*instance*/ logicalOperator_8 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ logicalOperator1_8 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ logicalOperator2_7 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      in Boolean in3,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                    <<effector>> connect in3 -> out1;
                  }
                  component /*instance*/ logicalOperator3_2 {
                    port
                      in Boolean in1,
                      in Boolean in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ logicalOperator4_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ logicalOperator5_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ logicalOperator6 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ rSFlipFlop_2 {
                    port
                      out Boolean nOT_Q,
                      out Boolean q,
                      in Boolean r,
                      in Boolean s;
                    component /*instance*/ logOp_N_5 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory_Q_2 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ one_S_2 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ switch_R_5 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ switch_S_2 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ zero_R_2 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ condition_17 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ condition1_8 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.qOut1*/
                    connect switch_R_5.out1 -> q;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.logOp_N.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.nOT_QOut2*/
                    connect logOp_N_5.out1 -> nOT_Q;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_S.condition*/
                    connect condition_17.out1 -> switch_S_2.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.condition*/
                    connect condition1_8.out1 -> switch_R_5.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.sIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.condition.in1*/
                    connect s -> condition_17.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.rIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.condition1.in1*/
                    connect r -> condition1_8.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.memory_Q.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_S.elseIn*/
                    connect memory_Q_2.out1 -> switch_S_2.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_S.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.elseIn*/
                    connect switch_S_2.out1 -> switch_R_5.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.one_S.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_S.ifIn*/
                    connect one_S_2.out1 -> switch_S_2.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.memory_Q.in1*/
                    connect switch_R_5.out1 -> memory_Q_2.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.logOp_N.in1*/
                    connect switch_R_5.out1 -> logOp_N_5.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.zero_R.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.switch_R.ifIn*/
                    connect zero_R_2.out1 -> switch_R_5.ifIn;

                  }
                  component /*instance*/ relOp_2 {
                    port
                      in Double in1,
                      in Double in2,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                    <<effector>> connect in2 -> out1;
                  }
                  component /*instance*/ sysInit_2 {
                    port
                      out Double y;
                    component /*instance*/ memory_Init_2 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ zero_Init_2 {
                      port
                        out Double out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.sysInit.memory_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.sysInit.yOut1*/
                    connect memory_Init_2.out1 -> y;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.sysInit.zero_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.sysInit.memory_Init.in1*/
                    connect zero_Init_2.out1 -> memory_Init_2.in1;

                  }
                  component /*instance*/ terminator_2 {
                    port
                      in Boolean in1;
                  }
                  component /*instance*/ vERSION_INFO_20 {
                    component /*instance*/ copyright_20 {
                    }
                  }
                  component /*instance*/ risingEdgeDetector_4 {
                    port
                      in Double in1,
                      out Boolean out1;
                    component /*instance*/ compareToZero_8 {
                      port
                        in Double u,
                        out Boolean y;
                      component /*instance*/ compare_33 {
                        port
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_47 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.yOut1*/
                      connect compare_33.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.compare.in1*/
                      connect u -> compare_33.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.compare.in2*/
                      connect constant_47.out1 -> compare_33.in2;

                    }
                    component /*instance*/ compareToZero1_8 {
                      port
                        in Double u,
                        out Boolean y;
                      component /*instance*/ compare_34 {
                        port
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_48 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.yOut1*/
                      connect compare_34.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.compare.in1*/
                      connect u -> compare_34.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.compare.in2*/
                      connect constant_48.out1 -> compare_34.in2;

                    }
                    component /*instance*/ compareToZero2_7 {
                      port
                        in Double u,
                        out Boolean y;
                      component /*instance*/ compare_35 {
                        port
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_49 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.yOut1*/
                      connect compare_35.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.compare.in1*/
                      connect u -> compare_35.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.compare.in2*/
                      connect constant_49.out1 -> compare_35.in2;

                    }
                    component /*instance*/ compareToZero3_7 {
                      port
                        in Double u,
                        out Boolean y;
                      component /*instance*/ compare_36 {
                        port
                          in Double in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_50 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.yOut1*/
                      connect compare_36.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.compare.in1*/
                      connect u -> compare_36.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.compare.in2*/
                      connect constant_50.out1 -> compare_36.in2;

                    }
                    component /*instance*/ compareToZero4_7 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_37 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_51 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.yOut1*/
                      connect compare_37.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.compare.in1*/
                      connect u -> compare_37.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.compare.in2*/
                      connect constant_51.out1 -> compare_37.in2;

                    }
                    component /*instance*/ constant_52 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ constant1_9 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ logicalOperator_9 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                      <<effector>> connect in3 -> out1;
                    }
                    component /*instance*/ logicalOperator1_9 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logicalOperator2_8 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ memory_7 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory1_7 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory2_7 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock_13 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_18 {
                      port
                        in Double in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.out1Out1*/
                    connect switchBlock_13.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator.in3*/
                    connect compareToZero4_7.y -> logicalOperator_9.in3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero4.uIn1*/
                    connect memory2_7.out1 -> compareToZero4_7.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator2.in1*/
                    connect logicalOperator_9.out1 -> logicalOperator2_8.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.switchBlock.elseIn*/
                    connect constant1_9.out1 -> switchBlock_13.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory1.in1*/
                    connect constant_52.out1 -> memory1_7.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.switchBlock.condition*/
                    connect condition_18.out1 -> switchBlock_13.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.switchBlock.ifIn*/
                    connect logicalOperator2_8.out1 -> switchBlock_13.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory2.in1*/
                    connect logicalOperator2_8.out1 -> memory2_7.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator2.in2*/
                    connect logicalOperator1_9.out1 -> logicalOperator2_8.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator1.in2*/
                    connect compareToZero3_7.y -> logicalOperator1_9.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator1.in1*/
                    connect compareToZero2_7.y -> logicalOperator1_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator.in2*/
                    connect compareToZero1_8.y -> logicalOperator_9.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.logicalOperator.in1*/
                    connect compareToZero_8.y -> logicalOperator_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero.uIn1*/
                    connect memory_7.out1 -> compareToZero_8.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero2.uIn1*/
                    connect memory_7.out1 -> compareToZero2_7.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory.in1*/
                    connect in1 -> memory_7.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero1.uIn1*/
                    connect in1 -> compareToZero1_8.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.compareToZero3.uIn1*/
                    connect in1 -> compareToZero3_7.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.condition.in1*/
                    connect memory1_7.out1 -> condition_18.in1;

                  }
                  component /*instance*/ risingEdgeDetector1_4 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    component /*instance*/ compareToZero_9 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_38 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_53 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.yOut1*/
                      connect compare_38.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.compare.in1*/
                      connect u -> compare_38.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.compare.in2*/
                      connect constant_53.out1 -> compare_38.in2;

                    }
                    component /*instance*/ compareToZero1_9 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_39 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_54 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.yOut1*/
                      connect compare_39.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.compare.in1*/
                      connect u -> compare_39.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.compare.in2*/
                      connect constant_54.out1 -> compare_39.in2;

                    }
                    component /*instance*/ compareToZero2_8 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_40 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_55 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.yOut1*/
                      connect compare_40.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.compare.in1*/
                      connect u -> compare_40.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.compare.in2*/
                      connect constant_55.out1 -> compare_40.in2;

                    }
                    component /*instance*/ compareToZero3_8 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_41 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_56 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.yOut1*/
                      connect compare_41.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.compare.in1*/
                      connect u -> compare_41.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.compare.in2*/
                      connect constant_56.out1 -> compare_41.in2;

                    }
                    component /*instance*/ compareToZero4_8 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_42 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_57 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.yOut1*/
                      connect compare_42.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.compare.in1*/
                      connect u -> compare_42.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.compare.in2*/
                      connect constant_57.out1 -> compare_42.in2;

                    }
                    component /*instance*/ constant_58 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ constant1_10 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ logicalOperator_10 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                      <<effector>> connect in3 -> out1;
                    }
                    component /*instance*/ logicalOperator1_10 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logicalOperator2_9 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ memory_8 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory1_8 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory2_8 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock_14 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_19 {
                      port
                        in Double in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.out1Out1*/
                    connect switchBlock_14.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator.in3*/
                    connect compareToZero4_8.y -> logicalOperator_10.in3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero4.uIn1*/
                    connect memory2_8.out1 -> compareToZero4_8.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator2.in1*/
                    connect logicalOperator_10.out1 -> logicalOperator2_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.switchBlock.elseIn*/
                    connect constant1_10.out1 -> switchBlock_14.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory1.in1*/
                    connect constant_58.out1 -> memory1_8.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.switchBlock.condition*/
                    connect condition_19.out1 -> switchBlock_14.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.switchBlock.ifIn*/
                    connect logicalOperator2_9.out1 -> switchBlock_14.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory2.in1*/
                    connect logicalOperator2_9.out1 -> memory2_8.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator2.in2*/
                    connect logicalOperator1_10.out1 -> logicalOperator2_9.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator1.in2*/
                    connect compareToZero3_8.y -> logicalOperator1_10.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator1.in1*/
                    connect compareToZero2_8.y -> logicalOperator1_10.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator.in2*/
                    connect compareToZero1_9.y -> logicalOperator_10.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.logicalOperator.in1*/
                    connect compareToZero_9.y -> logicalOperator_10.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero.uIn1*/
                    connect memory_8.out1 -> compareToZero_9.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero2.uIn1*/
                    connect memory_8.out1 -> compareToZero2_8.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory.in1*/
                    connect in1 -> memory_8.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero1.uIn1*/
                    connect in1 -> compareToZero1_9.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.compareToZero3.uIn1*/
                    connect in1 -> compareToZero3_8.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.condition.in1*/
                    connect memory1_8.out1 -> condition_19.in1;

                  }
                  component /*instance*/ risingEdgeDetector2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    component /*instance*/ compareToZero_10 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_43 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_59 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.yOut1*/
                      connect compare_43.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.compare.in1*/
                      connect u -> compare_43.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.compare.in2*/
                      connect constant_59.out1 -> compare_43.in2;

                    }
                    component /*instance*/ compareToZero1_10 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_44 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_60 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.yOut1*/
                      connect compare_44.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.compare.in1*/
                      connect u -> compare_44.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.compare.in2*/
                      connect constant_60.out1 -> compare_44.in2;

                    }
                    component /*instance*/ compareToZero2_9 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_45 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_61 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.yOut1*/
                      connect compare_45.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.compare.in1*/
                      connect u -> compare_45.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.compare.in2*/
                      connect constant_61.out1 -> compare_45.in2;

                    }
                    component /*instance*/ compareToZero3_9 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_46 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_62 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.yOut1*/
                      connect compare_46.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.compare.in1*/
                      connect u -> compare_46.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.compare.in2*/
                      connect constant_62.out1 -> compare_46.in2;

                    }
                    component /*instance*/ compareToZero4_9 {
                      port
                        in Boolean u,
                        out Boolean y;
                      component /*instance*/ compare_47 {
                        port
                          in Boolean in1,
                          in Double in2,
                          out Boolean out1;
                        <<effector>> connect in1 -> out1;
                        <<effector>> connect in2 -> out1;
                      }
                      component /*instance*/ constant_63 {
                        port
                          out Double out1;
                      }
                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.compare.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.yOut1*/
                      connect compare_47.out1 -> y;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.compare.in1*/
                      connect u -> compare_47.in1;

                      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.compare.in2*/
                      connect constant_63.out1 -> compare_47.in2;

                    }
                    component /*instance*/ constant_64 {
                      port
                        out Double out1;
                    }
                    component /*instance*/ constant1_11 {
                      port
                        out Boolean out1;
                    }
                    component /*instance*/ logicalOperator_11 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        in Boolean in3,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                      <<effector>> connect in3 -> out1;
                    }
                    component /*instance*/ logicalOperator1_11 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ logicalOperator2_10 {
                      port
                        in Boolean in1,
                        in Boolean in2,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                      <<effector>> connect in2 -> out1;
                    }
                    component /*instance*/ memory_9 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory1_9 {
                      port
                        in Double in1,
                        out Double out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ memory2_9 {
                      port
                        in Boolean in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    component /*instance*/ switchBlock_15 {
                      port
                        in Boolean condition,
                        in Boolean elseIn,
                        in Boolean ifIn,
                        out Boolean out1;
                      <<effector>> connect ifIn -> out1;
                      <<effector>> connect condition -> out1;
                      <<effector>> connect elseIn -> out1;
                    }
                    component /*instance*/ condition_20 {
                      port
                        in Double in1,
                        out Boolean out1;
                      <<effector>> connect in1 -> out1;
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.out1Out1*/
                    connect switchBlock_15.out1 -> out1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator.in3*/
                    connect compareToZero4_9.y -> logicalOperator_11.in3;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero4.uIn1*/
                    connect memory2_9.out1 -> compareToZero4_9.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator2.in1*/
                    connect logicalOperator_11.out1 -> logicalOperator2_10.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.switchBlock.elseIn*/
                    connect constant1_11.out1 -> switchBlock_15.elseIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory1.in1*/
                    connect constant_64.out1 -> memory1_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.switchBlock.condition*/
                    connect condition_20.out1 -> switchBlock_15.condition;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.switchBlock.ifIn*/
                    connect logicalOperator2_10.out1 -> switchBlock_15.ifIn;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory2.in1*/
                    connect logicalOperator2_10.out1 -> memory2_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator2.in2*/
                    connect logicalOperator1_11.out1 -> logicalOperator2_10.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator1.in2*/
                    connect compareToZero3_9.y -> logicalOperator1_11.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator1.in1*/
                    connect compareToZero2_9.y -> logicalOperator1_11.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator.in2*/
                    connect compareToZero1_10.y -> logicalOperator_11.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.logicalOperator.in1*/
                    connect compareToZero_10.y -> logicalOperator_11.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero.uIn1*/
                    connect memory_9.out1 -> compareToZero_10.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero2.uIn1*/
                    connect memory_9.out1 -> compareToZero2_9.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory.in1*/
                    connect in1 -> memory_9.in1;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero1.uIn1*/
                    connect in1 -> compareToZero1_10.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.compareToZero3.uIn1*/
                    connect in1 -> compareToZero3_9.u;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.condition.in1*/
                    connect memory1_9.out1 -> condition_20.in1;

                  }
                  component /*instance*/ switchBlock_16 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_21 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock1_7 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition1_9 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock2_6 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition2_6 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock3_5 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition3_5 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock4_3 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition4_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock5_3 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition5_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock6_3 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition6_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock7_3 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition7_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock8_3 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition8_3 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock9_2 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition9_2 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock10 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition10 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.out1*/
                  connect switchBlock1_7.out1 -> out1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.out2*/
                  connect switchBlock6_3.out1 -> out2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.sysInit.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.in1In1*/
                  connect sysInit_2.y -> risingEdgeDetector_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.in1In1*/
                  connect logicalOperator2_7.out1 -> risingEdgeDetector1_4.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.in1In1*/
                  connect logicalOperator_8.out1 -> risingEdgeDetector2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in1*/
                  connect in1 -> cC_ChangeSetValue.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in2*/
                  connect in2 -> cC_ChangeSetValue.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in3*/
                  connect in3 -> cC_ChangeSetValue.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition.in1*/
                  connect cC_enabled_b -> condition_21.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock.ifIn*/
                  connect cC_ChangeSetValue.out1 -> switchBlock_16.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock.condition*/
                  connect condition_21.out1 -> switchBlock_16.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock.elseIn*/
                  connect in4 -> switchBlock_16.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in1*/
                  connect switchBlock_16.out1 -> cC_InitialSetValue.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition1.in1*/
                  connect risingEdgeDetector_4.out1 -> condition1_9.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock1.ifIn*/
                  connect cC_StartUpSetValue.out1 -> switchBlock1_7.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock1.condition*/
                  connect condition1_9.out1 -> switchBlock1_7.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition2.in1*/
                  connect risingEdgeDetector1_4.out1 -> condition2_6.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock2.ifIn*/
                  connect cC_OnSet_SetValue.out1 -> switchBlock2_6.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock2.condition*/
                  connect condition2_6.out1 -> switchBlock2_6.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in4*/
                  connect in5 -> cC_ChangeSetValue.in4;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition3.in1*/
                  connect cC_enabled_b -> condition3_5.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock3.ifIn*/
                  connect cC_ChangeSetValue.out2 -> switchBlock3_5.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock3.condition*/
                  connect condition3_5.out1 -> switchBlock3_5.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock3.elseIn*/
                  connect in6 -> switchBlock3_5.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in2*/
                  connect switchBlock3_5.out1 -> cC_InitialSetValue.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition4.in1*/
                  connect risingEdgeDetector2.out1 -> condition4_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock4.ifIn*/
                  connect cC_InitialSetValue.out1 -> switchBlock4_3.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock4.condition*/
                  connect condition4_3.out1 -> switchBlock4_3.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in5*/
                  connect in7 -> cC_ChangeSetValue.in5;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition5.in1*/
                  connect cC_enabled_b -> condition5_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock5.ifIn*/
                  connect cC_ChangeSetValue.out3 -> switchBlock5_3.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock5.condition*/
                  connect condition5_3.out1 -> switchBlock5_3.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock5.elseIn*/
                  connect in8 -> switchBlock5_3.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock4.elseIn*/
                  connect switchBlock5_3.out1 -> switchBlock4_3.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock2.elseIn*/
                  connect switchBlock4_3.out1 -> switchBlock2_6.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock1.elseIn*/
                  connect switchBlock2_6.out1 -> switchBlock1_7.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition6.in1*/
                  connect risingEdgeDetector_4.out1 -> condition6_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_StartUpSetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock6.ifIn*/
                  connect cC_StartUpSetValue.out2 -> switchBlock6_3.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock6.condition*/
                  connect condition6_3.out1 -> switchBlock6_3.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector1.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition7.in1*/
                  connect risingEdgeDetector1_4.out1 -> condition7_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock7.ifIn*/
                  connect cC_OnSet_SetValue.out2 -> switchBlock7_3.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock7.condition*/
                  connect condition7_3.out1 -> switchBlock7_3.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in9 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in6*/
                  connect in9 -> cC_ChangeSetValue.in6;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition8.in1*/
                  connect cC_enabled_b -> condition8_3.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock8.ifIn*/
                  connect cC_ChangeSetValue.out4 -> switchBlock8_3.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock8.condition*/
                  connect condition8_3.out1 -> switchBlock8_3.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in10 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock8.elseIn*/
                  connect in10 -> switchBlock8_3.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock8.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.in3*/
                  connect switchBlock8_3.out1 -> cC_InitialSetValue.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.risingEdgeDetector2.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition9.in1*/
                  connect risingEdgeDetector2.out1 -> condition9_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock9.ifIn*/
                  connect cC_InitialSetValue.out2 -> switchBlock9_2.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition9.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock9.condition*/
                  connect condition9_2.out1 -> switchBlock9_2.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in11 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.in7*/
                  connect in11 -> cC_ChangeSetValue.in7;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition10.in1*/
                  connect cC_enabled_b -> condition10.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.out5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock10.ifIn*/
                  connect cC_ChangeSetValue.out5 -> switchBlock10.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.condition10.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock10.condition*/
                  connect condition10.out1 -> switchBlock10.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in12 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock10.elseIn*/
                  connect in12 -> switchBlock10.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock10.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock9.elseIn*/
                  connect switchBlock10.out1 -> switchBlock9_2.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock9.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock7.elseIn*/
                  connect switchBlock9_2.out1 -> switchBlock7_3.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock7.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.switchBlock6.elseIn*/
                  connect switchBlock7_3.out1 -> switchBlock6_3.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator3.in1*/
                  connect compareToZero_7.y -> logicalOperator3_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator3.in2*/
                  connect compareToZero1_7.y -> logicalOperator3_2.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverDown_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.cCDown_bIn2*/
                  connect leverDown_b -> cC_ChangeSetValue.cCDown_b;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverDown_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero1.uIn1*/
                  connect leverDown_b -> compareToZero1_7.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverUp_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_ChangeSetValue.cCUp_bIn1*/
                  connect leverUp_b -> cC_ChangeSetValue.cCUp_b;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverUp_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.compareToZero.uIn1*/
                  connect leverUp_b -> compareToZero_7.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.qOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator2.in3*/
                  connect rSFlipFlop_2.q -> logicalOperator2_7.in3;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.nOT_QOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.terminator.in1*/
                  connect rSFlipFlop_2.nOT_Q -> terminator_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator6.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.rIn2*/
                  connect logicalOperator6.out1 -> rSFlipFlop_2.r;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator2.in1*/
                  connect logicalOperator5_2.out1 -> logicalOperator2_7.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.limiter_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator5.in1*/
                  connect limiter_b -> logicalOperator5_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.rSFlipFlop.sIn1*/
                  connect edgeRising_2.y -> rSFlipFlop_2.s;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator2.in2*/
                  connect logicalOperator4_2.out1 -> logicalOperator2_7.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.rIn2*/
                  connect logicalOperator1_8.out1 -> edgeRising_2.r;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.falseBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.iVIn3*/
                  connect falseBlock_3.y -> edgeRising_2.iV;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.edgeRising.uIn1*/
                  connect logicalOperator3_2.out1 -> edgeRising_2.u;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator6.in1*/
                  connect cC_enabled_b -> logicalOperator6.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator1.in1*/
                  connect cC_enabled_b -> logicalOperator1_8.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator.in1*/
                  connect cruiseControl_b -> logicalOperator_8.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator4.in1*/
                  connect cruiseControl_b -> logicalOperator4_2.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.relOp.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.logicalOperator.in2*/
                  connect relOp_2.out1 -> logicalOperator_8.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.relOp.in2*/
                  connect constant_46.out1 -> relOp_2.in2;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_InitialSetValue.v_Vehicle_kmhIn1*/
                  connect v_Vehicle_kmh -> cC_InitialSetValue.v_Vehicle_kmh;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_OnSet_SetValue.v_Vehicle_kmhIn1*/
                  connect v_Vehicle_kmh -> cC_OnSet_SetValue.v_Vehicle_kmh;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.relOp.in1*/
                  connect v_Vehicle_kmh -> relOp_2.in1;

                }
                component /*instance*/ cC_enabled {
                  port
                    in Boolean cC_active_b,
                    in Double in1,
                    out Double v_CC_delta_kmh,
                    in Double v_Vehicle_kmh;
                  component /*instance*/ tempomat_Active {
                    port
                      in Double in1,
                      out Double v_CC_delta_kmh,
                      in Double v_Vehicle_kmh;
                    component /*instance*/ sum_5 {
                      port
                        in Double in1,
                        in Double in2,
                        out Double v_CC_delta_kmh;
                      <<effector>> connect in1 -> v_CC_delta_kmh;
                      <<effector>> connect in2 -> v_CC_delta_kmh;
                    }
                    component /*instance*/ vERSION_INFO_21 {
                      component /*instance*/ copyright_21 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.sum.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.v_CC_delta_kmhOut1*/
                    connect sum_5.v_CC_delta_kmh -> v_CC_delta_kmh;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.sum.in2*/
                    connect in1 -> sum_5.in2;

                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.v_Vehicle_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.sum.in1*/
                    connect v_Vehicle_kmh -> sum_5.in1;

                  }
                  component /*instance*/ tempomat_Deactive {
                    port
                      out Double v_CC_delta_kmh;
                    component /*instance*/ constant_65 {
                      port
                        out Double v_CC_delta_kmh;
                    }
                    component /*instance*/ vERSION_INFO_22 {
                      component /*instance*/ copyright_22 {
                      }
                    }
                    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Deactive.constant.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Deactive.v_CC_delta_kmhOut1*/
                    connect constant_65.v_CC_delta_kmh -> v_CC_delta_kmh;

                  }
                  component /*instance*/ vERSION_INFO_23 {
                    component /*instance*/ copyright_23 {
                    }
                  }
                  component /*instance*/ switchBlock_17 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition_22 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ switchBlock1_8 {
                    port
                      in Boolean condition,
                      in Double elseIn,
                      in Double ifIn,
                      out Double out1;
                    <<effector>> connect ifIn -> out1;
                    <<effector>> connect condition -> out1;
                    <<effector>> connect elseIn -> out1;
                  }
                  component /*instance*/ condition1_10 {
                    port
                      in Boolean in1,
                      out Boolean out1;
                    <<effector>> connect in1 -> out1;
                  }
                  component /*instance*/ unitDelay_2 {
                    port
                      in Double valueIn,
                      out Double valueOut;
                    <<effector>> connect valueIn -> valueOut;
                  }
                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.v_CC_delta_kmhOut1*/
                  connect switchBlock_17.out1 -> v_CC_delta_kmh;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.in1*/
                  connect in1 -> tempomat_Active.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.cC_active_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.condition.in1*/
                  connect cC_active_b -> condition_22.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock.condition*/
                  connect condition_22.out1 -> switchBlock_17.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock.ifIn*/
                  connect tempomat_Active.v_CC_delta_kmh -> switchBlock_17.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.cC_active_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.condition1.in1*/
                  connect cC_active_b -> condition1_10.in1;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock1.condition*/
                  connect condition1_10.out1 -> switchBlock1_8.condition;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Deactive.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock1.ifIn*/
                  connect tempomat_Deactive.v_CC_delta_kmh -> switchBlock1_8.ifIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock.elseIn*/
                  connect switchBlock1_8.out1 -> switchBlock_17.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.unitDelay.valueIn*/
                  connect switchBlock_17.out1 -> unitDelay_2.valueIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.switchBlock1.elseIn*/
                  connect unitDelay_2.valueOut -> switchBlock1_8.elseIn;

                  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.v_Vehicle_kmhIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.tempomat_Active.v_Vehicle_kmhIn1*/
                  connect v_Vehicle_kmh -> tempomat_Active.v_Vehicle_kmh;

                }
                component /*instance*/ vERSION_INFO_24 {
                  component /*instance*/ copyright_24 {
                  }
                }
                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_CC_delta_kmhOut1*/
                connect cC_enabled.v_CC_delta_kmh -> v_CC_delta_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_active_bOut2*/
                connect cC_On_Off.cC_active_b -> cC_active_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.out2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.out1*/
                connect cC_SetValue.out2 -> out1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in1*/
                connect in1 -> cC_SetValue.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in2*/
                connect in2 -> cC_SetValue.in2;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in3*/
                connect in3 -> cC_SetValue.in3;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in4*/
                connect in4 -> cC_SetValue.in4;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in5*/
                connect in5 -> cC_SetValue.in5;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in6*/
                connect in6 -> cC_SetValue.in6;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in7*/
                connect in7 -> cC_SetValue.in7;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in8*/
                connect in8 -> cC_SetValue.in8;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.in1*/
                connect cC_SetValue.out1 -> cC_enabled.in1;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in9 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in9*/
                connect in9 -> cC_SetValue.in9;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in10 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in10*/
                connect in10 -> cC_SetValue.in10;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in11 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in11*/
                connect in11 -> cC_SetValue.in11;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in12 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.in12*/
                connect in12 -> cC_SetValue.in12;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.limiter_bIn5*/
                connect limiter_b -> cC_On_Off.limiter_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.limiter_bIn1*/
                connect limiter_b -> cC_SetValue.limiter_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.cC_active_bIn1*/
                connect cC_On_Off.cC_active_b -> cC_enabled.cC_active_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cC_enabled_bIn2*/
                connect cC_On_Off.cC_active_b -> cC_SetValue.cC_enabled_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.brakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.brakeForce_pedal_pcIn2*/
                connect brakeForce_pedal_pc -> cC_On_Off.brakeForce_pedal_pc;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.parkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.parkingBrake_bIn1*/
                connect parkingBrake_b -> cC_On_Off.parkingBrake_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.cruiseControl_bIn3*/
                connect cruiseControl_b -> cC_SetValue.cruiseControl_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.cruiseControl_bIn3*/
                connect cruiseControl_b -> cC_On_Off.cruiseControl_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.v_Vehicle_kmhIn4*/
                connect v_Vehicle_kmh -> cC_On_Off.v_Vehicle_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_enabled.v_Vehicle_kmhIn2*/
                connect v_Vehicle_kmh -> cC_enabled.v_Vehicle_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.v_Vehicle_kmhIn4*/
                connect v_Vehicle_kmh -> cC_SetValue.v_Vehicle_kmh;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverUp_bIn5*/
                connect leverUp_b -> cC_SetValue.leverUp_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.leverUp_bIn6*/
                connect leverUp_b -> cC_On_Off.leverUp_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_SetValue.leverDown_bIn6*/
                connect leverDown_b -> cC_SetValue.leverDown_b;

                /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_On_Off.leverDown_bIn7*/
                connect leverDown_b -> cC_On_Off.leverDown_b;

              }
              component /*instance*/ vERSION_INFO_25 {
                component /*instance*/ copyright_25 {
                }
              }
              component /*instance*/ switchBlock_18 {
                port
                  in Boolean condition,
                  in Double elseIn,
                  in Double ifIn,
                  out Double out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ condition_23 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ condition1_11 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ switchBlock1_9 {
                port
                  in Boolean condition,
                  in Double elseIn,
                  in Double ifIn,
                  out Double out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ constant1_12 {
                port
                  out Double out1;
              }
              component /*instance*/ condition2_7 {
                port
                  in Double in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ switchBlock2_7 {
                port
                  in Boolean condition,
                  in Boolean elseIn,
                  in Boolean ifIn,
                  out Boolean out1;
                <<effector>> connect ifIn -> out1;
                <<effector>> connect condition -> out1;
                <<effector>> connect elseIn -> out1;
              }
              component /*instance*/ constant2_2 {
                port
                  out Boolean out1;
              }
              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.v_CC_delta_kmhOut1*/
              connect switchBlock1_9.out1 -> v_CC_delta_kmh;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.cC_active_bOut2*/
              connect switchBlock2_7.out1 -> cC_active_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.out1*/
              connect switchBlock_18.out1 -> out1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in1*/
              connect in1 -> tempomat_Function.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in2*/
              connect in2 -> tempomat_Function.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in3*/
              connect in3 -> tempomat_Function.in3;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in4*/
              connect in4 -> tempomat_Function.in4;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in5*/
              connect in5 -> tempomat_Function.in5;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in6*/
              connect in6 -> tempomat_Function.in6;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in7*/
              connect in7 -> tempomat_Function.in7;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in8*/
              connect in8 -> tempomat_Function.in8;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in9 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in9*/
              connect in9 -> tempomat_Function.in9;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in10 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in10*/
              connect in10 -> tempomat_Function.in10;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in11 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in11*/
              connect in11 -> tempomat_Function.in11;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in12 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.in12*/
              connect in12 -> tempomat_Function.in12;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition.in1*/
              connect constant_28.out1 -> condition_23.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock.ifIn*/
              connect tempomat_Function.out1 -> switchBlock_18.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock.condition*/
              connect condition_23.out1 -> switchBlock_18.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in13 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock.elseIn*/
              connect in13 -> switchBlock_18.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition1.in1*/
              connect constant_28.out1 -> condition1_11.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock1.condition*/
              connect condition1_11.out1 -> switchBlock1_9.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock1.ifIn*/
              connect tempomat_Function.v_CC_delta_kmh -> switchBlock1_9.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock1.elseIn*/
              connect constant1_12.out1 -> switchBlock1_9.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition2.in1*/
              connect constant_28.out1 -> condition2_7.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.condition2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock2.condition*/
              connect condition2_7.out1 -> switchBlock2_7.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cC_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock2.ifIn*/
              connect tempomat_Function.cC_active_b -> switchBlock2_7.ifIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.constant2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.switchBlock2.elseIn*/
              connect constant2_2.out1 -> switchBlock2_7.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverDown_bIn7*/
              connect leverDown_b -> tempomat_Function.leverDown_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.leverUp_bIn6*/
              connect leverUp_b -> tempomat_Function.leverUp_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.limiter_bIn5*/
              connect limiter_b -> tempomat_Function.limiter_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.v_Vehicle_kmhIn4*/
              connect v_Vehicle_kmh -> tempomat_Function.v_Vehicle_kmh;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.cruiseControl_bIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.cruiseControl_bIn3*/
              connect cruiseControl_b -> tempomat_Function.cruiseControl_b;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.brakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.brakeForce_pedal_pcIn2*/
              connect brakeForce_pedal_pc -> tempomat_Function.brakeForce_pedal_pc;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.parkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.tempomat_Function.parkingBrake_bIn1*/
              connect parkingBrake_b -> tempomat_Function.parkingBrake_b;

            }
            component /*instance*/ vERSION_INFO_26 {
              component /*instance*/ copyright_26 {
              }
            }
            component /*instance*/ velocityControl {
              port
                out Double acceleration_pc,
                in Double acceleration_pedal_pc,
                in Double v_CC_delta_kmh,
                in Double v_Vehicle_kmh,
                in Double vMax_kmh;
              component /*instance*/ constant1_13 {
                port
                  out Double out1;
              }
              component /*instance*/ constant2_3 {
                port
                  out Double out1;
              }
              component /*instance*/ logOp_3 {
                port
                  in Boolean in1,
                  in Boolean in2,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
                <<effector>> connect in2 -> out1;
              }
              component /*instance*/ lookUpTable {
                port
                  in Double in1,
                  out Double out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ minMax1 {
                port
                  in Double in1,
                  in Double in2,
                  out Double out1;
                <<effector>> connect in1 -> out1;
                <<effector>> connect in2 -> out1;
              }
              component /*instance*/ relOp_3 {
                port
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
                <<effector>> connect in2 -> out1;
              }
              component /*instance*/ relOp1_2 {
                port
                  in Double in1,
                  in Double in2,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
                <<effector>> connect in2 -> out1;
              }
              component /*instance*/ saturation_1 {
                port
                  in Double in1,
                  out Double out1;
                <<effector>> connect in1 -> out1;
              }
              component /*instance*/ switchBlock_19 {
                port
                  out Double acceleration_pc,
                  in Boolean condition,
                  in Double elseIn,
                  in Double ifIn;
                <<effector>> connect ifIn -> acceleration_pc;
                <<effector>> connect condition -> acceleration_pc;
                <<effector>> connect elseIn -> acceleration_pc;
              }
              component /*instance*/ vERSION_INFO_27 {
                component /*instance*/ copyright_27 {
                }
              }
              component /*instance*/ condition_24 {
                port
                  in Boolean in1,
                  out Boolean out1;
                <<effector>> connect in1 -> out1;
              }
              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.switchBlock.acceleration_pcOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.acceleration_pcOut1*/
              connect switchBlock_19.acceleration_pc -> acceleration_pc;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.switchBlock.condition*/
              connect condition_24.out1 -> switchBlock_19.condition;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.logOp.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.condition.in1*/
              connect logOp_3.out1 -> condition_24.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.saturation.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.lookUpTable.in1*/
              connect saturation_1.out1 -> lookUpTable.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.constant2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp1.in2*/
              connect constant2_3.out1 -> relOp1_2.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.minMax1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.switchBlock.elseIn*/
              connect minMax1.out1 -> switchBlock_19.elseIn;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.minMax1.in1*/
              connect acceleration_pedal_pc -> minMax1.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.vMax_kmhIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp.in2*/
              connect vMax_kmh -> relOp_3.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.vMax_kmhIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp1.in1*/
              connect vMax_kmh -> relOp1_2.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.v_Vehicle_kmhIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp.in1*/
              connect v_Vehicle_kmh -> relOp_3.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.lookUpTable.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.minMax1.in2*/
              connect lookUpTable.out1 -> minMax1.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.v_CC_delta_kmhIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.saturation.in1*/
              connect v_CC_delta_kmh -> saturation_1.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.logOp.in1*/
              connect relOp_3.out1 -> logOp_3.in1;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.relOp1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.logOp.in2*/
              connect relOp1_2.out1 -> logOp_3.in2;

              /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.switchBlock.ifIn*/
              connect constant1_13.out1 -> switchBlock_19.ifIn;

            }
            component /*instance*/ unitDelay_3 {
              port
                in Double valueIn,
                out Double valueOut;
              <<effector>> connect valueIn -> valueOut;
            }
            component /*instance*/ unitDelay1_2 {
              port
                in Double valueIn,
                out Double valueOut;
              <<effector>> connect valueIn -> valueOut;
            }
            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.cC_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cC_active_b*/
            connect tempomat.cC_active_b -> cC_active_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.acceleration_pcOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pc*/
            connect velocityControl.acceleration_pc -> acceleration_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cCSetValue_kmh*/
            connect unitDelay1_2.valueOut -> cCSetValue_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiterSetValue_kmh*/
            connect unitDelay_3.valueOut -> limiterSetValue_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_active_bOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_active_b*/
            connect limiter.limiter_active_b -> limiter_active_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.limiter_bIn5*/
            connect fAS_Input.limiter_b -> tempomat.limiter_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.vMax_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.vMax_kmhIn2*/
            connect limiter.vMax_kmh -> velocityControl.vMax_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.v_CC_delta_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.v_CC_delta_kmhIn1*/
            connect tempomat.v_CC_delta_kmh -> velocityControl.v_CC_delta_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.acceleration_pedal_pcIn2*/
            connect fAS_Input.acceleration_pedal_pc3 -> limiter.acceleration_pedal_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.acceleration_pedal_pcIn3*/
            connect fAS_Input.acceleration_pedal_pc -> velocityControl.acceleration_pedal_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.velocityControl.v_Vehicle_kmhIn4*/
            connect fAS_Input.v_Vehicle_kmh1 -> velocityControl.v_Vehicle_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.leverUp_bIn4*/
            connect fAS_Input.leverUp_b3 -> limiter.leverUp_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.v_Vehicle_kmhIn3*/
            connect fAS_Input.v_Vehicle_kmh5 -> limiter.v_Vehicle_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.limiter_bIn1*/
            connect fAS_Input.limiter_b3 -> limiter.limiter_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.leverDown_bIn5*/
            connect fAS_Input.leverDown_b3 -> limiter.leverDown_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.cruiseControl_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.cruiseControl_bIn3*/
            connect fAS_Input.cruiseControl_b -> tempomat.cruiseControl_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.v_Vehicle_kmhIn4*/
            connect fAS_Input.v_Vehicle_kmh -> tempomat.v_Vehicle_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.leverDown_bIn7*/
            connect fAS_Input.leverDown_b -> tempomat.leverDown_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.leverUp_bIn6*/
            connect fAS_Input.leverUp_b -> tempomat.leverUp_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.brakeForce_pedal_pcIn2*/
            connect fAS_Input.brakeForce_pedal_pc -> tempomat.brakeForce_pedal_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.parkingBrake_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.parkingBrake_bIn1*/
            connect fAS_Input.parkingBrake_b -> tempomat.parkingBrake_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in1*/
            connect unitDelay_3.valueOut -> limiter.in1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in2*/
            connect unitDelay_3.valueOut -> limiter.in2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in3*/
            connect unitDelay_3.valueOut -> limiter.in3;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in4*/
            connect unitDelay_3.valueOut -> limiter.in4;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in5*/
            connect unitDelay_3.valueOut -> limiter.in5;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in6*/
            connect unitDelay_3.valueOut -> limiter.in6;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.in7*/
            connect unitDelay_3.valueOut -> limiter.in7;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay.valueIn*/
            connect limiter.out1 -> unitDelay_3.valueIn;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in1*/
            connect unitDelay1_2.valueOut -> tempomat.in1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in2*/
            connect unitDelay1_2.valueOut -> tempomat.in2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in3*/
            connect unitDelay1_2.valueOut -> tempomat.in3;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in4*/
            connect unitDelay1_2.valueOut -> tempomat.in4;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in5*/
            connect unitDelay1_2.valueOut -> tempomat.in5;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in6*/
            connect unitDelay1_2.valueOut -> tempomat.in6;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in7*/
            connect unitDelay1_2.valueOut -> tempomat.in7;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in8*/
            connect unitDelay1_2.valueOut -> tempomat.in8;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in9*/
            connect unitDelay1_2.valueOut -> tempomat.in9;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in10*/
            connect unitDelay1_2.valueOut -> tempomat.in10;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in11*/
            connect unitDelay1_2.valueOut -> tempomat.in11;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in12*/
            connect unitDelay1_2.valueOut -> tempomat.in12;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueOut -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.in13*/
            connect unitDelay1_2.valueOut -> tempomat.in13;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.tempomat.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.unitDelay1.valueIn*/
            connect tempomat.out1 -> unitDelay1_2.valueIn;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.parkingBrake_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.parkingBrake_b1*/
            connect parkingBrake_b -> fAS_Input.parkingBrake_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.parkingBrake_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.parkingBrake_b2*/
            connect parkingBrake_b1 -> fAS_Input.parkingBrake_b2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.brakeForce_pedal_pc1*/
            connect brakeForce_pedal_pc -> fAS_Input.brakeForce_pedal_pc1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeForce_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.brakeForce_pedal_pc2*/
            connect brakeForce_pedal_pc1 -> fAS_Input.brakeForce_pedal_pc2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc1*/
            connect acceleration_pedal_pc -> fAS_Input.acceleration_pedal_pc1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.acceleration_pedal_pc2*/
            connect acceleration_pedal_pc1 -> fAS_Input.acceleration_pedal_pc2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cruiseControl_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.cruiseControl_b1*/
            connect cruiseControl_b -> fAS_Input.cruiseControl_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cruiseControl_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.cruiseControl_b2*/
            connect cruiseControl_b1 -> fAS_Input.cruiseControl_b2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b1*/
            connect limiter_b -> fAS_Input.limiter_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.limiter_b2*/
            connect limiter_b1 -> fAS_Input.limiter_b2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverUp_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b1*/
            connect leverUp_b -> fAS_Input.leverUp_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverUp_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverUp_b2*/
            connect leverUp_b1 -> fAS_Input.leverUp_b2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverDown_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b1*/
            connect leverDown_b -> fAS_Input.leverDown_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverDown_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.leverDown_b2*/
            connect leverDown_b1 -> fAS_Input.leverDown_b2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh2*/
            connect v_Vehicle_kmh -> fAS_Input.v_Vehicle_kmh2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh3*/
            connect v_Vehicle_kmh1 -> fAS_Input.v_Vehicle_kmh3;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.fAS_Input.v_Vehicle_kmh4*/
            connect v_Vehicle_kmh2 -> fAS_Input.v_Vehicle_kmh4;

          }
          component /*instance*/ dEMO_FAS_Input {
            port
              in Double _Acceleration_pedal_pc,
              out Double acceleration_pedal_pc,
              out Double acceleration_pedal_pc1,
              out Double brakeForce_pedal_pc,
              out Double brakeForce_pedal_pc1,
              in Double brakeForce_pedal_pcIn2,
              out Boolean cruiseControl_b,
              out Boolean cruiseControl_b1,
              in Boolean cruiseControl_bIn4,
              out Boolean leverDown_b,
              out Boolean leverDown_b1,
              in Boolean leverDown_bIn7,
              out Boolean leverUp_b,
              out Boolean leverUp_b1,
              in Boolean leverUp_bIn6,
              out Boolean limiter_b,
              out Boolean limiter_b1,
              in Boolean limiter_bIn5,
              out Boolean parkingBrake_b,
              out Boolean parkingBrake_b1,
              in Boolean parkingBrake_bIn1,
              in Double v_Vehicle_b,
              out Double v_Vehicle_kmh,
              out Double v_Vehicle_kmh1,
              out Double v_Vehicle_kmh2;
            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_b*/
            connect parkingBrake_bIn1 -> parkingBrake_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_b1*/
            connect parkingBrake_bIn1 -> parkingBrake_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pc*/
            connect brakeForce_pedal_pcIn2 -> brakeForce_pedal_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pc1*/
            connect brakeForce_pedal_pcIn2 -> brakeForce_pedal_pc1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input._Acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.acceleration_pedal_pc*/
            connect _Acceleration_pedal_pc -> acceleration_pedal_pc;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input._Acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.acceleration_pedal_pc1*/
            connect _Acceleration_pedal_pc -> acceleration_pedal_pc1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_b*/
            connect cruiseControl_bIn4 -> cruiseControl_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_b1*/
            connect cruiseControl_bIn4 -> cruiseControl_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_b*/
            connect limiter_bIn5 -> limiter_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_b1*/
            connect limiter_bIn5 -> limiter_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_b*/
            connect leverUp_bIn6 -> leverUp_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_b1*/
            connect leverUp_bIn6 -> leverUp_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_b*/
            connect leverDown_bIn7 -> leverDown_b;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_b1*/
            connect leverDown_bIn7 -> leverDown_b1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_bIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh*/
            connect v_Vehicle_b -> v_Vehicle_kmh;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_bIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh1*/
            connect v_Vehicle_b -> v_Vehicle_kmh1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_bIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh2*/
            connect v_Vehicle_b -> v_Vehicle_kmh2;

          }
          component /*instance*/ dEMO_FAS_Output {
            port
              in Double acceleration_pc,
              out Double acceleration_pcOut2,
              in Boolean cC_active_b,
              out Boolean cC_active_bOut1,
              in Double cCSetValue_kmh,
              out Double cCSetValue_kmhOut3,
              in Boolean limiter_active_b,
              out Boolean limiter_active_bOut5,
              in Double limiterSetValue_kmh,
              out Double limiterSetValue_kmhOut4;
            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cC_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cC_active_bOut1*/
            connect cC_active_b -> cC_active_bOut1;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.acceleration_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.acceleration_pcOut2*/
            connect acceleration_pc -> acceleration_pcOut2;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cCSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cCSetValue_kmhOut3*/
            connect cCSetValue_kmh -> cCSetValue_kmhOut3;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiterSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiterSetValue_kmhOut4*/
            connect limiterSetValue_kmh -> limiterSetValue_kmhOut4;

            /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiter_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiter_active_bOut5*/
            connect limiter_active_b -> limiter_active_bOut5;

          }
          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CC_active_bOut1*/
          connect dEMO_FAS_Output.cC_active_bOut1 -> _CC_active_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.acceleration_pcOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Acceleration_pcOut2*/
          connect dEMO_FAS_Output.acceleration_pcOut2 -> _Acceleration_pc;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cCSetValue_kmhOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CCSetValue_kmhOut3*/
          connect dEMO_FAS_Output.cCSetValue_kmhOut3 -> _CCSetValue_kmh;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiterSetValue_kmhOut4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LimiterSetValue_kmhOut4*/
          connect dEMO_FAS_Output.limiterSetValue_kmhOut4 -> _LimiterSetValue_kmh;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiter_active_bOut5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Limiter_active_bOut5*/
          connect dEMO_FAS_Output.limiter_active_bOut5 -> _Limiter_active_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cC_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cC_active_b*/
          connect dEMO_FAS_Funktion.cC_active_b -> dEMO_FAS_Output.cC_active_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.acceleration_pc*/
          connect dEMO_FAS_Funktion.acceleration_pc -> dEMO_FAS_Output.acceleration_pc;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cCSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.cCSetValue_kmh*/
          connect dEMO_FAS_Funktion.cCSetValue_kmh -> dEMO_FAS_Output.cCSetValue_kmh;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiterSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiterSetValue_kmh*/
          connect dEMO_FAS_Funktion.limiterSetValue_kmh -> dEMO_FAS_Output.limiterSetValue_kmh;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Output.limiter_active_b*/
          connect dEMO_FAS_Funktion.limiter_active_b -> dEMO_FAS_Output.limiter_active_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.parkingBrake_b*/
          connect dEMO_FAS_Input.parkingBrake_b -> dEMO_FAS_Funktion.parkingBrake_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.parkingBrake_b1*/
          connect dEMO_FAS_Input.parkingBrake_b1 -> dEMO_FAS_Funktion.parkingBrake_b1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeForce_pedal_pc*/
          connect dEMO_FAS_Input.brakeForce_pedal_pc -> dEMO_FAS_Funktion.brakeForce_pedal_pc;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.brakeForce_pedal_pc1*/
          connect dEMO_FAS_Input.brakeForce_pedal_pc1 -> dEMO_FAS_Funktion.brakeForce_pedal_pc1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.acceleration_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pedal_pc*/
          connect dEMO_FAS_Input.acceleration_pedal_pc -> dEMO_FAS_Funktion.acceleration_pedal_pc;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.acceleration_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.acceleration_pedal_pc1*/
          connect dEMO_FAS_Input.acceleration_pedal_pc1 -> dEMO_FAS_Funktion.acceleration_pedal_pc1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cruiseControl_b*/
          connect dEMO_FAS_Input.cruiseControl_b -> dEMO_FAS_Funktion.cruiseControl_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.cruiseControl_b1*/
          connect dEMO_FAS_Input.cruiseControl_b1 -> dEMO_FAS_Funktion.cruiseControl_b1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_b*/
          connect dEMO_FAS_Input.limiter_b -> dEMO_FAS_Funktion.limiter_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.limiter_b1*/
          connect dEMO_FAS_Input.limiter_b1 -> dEMO_FAS_Funktion.limiter_b1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverUp_b*/
          connect dEMO_FAS_Input.leverUp_b -> dEMO_FAS_Funktion.leverUp_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverUp_b1*/
          connect dEMO_FAS_Input.leverUp_b1 -> dEMO_FAS_Funktion.leverUp_b1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverDown_b*/
          connect dEMO_FAS_Input.leverDown_b -> dEMO_FAS_Funktion.leverDown_b;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_b1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.leverDown_b1*/
          connect dEMO_FAS_Input.leverDown_b1 -> dEMO_FAS_Funktion.leverDown_b1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh*/
          connect dEMO_FAS_Input.v_Vehicle_kmh -> dEMO_FAS_Funktion.v_Vehicle_kmh;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh1*/
          connect dEMO_FAS_Input.v_Vehicle_kmh1 -> dEMO_FAS_Funktion.v_Vehicle_kmh1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_kmh2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Funktion.v_Vehicle_kmh2*/
          connect dEMO_FAS_Input.v_Vehicle_kmh2 -> dEMO_FAS_Funktion.v_Vehicle_kmh2;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._BrakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.brakeForce_pedal_pcIn2*/
          connect _BrakeForce_pedal_pc -> dEMO_FAS_Input.brakeForce_pedal_pcIn2;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._ParkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.parkingBrake_bIn1*/
          connect _ParkingBrake_b -> dEMO_FAS_Input.parkingBrake_bIn1;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CruiseControl_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.cruiseControl_bIn4*/
          connect _CruiseControl_b -> dEMO_FAS_Input.cruiseControl_bIn4;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input._Acceleration_pedal_pcIn3*/
          connect _Acceleration_pedal_pc -> dEMO_FAS_Input._Acceleration_pedal_pc;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.limiter_bIn5*/
          connect _Limiter_b -> dEMO_FAS_Input.limiter_bIn5;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LeverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverUp_bIn6*/
          connect _LeverUp_b -> dEMO_FAS_Input.leverUp_bIn6;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LeverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.leverDown_bIn7*/
          connect _LeverDown_b -> dEMO_FAS_Input.leverDown_bIn7;

          /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._V_Vehicle_kmhIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS.dEMO_FAS_Input.v_Vehicle_bIn8*/
          connect _V_Vehicle_kmh -> dEMO_FAS_Input.v_Vehicle_b;

        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CC_active_bOut1*/
        connect dEMO_FAS_3._CC_active_b -> _CC_active_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Acceleration_pcOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Acceleration_pcOut2*/
        connect dEMO_FAS_3._Acceleration_pc -> _Acceleration_pc;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CCSetValue_kmhOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CCSetValue_kmhOut3*/
        connect dEMO_FAS_3._CCSetValue_kmh -> _CCSetValue_kmh;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LimiterSetValue_kmhOut4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LimiterSetValue_kmhOut4*/
        connect dEMO_FAS_3._LimiterSetValue_kmh -> _LimiterSetValue_kmh;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Limiter_active_bOut5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Limiter_active_bOut5*/
        connect dEMO_FAS_3._Limiter_active_b -> _Limiter_active_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._V_Vehicle_kmhIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._V_Vehicle_kmhIn8*/
        connect _V_Vehicle_kmh -> dEMO_FAS_3._V_Vehicle_kmh;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LeverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LeverDown_bIn7*/
        connect _LeverDown_b -> dEMO_FAS_3._LeverDown_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LeverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._LeverUp_bIn6*/
        connect _LeverUp_b -> dEMO_FAS_3._LeverUp_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Limiter_bIn5*/
        connect _Limiter_b -> dEMO_FAS_3._Limiter_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CruiseControl_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._CruiseControl_bIn4*/
        connect _CruiseControl_b -> dEMO_FAS_3._CruiseControl_b;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._Acceleration_pedal_pcIn3*/
        connect _Acceleration_pedal_pc -> dEMO_FAS_3._Acceleration_pedal_pc;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._BrakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._BrakeForce_pedal_pcIn2*/
        connect _BrakeForce_pedal_pc -> dEMO_FAS_3._BrakeForce_pedal_pc;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._ParkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem.dEMO_FAS._ParkingBrake_bIn1*/
        connect _ParkingBrake_b -> dEMO_FAS_3._ParkingBrake_b;

      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CC_active_bOut1*/
      connect subsystem._CC_active_b -> _CC_active_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Acceleration_pcOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Acceleration_pcOut2*/
      connect subsystem._Acceleration_pc -> _Acceleration_pc;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CCSetValue_kmhOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CCSetValue_kmhOut3*/
      connect subsystem._CCSetValue_kmh -> _CCSetValue_kmh;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LimiterSetValue_kmhOut4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LimiterSetValue_kmhOut4*/
      connect subsystem._LimiterSetValue_kmh -> _LimiterSetValue_kmh;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Limiter_active_bOut5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Limiter_active_bOut5*/
      connect subsystem._Limiter_active_b -> _Limiter_active_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._V_Vehicle_kmhIn8 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._V_Vehicle_kmhIn8*/
      connect _V_Vehicle_kmh -> subsystem._V_Vehicle_kmh;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LeverDown_bIn7 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LeverDown_bIn7*/
      connect _LeverDown_b -> subsystem._LeverDown_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LeverUp_bIn6 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._LeverUp_bIn6*/
      connect _LeverUp_b -> subsystem._LeverUp_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Limiter_bIn5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Limiter_bIn5*/
      connect _Limiter_b -> subsystem._Limiter_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CruiseControl_bIn4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._CruiseControl_bIn4*/
      connect _CruiseControl_b -> subsystem._CruiseControl_b;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Acceleration_pedal_pcIn3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._Acceleration_pedal_pcIn3*/
      connect _Acceleration_pedal_pc -> subsystem._Acceleration_pedal_pc;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._BrakeForce_pedal_pcIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._BrakeForce_pedal_pcIn2*/
      connect _BrakeForce_pedal_pc -> subsystem._BrakeForce_pedal_pc;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._ParkingBrake_bIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS.subsystem._ParkingBrake_bIn1*/
      connect _ParkingBrake_b -> subsystem._ParkingBrake_b;

    }
    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CC_active_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cC_active_b*/
    connect dEMO_FAS_2._CC_active_b -> cC_active_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CCSetValue_kmhOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cCSetValue_kmh*/
    connect dEMO_FAS_2._CCSetValue_kmh -> cCSetValue_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LimiterSetValue_kmhOut4 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiterSetValue_kmh*/
    connect dEMO_FAS_2._LimiterSetValue_kmh -> limiterSetValue_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Limiter_active_bOut5 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiter_active_b*/
    connect dEMO_FAS_2._Limiter_active_b -> limiter_active_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Acceleration_pcOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.acceleration_pc*/
    connect dEMO_FAS_2._Acceleration_pc -> acceleration_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.v_Vehicle_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._V_Vehicle_kmhIn8*/
    connect v_Vehicle_kmh -> dEMO_FAS_2._V_Vehicle_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.leverDown_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LeverDown_bIn7*/
    connect leverDown_b -> dEMO_FAS_2._LeverDown_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.leverUp_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._LeverUp_bIn6*/
    connect leverUp_b -> dEMO_FAS_2._LeverUp_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiter_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Limiter_bIn5*/
    connect limiter_b -> dEMO_FAS_2._Limiter_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cruiseControl_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._CruiseControl_bIn4*/
    connect cruiseControl_b -> dEMO_FAS_2._CruiseControl_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.acceleration_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._Acceleration_pedal_pcIn3*/
    connect acceleration_pedal_pc -> dEMO_FAS_2._Acceleration_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._BrakeForce_pedal_pcIn2*/
    connect brakeForce_pedal_pc -> dEMO_FAS_2._BrakeForce_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.parkingBrake_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.dEMO_FAS._ParkingBrake_bIn1*/
    connect parkingBrake_b -> dEMO_FAS_2._ParkingBrake_b;

  }
  component /*instance*/ simToRealTime {
    component /*instance*/ constant_66 {
      port
        out Double out1;
    }
    component /*instance*/ constant1_14 {
      port
        out Double out1;
    }
    component /*instance*/ terminator_3 {
      port
        in Double in1;
    }
    component /*instance*/ terminator1 {
      port
        in Double in1;
    }
    component /*instance*/ terminator2 {
      port
        in Double in1;
    }
    component /*instance*/ terminator3 {
      port
        in Double in1;
    }
    component /*instance*/ terminator4 {
      port
        in Double in1;
    }
    component /*instance*/ terminator5 {
      port
        in Double in1;
    }
    component /*instance*/ vAPS_TimeConfiguration {
      port
        out Double factor_SimTime,
        in Double maxSpeed,
        out Double timeConfig_Consumer,
        out Double timeConfig_Producer,
        in Double timeConfig,
        out Double tsRTimeEff,
        out Double tsRTime,
        out Double tsSim;
      component /*instance*/ constant1_15 {
        port
          out Double out1;
      }
      component /*instance*/ constant10_1 {
        port
          out Double out1;
      }
      component /*instance*/ constant4 {
        port
          out Double out1;
      }
      component /*instance*/ realTimeTimerAdapter {
        port
          in Double in1,
          out Double out1;
        component /*instance*/ constant1_16 {
          port
            out Double out1;
        }
        component /*instance*/ constant10_2 {
          port
            out Double out1;
        }
        component /*instance*/ constant11_1 {
          port
            out Double out1;
        }
        component /*instance*/ constant12_1 {
          port
            out Double out1;
        }
        component /*instance*/ constant13_1 {
          port
            out Double out1;
        }
        component /*instance*/ constant15 {
          port
            out Double out1;
        }
        component /*instance*/ div1 {
          port
            in Double in1,
            in Double in2,
            out Double out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ logOp1_1 {
          port
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ relOp2_2 {
          port
            in Double in1,
            in Double in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ relOp3_1 {
          port
            in Double in1,
            in Double in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ relOp4_1 {
          port
            in Double in1,
            in Double in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ sum_6 {
          port
            in Double in1,
            in Double in2,
            out Double out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ switchBlock2_8 {
          port
            in Boolean condition,
            in Double elseIn,
            in Double ifIn,
            out Double out1;
          <<effector>> connect ifIn -> out1;
          <<effector>> connect condition -> out1;
          <<effector>> connect elseIn -> out1;
        }
        component /*instance*/ switchBlock3_6 {
          port
            in Boolean condition,
            in Double elseIn,
            in Double ifIn,
            out Double out1;
          <<effector>> connect ifIn -> out1;
          <<effector>> connect condition -> out1;
          <<effector>> connect elseIn -> out1;
        }
        component /*instance*/ condition_25 {
          port
            in Boolean in1,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
        }
        component /*instance*/ condition1_12 {
          port
            in Boolean in1,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.out1Out1*/
        connect switchBlock2_8.out1 -> out1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock2.condition*/
        connect condition1_12.out1 -> switchBlock2_8.condition;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock3.condition*/
        connect condition_25.out1 -> switchBlock3_6.condition;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.logOp1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.condition.in1*/
        connect logOp1_1.out1 -> condition_25.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.condition1.in1*/
        connect relOp2_2.out1 -> condition1_12.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp2.in1*/
        connect in1 -> relOp2_2.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock2.ifIn*/
        connect in1 -> switchBlock2_8.ifIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp3.in1*/
        connect in1 -> relOp3_1.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp4.in1*/
        connect in1 -> relOp4_1.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.div1.in1*/
        connect in1 -> div1.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant10.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp2.in2*/
        connect constant10_2.out1 -> relOp2_2.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant11.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp3.in2*/
        connect constant11_1.out1 -> relOp3_1.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock2.elseIn*/
        connect switchBlock3_6.out1 -> switchBlock2_8.elseIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant12.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp4.in2*/
        connect constant12_1.out1 -> relOp4_1.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.logOp1.in1*/
        connect relOp3_1.out1 -> logOp1_1.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.relOp4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.logOp1.in2*/
        connect relOp4_1.out1 -> logOp1_1.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant13.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock3.ifIn*/
        connect constant13_1.out1 -> switchBlock3_6.ifIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.sum.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.switchBlock3.elseIn*/
        connect sum_6.out1 -> switchBlock3_6.elseIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.div1.in2*/
        connect constant1_16.out1 -> div1.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.constant15.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.sum.in1*/
        connect constant15.out1 -> sum_6.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.div1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.sum.in2*/
        connect div1.out1 -> sum_6.in2;

      }
      component /*instance*/ realTimeTimerAdapter_Value {
        port
          in Double in1,
          out Double out1;
        component /*instance*/ constant11_2 {
          port
            out Double out1;
        }
        component /*instance*/ constant12_2 {
          port
            out Double out1;
        }
        component /*instance*/ constant13_2 {
          port
            out Double out1;
        }
        component /*instance*/ logOp1_2 {
          port
            in Boolean in1,
            in Boolean in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ relOp3_2 {
          port
            in Double in1,
            in Double in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ relOp4_2 {
          port
            in Double in1,
            in Double in2,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
          <<effector>> connect in2 -> out1;
        }
        component /*instance*/ switchBlock3_7 {
          port
            in Boolean condition,
            in Double elseIn,
            in Double ifIn,
            out Double out1;
          <<effector>> connect ifIn -> out1;
          <<effector>> connect condition -> out1;
          <<effector>> connect elseIn -> out1;
        }
        component /*instance*/ condition_26 {
          port
            in Boolean in1,
            out Boolean out1;
          <<effector>> connect in1 -> out1;
        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.switchBlock3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.out1Out1*/
        connect switchBlock3_7.out1 -> out1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.switchBlock3.condition*/
        connect condition_26.out1 -> switchBlock3_7.condition;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.logOp1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.condition.in1*/
        connect logOp1_2.out1 -> condition_26.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.constant13.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.switchBlock3.ifIn*/
        connect constant13_2.out1 -> switchBlock3_7.ifIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.logOp1.in2*/
        connect relOp4_2.out1 -> logOp1_2.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.logOp1.in1*/
        connect relOp3_2.out1 -> logOp1_2.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.constant12.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp4.in2*/
        connect constant12_2.out1 -> relOp4_2.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.constant11.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp3.in2*/
        connect constant11_2.out1 -> relOp3_2.in2;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.switchBlock3.elseIn*/
        connect in1 -> switchBlock3_7.elseIn;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp4.in1*/
        connect in1 -> relOp4_2.in1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.relOp3.in1*/
        connect in1 -> relOp3_2.in1;

      }
      component /*instance*/ relOp4_3 {
        port
          in Double in1,
          in Double in2,
          out Boolean out1;
        <<effector>> connect in1 -> out1;
        <<effector>> connect in2 -> out1;
      }
      component /*instance*/ switchBlock1_10 {
        port
          in Boolean condition,
          in Double elseIn,
          in Double ifIn,
          out Double out1;
        <<effector>> connect ifIn -> out1;
        <<effector>> connect condition -> out1;
        <<effector>> connect elseIn -> out1;
      }
      component /*instance*/ switchBlock2_9 {
        port
          in Boolean condition,
          in Double elseIn,
          in Double ifIn,
          out Double out1;
        <<effector>> connect ifIn -> out1;
        <<effector>> connect condition -> out1;
        <<effector>> connect elseIn -> out1;
      }
      component /*instance*/ sysInit1 {
        port
          out Double y;
        component /*instance*/ memory_Init_3 {
          port
            in Double in1,
            out Double out1;
          <<effector>> connect in1 -> out1;
        }
        component /*instance*/ zero_Init_3 {
          port
            out Double out1;
        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.sysInit1.memory_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.sysInit1.yOut1*/
        connect memory_Init_3.out1 -> y;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.sysInit1.zero_Init.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.sysInit1.memory_Init.in1*/
        connect zero_Init_3.out1 -> memory_Init_3.in1;

      }
      component /*instance*/ vAPSRealTimeTimer {
        port
          in Double timeFactor,
          out Double tsRTimeEffective,
          out Double tsRTime,
          out Double tsSim;
        component /*instance*/ level2MfileSFunction {
          port
            in Double in1,
            out Double tsRTimeEffective,
            out Double tsRTime,
            out Double tsSim;
          <<effector>> connect in1 -> tsSim;
          <<effector>> connect in1 -> tsRTime;
          <<effector>> connect in1 -> tsRTimeEffective;
        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.level2MfileSFunction.tsSimOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsSimOut1*/
        connect level2MfileSFunction.tsSim -> tsSim;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.level2MfileSFunction.tsRTimeOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsRTimeOut2*/
        connect level2MfileSFunction.tsRTime -> tsRTime;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.level2MfileSFunction.tsRTimeEffectiveOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsRTimeEffectiveOut3*/
        connect level2MfileSFunction.tsRTimeEffective -> tsRTimeEffective;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.timeFactorIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.level2MfileSFunction.in1*/
        connect timeFactor -> level2MfileSFunction.in1;

      }
      component /*instance*/ condition_27 {
        port
          in Boolean in1,
          out Boolean out1;
        <<effector>> connect in1 -> out1;
      }
      component /*instance*/ condition1_13 {
        port
          in Double in1,
          out Boolean out1;
        <<effector>> connect in1 -> out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.factor_SimTimeOut1*/
      connect realTimeTimerAdapter.out1 -> factor_SimTime;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfig_ConsumerOut2*/
      connect realTimeTimerAdapter_Value.out1 -> timeConfig_Consumer;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfig_ProducerOut3*/
      connect realTimeTimerAdapter_Value.out1 -> timeConfig_Producer;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsSimOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsSimOut4*/
      connect vAPSRealTimeTimer.tsSim -> tsSim;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsRTimeOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsRTimeOut5*/
      connect vAPSRealTimeTimer.tsRTime -> tsRTime;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.tsRTimeEffectiveOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsRTimeEffOut6*/
      connect vAPSRealTimeTimer.tsRTimeEffective -> tsRTimeEff;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock1.condition*/
      connect condition_27.out1 -> switchBlock1_10.condition;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.condition1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock2.condition*/
      connect condition1_13.out1 -> switchBlock2_9.condition;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.relOp4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.condition.in1*/
      connect relOp4_3.out1 -> condition_27.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.sysInit1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.condition1.in1*/
      connect sysInit1.y -> condition1_13.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock1.ifIn*/
      connect constant1_15.out1 -> switchBlock1_10.ifIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.maxSpeedIn2 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.relOp4.in1*/
      connect maxSpeed -> relOp4_3.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.vAPSRealTimeTimer.timeFactorIn1*/
      connect switchBlock1_10.out1 -> vAPSRealTimeTimer.timeFactor;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.constant10.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.relOp4.in2*/
      connect constant10_1.out1 -> relOp4_3.in2;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock1.elseIn*/
      connect realTimeTimerAdapter.out1 -> switchBlock1_10.elseIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfigIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock2.elseIn*/
      connect timeConfig -> switchBlock2_9.elseIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.constant4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock2.ifIn*/
      connect constant4.out1 -> switchBlock2_9.ifIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter_Value.in1In1*/
      connect switchBlock2_9.out1 -> realTimeTimerAdapter_Value.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.switchBlock2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.realTimeTimerAdapter.in1In1*/
      connect switchBlock2_9.out1 -> realTimeTimerAdapter.in1;

    }
    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfig_ProducerOut3 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator2.in1*/
    connect vAPS_TimeConfiguration.timeConfig_Producer -> terminator2.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsSimOut4 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator3.in1*/
    connect vAPS_TimeConfiguration.tsSim -> terminator3.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsRTimeOut5 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator4.in1*/
    connect vAPS_TimeConfiguration.tsRTime -> terminator4.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.tsRTimeEffOut6 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator5.in1*/
    connect vAPS_TimeConfiguration.tsRTimeEff -> terminator5.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfig_ConsumerOut2 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator1.in1*/
    connect vAPS_TimeConfiguration.timeConfig_Consumer -> terminator1.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.factor_SimTimeOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.terminator.in1*/
    connect vAPS_TimeConfiguration.factor_SimTime -> terminator_3.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.maxSpeedIn2*/
    connect constant1_14.out1 -> vAPS_TimeConfiguration.maxSpeed;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.simToRealTime.vAPS_TimeConfiguration.timeConfigIn1*/
    connect constant_66.out1 -> vAPS_TimeConfiguration.timeConfig;

  }
  component /*instance*/ umgebung {
    port
      in Double acceleration_pc,
      out Double acceleration_pedal_pc,
      out Double brakeForce_pedal_pc,
      in Boolean cC_active_b,
      in Double cCSetValue_kmh,
      out Boolean cruiseControl_b,
      out Boolean leverDown_b,
      out Boolean leverUp_b,
      in Boolean limiter_active_b,
      out Boolean limiter_b,
      in Double limiterSetValue_kmh,
      out Boolean parkingBrake_b,
      out Double v_Vehicle_kmh;
    component /*instance*/ v_Vehicle_kmh {
      port
        in Double v_Vehicle_kmh;
    }
    component /*instance*/ cCSetValue {
      port
        in Double cCSetValue_kmh;
    }
    component /*instance*/ cC_active {
      port
        in Boolean cC_active_b;
    }
    component /*instance*/ constant_67 {
      port
        out Double out1;
    }
    component /*instance*/ constant1_17 {
      port
        out Double out1;
    }
    component /*instance*/ falseBlock_4 {
      port
        out Double y;
      component /*instance*/ zero_5 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock.yOut1*/
      connect zero_5.out1 -> y;

    }
    component /*instance*/ falseBlock2 {
      port
        out Double y;
      component /*instance*/ zero_6 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock2.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock2.yOut1*/
      connect zero_6.out1 -> y;

    }
    component /*instance*/ falseBlock3 {
      port
        out Double y;
      component /*instance*/ zero_7 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock3.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock3.yOut1*/
      connect zero_7.out1 -> y;

    }
    component /*instance*/ falseBlock4 {
      port
        out Double y;
      component /*instance*/ zero_8 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock4.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock4.yOut1*/
      connect zero_8.out1 -> y;

    }
    component /*instance*/ falseBlock5 {
      port
        out Double y;
      component /*instance*/ zero_9 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock5.zero.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock5.yOut1*/
      connect zero_9.out1 -> y;

    }
    component /*instance*/ limiterSetValue {
      port
        in Double limiterSetValue_kmh;
    }
    component /*instance*/ limiter_active {
      port
        in Boolean limiter_active_b;
    }
    component /*instance*/ manualSwitch {
      port
        in Double in1,
        in Double in2,
        out Boolean parkingBrake_b;
      <<effector>> connect in1 -> parkingBrake_b;
      <<effector>> connect in2 -> parkingBrake_b;
    }
    component /*instance*/ manualSwitch2 {
      port
        out Boolean cruiseControl_b,
        in Double in1,
        in Double in2;
      <<effector>> connect in1 -> cruiseControl_b;
      <<effector>> connect in2 -> cruiseControl_b;
    }
    component /*instance*/ manualSwitch3 {
      port
        in Double in1,
        in Double in2,
        out Boolean leverUp_b;
      <<effector>> connect in1 -> leverUp_b;
      <<effector>> connect in2 -> leverUp_b;
    }
    component /*instance*/ manualSwitch4 {
      port
        in Double in1,
        in Double in2,
        out Boolean leverDown_b;
      <<effector>> connect in1 -> leverDown_b;
      <<effector>> connect in2 -> leverDown_b;
    }
    component /*instance*/ manualSwitch5 {
      port
        in Double in1,
        in Double in2,
        out Boolean limiter_b;
      <<effector>> connect in1 -> limiter_b;
      <<effector>> connect in2 -> limiter_b;
    }
    component /*instance*/ memory_10 {
      port
        in Double brakeForce_pedal_pc,
        out Double brakeForce_pedal_pcOut1;
      <<effector>> connect brakeForce_pedal_pc -> brakeForce_pedal_pcOut1;
    }
    component /*instance*/ sliderGain_1 {
      port
        in Double u,
        out Double y;
      component /*instance*/ sliderGain_2 {
        port
          in Double in1,
          out Double out1;
        <<effector>> connect in1 -> out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.sliderGain.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.yOut1*/
      connect sliderGain_2.out1 -> y;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.sliderGain.in1*/
      connect u -> sliderGain_2.in1;

    }
    component /*instance*/ sliderGain1 {
      port
        in Double u,
        out Double y;
      component /*instance*/ sliderGain_3 {
        port
          in Double in1,
          out Double out1;
        <<effector>> connect in1 -> out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.sliderGain.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.yOut1*/
      connect sliderGain_3.out1 -> y;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.uIn1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.sliderGain.in1*/
      connect u -> sliderGain_3.in1;

    }
    component /*instance*/ trueBlock_2 {
      port
        out Double y;
      component /*instance*/ one_2 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock.yOut1*/
      connect one_2.out1 -> y;

    }
    component /*instance*/ trueBlock2 {
      port
        out Double y;
      component /*instance*/ one_3 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock2.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock2.yOut1*/
      connect one_3.out1 -> y;

    }
    component /*instance*/ trueBlock3 {
      port
        out Double y;
      component /*instance*/ one_4 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock3.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock3.yOut1*/
      connect one_4.out1 -> y;

    }
    component /*instance*/ trueBlock4 {
      port
        out Double y;
      component /*instance*/ one_5 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock4.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock4.yOut1*/
      connect one_5.out1 -> y;

    }
    component /*instance*/ trueBlock5 {
      port
        out Double y;
      component /*instance*/ one_6 {
        port
          out Double out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock5.one.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock5.yOut1*/
      connect one_6.out1 -> y;

    }
    component /*instance*/ vehicle {
      port
        in Double acceleration_pc,
        in Double brakeForce_pedal_pc,
        in Double brakeForce_pedal_pc1,
        out Double v_Vehicle_kmh;
      component /*instance*/ discreteTransferFcnwithinitialstates {
        port
          in Double in1,
          out Double out1;
        component /*instance*/ discreteStateSpace {
          port
            in Double in1,
            out Double out1;
          <<effector>> connect in1 -> out1;
        }
        /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.discreteStateSpace.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.out1Out1*/
        connect discreteStateSpace.out1 -> out1;

        /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.in1In1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.discreteStateSpace.in1*/
        connect in1 -> discreteStateSpace.in1;

      }
      component /*instance*/ gain_3 {
        port
          in Double brakeForce_pedal_pc,
          out Double out1;
        <<effector>> connect brakeForce_pedal_pc -> out1;
      }
      component /*instance*/ saturation_2 {
        port
          in Double in1,
          out Double v_Vehicle_kmh;
        <<effector>> connect in1 -> v_Vehicle_kmh;
      }
      component /*instance*/ sum1 {
        port
          in Double in2,
          in Double in3,
          out Double out1,
          in Double v_Vehicle_kmh;
        <<effector>> connect v_Vehicle_kmh -> out1;
        <<effector>> connect in2 -> out1;
        <<effector>> connect in3 -> out1;
      }
      component /*instance*/ switchBlock_20 {
        port
          in Boolean condition,
          in Double elseIn,
          in Double ifIn,
          out Double out1;
        <<effector>> connect ifIn -> out1;
        <<effector>> connect condition -> out1;
        <<effector>> connect elseIn -> out1;
      }
      component /*instance*/ uniformRandomNumber {
        port
          out Double out1;
      }
      component /*instance*/ unitDelay_4 {
        port
          out Double out1,
          in Double v_Vehicle_kmh;
        <<effector>> connect v_Vehicle_kmh -> out1;
      }
      component /*instance*/ widerstand {
        port
          in Double in1,
          out Double out1;
        <<effector>> connect in1 -> out1;
      }
      component /*instance*/ condition_28 {
        port
          in Double in1,
          out Boolean out1;
        <<effector>> connect in1 -> out1;
      }
      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.saturation.v_Vehicle_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.v_Vehicle_kmhOut1*/
      connect saturation_2.v_Vehicle_kmh -> v_Vehicle_kmh;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.acceleration_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.switchBlock.elseIn*/
      connect acceleration_pc -> switchBlock_20.elseIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.gain.brakeForce_pedal_pcIn1*/
      connect brakeForce_pedal_pc -> gain_3.brakeForce_pedal_pc;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.condition.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.switchBlock.condition*/
      connect condition_28.out1 -> switchBlock_20.condition;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.brakeForce_pedal_pc1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.condition.in1*/
      connect brakeForce_pedal_pc1 -> condition_28.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.uniformRandomNumber.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.sum1.in3*/
      connect uniformRandomNumber.out1 -> sum1.in3;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.unitDelay.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.widerstand.in1*/
      connect unitDelay_4.out1 -> widerstand.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.widerstand.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.sum1.in2*/
      connect widerstand.out1 -> sum1.in2;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.out1Out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.sum1.v_Vehicle_kmhIn1*/
      connect discreteTransferFcnwithinitialstates.out1 -> sum1.v_Vehicle_kmh;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.sum1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.saturation.in1*/
      connect sum1.out1 -> saturation_2.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.switchBlock.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.discreteTransferFcnwithinitialstates.in1In1*/
      connect switchBlock_20.out1 -> discreteTransferFcnwithinitialstates.in1;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.gain.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.switchBlock.ifIn*/
      connect gain_3.out1 -> switchBlock_20.ifIn;

      /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.saturation.v_Vehicle_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.unitDelay.v_Vehicle_kmhIn1*/
      connect saturation_2.v_Vehicle_kmh -> unitDelay_4.v_Vehicle_kmh;

    }
    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.v_Vehicle_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.v_Vehicle_kmh*/
    connect vehicle.v_Vehicle_kmh -> v_Vehicle_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch.parkingBrake_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.parkingBrake_b*/
    connect manualSwitch.parkingBrake_b -> parkingBrake_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.brakeForce_pedal_pc*/
    connect sliderGain1.y -> brakeForce_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.acceleration_pedal_pc*/
    connect sliderGain_1.y -> acceleration_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch2.cruiseControl_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cruiseControl_b*/
    connect manualSwitch2.cruiseControl_b -> cruiseControl_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch5.limiter_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiter_b*/
    connect manualSwitch5.limiter_b -> limiter_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch3.leverUp_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.leverUp_b*/
    connect manualSwitch3.leverUp_b -> leverUp_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch4.leverDown_bOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.leverDown_b*/
    connect manualSwitch4.leverDown_b -> leverDown_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiter_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiter_active.limiter_active_bIn1*/
    connect limiter_active_b -> limiter_active.limiter_active_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiterSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiterSetValue.limiterSetValue_kmhIn1*/
    connect limiterSetValue_kmh -> limiterSetValue.limiterSetValue_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cCSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cCSetValue.cCSetValue_kmhIn1*/
    connect cCSetValue_kmh -> cCSetValue.cCSetValue_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cC_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cC_active.cC_active_bIn1*/
    connect cC_active_b -> cC_active.cC_active_b;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.memory.brakeForce_pedal_pcOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.brakeForce_pedal_pc*/
    connect memory_10.brakeForce_pedal_pcOut1 -> vehicle.brakeForce_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.memory.brakeForce_pedal_pcOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.brakeForce_pedal_pc1*/
    connect memory_10.brakeForce_pedal_pcOut1 -> vehicle.brakeForce_pedal_pc1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.acceleration_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.acceleration_pc*/
    connect acceleration_pc -> vehicle.acceleration_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.constant1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.uIn1*/
    connect constant1_17.out1 -> sliderGain1.u;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.constant.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain.uIn1*/
    connect constant_67.out1 -> sliderGain_1.u;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.vehicle.v_Vehicle_kmhOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.v_Vehicle_kmh.v_Vehicle_kmhIn1*/
    connect vehicle.v_Vehicle_kmh -> v_Vehicle_kmh.v_Vehicle_kmh;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock5.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch5.in2*/
    connect falseBlock5.y -> manualSwitch5.in2;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock5.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch5.in1*/
    connect trueBlock5.y -> manualSwitch5.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch4.in2*/
    connect falseBlock4.y -> manualSwitch4.in2;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock4.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch4.in1*/
    connect trueBlock4.y -> manualSwitch4.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch3.in2*/
    connect falseBlock3.y -> manualSwitch3.in2;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock3.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch3.in1*/
    connect trueBlock3.y -> manualSwitch3.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch2.in2*/
    connect falseBlock2.y -> manualSwitch2.in2;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock2.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch2.in1*/
    connect trueBlock2.y -> manualSwitch2.in1;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.sliderGain1.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.memory.brakeForce_pedal_pcIn1*/
    connect sliderGain1.y -> memory_10.brakeForce_pedal_pc;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.falseBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch.in2*/
    connect falseBlock_4.y -> manualSwitch.in2;

    /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.trueBlock.yOut1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.manualSwitch.in1*/
    connect trueBlock_2.y -> manualSwitch.in1;

  }
  component /*instance*/ memory1_10 {
    port
      in Boolean in1,
      out Boolean out1;
    <<effector>> connect in1 -> out1;
  }
  component /*instance*/ memory2_10 {
    port
      in Double in1,
      out Double out1;
    <<effector>> connect in1 -> out1;
  }
  component /*instance*/ memory3 {
    port
      in Double in1,
      out Double out1;
    <<effector>> connect in1 -> out1;
  }
  component /*instance*/ memory4 {
    port
      in Boolean in1,
      out Boolean out1;
    <<effector>> connect in1 -> out1;
  }
  component /*instance*/ memory5 {
    port
      in Double in1,
      out Double out1;
    <<effector>> connect in1 -> out1;
  }
  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.parkingBrake_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.parkingBrake_b*/
  connect umgebung.parkingBrake_b -> dEMO_FAS_1.parkingBrake_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.brakeForce_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.brakeForce_pedal_pc*/
  connect umgebung.brakeForce_pedal_pc -> dEMO_FAS_1.brakeForce_pedal_pc;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.acceleration_pedal_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.acceleration_pedal_pc*/
  connect umgebung.acceleration_pedal_pc -> dEMO_FAS_1.acceleration_pedal_pc;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cruiseControl_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cruiseControl_b*/
  connect umgebung.cruiseControl_b -> dEMO_FAS_1.cruiseControl_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiter_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiter_b*/
  connect umgebung.limiter_b -> dEMO_FAS_1.limiter_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.leverUp_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.leverUp_b*/
  connect umgebung.leverUp_b -> dEMO_FAS_1.leverUp_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.leverDown_b -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.leverDown_b*/
  connect umgebung.leverDown_b -> dEMO_FAS_1.leverDown_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.v_Vehicle_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.v_Vehicle_kmh*/
  connect umgebung.v_Vehicle_kmh -> dEMO_FAS_1.v_Vehicle_kmh;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.memory1.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cC_active_b*/
  connect memory1_10.out1 -> umgebung.cC_active_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.memory2.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.cCSetValue_kmh*/
  connect memory2_10.out1 -> umgebung.cCSetValue_kmh;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.memory3.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiterSetValue_kmh*/
  connect memory3.out1 -> umgebung.limiterSetValue_kmh;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.memory4.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.limiter_active_b*/
  connect memory4.out1 -> umgebung.limiter_active_b;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.memory5.out1 -> adas.oeffentlicher_Demonstrator_FAS_v01.umgebung.acceleration_pc*/
  connect memory5.out1 -> umgebung.acceleration_pc;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cC_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.memory1.in1*/
  connect dEMO_FAS_1.cC_active_b -> memory1_10.in1;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.cCSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.memory2.in1*/
  connect dEMO_FAS_1.cCSetValue_kmh -> memory2_10.in1;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiterSetValue_kmh -> adas.oeffentlicher_Demonstrator_FAS_v01.memory3.in1*/
  connect dEMO_FAS_1.limiterSetValue_kmh -> memory3.in1;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.limiter_active_b -> adas.oeffentlicher_Demonstrator_FAS_v01.memory4.in1*/
  connect dEMO_FAS_1.limiter_active_b -> memory4.in1;

  /* adas.oeffentlicher_Demonstrator_FAS_v01.dEMO_FAS.acceleration_pc -> adas.oeffentlicher_Demonstrator_FAS_v01.memory5.in1*/
  connect dEMO_FAS_1.acceleration_pc -> memory5.in1;

}
